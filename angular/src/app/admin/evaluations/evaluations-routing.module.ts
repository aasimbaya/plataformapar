import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExternalEvaluationComponent } from './external-evaluation/external-evaluation.component';
import { ManagementSpecialDistinctionsComponent } from './management-special-distinctions/management-special-distinctions.component';

const routes: Routes = [
  {
    path: 'external-evaluation',
    component: ExternalEvaluationComponent,
    pathMatch: 'full',
},
{
  path: 'management-special-distinctions',
  component: ManagementSpecialDistinctionsComponent,
  pathMatch: 'full',
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluationsRoutingModule { }
