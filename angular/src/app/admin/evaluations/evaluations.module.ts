import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AppCommonModule } from '@app/shared/common/app-common.module';
import { AdminSharedModule } from '../shared/admin-shared.module';
import { EvaluationsRoutingModule } from './evaluations-routing.module';
import { ExternalEvaluationComponent } from './external-evaluation/external-evaluation.component';
import { ManagementSpecialDistinctionsComponent } from './management-special-distinctions/management-special-distinctions.component';


@NgModule({
  declarations: [
    ExternalEvaluationComponent,
    ManagementSpecialDistinctionsComponent
  ],
  imports: [
    CommonModule,
    EvaluationsRoutingModule,
    AppCommonModule,
    AdminSharedModule,
  ]
})
export class EvaluationsModule { }
