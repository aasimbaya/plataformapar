import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalEvaluationComponent } from './external-evaluation.component';

describe('ExternalEvaluationComponent', () => {
  let component: ExternalEvaluationComponent;
  let fixture: ComponentFixture<ExternalEvaluationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExternalEvaluationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalEvaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
