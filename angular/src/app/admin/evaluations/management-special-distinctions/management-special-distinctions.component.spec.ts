import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementSpecialDistinctionsComponent } from './management-special-distinctions.component';

describe('ManagementSpecialDistinctionsComponent', () => {
  let component: ManagementSpecialDistinctionsComponent;
  let fixture: ComponentFixture<ManagementSpecialDistinctionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementSpecialDistinctionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementSpecialDistinctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
