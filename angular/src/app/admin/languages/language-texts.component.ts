import { Component, ElementRef, Inject, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LanguageServiceProxy, PagedResultDtoOfLanguageTextListDto, UpsertLanguageTextDto } from '@shared/service-proxies/service-proxies';
import { filter as _filter, map as _map } from 'lodash-es';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { finalize } from 'rxjs/operators';
import { EditTextModalComponent } from './edit-text-modal.component';

@Component({
    templateUrl: './language-texts.component.html',
    styleUrls: ['./language-texts.component.less'],
    animations: [appModuleAnimation()],
})
export class LanguageTextsComponent extends AppComponentBase implements OnInit {
    @ViewChild('targetLanguageNameCombobox', { static: true }) targetLanguageNameCombobox: ElementRef;
    @ViewChild('baseLanguageNameCombobox', { static: true }) baseLanguageNameCombobox: ElementRef;
    @ViewChild('sourceNameCombobox', { static: true }) sourceNameCombobox: ElementRef;
    @ViewChild('targetValueFilterCombobox', { static: true }) targetValueFilterCombobox: ElementRef;
    @ViewChild('textsTable', { static: true }) textsTable: ElementRef;
    @ViewChild('editTextModal', { static: true }) editTextModal: EditTextModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('inputFile', { static: true }) inputFile: ElementRef;
    @ViewChild('buttonFile', { static: true }) buttonFile: ElementRef;


    sourceNames: string[] = [];
    languages: abp.localization.ILanguageInfo[] = [];
    targetLanguageName: string;
    sourceName: string;
    baseLanguageName: string;
    targetValueFilter: string;
    filterText: string;
    stringJson: string;
    fileJSONContent: UpsertLanguageTextDto[] = [];
    CODE_ISO :string  = 'ISO-8859-1';
    loader : HTMLElement;

    constructor(
        injector: Injector,
        private _languageService: LanguageServiceProxy,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        @Inject(AppComponent) private parent: AppComponent
        //private renderer: Renderer2
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.sourceNames = _map(
            _filter(abp.localization.sources, (source) => source.type === 'MultiTenantLocalizationSource'),
            (value) => value.name
        );
        this.languages = abp.localization.languages;
        this.init();        
    }

    getLanguageTexts(event?: LazyLoadEvent) {
        if (!this.paginator || !this.dataTable || !this.sourceName) {
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._languageService
            .getLanguageTexts(
                this.primengTableHelper.getMaxResultCount(this.paginator, event),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getSorting(this.dataTable),
                this.sourceName,
                this.baseLanguageName,
                this.targetLanguageName,
                this.targetValueFilter,
                this.filterText
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    init(): void {
        this._activatedRoute.params.subscribe((params: Params) => {
            this.baseLanguageName = params['baseLanguageName'] || abp.localization.currentLanguage.name;
            this.targetLanguageName = params['name'];
            this.sourceName = params['sourceName'] || 'PARPlatform';
            this.targetValueFilter = params['targetValueFilter'] || 'ALL';
            this.filterText = params['filterText'] || '';
            this.reloadPage();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    applyFilters(event?: LazyLoadEvent): void {
        this._router.navigate([
            'app/admin/languages',
            this.targetLanguageName,
            'texts',
            {
                sourceName: this.sourceName,
                baseLanguageName: this.baseLanguageName,
                targetValueFilter: this.targetValueFilter,
                filterText: this.filterText,
            },
        ]);

        this.paginator.changePage(0);
    }

    truncateString(text): string {
        return abp.utils.truncateStringWithPostfix(text, 32, '...');
    }

    refreshTextValueFromModal(): void {
        for (let i = 0; i < this.primengTableHelper.records.length; i++) {
            if (this.primengTableHelper.records[i].key === this.editTextModal.model.key) {
                this.primengTableHelper.records[i].targetValue = this.editTextModal.model.value;
                return;
            }
        }
    }

    downloadFile() {
        this._languageService
        .getLanguageTexts(
            0,
            0,
            "",
            this.sourceName,
            this.baseLanguageName,
            this.targetLanguageName,
            this.targetValueFilter,
            ""
        )
        .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
        .subscribe((result) => {
            
            let JSONReturn = this.generateJSON(result);
            let fileURL = this.generateBlob(JSONReturn);
            
            const auxLink = document.createElement('a');
            document.body.appendChild(auxLink);
            auxLink.href = fileURL;
            auxLink.download =  "lenguage_".concat((Math.random() * 1000).toString()).concat('.rtf');
            auxLink.hidden = true;
            auxLink.click();
            window.URL.revokeObjectURL(fileURL);
            this.changeTextButtonUpload(this.l('SelectJSON'));
        });     
    }

    generateJSON(result: PagedResultDtoOfLanguageTextListDto): string {
        let JSONReturn = '[';
        let lengthItem = result.items.length;
        for (let i = 0; i < lengthItem; i++) {
            let data = result.items[i];
            let objectJson = '{"'+data.key+'":"'+data.targetValue+'"}';
            JSONReturn = (i < (lengthItem-1)) ? JSONReturn.concat(objectJson).concat(',')
                            :JSONReturn.concat(objectJson).concat(']')
            JSONReturn = JSONReturn.concat('\n');
        }
        return JSONReturn;
    }

    generateBlob(JSONReturn: string){
        const byteNumbers = new Array(JSONReturn.length);
        for (let i = 0; i < JSONReturn.length; i++) {
            byteNumbers[i] = JSONReturn.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const blob = new Blob([byteArray], { type: "text/html;charset=UTF-8" });
        return URL.createObjectURL(blob);
    }

    uploadFile(){
        this.parent.showMainSpinner();
        this.stringJson = this.stringJson.replace("[","");
        this.stringJson = this.stringJson.replace("]","");
        var file = this.stringJson.split('\n');
        
        this.fileJSONContent=[];
        for(var i=0; i<(file.length-1); i++){
            let obj = new UpsertLanguageTextDto();
            obj.sourceName = this.sourceName;
            obj.languageName = this.baseLanguageName;
            
            let element = file[i].split(":");
            const reg = /\"/g;
            obj.key = element[0].replace("{","");
            obj.targetValue = element[1];
            
            if (obj.targetValue.lastIndexOf("}")>0) {
                obj.targetValue = obj.targetValue.substring(0,obj.targetValue.lastIndexOf("}"));    
            }
            
            obj.key = obj.key.replace(reg,"");
            obj.targetValue = obj.targetValue.replace(reg,"");
   
            if (obj.key!='' && obj.targetValue !=''){
                this.fileJSONContent.push(obj);
            }
        }
        this._languageService.updateMassiveLanguage(this.fileJSONContent)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.hideLoadingIndicator();
                this.notify.info(this.l('SavedSuccessfully'));
                this.parent.hideMainSpinner();
                this.changeTextButtonUpload(this.l('SelectJSON'));
                this.inputFile.nativeElement.value = "";
                this.reloadPage();
        });
    }

    processJSON(event){
        var file = event.target.files[0];
        this.changeTextButtonUpload(file.name);
        var reader = new FileReader();
        var serviceScope = this;
        reader.onload = function(e) {
            serviceScope.stringJson = e.target.result.toString();
        };
        reader.readAsText(file,this.CODE_ISO); 
    }

    selectFile(){
        this.inputFile.nativeElement.click();
    }

    changeTextButtonUpload(text :string){
        this.buttonFile.nativeElement.innerHTML = text;
    }
}

