import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CreateOrEditLanguageModalComponent } from './create-or-edit-language-modal.component';
import { LanguagesRoutingModules } from './languages-routing-modules.module';
import { LanguagesComponent } from './languages.component';

@NgModule({
    declarations: [LanguagesComponent, CreateOrEditLanguageModalComponent],
    imports: [AppSharedModule, AdminSharedModule, LanguagesRoutingModules],
})
export class LanguagesModule {}
