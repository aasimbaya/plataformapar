import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CertificationsComponent } from './certifications/certifications.component';
import { GoodPracticesComponent } from './good-practices/good-practices.component';
import { PerceptionSurveysComponent } from './perception-surveys/perception-surveys.component';
import { ToolkitEquityComponent } from './toolkit-equity/toolkit-equity.component';

const routes: Routes = [
  {
      path: 'toolkit-equity',
      component: ToolkitEquityComponent,
      pathMatch: 'full',
  },
  {
    path: 'good-practices',
    component: GoodPracticesComponent,
    pathMatch: 'full',
},
{
  path: 'perception-surveys',
  component: PerceptionSurveysComponent,
  pathMatch: 'full',
},
{
  path: 'certifications',
  component: CertificationsComponent,
  pathMatch: 'full',
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OtherProductsRoutingModule { }
