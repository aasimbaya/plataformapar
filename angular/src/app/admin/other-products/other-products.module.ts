import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AppCommonModule } from '@app/shared/common/app-common.module';
import { AdminSharedModule } from '../shared/admin-shared.module';
import { CertificationsComponent } from './certifications/certifications.component';
import { GoodPracticesComponent } from './good-practices/good-practices.component';
import { OtherProductsRoutingModule } from './other-products-routing.module';
import { PerceptionSurveysComponent } from './perception-surveys/perception-surveys.component';
import { ToolkitEquityComponent } from './toolkit-equity/toolkit-equity.component';


@NgModule({
  declarations: [
    ToolkitEquityComponent,
    GoodPracticesComponent,
    PerceptionSurveysComponent,
    CertificationsComponent
  ],
  imports: [
    CommonModule,
    AppCommonModule,
    AdminSharedModule,
    OtherProductsRoutingModule
  ]
})
export class OtherProductsModule { }
