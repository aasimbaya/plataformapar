import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolkitEquityComponent } from './toolkit-equity.component';

describe('ToolkitEquityComponent', () => {
  let component: ToolkitEquityComponent;
  let fixture: ComponentFixture<ToolkitEquityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToolkitEquityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolkitEquityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
