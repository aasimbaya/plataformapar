import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus, RecordsTable } from '@shared/core/dynamic-select-table/dynamic-select-table.model';
import { SpecialDistintionService } from '@shared/core/measuring-tools/special-distintion/service/special-distintion.service';
import { QuestionUseTypeEnum } from '@shared/core/shared/measuring-tools';
import { MailingServiceProxy, PagedResultDtoOfListItemDestinaryDto, PagedResultDtoOfQuestionsBankListItemDto, QuestionsBankServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent extends AppComponentBase implements OnInit {
  @ViewChild('upsertElementModal', { static: true }) modal: ModalDirective;
  @Output() returnItemSelectedEvent: EventEmitter<any> = new EventEmitter<any>();
  saving = false;
  isNew = false;
  form = new FormGroup({
    id: new FormControl(null),
    title: new FormControl(''),
    description: new FormControl('')
  });
  use: QuestionUseTypeEnum = QuestionUseTypeEnum.All;
  pageStatus: PageStatus = { first: 0, page: 0, pageCount: 10, rows: 0 };
  private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
    countTotal: 0,
    records: [],
  });
  set records(value: RecordsTable) {
    this._records.next(value);
  }
  get records(): RecordsTable {
    return this._records.value;
  }
  records$(): Observable<RecordsTable> {
    return this._records.asObservable();
  }

  subscriptions: Subscription[] = [];

  constructor(injector: Injector,
    private _specialDistintionService: SpecialDistintionService,
    private _mailingProxy: MailingServiceProxy,
    private _questionsBankService: QuestionsBankServiceProxy) {
    super(injector);
  }

  ngOnInit(): void {
    this.loadQuestionsData = this.loadQuestionsData.bind(this);
  }

  loadQuestionsData(data: PageStatus) {
    return this._questionsBankService.getAllPagedQuestionsBank(data.filter, this.use, true, data.sorting, data.pageCount, data.page);
}

  private getSpecialDistintionList(pag: PageStatus): void {
    const sub = this._specialDistintionService.getData(pag).subscribe(
      (res: RecordsTable) => {
        this.records = res;
      },
      (err) => {
        console.error('_measuringToolsService.getData()');
      }
    );
    this.subscriptions = [...this.subscriptions, sub];
  }

  onSelectedItemClose(data: boolean): void {
    if (data) {
      this.modal.hide();
    }
  }

  onSelectedItem(values): void {
    this.returnItemSelectedEvent.emit({data:values, form: this.form.value})
  }

  onShown(): void { }

  show(data?: any): void {
    const self = this;
    self.modal.show();
    if (data) {
      this.form.patchValue(data);
    }
  }

  close(): void {
    this.form.reset;
    this.modal.hide();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  columns: ConfigColumns[] = [
    {
      field: 'question',
      titleLabel: 'Question',
      type: 'string'
    },
    {
      field: 'questionnaireType',
      titleLabel: 'QuestionnaireType',
      type: 'string'
    },
    {
      field: 'use',
      titleLabel: 'Use',
      type: 'string'
    },
    {
      field: 'category',
      titleLabel: 'Category',
      type: 'string'
    },
    {
      field: 'theme',
      titleLabel: 'Theme',
      type: 'string',
    },
    {
      field: 'typeQuestion',
      titleLabel: 'TypeQuestion',
      type: 'string',
    },
    {
      field: 'lastModificationTime',
      titleLabel: 'LastModificationTime',
      type: 'date',
    }
  ];
}

