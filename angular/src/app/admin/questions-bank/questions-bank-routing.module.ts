import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionsBankComponent } from '@app/admin/questions-bank/questions-bank/questions-bank.component';
import { PageMode } from '@shared/AppEnums';
import { QuestionsBankUpsertComponent } from './questions-bank-upsert/questions-bank-upsert.component';


const routes: Routes = [
    {
        path: '',
        component: QuestionsBankComponent,
        pathMatch: 'full',
    },
    {
        path: 'add',
        component: QuestionsBankUpsertComponent,
        data: { pageMode: PageMode.Add },
        pathMatch: 'full'
    },
    {
        path: ':id/view',
        component: QuestionsBankUpsertComponent ,
        data: { pageMode: PageMode.View },
        pathMatch: 'full'
    },
    {
        path: ':id/edit',
        component: QuestionsBankUpsertComponent,
        data: { pageMode: PageMode.Edit },
        pathMatch: 'full'
    },
    {
        path: ':id/duplicate',
        component: QuestionsBankUpsertComponent,
        data: { pageMode: PageMode.Duplicate },
        pathMatch: 'full'
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class QuestionsBankRoutingModule {

}
