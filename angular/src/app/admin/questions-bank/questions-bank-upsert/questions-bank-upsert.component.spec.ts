import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionsBankUpsertComponent } from './questions-bank-upsert.component';

describe('QuestionsBankUpsertComponent', () => {
  let component: QuestionsBankUpsertComponent;
  let fixture: ComponentFixture<QuestionsBankUpsertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionsBankUpsertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsBankUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
