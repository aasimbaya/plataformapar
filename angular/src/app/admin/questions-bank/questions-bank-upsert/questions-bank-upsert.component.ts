import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { PageMode } from '@shared/AppEnums';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { QuestionsBankServiceProxy, UpsertQuestionsBankDto, GetAllDropdownServiceProxy, UpsertAnswerDto, LevelQuestionDataDto, FormQuestionLevelListItemDto } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { QuestionUseTypeEnum } from '@shared/core/shared/measuring-tools';
import { ResponseUpsertComponent } from '../response-upsert/response-upsert.component';
import { AddQuestionComponent } from '../add-question/add-question.component';
import { json } from 'stream/consumers';
import { map } from 'rxjs/operators';

export interface List {
  name: string;
  id: number
}

@Component({
  selector: 'app-questions-bank-upsert',
  templateUrl: './questions-bank-upsert.component.html',
  styleUrls: ['./questions-bank-upsert.component.css'],
  animations: [appModuleAnimation()]
})

export class QuestionsBankUpsertComponent extends AppComponentBase implements OnInit {
  @ViewChild('responseUpsert', { static: true }) responseUpsert: ResponseUpsertComponent;
  @ViewChild('addQuestion', { static: true }) addQuestion: AddQuestionComponent;
  //tipos de preguntas
  boolean = '1';
  unica = '3';
  multiple = '2';
  open = '4';
  structure = '5';
  specialDistinction = QuestionUseTypeEnum.SpecialDistinction;
  numberResponse: number[] = [1, 2, 3, 4, 5];
  levels: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  maxAnswer = 5;
  pageMode: PageMode;
  id: number;
  idStructure: number;
  controlType = new FormControl(true, Validators.compose([Validators.required]));
  numberResponses = new FormControl('');
  numberLevels = new FormControl('');
  tag: string = '';
  answersBoolean: UpsertAnswerDto[] = [
    {
      id: 1,
      answerContent: 'Si',
      formQuestionId: 1,
      validationTypeId: 1,
      score: 100,
      percentage: 10,
      autoNull: false,
      specializedResource: true,
      personalId: "person1"
    } as UpsertAnswerDto,
    {
      id: 2,
      answerContent: 'No',
      formQuestionId: 1,
      validationTypeId: 1,
      score: 100,
      percentage: 10,
      autoNull: false,
      specializedResource: false,
      personalId: "person2"
    } as UpsertAnswerDto,

  ];
  listQuestionUse: List[] = [];
  listCategories: List[] = [];
  listQuestionnaireType: List[] = [];
  listQuestionType: List[] = [];
  listThematics: List[] = [];
  formTemplate = new FormGroup({
    question: new FormControl('', Validators.compose([Validators.required])),
    shortVersion: new FormControl('', Validators.compose([Validators.required])),
    fixedText: new FormControl(''),
    tooltip: new FormControl('', Validators.compose([Validators.required])),
    implementationTip: new FormControl('', Validators.compose([Validators.required])),
    questionTypeId: new FormControl('', Validators.compose([Validators.required])),
    usageId: new FormControl('', Validators.compose([Validators.required])),
    questionCategoryId: new FormControl('', Validators.compose([Validators.required])),
    formThematicId: new FormControl('', Validators.compose([Validators.required])),
    formTypeId: new FormControl('', Validators.compose([Validators.required])),
    typeScore: new FormControl(''),
    formula: new FormControl(''),
    questionDependentQuestions: new FormControl(false),
    isNullable: new FormControl(false),
    isCertified: new FormControl(false),
    isPriority: new FormControl(false),

  });

  answersDefault: UpsertAnswerDto[] = [
    {
      id: 1,
      answerContent: 'Pregunta 1',
      score: 100,
      percentage: 10,
      autoNull: false,
      specializedResource: false
    } as UpsertAnswerDto,
    {
      id: 2,
      answerContent: 'Pregunta 2',
      score: 100,
      percentage: 10,
      autoNull: false,
      specializedResource: false
    } as UpsertAnswerDto,
    {
      id: 3,
      answerContent: 'Pregunta 3',
      score: 100,
      percentage: 10,
      autoNull: false,
      specializedResource: false
    } as UpsertAnswerDto,
    {
      id: 4,
      answerContent: 'Pregunta 4',
      score: 100,
      percentage: 10,
      autoNull: false,
      specializedResource: false
    } as UpsertAnswerDto,
    {
      id: 5,
      answerContent: 'Pregunta 5',
      score: 100,
      percentage: 10,
      autoNull: false,
    } as UpsertAnswerDto,

  ]

  responses: ConfigColumns[] = [
    {
      field: 'answerContent',
      titleLabel: 'Response',
      type: 'string'
    },
    {
      field: 'autoNull',
      titleLabel: 'ConsiderationForAnnulment',
      type: 'string',
      icon: true
    },
    {
      field: 'specializedResource',
      titleLabel: 'SpecializedResource',
      type: 'string'
    }
  ];

  responsesWithScore: ConfigColumns[] = [
    {
      field: 'answerContent',
      titleLabel: 'Response',
      type: 'string'
    },
    {
      field: 'autoNull',
      titleLabel: 'ConsiderationForAnnulment',
      type: 'check',
      icon: true
    },
    {
      field: 'specializedResource',
      titleLabel: 'SpecializedResource',
      type: 'check'
    },
    {
      field: 'score',
      titleLabel: 'Score',
      type: 'string'
    }
  ];

  responsesForOpen: ConfigColumns[] = [
    {
      field: 'id',
      titleLabel: 'id',
      type: 'string'
    },
    {
      field: 'answerContent',
      titleLabel: 'Response',
      type: 'string'
    },
    {
      field: 'autoNull',
      titleLabel: 'ConsiderationForAnnulment',
      type: 'check',
      icon: true
    },
    {
      field: 'specializedResource',
      titleLabel: 'SpecializedResource',
      type: 'check'
    },
    {
      field: 'validationTypeId',
      titleLabel: 'Validation',
      type: 'string'
    }
  ];

  structureQuestion: ConfigColumns[] = [
    {
      field: 'title',
      titleLabel: 'Title',
      type: 'string'
    },
    {
      field: 'description',
      titleLabel: 'Description',
      type: 'string'
    },
    {
      field: 'childrenQuestion',
      titleLabel: 'Questions',
      type: 'string',
      icon: true
    }
  ];

  subscriptions: Subscription[] = [];
  answerIds: number[] = [];

  private _answers: BehaviorSubject<RecordsTable> = new BehaviorSubject({
    countTotal: 0,
    records: []
  });

  set answersQ(value: RecordsTable) { this._answers.next(value) };
  get answersQ(): RecordsTable { return this._answers.value };
  answersQ$(): Observable<RecordsTable> { return this._answers.asObservable() };
  answersQToTable$(): Observable<RecordsTable> {
    return this._answers.asObservable().pipe(map(val => ({
      ...val,
      records: val.records.map(el => ({
        ...el,
        validationTypeId: el.validationTypeId?.name || ''
      }))
    })));
  };

  private _structureQuestions: BehaviorSubject<RecordsTable> = new BehaviorSubject({
    countTotal: 0,
    records: []
  });

  set structureQuestionsQ(value: RecordsTable) { this._structureQuestions.next(value) };
  get structureQuestionsQ(): RecordsTable { return this._structureQuestions.value };
  structureQuestionsQ$(): Observable<RecordsTable> { return this._structureQuestions.asObservable() };

  constructor(
    injector: Injector,
    private _router: Router,
    private _rutActive: ActivatedRoute,
    private _questionsBank: QuestionsBankServiceProxy,
    private _getAllDropdown: GetAllDropdownServiceProxy
  ) {
    super(injector);
  }
  ngOnInit(): void {
    this.getLists();
    this.pageMode = this._rutActive.snapshot.data.pageMode;
    this._rutActive.params.subscribe(
      (params: Params) => {
        if (params.id) {
          this.id = params.id;
          this.getQuestion(this.id);
        } else {
          if (this.formTemplate.controls?.usageId?.value != undefined) {
            const { usageId } = this.formTemplate.value;
            this.getCategories(usageId);
          }
        }
      }
    )
  }

  getQuestion(id: number) {
    const sub = this._questionsBank.get(id).subscribe((res: UpsertQuestionsBankDto) => {
      this.getCategories(res.usageId);
      this.formTemplate.patchValue(res);
      this.answersQ = {
        records: res.answers,
        countTotal: res.answers.length
      };

      this.answerIds = res.answers.map(a => a.id);

      res.loadFormQuestionLevelData = res.loadFormQuestionLevelData.map((el) => ({
        ...el,
        childrenQuestion: el.childrenQuestionData.map(e => e.id).join()
      })) as unknown as FormQuestionLevelListItemDto[];
      this.structureQuestionsQ = {
        records: res.loadFormQuestionLevelData,
        countTotal: res.loadFormQuestionLevelData.length
      };

    }, err => {
      console.error('_questionsBankServiceProxy.get');
    });
    this.subscriptions = [...this.subscriptions, sub];

  }

  closeOnClick(): void {
    if (!this.formTemplate.pristine) {
      abp.message.confirm(
        this.l(''),
        this.l('AreYouSureToGoOut'),
        (result: boolean) => {
          if (result) {
            this._redirectToQuestionsBankListPage();
          }
        }
      );
    } else {
      this._redirectToQuestionsBankListPage();
    }
  }

  private _redirectToQuestionsBankListPage(): void {
    this._router.navigate(['app', 'admin', 'questions-bank']);
  }

  save() {
    if (this.id && !this.isDuplicate()) {
      this.updateQuestion();
    } else {
      this.add();
    }
  }

  updateQuestion(): void {
    const payload = this._getUpsertQuestionDto();

    payload.answers = payload.answers.map(el => {
      if (!this.answerIds.find(e => e == el.id)) {
        delete el.id;
      }
      return {...el, validationTypeId: (el.validationTypeId as any).id} as UpsertAnswerDto;
    });
    payload.formQuestionLevelsForSave = payload.formQuestionLevelsForSave.map((el: any) => {
      delete el.childrenQuestionData;
      return {
        ...el,
        childrenQuestion: String(el.childrenQuestion).split(',').map(n => Number(n))
      }
    });

    const sub = this._questionsBank.update(this.id, payload).subscribe((res: any) => {
      abp.notify.success(this.l('SuccessfullyUpdated'));
      this._redirectToListPage();
    }, err => {
      console.error('_questionsBankServiceProxy.update');
    });
    this.subscriptions = [...this.subscriptions, sub];
  }

  add(): void {
    const payload: any = this._getUpsertQuestionDto();
    payload.answers = payload.answers.map(el => {
      if (el.id === null) {
        delete el.id;
      }

      if (el.formQuestionId === null) {
        delete el.formQuestionId;
      }

      return {...el, validationTypeId: el.validationTypeId.id};
    });
    payload.formQuestionLevelsForSave = payload.formQuestionLevelsForSave.map(el => ({
      ...el,
      childrenQuestion: String(el.childrenQuestion).split(',').map(n => Number(n))
    }));

    const sub = this._questionsBank.create(payload).subscribe((res: any) => {
      abp.notify.success(this.l('SavedSuccessfully'));
      this._redirectToListPage();
    }, err => {
      console.error('_questionsBankServiceProxy.create');
    });
    this.subscriptions = [...this.subscriptions, sub];

  }

  private _getUpsertQuestionDto(): UpsertQuestionsBankDto {
    let data: UpsertQuestionsBankDto = new UpsertQuestionsBankDto();
    data = this.formTemplate.value;
    if (this.isAddMode() || this.isDuplicate()) {
      data.tag = this.isAddMode() ? '' : String(this.id) + DateTime.local();
      data.isActive = true;
      data.conditionalParentIds = [];
      data.creatorUserName = this.appSession.user.userName;
      data.creationTime = DateTime.local();
      data.lastModificationTime = DateTime.local();
      data.lastModifierUserName = this.appSession.user.userName;
    }

    if (this.isEditMode()) {
      data.tag = '';
      data.lastModificationTime = DateTime.local();
      data.lastModifierUserName = this.appSession.user.userName;
    }

    data.answers = this.answersQ.records;
    data.formQuestionLevelsForSave = this.structureQuestionsQ.records;
    return data;
  }

  isAddMode(): boolean {
    return this.pageMode === PageMode.Add;
  }

  isEditMode(): boolean {
    return this.pageMode === PageMode.Edit;
  }

  isDuplicate(): boolean {
    return [PageMode.Duplicate].includes(this.pageMode);
  }
  /**
   * Redirect to list page.
   */
  private _redirectToListPage(): void {
    this._router.navigate(['app', 'admin', 'questions-bank']);
  }

  getQuestionUse() {
    const sub = this._getAllDropdown.getAllQuestionUse().subscribe((res: List[]) => {
      this.listQuestionUse = res
    }, err => {
      console.error('_questionsBankServiceProxy.create');
    });
    this.subscriptions = [...this.subscriptions, sub];
  }

  getCategories(use: number) {
    if (use) {
      const sub = this._getAllDropdown.getAllCategories(use).subscribe((res: List[]) => {
        this.listCategories = res;
      }, err => {
        console.error('_questionsBankServiceProxy.getAllCategories');
      });
      this.subscriptions = [...this.subscriptions, sub];
    }
    else
      this.listCategories = [];
  }

  getQuestionnaireType() {
    const sub = this._getAllDropdown.getAllQuestionnaireType().subscribe((res: List[]) => {
      this.listQuestionnaireType = res
    }, err => {
      console.error('_questionsBankServiceProxy.getAllQuestionnaireType');
    });
    this.subscriptions = [...this.subscriptions, sub];
  }

  getQuestionType() {
    const sub = this._getAllDropdown.getAllQuestionType().subscribe((res: List[]) => {
      this.listQuestionType = res
    }, err => {
      console.error('_questionsBankServiceProxy.getAllQuestionType');
    });
    this.subscriptions = [...this.subscriptions, sub];
  }

  getThematics() {
    const sub = this._getAllDropdown.getAllThematics().subscribe((res: List[]) => {
      this.listThematics = res
    }, err => {
      console.error('_questionsBankServiceProxy.getAllThematics');
    });
    this.subscriptions = [...this.subscriptions, sub];
  }

  getLists() {
    this.getQuestionUse();
    this.getQuestionnaireType();
    this.getQuestionType();
    this.getThematics();
  }

  loadBooleanResponses(): void {
    this.answersQ = {
      records: this.answersBoolean,
      countTotal: this.answersBoolean.length
    }
  }

  getLengthAnswers(): number {
    return this.answersQ.records.length;
  }

  getResponses(id: number) {
    if (id == Number(this.boolean)) {
      this.loadBooleanResponses();
    }
    else
      this.answersQ = {
        records: [],
        countTotal: 0
      }
  }

  /**
 * opens the window to add or edit answer
 */
  addOnClick(hasValidations = false): void {
    this.responseUpsert.show(null,hasValidations);
  }

  editOnClick(data: UpsertAnswerDto, hasValidations: boolean): void {
    this.responseUpsert.show(data, hasValidations);
  }

  /**
* delete an answer
*/
  deleteOnClick(data: UpsertAnswerDto, isStructure: boolean = false): void {
    abp.message.confirm(
      this.l(''),
      this.l('AreYouSureYouWantToDeleteTheItem'),
      (result: boolean) => {
        if (result) {
          if (isStructure) {
            const index = this.structureQuestionsQ.records.findIndex(el => el.id == data.id);
            this.structureQuestionsQ.records.splice(index, 1);
            this.numberLevels.setValue(Number(this.numberLevels.value) - 1);
          }
          else {
            const index = this.answersQ.records.findIndex(el => el.id == data.id);
            this.answersQ.records.splice(index, 1);
            this.answersQ = this.answersQ;
            this.numberResponses.setValue(Number(this.numberResponses.value) - 1);
          }
        }
      }
    );
  }

  /**
 *  Destroyed customized behavior when a component is destroyed.
 */
  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  /**
*  Add or edit a reply.
*/
  addResponse(value) {
    //Add
    if (!value.id) {
      value.id = Math.max(...(this.answersQ.records.length ? this.answersQ.records.map(el => el.id) : [0])) + 1;
      this.numberResponses.setValue(Number(this.numberResponses.value) + 1);
      this.answersQ = {
        records: [...this.answersQ.records, value],
        countTotal: this.answersQ.countTotal + 1
      }
    }
    //Edit
    else {
      const index = this.answersQ.records.findIndex(el => el.id == value.id);
      const newAnswer = this.answersQ
      newAnswer.records[index] = value;
      this.answersQ = newAnswer;
    }
  }

  /**
 *  create automatic responses.
 */
  createAnswer(count: number) {
    this.answersQ = {
      records: this.answersDefault.slice(0, count),
      countTotal: this.answersQ.records.length
    }
  }


  onSelectedItem(data: any): void {
    let values = {
      title: data.form.title,
      description: data.form.description,
      childrenQuestion: data.data.map((data) => data.id).join()
    };
    const index = this.structureQuestionsQ.records.findIndex(el => el.id == data.form.id);
    this.structureQuestionsQ.records[index] = values;
  }

  addDestinaryOnClick(data): void {
    this.addQuestion.show(data);
  }

  createLevels(count: number) {
    let valueDefault = {
      id: null,
      title: '',
      description: '',
      childrenQuestion: null
    };
    if (count < this.structureQuestionsQ.records.length) {
      abp.message.confirm(
        this.l(''),
        this.l('AreYouSureYouWantToDeleteTheRecords', this.structureQuestionsQ.records.length - count),
        (result: boolean) => {
          if (result) {
            this.structureQuestionsQ = {
              records: this.structureQuestionsQ.records.slice(0, count),
              countTotal: this.structureQuestionsQ.records.length
            }
          }
        }
      )
    }
    else {
      const result = [
        ...this.structureQuestionsQ.records,
        ...Array(count - this.structureQuestionsQ.records.length)
      ].map((el, i) => el ? el : { ...valueDefault, id: i + 1 });

      this.structureQuestionsQ = {
        records: [...result],
        countTotal: result.length
      }
    }
  }
}
