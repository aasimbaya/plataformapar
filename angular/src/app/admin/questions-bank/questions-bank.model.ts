export interface Question {
    id: number;
    question: string;
    questionnaireType: string;
    use: string;
    category: string;
    theme: string,
    typeQuestion: string
}
