import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { CoreModule } from '@shared/core/core.module';
import { FormTemplateModule } from '@shared/core/form-template/form-template.module';
import { QuestionsBankServiceProxy } from '@shared/service-proxies/service-proxies';
import { QuestionsBankRoutingModule } from './questions-bank-routing.module';
import { QuestionsBankUpsertComponent } from './questions-bank-upsert/questions-bank-upsert.component';
import { QuestionsBankComponent } from './questions-bank/questions-bank.component';
import { ResponseUpsertComponent } from './response-upsert/response-upsert.component';
import { AddQuestionComponent } from './add-question/add-question.component';


@NgModule({
  declarations: [
    QuestionsBankComponent,
    QuestionsBankUpsertComponent,
    ResponseUpsertComponent,
    AddQuestionComponent
  ],
  imports: [
    CommonModule,
    AppCommonModule,
    AdminSharedModule,
    QuestionsBankRoutingModule,
    FormTemplateModule,
    CoreModule
  ],
  providers: [QuestionsBankServiceProxy]
})
export class QuestionsBankModule { }
