import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { QuestionUseTypeEnum } from '@shared/core/shared/measuring-tools';
import { PagedResultDtoOfQuestionsBankListItemDto, QuestionsBankServiceProxy, GetAllDropdownServiceProxy } from '@shared/service-proxies/service-proxies';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Question } from '../questions-bank.model';

export interface List {
  name: string;
  id: number
}
@Component({
  selector: 'app-questions-bank',
  templateUrl: './questions-bank.component.html',
  styleUrls: ['./questions-bank.component.css'],
  animations: [appModuleAnimation()],
})
export class QuestionsBankComponent extends AppComponentBase implements OnInit {
  columns: ConfigColumns[] = [
    {
      field: 'question',
      titleLabel: 'Question',
      type: 'string'
    },
    {
      field: 'questionnaireType',
      titleLabel: 'QuestionnaireType',
      type: 'string'
    },
    {
      field: 'use',
      titleLabel: 'Use',
      type: 'string'
    },
    {
      field: 'category',
      titleLabel: 'Category',
      type: 'string'
    },
    {
      field: 'theme',
      titleLabel: 'Theme',
      type: 'string',
    },
    {
      field: 'typeQuestion',
      titleLabel: 'TypeQuestion',
      type: 'string',
    },
    {
      field: 'lastModificationTime',
      titleLabel: 'LastModificationTime',
      type: 'date',
    }
  ];
  filtersByColumns: string[] = [];
  uses: List[] = [];
  private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
    countTotal: 0,
    records: []
  })
  set records(value: RecordsTable) { this._records.next(value) }
  get records(): RecordsTable { return this._records.value }
  records$(): Observable<RecordsTable> { return this._records.asObservable() }
  subscriptions: Subscription[] = [];
  pageStatus: PageStatus = { first: 0, page: 0, pageCount: 10, rows: 0 };
  filter: string='';
  use: QuestionUseTypeEnum = QuestionUseTypeEnum.All;
  constructor(
    injector: Injector,
    private _router: Router,
    private _questionsBankService: QuestionsBankServiceProxy,
    private _getAllDropdown: GetAllDropdownServiceProxy
  ) {
    super(injector);
  }
  ngOnInit(): void {
    this.loadQuestionsBankData = this.loadQuestionsBankData.bind(this);
    this.getQuestionUse();
  }

  loadQuestionsBankData(data: PageStatus) {
      this.getIdUse(data.filtersByColumns);
      return this._questionsBankService.getAllPagedQuestionsBank(data.filter, this.use, undefined, data.sorting, data.pageCount, data.page);
  }

  private _getInformation(): void {
    const sub = this._questionsBankService.getAllPagedQuestionsBank(this.filter, this.use, undefined, this.pageStatus.sorting, this.pageStatus.pageCount, this.pageStatus.page).subscribe((res: PagedResultDtoOfQuestionsBankListItemDto) => {
      this.records = { countTotal: res.totalCount, records: res.items};
    }, err => {
      console.error("_measuringToolsService.getData()");
    });
    this.subscriptions = [...this.subscriptions, sub];
  }

  addOnClick(): void {
    this._router.navigate(['app','admin', 'questions-bank', 'add']);
  }

  editOnClick(data: Question): void {
    this._router.navigate(['app','admin', 'questions-bank', data.id, 'edit']);
  }

  duplicateOnClick(data: Question): void {
    this._router.navigate(['app','admin', 'questions-bank', data.id, 'duplicate']);
  }
  
  deleteOnClick(data: Question): void {
    this.message.confirm(
      '',
      this.l('AreYouSureYouWantToDeleteTheItem'),
      (isConfirmed) => {
        if (isConfirmed) {
          //this.saving = true;
          this._questionsBankService.delete(data.id).subscribe(() => {
            this.notify.success(this.l('SuccessfullyDeleted'));
            this._getInformation();
          }, () => {

          }, () => {
           // this.saving = false;
          });
        }
      }
    );
  }

  pagingChange(event: PageStatus) {
    this.pageStatus = event;
    this._getInformation();
}

  filterOnClick(): void {
    // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
  }

  closeOnClick(): void {
    // this._router.navigate(['app', 'main', 'administration']);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe())
  }

  getQuestionUse() {
    const sub = this._getAllDropdown.getAllQuestionUse().subscribe((res: List[]) => {
      this.uses = res;
      res.forEach((element, index, array) => {
        this.filtersByColumns[index] = element.name;
      })
    }, err => {
      console.error('_questionsBankServiceProxy.create');
    });
    this.subscriptions = [...this.subscriptions, sub];
  } 

  getIdUse(name) {
    if (name) {
      const index = this.uses.findIndex(el => el.name == name);
      this.use = this.uses[index].id;
    }
    else {
      this.use = QuestionUseTypeEnum.All;
    }
  }

}
