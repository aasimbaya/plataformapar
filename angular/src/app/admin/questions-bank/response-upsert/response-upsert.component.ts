import { Component, OnInit, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ReloadTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { OptionsMenu } from '@shared/core/premium-dashboard/premium-dashboard.model';
import { UpsertAnswerDto } from '@shared/service-proxies/service-proxies';
import {
  GetAllDropdownServiceProxy
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { runInThisContext } from 'vm';

interface Validation {
  id: 0,
  name: string,
  code: string,
  type: string
}

@Component({
  selector: 'app-response-upsert',
  templateUrl: './response-upsert.component.html',
  styleUrls: ['./response-upsert.component.css']
})
export class ResponseUpsertComponent extends AppComponentBase implements OnInit {

  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

  @Output() modalSave: EventEmitter<ReloadTable> = new EventEmitter<ReloadTable>();
  hasValidations = false;
  saving = false;
  isNew = false;
  subscriptions: Subscription[] = [];
  validations: Validation[] = [];
  form = new FormGroup({
    id: new FormControl(),
    formQuestionId: new FormControl(),
    answerContent: new FormControl(),
    validationTypeId: new FormControl(),
    score: new FormControl(),
    percentage: new FormControl(),
    autoNull: new FormControl(),
    specializedResource: new FormControl()
  });
  constructor(
    injector: Injector,
    private _getAllDropdown: GetAllDropdownServiceProxy
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getAllValidations();
  }

  show(data?: UpsertAnswerDto, hasValidations = false): void {
    this.hasValidations = hasValidations;
    const self = this;
    self.modal.show();
    if (data?.id) {
      console.log('data', data);
      this.form.patchValue(data);
      this.isNew = false;
    }
    else {
      this.formInitialize();
      this.isNew = true;
    }
  }

  onShown(): void {
  }

  closeOnClick(): void {
    if (!this.form.pristine) {
      abp.message.confirm(
        this.l(''),
        this.l('AreYouSureToGoOut'),
        (result: boolean) => {
          if (result) {
            this.close();
          }
        }
      );
    } else {
      this.close();
    }
  }

  save(): void {
    this.modalSave.emit(this.form.value);
    abp.notify.success(this.l('SavedSuccessfully'));
    this.close();
  }

  onFormSubmit($event, form) {
    $event.preventDefault;
  }

  private close(): void {
    this.form.reset();
    this.modal.hide();
  }

  /**
   * Set default values for certain answer fields
   */
  formInitialize(): void {
    this.form.controls.specializedResource.setValue(true);
    this.form.controls.autoNull.setValue(false);
  }

  
  getAllValidations() {
    const sub = this._getAllDropdown.getAllValidationType().subscribe((res: any[]) => {
      this.validations = res;
    }, err => {
      console.error('_questionsBankServiceProxy.getAllQuestionnaireType');
    });
    this.subscriptions = [...this.subscriptions, sub];
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub?.unsubscribe());
  }
}
