import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RenderedQuizzesComponent } from './rendered-quizzes/rendered-quizzes.component';

const routes: Routes = [
  {
    path: '',
    component: RenderedQuizzesComponent,
    pathMatch: 'full',
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RenderedQuizzesRoutingModule { }
