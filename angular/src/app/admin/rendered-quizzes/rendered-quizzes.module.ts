import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RenderedQuizzesRoutingModule } from './rendered-quizzes-routing.module';
import { RenderedQuizzesComponent } from './rendered-quizzes/rendered-quizzes.component';
import { CoreModule } from '@shared/core/core.module';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { AdminSharedModule } from '../shared/admin-shared.module';



@NgModule({
  declarations: [
    RenderedQuizzesComponent
  ],
  imports: [
    CommonModule,
    RenderedQuizzesRoutingModule,
    CoreModule,
    AppCommonModule,
    AdminSharedModule,
  ]
})
export class RenderedQuizzesModule { }
