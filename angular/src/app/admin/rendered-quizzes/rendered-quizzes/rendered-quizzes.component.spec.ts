import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenderedQuizzesComponent } from './rendered-quizzes.component';

describe('RenderedQuizzesComponent', () => {
  let component: RenderedQuizzesComponent;
  let fixture: ComponentFixture<RenderedQuizzesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenderedQuizzesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RenderedQuizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
