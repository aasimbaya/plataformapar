import { Component, EventEmitter, Injector, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponentBase } from '@shared/common/app-component-base';
import { QuestionConfig } from '@shared/core/dynamic-input/dynamic-input.model';
import { QuestionTypeEnum } from '@shared/core/shared/measuring-tools';
import { ConfigColumns, PageStatus, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { QuestionUseTypeEnum } from '@shared/core/shared/measuring-tools';
import { PagedResultDtoOfQuestionsBankListItemDto, QuestionsBankServiceProxy, GetAllDropdownServiceProxy } from '@shared/service-proxies/service-proxies';
import { Subscription } from 'rxjs';
import { Value } from 'aws-sdk/clients/s3';
import { Console } from 'console';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-rendered-quizzes',
  templateUrl: './rendered-quizzes.component.html',
  styleUrls: ['./rendered-quizzes.component.css'],
  animations: [appModuleAnimation()]
})
export class RenderedQuizzesComponent extends AppComponentBase implements OnInit {
    
  @Output() questions: EventEmitter<Array<QuestionConfig>> = new EventEmitter<Array<QuestionConfig>>();
  @Output() question: EventEmitter<QuestionConfig> = new EventEmitter<QuestionConfig>();
  
  public Questions = new Array<QuestionConfig>();
  public booleanQuestion = new QuestionConfig();
  /*public multipleQuestion = new QuestionConfig();
  public openQuestion = new QuestionConfig();
  */
  public use = QuestionUseTypeEnum.All;
  subscriptions: Subscription[] = [];
  pageStatus: PageStatus = { first: 0, page: 0, pageCount: 10, rows: 0 };
  filter: string='';
  constructor(injector: Injector, private _router: Router, private _questionsBankService: QuestionsBankServiceProxy,) {
      super(injector);
  }
  ngOnInit(): void {
    this._getInformation();
      /*this.booleanQuestion.question = "¿Desea realizar una prueba?";
      this.booleanQuestion.tooltip = "Este es un tooltip";
      this.booleanQuestion.implementationTip = "Este es un ejemplo de tooltip implementación";
      this.booleanQuestion.type = QuestionTypeEnum.boolean.toString();
      this.booleanQuestion.responses = ['Si','No'];
      this.question.emit(this.booleanQuestion);
      */

      /*this.Questions[0] = new QuestionConfig();
      this.Questions[0].question = "¿Desea realizar una prueba?";
      this.Questions[0].tooltip = "Este es un tooltip";
      this.Questions[0].implementationTip = "Este es un ejemplo de tooltip implementación";
      this.Questions[0].type = QuestionTypeEnum.boolean.toString();
      this.Questions[0].responses = ['Si','No'];
      
      this.Questions[1] = new QuestionConfig();
      this.Questions[1].question = "¿Con qué frecuencia haces tal cosa?";
      this.Questions[1].tooltip = "Este es un tooltip";
      this.Questions[1].implementationTip = "Este es un ejemplo de tooltip implementación";
      this.Questions[1].type = QuestionTypeEnum.single.toString();
      this.Questions[1].responses = ['Siempre','Ocasionalmente','Rara Vez','Nunca'];
      
      this.Questions[2] = new QuestionConfig();
      this.Questions[2].question = "¿Que clases de alimentos te gustan?";
      this.Questions[2].tooltip = "Este es un tooltip";
      this.Questions[2].implementationTip = "Este es un ejemplo de tooltip implementación";
      this.Questions[2].type = QuestionTypeEnum.multiple.toString();
      this.Questions[2].responses = ['Carnes','Pollo','Pescado','Arroz','Pasta','Granos'];
      
      this.Questions[3] = new QuestionConfig();
      this.Questions[3].question = "Explique el por qué de tal cosa";
      this.Questions[3].tooltip = "Este es un tooltip";
      this.Questions[3].implementationTip = "Este es un ejemplo de tooltip implementación";
      this.Questions[3].type = QuestionTypeEnum.open.toString();
      this.Questions[3].responses =[];
       console.log('questions', this.Questions);*/
      // this.questions.emit(this.Questions);
  }

  
  private _getInformation(): void {
    const sub = this._questionsBankService.getAllPagedQuestionsBank(this.filter, this.use, undefined, this.pageStatus.sorting, this.pageStatus.pageCount, this.pageStatus.page).subscribe((res: PagedResultDtoOfQuestionsBankListItemDto) => {
      this.Questions= res.items.map((value) => {
       const question = new QuestionConfig();
       question.question = value.question;
       question.tooltip = "Este es un tooltip";
       question.implementationTip = "Este es un ejemplo de tooltip implementación";
       question.type = QuestionTypeEnum.open.toString()
       question.responses =[];
        return question
      });
      this.questions.emit([...this.Questions]);
    }, err => {
      console.error("_measuringToolsService.getData()");
    });

    this.subscriptions = [...this.subscriptions, sub];
  }

  ngOnDestroy(): void {
    }
}
