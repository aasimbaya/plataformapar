import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EquityScaleComponent } from './equity-scale/equity-scale.component';
import { GroupFeedBackComponent } from './group-feed-back/group-feed-back.component';
import { SubscriptionPlansComponent } from './subscription-plans/subscription-plans.component';

const routes: Routes = [
    {
        path: 'equity-scale',
        component: EquityScaleComponent,
        pathMatch: 'full',
    },
    {
        path: 'recomendations',
        component: EquityScaleComponent,
        pathMatch: 'full',
    },
    {
        path: 'group-feed-back',
        component: GroupFeedBackComponent,
        pathMatch: 'full',
    },
    {
        path: 'subscription-plans',
        component: SubscriptionPlansComponent,
        pathMatch: 'full',
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ResultSettingRoutingModule {

}
