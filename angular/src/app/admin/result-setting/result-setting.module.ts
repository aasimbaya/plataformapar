import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { EquityScaleComponent } from './equity-scale/equity-scale.component';
import { GroupFeedBackComponent } from './group-feed-back/group-feed-back.component';
import { RecomendationsComponent } from './recomendations/recomendations.component';
import { ResultSettingRoutingModule } from './result-setting-routing.module';
import { SubscriptionPlansComponent } from './subscription-plans/subscription-plans.component';



@NgModule({
  declarations: [
  
    EquityScaleComponent,
    RecomendationsComponent,
    GroupFeedBackComponent,
    SubscriptionPlansComponent
  ],
  imports: [
    CommonModule,
    AppCommonModule,
    AdminSharedModule,
    ResultSettingRoutingModule
  ]
})
export class ResultSettingModule { }
