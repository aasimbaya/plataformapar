import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrUpdateRoleInput,
    DropdownItemDto,
    GetAllDropdownServiceProxy,
    GetRoleForEditOutput,
    RoleEditDto,
    RoleServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { PermissionTreeComponent } from '../shared/permission-tree.component';

@Component({
    selector: 'createOrEditRoleModal',
    templateUrl: './create-or-edit-role-modal.component.html',
})
export class CreateOrEditRoleModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @ViewChild('permissionTree') permissionTree: PermissionTreeComponent;
    form: FormGroup;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    roleForEditOutput: GetRoleForEditOutput;
    role: RoleEditDto = new RoleEditDto();
    optionsStructure: any[] = [];
    subscriptions: Subscription[] = [];

    constructor(
        injector: Injector,
        private _roleService: RoleServiceProxy,
        private _getAllDropdownServiceProxy: GetAllDropdownServiceProxy
    ) {
        super(injector);
        const subSector = this._getAllDropdownServiceProxy.getAllTenants().subscribe(
            (res: DropdownItemDto[]) => {
                this.optionsStructure = res;
            },
            (err) => {
                console.error('_getAllDropdownServiceProxy.getAllSectors');
            }
        );
        this.subscriptions = [...this.subscriptions, subSector];
    }

    show(roleId?: number): void {
        const self = this;
        self.active = true;

        self._roleService.getRoleForEdit(roleId).subscribe((result) => {
            self.role = result.role;
            this._formInitialize(result.role);
            this.roleForEditOutput = result;

            self.modal.show();
        });
    }
    private _formInitialize(data: RoleEditDto) {
        this.form = new FormGroup({
            displayName: new FormControl(data.displayName || '', [Validators.required]),
            tenantId: new FormControl(data.tenantId || '', [Validators.required]),
        });
    }

    onShown(): void {
        document.getElementById('RoleDisplayName').focus();
    }
    save(): void {
        const { valid } = this.form;
        if (valid) {
            this._save();
        } else {
            this.form.markAllAsTouched();
        }
    }
    private _save(): void {
        const { value } = this.form;
        const { displayName, tenantId } = value;
        const self = this;
        const input = new CreateOrUpdateRoleInput();
        input.role = self.role;
        input.grantedPermissionNames = self.permissionTree.getGrantedPermissionNames();
        input.role.displayName = displayName;
        input.role.tenantId = tenantId;

        this.saving = true;
        this._roleService
            .createOrUpdateRole(input)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.form = null;
        this.modal.hide();
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
