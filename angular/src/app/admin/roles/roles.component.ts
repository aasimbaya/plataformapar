import { Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FilterDynamicTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { GetRolesInput, RoleListDto, RoleServiceProxy } from '@shared/service-proxies/service-proxies';
import { debounce as _debounce, filter as _filter } from 'lodash-es';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { CreateOrEditRoleModalComponent } from './create-or-edit-role-modal.component';

@Component({
    templateUrl: './roles.component.html',
    animations: [appModuleAnimation()],
})
export class RolesComponent extends AppComponentBase implements OnInit, OnDestroy {
    @ViewChild('createOrEditRoleModal', { static: true }) createOrEditRoleModal: CreateOrEditRoleModalComponent;
    @ViewChild('entityTypeHistoryModal', { static: true }) entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    _entityTypeFullName = 'PARPlatform.Authorization.Roles.Role';
    entityHistoryEnabled = false;
    filters: FilterDynamicTable = { filterText: '' };
    filtersByColumns: string[] = ['Rol', 'Structure', 'Users'];
    subscriptions: Subscription[] = [];
    constructor(injector: Injector, private _roleService: RoleServiceProxy) {
        super(injector);
    }

    ngOnInit(): void {
        this.setIsEntityHistoryEnabled();
    }

    private setIsEntityHistoryEnabled(): void {
        let customSettings = (abp as any).custom;
        this.entityHistoryEnabled =
            customSettings.EntityHistory &&
            customSettings.EntityHistory.isEnabled &&
            _filter(
                customSettings.EntityHistory.enabledEntities,
                (entityType) => entityType === this._entityTypeFullName
            ).length === 1;
    }
    searchOnInput = _debounce(this.getData, AppConsts.SearchBarDelayMilliseconds);

    getData(event?: LazyLoadEvent): void {
        if (this.primengTableHelper.shouldResetPaging(event) && this.primengTableHelper.totalRecordsCount > 0) {
            //revisar
            this.paginator.changePage(0);
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
        const sub = this._roleService
            .getRoles(
                new GetRolesInput({
                    permissions: [],
                    filter: this.filters.filterText,
                    sorting: this.primengTableHelper.getSorting(this.dataTable),
                    maxResultCount: this.primengTableHelper.getMaxResultCount(this.paginator, event),
                    skipCount: this.primengTableHelper.getSkipCount(this.paginator, event),
                })
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((result) => {
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.totalRecordsCount = result.items.length;
                this.primengTableHelper.hideLoadingIndicator();
            });
        this.subscriptions = [...this.subscriptions, sub];
    }
    createRole(): void {
        this.createOrEditRoleModal.show();
    }

    showHistory(role: RoleListDto): void {
        this.entityTypeHistoryModal.show({
            entityId: role.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: role.displayName,
        });
    }

    deleteRole(role: RoleListDto): void {
        let self = this;
        self.message.confirm(
            self.l('RoleDeleteWarningMessage', role.displayName),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._roleService.deleteRole(role.id).subscribe(() => {
                        this.getData();
                        abp.notify.success(this.l('SuccessfullyDeleted'));
                    });
                }
            }
        );
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
