import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { InputsModule } from '@shared/core/inputs/inputs.module';
import { CreateOrEditRoleModalComponent } from './create-or-edit-role-modal.component';
import { RolesRoutingModule } from './roles-routing.module';
import { RolesComponent } from './roles.component';

@NgModule({
    declarations: [RolesComponent, CreateOrEditRoleModalComponent],
    imports: [AppSharedModule, AdminSharedModule, RolesRoutingModule, InputsModule],
})
export class RolesModule {}
