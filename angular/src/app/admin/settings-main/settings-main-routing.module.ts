import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchOptionsComponent } from './search-options/search-options.component';

const routes: Routes = [
{
  path: 'search-options',
  component: SearchOptionsComponent,
  pathMatch: 'full',
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsMainRoutingModule { }
