import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsMainRoutingModule } from './settings-main-routing.module';
import { SearchOptionsComponent } from './search-options/search-options.component';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { AdminSharedModule } from '../shared/admin-shared.module';


@NgModule({
  declarations: [
    SearchOptionsComponent,
  ],
  imports: [
    CommonModule,
    SettingsMainRoutingModule,
    AppCommonModule,
    AdminSharedModule
  ]
})
export class SettingsMainModule { }
