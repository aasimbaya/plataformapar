import { NgModule } from '@angular/core';
import { NavigationEnd, RouteConfigLoadEnd, RouteConfigLoadStart, Router, RouterModule } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppComponent } from './app.component';
import { PanelComponent } from './panel/panel.component';
import { AppRouteGuard } from './shared/common/auth/auth-route-guard';
import { NotificationsComponent } from './shared/layout/notifications/notifications.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'app',
                component: AppComponent,
                canActivate: [AppRouteGuard],
                canActivateChild: [AppRouteGuard],
                children: [
                    {
                        path: '',
                        children: [
                            { path: 'notifications', component: NotificationsComponent },
                            { path: '', component: PanelComponent , pathMatch: 'full' },
                        ],
                    },
                    {
                        path: 'main',
                        loadChildren: () => import('app/main/main.module').then((m) => m.MainModule), //Lazy load main module
                        data: { preload: true },
                    },
                    {
                        path: 'admin',
                        loadChildren: () => import('app/admin/admin.module').then((m) => m.AdminModule), //Lazy load admin module
                        data: { preload: true },
                        canLoad: [AppRouteGuard],
                    },
                    {
                        path: 'form-template',
                        loadChildren: () =>
                            import('@shared/core/form-template/form-template.module').then((m) => m.FormTemplateModule),
                        data: {},
                    },
                    {
                        path: 'users',
                        loadChildren: () => import('../shared/core/users-admin/users-admin.module').then((m) => m.UsersAdminModule),
                        // data: { permission: 'Pages.Administration.Roles' },
                    },
                    {
                        path: 'entities',
                        loadChildren: () => import('../shared/core/entities/entities.module').then((m) => m.EntitiesModule),
                        data: { permission: 'Pages.Entities' }
                    },
                    {
                        path: 'measuring-tools',
                        loadChildren: () => import('../shared/core/measuring-tools/measuring-tools.module').then((m) => m.MeasuringToolsModule),
                        // data: { permission: 'Pages.Administration.Roles' },
                    },
                    {
                        path: 'content',
                        loadChildren: () => import('../shared/core/content/content.module').then((m) => m.ContentModule),
                        // data: { permission: 'Pages.Administration.Roles' },
                    },
                    {
                        path: 'communication',
                        loadChildren: () => import('../shared/core/communication/communication.module').then((m) => m.CommunicationModule),
                        // data: { permission: 'Pages.Administration.Roles' },
                    },
                    {
                        path: 'configurations',
                        loadChildren: () => import('../shared/core/configurations/configurations.module').then((m) => m.ConfigurationsModule),
                        // data: { permission: 'Pages.Administration.Roles' },
                    },
                    {
                        path: 'external',
                        loadChildren: () => import('../app/external/external.module').then((m) => m.ExternalModule),
                        // data: { permission: 'Pages.Administration.Roles' },
                    },
                    {
                        path: '**',
                        redirectTo: 'notifications',
                    },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {
    constructor(private router: Router, private spinnerService: NgxSpinnerService) {
        router.events.subscribe((event) => {
            if (event instanceof RouteConfigLoadStart) {
                spinnerService.show();
            }

            if (event instanceof RouteConfigLoadEnd) {
                spinnerService.hide();
            }

            if (event instanceof NavigationEnd) {
                document.querySelector('meta[property=og\\:url').setAttribute('content', window.location.href);
            }
        });
    }
}
