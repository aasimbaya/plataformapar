import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompaniesUsersComponent } from './companies-users.component';

const routes: Routes = [
  {
    path: '',
    component: CompaniesUsersComponent,
    pathMatch: 'full',
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompaniesUsersRoutingModule { }
