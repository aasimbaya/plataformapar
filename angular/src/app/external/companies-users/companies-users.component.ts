import { Component, Injector, OnInit } from '@angular/core';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { PageStatus } from '@shared/core/dynamic-table/dynamic-table.model';
import { EnterpriseListItemDto, PagedResultDtoOfEnterpriseListItemDto, UserEnterpriseServiceProxy } from '@shared/service-proxies/service-proxies';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';


@Component({
  selector: 'app-companies-users',
  templateUrl: './companies-users.component.html',
  styleUrls: ['./companies-users.component.less'],
  animations: [accountModuleAnimation()]
})
export class CompaniesUsersComponent extends AppComponentBase implements OnInit {

  severalPeople = AppConsts.appBaseUrl + '/assets/common/images/premium-dashboard/several-people.png';
  improvement = AppConsts.appBaseUrl + '/assets/common/images/premium-dashboard/improvement-plan.png';
  public dataEnterpise = new Array<EnterpriseListItemDto>();


  constructor(injector: Injector, private _userEnterpriseServiceProxy: UserEnterpriseServiceProxy)
  {
    super(injector);
  }

  ngOnInit(): void {
    this._userEnterpriseServiceProxy
        .getAll(this.appSession.userId)
        .pipe()
          .subscribe((result) => {
              result.forEach(data=>{
                this.dataEnterpise.push(data);
              });
        });

  }



}
