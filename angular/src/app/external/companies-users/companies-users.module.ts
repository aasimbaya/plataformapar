import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompaniesUsersRoutingModule } from './companies-users-routing.module';
import { CompaniesUsersComponent } from './companies-users.component';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CompaniesUsersRoutingModule
  ]
})
export class CompaniesUsersModule { }
