import { NgModule } from '@angular/core';
import { NavigationEnd, Router, RouterModule } from '@angular/router';

@NgModule({
  imports: [
      RouterModule.forChild([
          {
              path: '',
              children: [
                  {
                      path: 'companies-users',
                      loadChildren: () => import('./companies-users/companies-users.module').then((m) => m.CompaniesUsersModule),
                      //data: { permission: 'Pages.Administration.Users' },
                  },
                  { path: '', redirectTo: 'hostDashboard', pathMatch: 'full' },
                  { path: '**', redirectTo: 'hostDashboard' },
              ],
          },
      ]),
  ],
  exports: [RouterModule],
})
export class ExternalRoutingModule {
  constructor(private router: Router) {
      router.events.subscribe((event) => {
          if (event instanceof NavigationEnd) {
              window.scroll(0, 0);
          }
      });
  }
}
