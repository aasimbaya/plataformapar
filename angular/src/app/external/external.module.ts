import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { ExternalRoutingModule } from './external-routing.module';
import { CompaniesUsersComponent } from './companies-users/companies-users.component';


@NgModule({
  declarations: [CompaniesUsersComponent],
  imports: [
    CommonModule,AppSharedModule, AdminSharedModule,ExternalRoutingModule]
})
export class ExternalModule { }
