export class ConfigPanel {
    public static Panel = [
        { tenant: 'Aequales', url: 'app/measuring-tools/questionnaire'},
        { tenant: 'Enterprise', url: 'app/measuring-tools/launch-call-page'},
        { tenant: 'Extern', url: 'app/external/companies-users'}
    ]
}