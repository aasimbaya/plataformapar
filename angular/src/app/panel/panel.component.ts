import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppSessionService } from '@shared/common/session/app-session.service';
import { ConfigPanel } from './config-panel';

@Component({
    selector: 'app-panel',
    templateUrl: './panel.component.html',
    styleUrls: ['./panel.component.css'],
})
export class PanelComponent implements OnInit {
    public indexPanel = 0;
    constructor(private _sessionService: AppSessionService, private _router: Router) {}

    ngOnInit(): void {
        /*
      the first thing we do is according to the Tenant is to look for the url that corresponds 
      to it as home page.
        { tenant: 'Aequales', url: 'app/measuring-tools/questionnaire'},
        { tenant: 'Enterprise', url: 'app/measuring-tools/launch-call-page'},
        { tenant: 'Extern', url: 'app/external/companies-users'}
      */
        this.indexPanel = ConfigPanel.Panel.findIndex((a) => a.tenant === this._sessionService.tenant?.name);
        /*
        now that we have the tenant index, we redirect to the settup url
      */

        if (this.indexPanel !== -1) this._router.navigate([ConfigPanel.Panel[this.indexPanel]?.url]);
        else this._router.navigate(['/account/login']);
    }
}
