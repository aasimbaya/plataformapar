import { PermissionCheckerService } from 'abp-ng2-module';
import { AppSessionService } from '@shared/common/session/app-session.service';
import { Injectable } from '@angular/core';
import { AppMenu } from './app-menu';
import { AppMenuItem } from './app-menu-item';
import { Console } from 'console';

@Injectable()
export class AppNavigationService {
    constructor(
        private _permissionCheckerService: PermissionCheckerService,
        private _appSessionService: AppSessionService
    ) {}

    getMenu(): AppMenu {
        return new AppMenu('MainMenu', 'MainMenu', [
            new AppMenuItem(
                'DashboardName',
                1,
                '',
                //"Pages.DashboardName",
                'flaticon2-pie-chart-1',
                '',
                [],
                [
                    new AppMenuItem(
                        'Summary',
                        1,
                        //'Pages.Administration.OrganizationUnits',
                        '',
                        '',
                        '/app/admin/hostDashboard',
                        [],
                        [
                            new AppMenuItem(
                                'RankingPar',
                                1,
                                //'Pages.Administration.OrganizationUnits',
                                '',
                                'flaticon-doc',
                                '',
                                [],
                                [new AppMenuItem('Questionnaire', 1, '', '', '/app/admin/rendered-quizzes')]
                            ),
                        ]
                    ),
                    new AppMenuItem('Settings', 0, '', '', '/app/admin/hostSettings'),
                ]
            ),
            new AppMenuItem('Tenants', 0, 'Pages.Tenants', 'flaticon-list-3', '/app/admin/tenants'),
            new AppMenuItem(
                'measuringtools_navbar_title',
                1,
                '',
                'flaticon2-list-1',
                '',
                [],
                [
                    new AppMenuItem(
                        'measuringtools_navba_categories',
                        1,
                        'Pages.Measuringtools.Categories',
                        '',
                        '/app/measuring-tools/category'
                    ),
                    new AppMenuItem(
                        'SpecialDistinctions',
                        1,
                        'Pages.Measuringtools.SpecialDistinctions',
                        '',
                        '/app/measuring-tools/special-distintion'
                    ),
                    new AppMenuItem(
                        'QuestionsBank',
                        1,
                        'Pages.Measuringtools.QuestionsBank',
                        '',
                        '/app/admin/questions-bank'
                    ),
                    new AppMenuItem('ScoreTypes', 1, 'Pages.Measuringtools.ScoreTypes', '', '/app/score-types'),
                    new AppMenuItem(
                        'Questionnaires',
                        1,
                        'Pages.Measuringtools.Questionnaires',
                        '',
                        '/app/measuring-tools/questionnaire'
                    ),
                ]
            ),
            new AppMenuItem(
                'ResultsConfiguration',
                1,
                '',
                'flaticon2-calendar-5',
                '',
                [],
                [
                    new AppMenuItem(
                        'EquityScale',
                        1,
                        'Pages.ResultsConfiguration.EquityScale',
                        '',
                        '/app/admin/result-setting/equity-scale'
                    ),
                    new AppMenuItem(
                        'Recommendations',
                        1,
                        'Pages.ResultsConfiguration.Recommendations',
                        '',
                        '/app/admin/result-setting/recomendations'
                    ),
                    new AppMenuItem(
                        'GroupFeedBack',
                        1,
                        'Pages.ResultsConfiguration.GroupFeedBack',
                        '',
                        '/app/admin/result-setting/group-feed-back'
                    ),
                    new AppMenuItem(
                        'SubscriptionPlans',
                        1,
                        'Pages.ResultsConfiguration.SubscriptionPlans',
                        'flaticon2-list',
                        '/app/admin/result-setting/subscription-plans'
                    ),
                    new AppMenuItem(
                        'Roles',
                        1,
                        'Pages.ResultsConfiguration.Roles',
                        'flaticon2-shield',
                        '/app/admin/roles'
                    ),
                ]
            ),
            new AppMenuItem(
                'users_navbar_title',
                1,
                '',
                'flaticon-users-1',
                '',
                [],
                [
                    new AppMenuItem('users_navbar_useraequales', 1, '', '', '/app/users/aequales'),
                    new AppMenuItem('users_navbar_usersgroupings', 1, '', '', '/app/users/groupings'),
                    new AppMenuItem('Users', 1, 'Pages.Administration.Users', '', '/app/admin/users'),
                ]
            ),
            new AppMenuItem(
                'entities_navbar_title',
                1,
                'Pages.Entities',
                'flaticon-users-1',
                '',
                [],
                [
                    new AppMenuItem('entities_navbar_enterprise', 1, '', '', '/app/entities/enterprise'),
                    new AppMenuItem('entities_navbar_businessgroups', 1, '', '', '/app/entities/business-groups'),
                ]
            ),
            new AppMenuItem(
                'Content',
                [1],
                '',
                'flaticon2-open-text-book',
                '',
                [],
                [
                    new AppMenuItem('DocumentsContent', [1], '', '', '/app/content/documents'),
                    new AppMenuItem('Idioms', [1], 'Pages.Administration.Languages', '', '/app/admin/languages', [
                        '/app/admin/languages/{name}/texts',
                    ]),
                    new AppMenuItem(
                        'ModulesForExternalUsers',
                        [1],
                        'Pages.Content.ModulesForExternalUsers',
                        '',
                        '/app/content/modules-for-external'
                    ),
                    //new AppMenuItem('News', [1], '', '', '/app/content/news'),
                ]
            ),
            new AppMenuItem(
                'Communication',
                [1],
                '',
                'flaticon-multimedia',
                '',
                [],
                [new AppMenuItem('Mailing', [1], '', '', '/app/communication/mailing')]
            ),
            new AppMenuItem(
                'OtherProducts',
                [1],
                'Pages.OtherProducts',
                'flaticon2-box-1',
                '',
                [],
                [
                    new AppMenuItem('ToolkitForEquity', [1], '', '', '/app/admin/other-products/toolkit-equity'),
                    new AppMenuItem('GoodPractices', [1], '', '', '/app/admin/other-products/good-practices'),
                    new AppMenuItem('PerceptionSurveys', [1], '', '', '/app/admin/other-products/perception-surveys'),
                    new AppMenuItem('Certifications', [1], '', '', '/app/admin/other-products/certifications'),
                ]
            ),
            new AppMenuItem(
                'Evaluations',
                [1],
                'Pages.Evaluations',
                'flaticon-star',
                '',
                [],
                [
                    new AppMenuItem(
                        'ExternalEvaluationManagement',
                        [1],
                        '',
                        '',
                        '/app/admin/evaluations/external-evaluation'
                    ),
                    new AppMenuItem(
                        'ManagementOfSpecialDistinctions',
                        [1],
                        '',
                        '',
                        '/app/admin/evaluations/management-special-distinctions'
                    ),
                ]
            ),
            new AppMenuItem(
                'SettingsMain',
                [1],
                '',
                'flaticon2-settings',
                '',
                [],
                [
                    new AppMenuItem(
                        'SearchOptions',
                        [1],
                        'Pages.SettingsMain.SearchOptions',
                        '',
                        '/app/admin/settings-main/search-options'
                    ),
                    new AppMenuItem('Sector', [1], '', '', '/app/configurations/sector'),
                ]
            ),
            new AppMenuItem(
                'perception_surveys_title',
                [2],
                '',
                'flaticon2-settings',
                '',
                [],
                [new AppMenuItem('Users', 2, 'Pages.Administration.Users', 'flaticon-users', '/app/admin/users')]
            ),
            new AppMenuItem(
                'ranking_par_title',
                [2],
                '',
                'flaticon2-settings',
                '',
                [],
                [
                    new AppMenuItem('DocumentsContent', 2, '', '', '/app/content/documents'),
                    new AppMenuItem(
                        'ranking_par_cuestionario_title',
                        2,
                        '',
                        '',
                        '/app/measuring-tools/launch-call-page'
                    ),
                ]
            ),
        ]);
    }

    checkChildMenuItemPermission(menuItem): boolean {
        for (let i = 0; i < menuItem.items.length; i++) {
            let subMenuItem = menuItem.items[i];

            if (subMenuItem.permissionName === '' || subMenuItem.permissionName === null) {
                if (subMenuItem.route) {
                    return true;
                }
            } else if (this._permissionCheckerService.isGranted(subMenuItem.permissionName)) {
                return true;
            }

            if (subMenuItem.items && subMenuItem.items.length) {
                let isAnyChildItemActive = this.checkChildMenuItemPermission(subMenuItem);
                if (isAnyChildItemActive) {
                    return true;
                }
            }
        }
        return false;
    }

    showMenuItem(menuItem: AppMenuItem): boolean {
        if (this._appSessionService.tenant) {
            const { id } = this._appSessionService.tenant;
            if (menuItem.tenantId != id) {
                return false;
            }
        }
        if (
            menuItem.permissionName === 'Pages.Administration.Tenant.SubscriptionManagement' &&
            this._appSessionService.tenant &&
            !this._appSessionService.tenant.edition
        ) {
            return false;
        }

        let hideMenuItem = false;

        if (menuItem.requiresAuthentication && !this._appSessionService.user) {
            hideMenuItem = true;
        }

        if (menuItem.permissionName && !this._permissionCheckerService.isGranted(menuItem.permissionName)) {
            hideMenuItem = true;
        }

        if (this._appSessionService.tenant || !abp.multiTenancy.ignoreFeatureCheckForHostUsers) {
            if (menuItem.hasFeatureDependency() && !menuItem.featureDependencySatisfied()) {
                hideMenuItem = true;
            }
        }

        if (!hideMenuItem && menuItem.items && menuItem.items.length) {
            return this.checkChildMenuItemPermission(menuItem);
        }

        return !hideMenuItem;
    }

    /**
     * Returns all menu items recursively
     */
    getAllMenuItems(): AppMenuItem[] {
        let menu = this.getMenu();
        console.log('MENU', menu);
        let allMenuItems: AppMenuItem[] = [];
        menu.items.forEach((menuItem) => {
            allMenuItems = allMenuItems.concat(this.getAllMenuItemsRecursive(menuItem));
            console.log('concat', allMenuItems);
        });

        return allMenuItems;
    }

    getAllMenuItemsRecursive(menuItem: AppMenuItem): AppMenuItem[] {
        if (!menuItem.items) {
            return [menuItem];
        }

        let menuItems = [menuItem];
        menuItem.items.forEach((subMenu) => {
            menuItems = menuItems.concat(this.getAllMenuItemsRecursive(subMenu));
        });

        return menuItems;
    }
}
