import { Component, EventEmitter, Injector, Output, ViewChild, OnInit } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
  GetAllDropdownServiceProxy,
  UserEnterpriseServiceProxy,
  UpsertUserEnterpriseWrapperDto,
  UserAnonymousServiceProxy,
  UpsertEnterpriseDto,
  UpsertEnterpriseGroupDto,
  DropdownItemDto
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { SmsVerificationModalComponent } from './sms-verification-modal.component';
import { finalize } from 'rxjs/operators';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AbpSessionService } from 'abp-ng2-module';
import { DateTime } from 'luxon';

export interface List {
  name: string;
  id: number
}

@Component({
  selector: 'editCompanyModal',
  templateUrl: './edit-company-modal.component.html',
})
export class EditCompanyModalComponent extends AppComponentBase implements OnInit {
  @ViewChild('editCompanyModal', { static: true }) modal: ModalDirective;
  @ViewChild('smsVerificationModal') smsVerificationModal: SmsVerificationModalComponent;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  public disabled = true;
  public active = false;
  public saving = false;


  formTemplate = new FormGroup({
    name: new FormControl(''),
    idNumber: new FormControl(''),
    countryId: new FormControl(''),
    regionId: new FormControl(''),
    cityId: new FormControl(''),
    sectorId: new FormControl(''),
    employeesNumber: new FormControl(''),
    isInternational: new FormControl(true),
    receivePromoAccepted: new FormControl(false),
    inclusionRegionalManagerName: new FormControl(''),
    inclusionRegionalManagerEmail: new FormControl('')

  });

  countries: List[] = [];
  regions: List[] = [];
  cities: List[] = [];
  sectors: List[] = [];
  subscriptions: Subscription[] = [];
  companyData: UpsertUserEnterpriseWrapperDto;
  id: number = null;
  collaborators: DropdownItemDto[] = [];
  
  constructor(
    injector: Injector,
    private _listsService: GetAllDropdownServiceProxy,
    private _abpSessionService: AbpSessionService,
    private _userEnterpriseServiceProxy: UserEnterpriseServiceProxy,
    private _userService: UserAnonymousServiceProxy
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.id = this.appSession.enterprise?.id;
    // this.id = this.appSession.enterprise?.id || 2;

    this.getCollaborators();
    this.getItems();
    if(this.id)
      this._getInformation(this.id)
  }
  
  show(): void {
    this.active = true;
    this.modal.show();
  }


  close(): void {
    this.active = false;
    this.modal.hide();
  }

  save(): void {
    this.notify.info(this.l('SavedSuccessfully'));
    this.close();

    const payload = {
      id: this.companyData.user.id,
      name: this.companyData.user.name,
      enterpriseNumber: 0,
      isActive: this.companyData.user.isActive,
      userId: this.companyData.user.userId,
      countryId: this.companyData.user.countryId,
      regionId: this.companyData.user.regionId,
      cityId: this.companyData.user.cityId,
      sectorId: this.companyData.user.sectorId,
      hasInclusion: this.companyData.user.hasInclusion,
      isInternational: this.companyData.user.isInternational,
      receivePromoAccepted: this.companyData.user.receivePromoAccepted,
      isPartOfEconomicGroup: this.companyData.user.isPartOfEconomicGroup,
      economicGroupName: this.companyData.user.economicGroupName || '',
      isPartOfTradeAssociation: this.companyData.user.isPartOfTradeAssociation,
      tradeAssociationId: this.companyData.user.tradeAssociationId || 0,
      inclusionRegionalManagerName: this.companyData.user.inclusionRegionalManagerName || '',
      inclusionRegionalManagerEmail: this.companyData.user.inclusionRegionalManagerEmail || '',
      creatorUserName: this.companyData.user.creatorUserName || '',
      creationTime: this.companyData.user.creationTime,
      lastModifierUserName: this.companyData.user.lastModifierUserName || '',
      lastModificationTime:  DateTime.now()
    };

    console.log(payload);
    

    const sub = this._userService
      .createOrUpdateEnterpriseGroup(payload as unknown as UpsertEnterpriseGroupDto)
      .subscribe(
        (res: any) => {
          abp.notify.success(this.l('entities_enterprise_editor_success_updated'));
        },
        (err) => {
          console.error('_userEnterpriseServiceProxy.update');
        }
      );
    this.subscriptions = [...this.subscriptions, sub];
  }

  getCountries() {
    const sub = this._listsService.getAllCountries('', 1).subscribe((res: any) => {
      this.countries = res;
    }, err => {
      console.error('_questionCategoryServiceProxy.create');
    });
    this.subscriptions = [...this.subscriptions, sub];
  }

  getRegions(id: number) {
    const sub = this._listsService.getAllRegionsByCountryId(id).subscribe((res: any) => {
      this.regions = res;
    }, err => {
      console.error('_questionCategoryServiceProxy.create');
    });
    this.subscriptions = [...this.subscriptions, sub];
  }

  getCities(id: number) {
    const sub = this._listsService.getAllCitiesByRegionId(id).subscribe((res: any) => {
      this.cities = res;
    }, err => {
      console.error('_questionCategoryServiceProxy.create');
    });
    this.subscriptions = [...this.subscriptions, sub];
  }


  getSectors() {
    const sub = this._listsService.getAllSectors().subscribe((res: any) => {
      this.sectors = res;
    }, err => {
      console.error('_questionCategoryServiceProxy.create');
    });
    this.subscriptions = [...this.subscriptions, sub];
  }

  /**
     * Get collaborators
     *
     * @return
     */
   getCollaborators() {
    this._listsService
        .getAllCollaborators()
        .pipe(
            finalize(() => {
                this.saving = false;
            })
        )
        .subscribe((response) => {
            this.collaborators = response;
        });
}

  getItems() {
    this.getCountries();
  }

  private _getInformation(id?: number) {
    const sub = this._userEnterpriseServiceProxy.get(id).subscribe(
      (res: UpsertUserEnterpriseWrapperDto) => {
        this.companyData = res;
        this.formTemplate.patchValue({ ...res, ...res.user })
        this.getRegions(res.user.countryId);
        this.getCities(res.user.regionId);
        this.getSectors();
      },
      (err) => {
        console.error('_questionCategoryServiceProxy.get');
      }
    );
    this.subscriptions = [...this.subscriptions, sub];
  }
}
