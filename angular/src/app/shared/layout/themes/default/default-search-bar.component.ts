import { Injector, Component, ViewEncapsulation, Inject, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DOCUMENT } from '@angular/common';
import { filter as _filter } from 'lodash-es';

@Component({
    templateUrl: './default-search-bar.component.html',
    selector: 'default-search-bar',
     styleUrls: ['./default-search-bar.component.less'],
    encapsulation: ViewEncapsulation.None,
})
export class DefaultSearchBarComponent extends AppComponentBase {

    filters: {
        filterText: string;
    } = <any>{};

    constructor(injector: Injector, @Inject(DOCUMENT) private document: Document) {
        super(injector);
    }

    ngOnInit(): void {
        // this.filters.filterText = this.filterText || '';
    }
}
