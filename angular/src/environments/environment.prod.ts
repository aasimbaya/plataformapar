// "Production" enabled environment

export const environment = {
    production: true,
    hmr: false,
    appConfig: 'appconfig.production.json',
    TermsAndConditions: 'https://www.aequales.com/dev/terminos-y-condiciones/',
    PolicyUseDataAndInformationClients: 'https://www.aequales.com/dev/politicas-de-privacidad/',
    ForgotPasswordHelp: 'https://inforankingpar.refined.site/',
    keyCookieAccept: 'acceptCookiesNavigatorsAeQuales',
    KnowMoreCookiesUrl: 'https://www.aequales.com/dev/politicas-de-privacidad/?_ga=2.16854013.512014863.1662736062-2052482266.1656359293&_gl=1*yiz96a*_ga*MjA1MjQ4MjI2Ni4xNjU2MzU5Mjkz*_ga_Y93MGM1GZC*MTY2MjczNjA2Mi4yMS4xLjE2NjI3MzYxNTYuMC4wLjA',
};
