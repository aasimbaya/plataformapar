// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
    production: false,
    hmr: false,
    appConfig: 'appconfig.json',
    TermsAndConditions: 'https://www.aequales.com/dev/terminos-y-condiciones/',
    PolicyUseDataAndInformationClients: 'https://www.aequales.com/dev/politicas-de-privacidad/',
    ForgotPasswordHelp: 'https://inforankingpar.refined.site/',
    keyCookieAccept: 'acceptCookiesNavigatorsAeQuales',
    KnowMoreCookiesUrl: 'https://www.aequales.com/dev/politicas-de-privacidad/?_ga=2.16854013.512014863.1662736062-2052482266.1656359293&_gl=1*yiz96a*_ga*MjA1MjQ4MjI2Ni4xNjU2MzU5Mjkz*_ga_Y93MGM1GZC*MTY2MjczNjA2Mi4yMS4xLjE2NjI3MzYxNTYuMC4wLjA',
};
