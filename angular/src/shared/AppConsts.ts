export class AppConsts {
    static readonly tenancyNamePlaceHolderInUrl = '{TENANCY_NAME}';

    static remoteServiceBaseUrl: string;
    static remoteServiceBaseUrlFormat: string;
    static appBaseUrl: string;
    static appBaseHref: string; // returns angular's base-href parameter value if used during the publish
    static appBaseUrlFormat: string;
    static recaptchaSiteKey: string;
    static subscriptionExpireNootifyDayCount: number;

    static localeMappings: any = [];

    static readonly userManagement = {
        defaultAdminUserName: 'admin',
    };

    static readonly localization = {
        defaultLocalizationSourceName: 'PARPlatform',
    };

    static readonly authorization = {
        encrptedAuthTokenName: 'enc_auth_token',
    };

    static readonly grid = {
        defaultPageSize: 10,
    };

    static readonly MinimumUpgradePaymentAmount = 1;
    static readonly SearchBarDelayMilliseconds = 500;

    /// <summary>
    /// Gets current version of the application.
    /// It's also shown in the web page.
    /// </summary>
    static readonly WebAppGuiVersion = '11.2.1';

    static readonly OptionsEnterpriseTypeId: any[] = [
        {
            id: 'public',
            name: 'enterprisetype_public',
        },
        {
            id: 'privatePyme',
            name: 'enterprisetype_privatepyme',
        },
        {
            id: 'privatePymeNot',
            name: 'enterprisetype_privatepymenot',
        },
        {
            id: 'mix',
            name: 'enterprisetype_mix',
        },
    ];
    static readonly onlyCountries: any[] = [
        'ar',
        'bo',
        'br',
        'cl',
        'co',
        'cr',
        'cu',
        'ec',
        'sv',
        'gt',
        'ht',
        'hn',
        'mx',
        'ni',
        'pa',
        'py',
        'pe',
        'do',
        'uy',
        've',
    ];
}
