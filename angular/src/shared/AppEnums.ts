export class AppEditionExpireAction {
    static DeactiveTenant = 'DeactiveTenant';
    static AssignToAnotherEdition = 'AssignToAnotherEdition';
}

export enum PageMode {
    Add = 1,
    View,
    Edit,
    Duplicate
}