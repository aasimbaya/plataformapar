import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { CookiesProvider } from '@shared/core/services/cookies.service';
import { SweetalertProvider } from '@shared/core/services/sweetalert.service';
import { AppUrlService } from './nav/app-url.service';
import { AppSessionService } from './session/app-session.service';
import { CookieConsentService } from './session/cookie-consent.service';
import { AppUiCustomizationService } from './ui/app-ui-customization.service';

@NgModule({
    imports: [CommonModule],
})
export class PARPlatformCommonModule {
    static forRoot(): ModuleWithProviders<CommonModule> {
        return {
            ngModule: CommonModule,
            providers: [AppUiCustomizationService, CookieConsentService, AppSessionService, AppUrlService, CookiesProvider, SweetalertProvider],
        };
    }
}
