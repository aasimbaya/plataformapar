import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'mailing',
        loadChildren: () =>
            import('./mailing/mailing.module').then(
                (m) => m.MailingModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunicationRoutingModule { }
