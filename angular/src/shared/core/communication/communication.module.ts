import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommunicationRoutingModule } from './communication-routing.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';




@NgModule({
  declarations: [
  ],
  imports: [CommonModule, AppSharedModule, AdminSharedModule, CommunicationRoutingModule
  ]
})
export class CommunicationModule { }
