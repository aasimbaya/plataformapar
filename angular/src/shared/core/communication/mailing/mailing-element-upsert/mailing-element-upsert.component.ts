import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus, RecordsTable } from '@shared/core/dynamic-select-table/dynamic-select-table.model';
import { SpecialDistintionService } from '@shared/core/measuring-tools/special-distintion/service/special-distintion.service';
import { MailingServiceProxy, PagedResultDtoOfListItemDestinaryDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';

@Component({
    selector: 'app-mailing-element-upsert',
    templateUrl: './mailing-element-upsert.component.html',
    animations: [appModuleAnimation()],
})
export class MailingElementUpsertComponent extends AppComponentBase implements OnInit {
    @ViewChild('upsertElementModal', { static: true }) modal: ModalDirective;
    @Output() returnItemSelectedEvent: EventEmitter<any[]> = new EventEmitter<any[]>();
    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });
    set records(value: RecordsTable) {
        this._records.next(value);
    }
    get records(): RecordsTable {
        return this._records.value;
    }
    records$(): Observable<RecordsTable> {
        return this._records.asObservable();
    }

    subscriptions: Subscription[] = [];

    constructor(injector: Injector,
        private _specialDistintionService: SpecialDistintionService,
        private _mailingProxy : MailingServiceProxy) {
        super(injector);
    }

    ngOnInit(): void {
        //this.getSpecialDistintionList({ first: 0, page: 0, pageCount: 10, rows: 0 });
        this.getDestinaryList({ first: 0, page: 0, pageCount: 10, rows: 0 });
    }

    private getSpecialDistintionList(pag: PageStatus): void {
        const sub = this._specialDistintionService.getData(pag).subscribe(
            (res: RecordsTable) => {
                this.records = res;
                console.log('registro', res)
            },
            (err) => {
                console.error('_measuringToolsService.getData()');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    private getDestinaryList(pag: PageStatus): void {
        const sub = this._mailingProxy.getAllDestinary(pag.filter, '', '',
        pag.sorting, pag.pageCount, pag.page)
        .subscribe(
            (res: PagedResultDtoOfListItemDestinaryDto) => {
                this.records = { countTotal: res.totalCount, records: res.items };
            },
            (err) => {
                console.error('_mailingService.getDataDestinary()');
            }
        );
    this.subscriptions = [...this.subscriptions, sub];
    }

    onSelectedItemClose(data:boolean): void {
        if(data){
            this.modal.hide();
        }
    }

    onSelectedItem(data: any): void {
        this.returnItemSelectedEvent.emit(data)
    }

    onShown(): void {}

    show(id?: number): void {
        const self = this;
        self.modal.show();
    }

     close(): void {
        this.modal.hide();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }

    columns: ConfigColumns[] = [
        {
            field: 'name',
            titleLabel: 'name',
            type: 'string',
        },
        {
            field: 'emailAddress',
            titleLabel: 'email',
            type: 'string',
        },
    ];
}
