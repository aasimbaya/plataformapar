import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
    selector: 'app-mailing-list',
    templateUrl: './mailing-list.component.html',
    animations: [appModuleAnimation()],
})
export class MailingListComponent extends AppComponentBase implements OnInit {
    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });
    set records(value: RecordsTable) {
        this._records.next(value);
    }
    get records(): RecordsTable {
        return this._records.value;
    }
    records$(): Observable<RecordsTable> {
        return this._records.asObservable();
    }

    constructor(injector: Injector,
        private router: Router) {
        super(injector);
    }

    pagingChange(event: PageStatus) {
        //TODO
    }

    editOnClick(data: any): void {
        //TODO
    }

    deleteOnClick(data: any): void {
        //TODO
    }

    addOnClick(): void {
        this.router.navigate(['app', 'communication', 'mailing', 'add']);
    }

    ngOnInit(): void {}

    columns: ConfigColumns[] = [
        {
            field: 'emailTitle',
            titleLabel: 'EmailTitle',
            type: 'string',
        },
        {
            field: 'type',
            titleLabel: 'EmailType',
            type: 'string',
        },
        {
            field: 'structure',
            titleLabel: 'EmailStructure',
            type: 'string',
        },
        {
            field: 'createdOn',
            titleLabel: 'EmailCreatedOn',
            type: 'string',
        },
        {
            field: 'programFor',
            titleLabel: 'EmailProgramFor',
            type: 'string',
        },
    ];
}
