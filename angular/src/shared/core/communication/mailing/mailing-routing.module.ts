import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageMode } from '@shared/AppEnums';
import { MailingListComponent } from './mailing-list/mailing-list.component';
import { MailingUpsertComponent } from './mailing-upsert/mailing-upsert.component';

const routes: Routes = [
    {
        path: '',
        component: MailingListComponent,
        pathMatch: 'full',
        },
        {
            path: 'add',
            component: MailingUpsertComponent,
            data: { pageMode: PageMode.Add },
            pathMatch: 'full'
        },
        {
            path: ':id/edit',
            component: MailingUpsertComponent,
            data: { pageMode: PageMode.Edit },
            pathMatch: 'full'
        },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MailingRoutingModule { }
