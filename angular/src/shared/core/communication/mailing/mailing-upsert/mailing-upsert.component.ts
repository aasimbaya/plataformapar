import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { MailingElementUpsertComponent } from '../mailing-element-upsert/mailing-element-upsert.component';

@Component({
    selector: 'app-mailing-upsert',
    templateUrl: './mailing-upsert.component.html',
    styleUrls: ['./mailing-upsert.component.css'],
    animations: [appModuleAnimation()],
})
export class MailingUpsertComponent extends AppComponentBase implements OnInit {
    allIputTypes: string[] = ['SINGLE_LINE_STRING', 'COMBOBOX', 'CHECKBOX', 'MULTISELECTCOMBOBOX'];
    @ViewChild('mailingElementUpsertComponent', { static: true }) mailingElementUpsertComponent: MailingElementUpsertComponent;
    text: string;
    files: File[] = [];
    formMailing = new FormGroup({
        structure: new FormControl('', Validators.compose([Validators.required])),
        type: new FormControl('', Validators.compose([Validators.required])),
        byName: new FormControl('', Validators.compose([Validators.required])),
        byEmail: new FormControl('', Validators.compose([Validators.required])),
        bcc: new FormControl(''),
        titleContent: new FormControl('', Validators.compose([Validators.required])),
        footPage: new FormControl('', Validators.compose([Validators.required])),
        elementsMessage: new FormControl('', Validators.compose([Validators.required])),
    });

    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });
    set records(value: RecordsTable) {
        this._records.next(value);
    }
    get records(): RecordsTable {
        return this._records.value;
    }
    records$(): Observable<RecordsTable> {
        return this._records.asObservable();
    }

    constructor(injector: Injector,
        private router: Router) {
        super(injector);
    }

    ngOnInit(): void {}

    goBack(): void {
        this.router.navigate(['app', 'communication', 'mailing']);
    }

    addDestinaryOnClick(): void {
        this.mailingElementUpsertComponent.show();
    }
    deleteOnClick(data): void {
        debugger
        var items = this.records.records;
       /*  this.records.records.splice(_.indexOf(this.records.records, _.find(this.records.records,
            function (item) { return item.Id === data.id; })), 1);
 */
            items.splice(items.findIndex(function(i){
                return i.id === data.id;
            }), 1);
        console.log('data', data)
    }

    showPreview(): void {
        let w = window.innerWidth;
        let h = window.innerHeight;
        let myWindow = window.open('', 'MsgWindow', 'width=' + w + ',height=' + h);
        myWindow.document.write(this.text);
    }

    onSelectedItem(data: any): void {
        this.records = { countTotal: data.length, records: data};
    }

    onSelect(event) {
        this.files.push(...event.addedFiles);
    }

    onRemove(event) {
        console.log('position', this.files.indexOf(event))
        this.files.splice(this.files.indexOf(event), 1);
    }

    addFile(): void{
        const eventClick = new Event('click');
        let elem = document.querySelector('#dropzone');
        elem.dispatchEvent(eventClick);
    }

    updateFile(event): void {
        this.addFile();
        this.onRemove(event);
    }

    columns: ConfigColumns[] = [
        {
            field: 'name',
            titleLabel: 'Name',
            type: 'string',
        },
        {
            field: 'emailAddress',
            titleLabel: 'Email',
            type: 'string',
        },
    ];


    columnsIdioms: ConfigColumns[] = [
        {
            field: 'idiom',
            titleLabel: 'Idiom',
            type: 'string',
        }
    ];
}
