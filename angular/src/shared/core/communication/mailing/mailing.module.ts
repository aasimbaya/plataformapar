import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CoreModule } from '@shared/core/core.module';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { MailingElementUpsertComponent } from './mailing-element-upsert/mailing-element-upsert.component';
import { MailingListComponent } from './mailing-list/mailing-list.component';
import { MailingRoutingModule } from './mailing-routing.module';
import { MailingUpsertComponent } from './mailing-upsert/mailing-upsert.component';


@NgModule({
  declarations: [
    MailingUpsertComponent,
    MailingListComponent,
    MailingElementUpsertComponent
  ],
  imports: [
    CommonModule,
    MailingRoutingModule, AppSharedModule, AdminSharedModule,
    CoreModule, NgxDropzoneModule
  ]
})
export class MailingModule { }
