import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CompiledPageComponent } from './compiled-page.component';

const routes: Routes = [
    {
        path: '',
        component: CompiledPageComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CompiledPageRoutingModule {}
