import { Component, Injector } from '@angular/core';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

interface OptionsMenu {
    index: string,
    titleLabel: string,
    active: boolean
}
@Component({
    selector: 'compiled-page',
    templateUrl: './compiled-page.component.html',
    styleUrls: ['./compiled-page.component.less'],
    animations: [appModuleAnimation()],
})
export class CompiledPageComponent extends AppComponentBase {
    breadcrumbs: BreadcrumbItem[] = [
        new BreadcrumbItem(this.l('DashboardCompanyBusiness'))
    ];
    
    constructor(
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnDestroy(): void {
    }
}
