import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CoreModule } from '../core.module';
import { CompiledPageRoutingModule } from './compiled-page-routing.module';
import { CompiledPageComponent } from './compiled-page.component';
import { DynamicInputExampleComponent } from './dynamic-input-example/dynamic-input-example.component';
import { GraficComponentExampleComponent } from './grafic-component-example/grafic-component-example.component';
import { ModalComponentExampleComponent } from './modal-component-example/modal-component-example.component';
import { SelectInputFilterComponent } from './select-input-filter/select-input-filter.component';
import { TableComponentExampleComponent } from './table-component-example/table-component-example.component';

@NgModule({
    declarations: [CompiledPageComponent, TableComponentExampleComponent, GraficComponentExampleComponent, ModalComponentExampleComponent,DynamicInputExampleComponent, SelectInputFilterComponent],
    imports: [AppSharedModule, AdminSharedModule, CompiledPageRoutingModule, CoreModule],
    exports: [CompiledPageComponent, TableComponentExampleComponent, GraficComponentExampleComponent, ModalComponentExampleComponent,DynamicInputExampleComponent]
})
export class PremiumDashboardModule {}
