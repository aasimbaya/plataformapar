import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigGrafic, RangeConfig } from '@shared/core/dynamic-grafic/dynamic-grafic.model';
import { PremiumDashboardService } from '@shared/core/premium-dashboard/services/premium-dashboard.service';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';

interface OptionsMenu {
    index: string,
    titleLabel: string,
    active: boolean
}
@Component({
    templateUrl: './grafic-component-example.component.html',
    styleUrls: ['./grafic-component-example.component.less'],
    selector: 'grafic-component-example',
    animations: [appModuleAnimation()],
})
export class GraficComponentExampleComponent extends AppComponentBase implements OnInit {
    breadcrumbs: BreadcrumbItem[] = [
        new BreadcrumbItem(this.l('DashboardCompanyBusiness'))
    ];
    optionsMenu: OptionsMenu[] = [
        {
            index: 'score',
            titleLabel: 'Score',
            active: true
        },
        {
            index: 'representation',
            titleLabel: 'Representation',
            active: false
        },
        {
            index: 'promotions',
            titleLabel: 'Promotions',
            active: false
        },
        {
            index: 'hiring',
            titleLabel: 'Hiring',
            active: false
        },
        {
            index: 'retreats',
            titleLabel: 'Retreats',
            active: false
        },
        {
            index: 'salary',
            titleLabel: 'Salary',
            active: false
        },
    ];
    private _configGrafic: BehaviorSubject<ConfigGrafic> = new BehaviorSubject({
        yKey: "",
        xKey: []
    })
    set configGrafic(value: ConfigGrafic) { this._configGrafic.next(value) }
    get configGrafic(): ConfigGrafic { return this._configGrafic.value }
    configGrafic$(): Observable<ConfigGrafic> { return this._configGrafic.asObservable() }

    private _records: BehaviorSubject<any[]> = new BehaviorSubject([])
    set records(value: any[]) { this._records.next(value) }
    get records(): any[] { return this._records.value }
    records$(): Observable<any[]> { return this._records.asObservable() }

    subscriptions: Subscription[] = [];
    titleDescriptionTab = "";
    descriptionTab = "";
    descriptionGrafic = "";
    constructor(
        injector: Injector,
        private _router: Router,
        private _premiumDashboardService: PremiumDashboardService,
    ) {
        super(injector);
    }
    ngOnInit(): void {
        // const sub = this._measuringToolsService.getData().subscribe((res: Pokedex) => {
        //     this.records = { records: res.results, countTotal: res.count };
        // }, err => {
        //     console.error("_measuringToolsService.getData()");
        // });
        // this.subscriptions = [...this.subscriptions, sub];
        this._initData();
    }
    private _initData(): void {
        this.titleDescriptionTab = this.l('TitleDescriptionTabscore');
        this.descriptionTab = this.l('DescriptionTabscore');
        this.descriptionGrafic = this.l('DescriptionGraficscore');
        this._setConfigStore();
        this._showDataScore();
    }
    selectMenu(item: OptionsMenu): void {
        this.optionsMenu = this.optionsMenu.map(e => { return { ...e, active: e.index == item.index } });
        this._setDescription(item);
        this._setConfig(item);
        switch (item.index) {
            case 'score':
                this._showDataScore();
                break;
            case 'representation':
                this._showDataRepresentation();
                break;
            case 'promotions':
                this._showDataPromotions();
                break;
            case 'hiring':
                this._showDataHiring();
                break;
            case 'retreats':
                this._showDataRetreats();
                break;
            case 'salary':
                this._showDataSalary();
                break;
        }
    }
    private _setDescription(item: OptionsMenu): void {
        this.titleDescriptionTab = this.l('TitleDescriptionTab' + item.index);
        this.descriptionTab = this.l('DescriptionTab' + item.index);
        this.descriptionGrafic = this.l('DescriptionGrafic' + item.index);
    }
    private _showDataScore(): void {
        const sub = this._premiumDashboardService.getDataScore().subscribe((res: any[]) => {
            this.records = res;
        }, err => {
            console.error("_premiumDashboardService.getDataScore()");
        });
        this.subscriptions = [...this.subscriptions, sub];
    }
    private _showDataRepresentation(): void {
        const sub = this._premiumDashboardService.getDataRepresentation().subscribe((res: any[]) => {
            this.records = res;
        }, err => {
            console.error("_premiumDashboardService.getDataRepresentation()");
        });
        this.subscriptions = [...this.subscriptions, sub];
    }
    private _showDataPromotions(): void {
        const sub = this._premiumDashboardService.getDataPromotions().subscribe((res: any[]) => {
            this.records = res;
        }, err => {
            console.error("_premiumDashboardService.getDataPromotions()");
        });
        this.subscriptions = [...this.subscriptions, sub];
    }
    private _showDataHiring(): void {
        const sub = this._premiumDashboardService.getDataHiring().subscribe((res: any[]) => {
            this.records = res;
        }, err => {
            console.error("_premiumDashboardService.getDataHiring()");
        });
        this.subscriptions = [...this.subscriptions, sub];
    }
    private _showDataRetreats(): void {
        const sub = this._premiumDashboardService.getDataRetreats().subscribe((res: any[]) => {
            this.records = res;
        }, err => {
            console.error("_premiumDashboardService.getDataRetreats()");
        });
        this.subscriptions = [...this.subscriptions, sub];
    }
    private _showDataSalary(): void {
        const sub = this._premiumDashboardService.getDataSalary().subscribe((res: any[]) => {
            this.records = res;
        }, err => {
            console.error("_premiumDashboardService.getDataSalary()");
        });
        this.subscriptions = [...this.subscriptions, sub];
    }

    private _setConfig(item: OptionsMenu): void {
        switch (item.index) {
            case 'score':
                this._setConfigStore();
                break;
            case 'representation':
            case 'promotions':
            case 'hiring':
            case 'retreats':
                this._setConfigDefault();
                break;
            case 'salary':
                this._setConfigSalary();
                break;
        }
    }

    private _setConfigStore(): void {
        this.configGrafic = {
            yKey: "nameCompany",
            xKey: ["ScoreTotal", "ObejtiveManagement", "TalentManagement", "Culture", "Estructure"],
            rangeConfig: this._getConfigRange(),
            callFunValue: function (e) { return `${e}%`; }
        };
    }
    private _setConfigSalary(): void {
        this.configGrafic = {
            yKey: "nameCompany",
            xKey: ["JuntaDirectiva", "PrimerNivl", "Segundo", "TErcero", "Cuarto"],
            rangeConfig: this._getConfigRange(),
            propertyValue: "value",
            callFunValue: function (e) {
                return `
            ${e.value}% <span>
            <i class="fa fa-solid ${e.gender == 'h' ? 'fa-mars' : 'fa-venus'} fa-1x text-white"></i>
            </span>
            `;
            },
            aditionalLabel: `
            <span>
            <i class="fa fa-solid fa-mars fa-1x ms-2"></i> ${this.l('MaleDominance')}
            <i class="fa fa-solid fa-venus fa-1x ms-2"></i> ${this.l('FemaleDominance')}
            </span>
            `
        };
    }
    private _setConfigDefault(): void {
        this.configGrafic = {
            yKey: "nameCompany",
            xKey: ["JuntaDirectiva", "PrimerNivl", "Segundo", "TErcero", "Cuarto"],
            rangeConfig: this._getConfigRange(),
            callFunValue: function (e) { return `${e}%`; }
        };
    }

    private _getConfigRange(): RangeConfig[] {
        return [
            {
                min: 0,
                max: 30,
                color: '#ff0000',
                label: 'Bajo (<30%)'
            },
            {
                min: 31,
                max: 69,
                color: '#ff7f00',
                label: 'Medio (31 a 69%)'
            },
            {
                min: 70,
                max: 101,
                color: '#00ad4f',
                label: 'Alto (>70%)'
            },
        ];
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe())
    }
}
