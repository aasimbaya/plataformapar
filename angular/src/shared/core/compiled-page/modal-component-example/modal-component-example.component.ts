import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { AppConsts } from '@shared/AppConsts';
import { ModalBodyDescription, ModalBodyTitle, ModalFooterButtoms, ModalHeaderImage, ModalHeaderTitle } from '@shared/core/dynamic-modal/dynamic-modal.model';

@Component({
    selector: 'modal-component-example',
    templateUrl: './modal-component-example.component.html',
    animations: [appModuleAnimation()],
})
export class ModalComponentExampleComponent extends AppComponentBase implements OnInit {

    headerImage: ModalHeaderImage;
    headerTittle: ModalHeaderTitle;
    bodyTittle: ModalBodyTitle;
    bodyDescription: ModalBodyDescription;
    footerButtoms: ModalFooterButtoms;

    subscriptions: Subscription[] = [];

    constructor(
        injector: Injector,
        private _router: Router,
    ) {
        super(injector);
    }
    ngOnInit(): void {
        //Dynamic Modal props
        this.headerImage = {
            isShow: true,
            src: AppConsts.appBaseUrl + '/assets/common/images/Tablero_Bienvenida.png',
            alt: '¡Bienvenido a la Nueva Plataforma PAR!',
        };

        this.headerTittle = {
            isShow: true,
            text: 'Cosas nuevas a la vista!',
        };
        this.bodyTittle = {
            isShow: true,
            text: 'Actualizacion de Plataforma',
        };
        this.bodyDescription = {
            isShow: true,
            text: `¡Bienvenido a la Nueva Plataforma PAR!
            Notarás algunos cambios en la apariencia y
            nuevas funcionalidades. Hemos incorporado
            nuevos módulos y herramientas para
            ofrecer una nueva experiencia.
            ¡Comienza a explorar!
            `,
        };
        this.footerButtoms = {
            isSuccess: true,
            isFaileru: true,
            isSeeMore: true,
            isOptional: true,
        };

    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe())
    }
}
