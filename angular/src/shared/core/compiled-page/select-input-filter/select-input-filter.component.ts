import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

interface City {
    name: string,
    code: string
}

@Component({
    selector: 'app-select-input-filter',
    templateUrl: './select-input-filter.component.html',
    styleUrls: ['./select-input-filter.component.css'],
    animations: [appModuleAnimation()],
})


export class SelectInputFilterComponent extends AppComponentBase implements OnInit {
    cities: City[];
    selectedCity1: City;

    allIputTypes: string[] = [
        "SINGLE_LINE_STRING",
        "COMBOBOX",
        "CHECKBOX",
        "MULTISELECTCOMBOBOX"
    ]
    formTemplate = new FormGroup({
        selectRequired: new FormControl('', Validators.compose([Validators.required])),

    })


    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {
        this. cities = [
            { name: 'New York', code: 'NY' },
            { name: 'Rome', code: 'RM' },
            { name: 'London', code: 'LDN' },
            { name: 'Istanbul', code: 'IST' },
            { name: 'Paris', code: 'PRS' },
        ];
    }
}
