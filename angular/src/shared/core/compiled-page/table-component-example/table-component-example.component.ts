import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import { filter as _filter } from 'lodash-es';
import { Subscription, BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { debounce as _debounce } from 'lodash-es';
import { ConfigColumns, PageStatus, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { MeasuringToolsService } from '@shared/core/measuring-tools/measuring-tools-category/services/measuring-tools.service';
import { MeasuringToolCategory } from '@shared/core/measuring-tools/measuring-tools-category/measuring-tools-category.model';


@Component({
    selector: 'table-component-example',
    templateUrl: './table-component-example.component.html',
    animations: [appModuleAnimation()],
})
export class TableComponentExampleComponent extends AppComponentBase implements OnInit {
    columns: ConfigColumns[] = [
        {
            field: 'category',
            titleLabel: 'CategoryMeasuringTools',
            type: 'string'
        },
        {
            field: 'type',
            titleLabel: 'TypeMeasuringTools',
            type: 'string'
        },
        {
            field: 'active',
            titleLabel: 'ActiveMeasuringTools',
            type: 'boolean'
        },
        {
            field: 'AssociatedQuestions',
            titleLabel: 'AssociatedQuestionsMeasuringTools',
            type: 'number'
        },
        {
            field: 'latestModificatedDate',
            titleLabel: 'LatestModificatedDateMT',
            type: 'date',
        }
    ];

    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: []
    })
    set records(value: RecordsTable) { this._records.next(value) }
    get records(): RecordsTable { return this._records.value }
    records$(): Observable<RecordsTable> { return this._records.asObservable() }

    subscriptions: Subscription[] = [];
    constructor(
        injector: Injector,
        private _router: Router,
        private _measuringToolsService: MeasuringToolsService,
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._getInformation({ first: 0, page: 0, pageCount: 10, rows: 0 });
    }
    private _getInformation(pag: PageStatus): void {
        const sub = this._measuringToolsService.getData(pag).subscribe((res: RecordsTable) => {
            this.records = res;
        }, err => {
            console.error("_measuringToolsService.getData()");
        });
        this.subscriptions = [...this.subscriptions, sub];
    }
    /**
     * add button on click action
     */
    addOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
    }
    pagingChange(event: PageStatus) {
        this._getInformation(event);
    }
    /**
     * filter button on click action
     */
    filterOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
    }

    /**
     * close button on click action
     */
    closeOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration']);
    }




    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe())
    }
}
