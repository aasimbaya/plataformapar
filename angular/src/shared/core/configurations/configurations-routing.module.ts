import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'sector',
        loadChildren: () =>
            import('./sector/sector.module').then((m) => m.SectorModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ConfigurationsRoutingModule {}
