import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { ConfigurationsRoutingModule } from './configurations-routing.module';

@NgModule({
    declarations: [
    ],
    imports: [CommonModule, AppSharedModule, AdminSharedModule, ConfigurationsRoutingModule],
})
export class ConfigurationsModule {}
