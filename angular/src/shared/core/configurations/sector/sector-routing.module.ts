import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageMode } from '@shared/AppEnums';
import { SectorUpsertComponent } from './sector-upsert/sector-upsert.component';
import { SectorComponent } from './sector.component';

const routes: Routes = [
    {
        path: '',
        component: SectorComponent,
        pathMatch: 'full',
        },
        {
            path: 'add',
            component: SectorUpsertComponent,
            data: { pageMode: PageMode.Add },
            pathMatch: 'full'
        },
        {
            path: ':id/edit',
            component: SectorUpsertComponent,
            data: { pageMode: PageMode.Edit },
            pathMatch: 'full'
        },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SectorRoutingModule {}
