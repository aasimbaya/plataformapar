import { Component, Injector } from '@angular/core';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PageMode } from '@shared/AppEnums';
import { SectorServiceProxy, UpsertSectorDto } from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'sector-upsert',
    templateUrl: './sector-upsert.component.html',
})
export class SectorUpsertComponent extends AppComponentBase {    
    flatHasQuestions = false;
    breadcrumbs: BreadcrumbItem[] = [];
    loading: boolean = false;
    pageMode: PageMode;
    subscriptions: Subscription[] = [];
    form: FormGroup;
    isNew = true;
    filter: string = '';
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _sectorService: SectorServiceProxy,
        private _formBuilder: FormBuilder
    ) {
        super(injector);
    }

    /**
     * OnInit
     */
    ngOnInit(): void {
        this.pageMode = this._activatedRoute.snapshot.data.pageMode;
        const id = this._activatedRoute.snapshot.params.id;
        this._init(id);
    }

    /**
     * Initialize component
     * 
     * @return 
     */
    private async _init(id) {
        if (id) {
            this.breadcrumbs = [new BreadcrumbItem(this.l('configurations_sector_editor_subtitle_create'))];
            this.isNew = false;
            this.getSector(id);
        } else {
            this.breadcrumbs = [new BreadcrumbItem(this.l('configurations_sector_editor_subtitle_update'))];
            this.isNew = true;
            this.form = await this._formInitialize(new UpsertSectorDto());
        }
    }

    /**
     * Allow get sector by id
     * 
     * @return 
     */
    private async getSector(id: number) {
        const sub = this._sectorService.get(id).subscribe(
            async (res: UpsertSectorDto) => {
                this.form = await this._formInitialize(res);
            },
            (err) => {
                console.error('_sectorService.get');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Initialize form data
     * 
     * @return 
     */
    private async _formInitialize(data: UpsertSectorDto) {
        if(data.id > 0) {
            return this._formBuilder.group({
                id: [data.id || 0, []],
                name: [data.name || '', [Validators.required]],                
                type: [data.type || '', [Validators.required]],
                isActive: [data.isActive || true, []]
            });
        } else {
            return this._formBuilder.group({
                id: [data.id || 0, []],
                name: [data.name || '', [Validators.required]],
                ciiu: [data.ciiu || '', [Validators.required]],
                type: [data.type || '', [Validators.required]],
                isActive: [data.isActive || true, []]
            });
        }        
    }

    /**
     * Allow handle edit event
     * 
     * @return 
     */
    editOnClick(data: any): void {
        // Not implemented
    }

    /**
     * Allow set filter 
     * 
     * @return 
     */
    findEvent(data: string): void {
        this.filter = data;
    }

    /**
     * Allow close edit form
     * 
     * @return 
     */
    closeOnClick(): void {
        if (!this.form.pristine) {
            abp.message.confirm(
                this.l('sector_categories_editor_confirm_close_mgs'),
                this.l('sector_categories_editor_confirm_close_title'),
                (result: boolean) => {
                    if (result) {
                        this.redirectToSectorList();
                    }
                }
            );
        } else {
            this.redirectToSectorList();
        }
    }

    /**
     * Allow save current sector
     * 
     * @return 
     */
    saveOnClick(): void {
        const { valid } = this.form;
        if (valid) {
            if (this.isNew) {
                this.create();
            } else {
                this.edit();
            }
        } else {
            this.form.markAllAsTouched();
        }
    }

    /**
     * Prevent
     * 
     * @return 
     */
    onFormSubmit($event, form) {
        $event.preventDefault;
    }

    /**
     * Edit handle
     * 
     * @return 
     */
    private edit() {
        const sub = this._sectorService
            .update(this.form.controls.id.value, this.getUpsertSectorDto())
            .subscribe(
                (res: any) => {
                    abp.notify.success(this.l('sector_categories_editor_success_updated'));
                    this.redirectToSectorList();
                },
                (err) => {
                    console.error('_sectorService.update');
                }
            );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Create handle
     * 
     * @return 
     */
    private create() {
        const sub = this._sectorService.create(this.getUpsertSectorDto()).subscribe(
            (res: any) => {
                abp.notify.success(this.l('sector_categories_editor_success_created'));
                this.redirectToSectorList();
            },
            (err) => {
                console.error('_sectorService.create');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Allow create upsert model from form data
     * 
     * @return 
     */
    private getUpsertSectorDto(): UpsertSectorDto {
        const { value } = this.form;
        const { name, type, innerType, isActive, ciiu } = value;
        const data = new UpsertSectorDto();
        data.name = name;
        data.type = type;
        data.isActive = isActive;
        data.innerType = innerType;
        data.ciiu = ciiu;
        return data;
    }

    /**
     * Return to previous component
     * 
     * @return 
     */
    private redirectToSectorList(): void {
        this._router.navigate(['app', 'configurations', 'sector']);
    }

    /**
     * Destroy event
     * 
     * @return 
     */
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
