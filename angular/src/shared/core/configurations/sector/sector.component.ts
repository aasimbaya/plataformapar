import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { filter as _filter } from 'lodash-es';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { debounce as _debounce } from 'lodash-es';
import { ConfigColumns, PageStatus, ReloadTable } from '../../dynamic-table/dynamic-table.model';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';
import { SectorServiceProxy, UpsertSectorDto } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './sector.component.html',
    animations: [appModuleAnimation()],
})
export class SectorComponent extends AppComponentBase implements OnInit {
    breadcrumbs: BreadcrumbItem[] = [new BreadcrumbItem(this.l('configurations_sector_subtitle'))];
    columns: ConfigColumns[] = [
        {
            field: 'ciiu',
            titleLabel: 'configurations_sector_grid_ciiu',
            type: 'string',
        },
        {
            field: 'name',
            titleLabel: 'configurations_sector_grid_name',
            type: 'string',
        },
        {
            field: 'type',
            titleLabel: 'configurations_sector_grid_type',
            type: 'string',
        }
    ];

    subscriptions: Subscription[] = [];
    private _reload: BehaviorSubject<ReloadTable> = new BehaviorSubject({});
    set reload(value: ReloadTable) {
        this._reload.next(value);
    }

    get reload(): ReloadTable {
        return this._reload.value;
    }

    reload$(): Observable<ReloadTable> {
        return this._reload.asObservable();
    }

    constructor(
        injector: Injector,
        private _router: Router,
        private _sectorServiceProxy: SectorServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.loadDataTable = this.loadDataTable.bind(this);
    }

    loadDataTable(data: PageStatus) {
        return this._sectorServiceProxy.getAllPaged(data.filter, data.sorting, data.pageCount, data.page);
    }

    /**
     * Allow navigate to create
     * 
     * @return 
     */
    addOnClick(): void {
        this._router.navigate(['app', 'configurations', 'sector', 'add']);
    }

    /**
     * Allow navigate to edit
     * 
     * @return 
     */
    editOnClick(data: UpsertSectorDto): void {
        this._router.navigate(['app', 'configurations', 'sector', data.id, 'edit']);
    }

    /**
     * Delete event
     * 
     * @return 
     */
    deleteOnClick(data: UpsertSectorDto): void {
        abp.message.confirm(
            this.l('measuringtools_categories_confirm_delete_mgs'),
            this.l('measuringtools_categories_confirm_delete_title'),
            (result: boolean) => {
                if (result) {
                    this.delete(data.id);
                }
            }
        );
    }

    /**
     * Allow delete a record
     * 
     * @return 
     */
    private delete(id: number) {
        const sub = this._sectorServiceProxy.delete(id).subscribe(
            (res: any) => {
                this.getSectors({ reset: true });
                abp.notify.success(this.l('configurations_sector_success_delete'));
            },
            (err) => {
                console.error('_sectorServiceProxy.delete');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Load sectors
     * 
     * @return 
     */
    getSectors(data: ReloadTable): void {
        this.reload = data;
    }

    /**
     * Destroy event
     * 
     * @return 
     */
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
