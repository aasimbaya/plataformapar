import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CoreModule } from '@shared/core/core.module';
import { InputsModule } from '@shared/core/inputs/inputs.module';
import { SectorRoutingModule } from './sector-routing.module';
import { SectorUpsertComponent } from './sector-upsert/sector-upsert.component';
import { SectorComponent } from './sector.component';

@NgModule({
    declarations: [SectorComponent, SectorUpsertComponent],
    imports: [CommonModule, AppSharedModule, AdminSharedModule, SectorRoutingModule, CoreModule, InputsModule],
})
export class SectorModule {}
