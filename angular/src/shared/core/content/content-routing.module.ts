import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'documents',
        loadChildren: () =>
            import('./documents/documents.module').then(
                (m) => m.DocumentsModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
    {
        path: 'modules-for-external',
        loadChildren: () =>
            import('./modules-for-external/modules-for-external.module').then(
                (m) => m.ModulesForExternalModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
    {
        path: 'news',
        loadChildren: () =>
            import('./news/news.module').then(
                (m) => m.NewsModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ContentRoutingModule {}
