import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { ContentRoutingModule } from './content-routing.module';
@NgModule({
    declarations: [
  ],
    imports: [AppSharedModule, AdminSharedModule, ContentRoutingModule],
})
export class ContentModule {}
