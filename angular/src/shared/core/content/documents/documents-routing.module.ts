import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageMode } from '@shared/AppEnums';
// import { DocumentsUpsertComponent } from './upsert/upsert.component';
import { DocumentsListComponent } from './list/list.component';
import { DocumentsUpsertComponent } from './documents-upsert/documents-upsert.component';

const routes: Routes = [
    {
        path: '',
        component: DocumentsListComponent,
        pathMatch: 'full',
        },
        // {
        //     path: 'add',
        //     component: DocumentsUpsertComponent,
        //     data: { pageMode: PageMode.Add },
        //     pathMatch: 'full'
        // },
        {
            path: ':id/edit',
            component: DocumentsUpsertComponent,
            data: { pageMode: PageMode.Edit },
            pathMatch: 'full'
        },
        // {
        //     path: ':id/edit',
        //     component: DocumentsUpsertComponent,
        //     data: { pageMode: PageMode.Edit },
        //     pathMatch: 'full'
        // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentsRoutingModule {
}
