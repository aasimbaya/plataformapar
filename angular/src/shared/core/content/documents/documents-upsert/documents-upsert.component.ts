import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    DocumentsServiceProxy,
    DropdownDocumentEstructureDto,
    DropdownDocumentLanguageDto,
    DropdownDocumentLocationDto,
    FileParameter,
    GetAllDropdownDocumentServiceProxy,
    UpsertDocumentDto,
} from '@shared/service-proxies/service-proxies';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { UpsertFormDto } from '../../../../service-proxies/service-proxies';
import { PageMode } from '@shared/AppEnums';
import { FileUploader } from 'ng2-file-upload';

@Component({
    selector: 'app-documents-upsert',
    templateUrl: './documents-upsert.component.html',
    styleUrls: ['./documents-upsert.component.css'],
    animations: [appModuleAnimation()],
})

/**
 * Class to create and update a Special Distintion
 */
export class DocumentsUpsertComponent extends AppComponentBase implements OnInit {
    @Output() reloadTable: EventEmitter<boolean>;

    allEstructures: DropdownDocumentEstructureDto[] = [];
    allLocations: DropdownDocumentLocationDto[] = [];
    allLanguages: DropdownDocumentLanguageDto[] = [];
    uploadFile: FileUploader;
    allowedExtensions: string = 'image/x-png,image/gif,image/jpg,image/jpeg';
    allowedExtensionsText: string = 'JPG, JPEG, PNG, GIF';
    allowedFileSize: string = '30KB';
    allowedLimitResolution: string = '139x35';
    cantFiles: number = 0;
    isEdit: boolean = false;
    idEdit: number = 0;

    files: File[] = [];
    upsertSpecialDistintion: UpsertFormDto = new UpsertFormDto();
    pageMode: PageMode;

    formDocuments = new FormGroup({
        Name: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(50)])),
        EstructureId: new FormControl('', Validators.compose([Validators.required])),
        LocationId: new FormControl('', Validators.compose([Validators.required])),
        LanguageId: new FormControl('', Validators.compose([Validators.maxLength(150)])),
        Size: new FormControl('', Validators.compose([Validators.maxLength(10)])),
        URL: new FormControl('', Validators.compose([Validators.maxLength(150)])),
        isActive: new FormControl('true'),
    });

    subscriptions: Subscription[] = [];

    constructor(
        injector: Injector,
        private router: Router,
        private _documentCatalogosServices: GetAllDropdownDocumentServiceProxy,
        private _documentServicesBack: DocumentsServiceProxy,
        private routeActive: ActivatedRoute /*private _specialDistintionService: SpecialDistintionService,
        private _formService: FormServiceProxy,
        private _dropdownService: GetAllDropdownServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _questionsBankService:  QuestionsBankServiceProxy*/
    ) {
        super(injector);
        this.reloadTable = new EventEmitter();
    }

    ngOnInit(): void {
        this.loadEstructure();
        this.loadLenguages();
        this.loadLocations();
        this.idEdit = this.routeActive.snapshot.params.id;
        this.isEdit = this.idEdit ? true : false;
        if (this.isEdit) {
            this._getDocumentById(this.idEdit);
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }

    goBack(): void {
        this.router.navigate(['app', 'content', 'documents']);
    }

    save(): void {
        if (this.idEdit) {
            //this.updateSpecialDistintion();
        }
    }

    allowSave(): boolean {
        return true;
    }

    loadEstructure() {
        const sub = this._documentCatalogosServices.getAllDocumentEstructure().subscribe(
            (res: DropdownDocumentEstructureDto[]) => {
                this.allEstructures = res;
            },
            (err) => {
                console.error('_documentCatalogosServices.getAllDocumentEstructure');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    loadLocations() {
        const sub = this._documentCatalogosServices.getAllDocumentLocation().subscribe(
            (res: DropdownDocumentLocationDto[]) => {
                this.allLocations = res;
            },
            (err) => {
                console.error('_documentCatalogosServices.getAllDocumentLocation');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    loadLenguages() {
        const sub = this._documentCatalogosServices.getAllDocumentLanguage().subscribe(
            (res: DropdownDocumentLanguageDto[]) => {
                this.allLanguages = res;
            },
            (err) => {
                console.error('_documentCatalogosServices.getAllDocumentLanguage');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    addDocument(): void {
        let objDocument = Object.assign({}, this.formDocuments.getRawValue(), UpsertDocumentDto);
        // debugger;
        const sub = this._documentServicesBack.create(objDocument).subscribe(
            (res: any) => {
                abp.notify.success(this.l('SavedSuccessfully'));
                this._redirectToListPage();
            },
            (err) => {
                console.error('_documentServicesBackProxy.create');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    getDataForEdit(res: UpsertDocumentDto) {
        this.formDocuments.controls['Name'].setValue(res.name);
        this.formDocuments.controls['EstructureId'].setValue(res.estructureId);
        this.formDocuments.controls['LocationId'].setValue(res.locationId);
        this.formDocuments.controls['LanguageId'].setValue(res.languageId);
        this.formDocuments.controls['URL'].setValue(res.url);
        this.formDocuments.controls['isActive'].setValue(res.isActive);

        let sizeStringFile = (parseInt(res.size) / 1024).toString();
        sizeStringFile = sizeStringFile.substring(0, sizeStringFile.indexOf('.') + 3);
        sizeStringFile = sizeStringFile.concat(' ').concat('MB');
        this.formDocuments.controls['Size'].setValue(sizeStringFile);

        // debugger;
    }

    _getDocumentById(id?: number): void {
        const sub = this._documentServicesBack.getDocumentById(id).subscribe(
            async (res) => {
                await this.getDataForEdit(res);
            },
            (err) => {
                console.error('_documentServicesBackProxy.getDocumentById');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    async deleteDocument() {
        await this._deleteDocumentById(this.idEdit);
        this.reloadTable.emit(true);
        this.router.navigate(['app', 'content', 'documents']);
    }

    _deleteDocumentById(id?: number): void {
        const sub = this._documentServicesBack.deleteDocumentById(id).subscribe(
            async (res) => {
                this._redirectToListPage();
                this.notify.success(this.l('SuccessfullyDeleted'));
            },
            (err) => {
                console.error('_documentServicesBackProxy.getDocumentById');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    updateDocument(): void {
        let objDocument = Object.assign({}, this.formDocuments.getRawValue(), UpsertDocumentDto);
        const sub = this._documentServicesBack.update(this.idEdit, objDocument).subscribe(
            (res: any) => {
                abp.notify.success(this.l('SuccessfullyUpdate'));
                this._redirectToListPage();
            },
            (err) => {
                console.error('_documentServicesBackProxy.update');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    isEdition() {
        if (this.idEdit) return true;
    }

    private _redirectToListPage(): void {
        this.reloadTable.emit(true);
        this.router.navigate(['app', 'content', 'documents']);
    }

    onSelect(event) {
        this.files.push(...event.addedFiles);
        //TODO: se debe definir si se guardan varios archivos ya que segun diseño es uno solo por iteración
        this.formDocuments.controls.Size.setValue(this.files[0].size);
        let url = this.uploadS3File(this.files[0]);
        // console.log(this.files[0])
        console.log(url);
        this.formDocuments.controls.URL.setValue(url);
    }

    onRemove(event) {
        this.files.splice(this.files.indexOf(event), 1);
    }

    uploadS3File(file: any): string {
        let document:FileParameter = {
            data: file,
            // fileName:  Math.random() * 100000000000000000 + '_' + file.name
            fileName: file.name,
        };

        const params = {
            Bucket: 'pardevaequalestest', //TODO: hacer esto dinamico o traerlo desde la api
            region: 'us-east-2',
            Key: document.fileName,
        };

        console.log(params, 'params');
        try {
            console.log(document, 'document');
            let resp = this._documentServicesBack.uploadDocumentToS3(document);
            console.log(resp, 'response');
        } catch (error) {
            console.log('FAILURE', error);
        }
        return `https://${params.Bucket}.s3.${params.region}.amazonaws.com/${params.Key}`;
    }

    onUploadClick(): void {
        this.uploadFile.uploadAll();
    }

    removeFileOnClick(event) {
        this.onRemove(event);
    }

    updateFileOnClick(event) {
        this.onSelect(event);
    }
}
