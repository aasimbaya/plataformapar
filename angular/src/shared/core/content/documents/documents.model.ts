export interface DocumentsModel {
    id: number;
    name: string;
    size: string;
    extension: string;
    structure: string;
    location: string;
    language: string;
    isActive: boolean;
    createDate: Date;
}
