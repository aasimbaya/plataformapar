import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { CoreModule } from '@shared/core/core.module';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { DocumentsRoutingModule } from './documents-routing.module';
// import { DocumentsUpsertComponent } from './upsert/upsert.component';
import { DocumentsServiceProxy, GetAllDropdownDocumentServiceProxy } from '@shared/service-proxies/service-proxies';
import { DocumentsUpsertComponent } from './documents-upsert/documents-upsert.component';
import { DocumentsListComponent } from './list/list.component';

@NgModule({
    declarations: [DocumentsListComponent, DocumentsUpsertComponent],
    //   declarations: [DocumentsComponent,DocumentsUpsertComponent],
    imports: [AppSharedModule, AdminSharedModule, CoreModule, CommonModule, DocumentsRoutingModule, NgxDropzoneModule],
    providers: [GetAllDropdownDocumentServiceProxy, DocumentsServiceProxy],
})
export class DocumentsModule {}
