import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';

import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';
import { DocumentsService } from '@shared/core/content/documents/service/document-dummy.service';
import { FileUploader } from 'ng2-file-upload';
import { DocumentsModel } from '../documents.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
    DocumentsServiceProxy,
    DropdownDocumentEstructureDto,
    DropdownDocumentLanguageDto,
    DropdownDocumentLocationDto,
    FileParameter,
    GetAllDropdownDocumentServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { AppConsts } from '@shared/AppConsts';
import { debounce as _debounce } from 'lodash-es';
import { DocumentsUpsertComponent } from '../documents-upsert/documents-upsert.component';
import { UploadS3V2Service } from '../service/upload-v2.service';

// import { DocumentsUpsertComponent } from './upsert/documents-upsert.component';

const DOCUMENT_COLLUMS_SCHEMA = [
    {
        field: 'name',
        titleLabel: 'Name',
        type: 'string',
    },
    {
        field: 'size',
        titleLabel: 'Size',
        type: 'string',
    },
    {
        field: 'extension',
        titleLabel: 'Extension',
        type: 'string',
    },
    {
        field: 'estructure',
        titleLabel: 'Structure',
        type: 'string',
    },
    {
        field: 'location',
        titleLabel: 'Location',
        type: 'string',
    },
    {
        field: 'language',
        titleLabel: 'Language',
        type: 'string',
    },
    {
        field: 'isActive',
        titleLabel: 'IsActive',
        type: 'boolean',
    },
    {
        field: 'lastModificationTime',
        titleLabel: 'CreationUploadTime',
        type: 'date',
    },
];

@Component({
    styleUrls: ['./list.component.css'],
    templateUrl: './list.component.html',
    animations: [appModuleAnimation()],
})
export class DocumentsListComponent extends AppComponentBase implements OnInit {
    breadcrumbs: BreadcrumbItem[] = [new BreadcrumbItem(this.l('DocumentsContent'))];
    subscriptions: Subscription[] = [];

    filtersByColumns: string[] = DOCUMENT_COLLUMS_SCHEMA.map((col) => this.uppercaseFirstLetter(this.l(col.field)));
    columns: ConfigColumns[] = DOCUMENT_COLLUMS_SCHEMA;

    allowedExtensions: string = 'image/x-png,image-/gif,image/jpg,image/jpeg';
    allowedExtensionsText: string = 'JPG, JPEG, PNG, GIF';
    allowedFileSize: string = '30KB';
    allowedLimitResolution: string = '139x35';

    // @ViewChild('DocumentsUpsert', { static: true }) DocumentsUpsert: DocumentsUpsertComponent;
    @ViewChild('uploaInputLabel') uploadInputLabel: ElementRef;
    @ViewChild('documentUpsert', { static: true }) documentUpsert: DocumentsUpsertComponent;

    file: FileParameter;
    cantFiles: number = 0;
    // uploadFile: FileUploader;
    selectedFiles: FileList;

    title = 's3-img-upload';
    renderImages: any = [];

    allEstructures: DropdownDocumentEstructureDto[] = [];
    allLocations: DropdownDocumentLocationDto[] = [];
    allLanguages: DropdownDocumentLanguageDto[] = [];

    uploadFile: FileUploader;

    formDocuments = new FormGroup({
        Name: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(50)])),
        EstructureId: new FormControl('', Validators.compose([Validators.required])),
        LocationId: new FormControl('', Validators.compose([Validators.required])),
        LanguageId: new FormControl('', Validators.compose([Validators.maxLength(150)])),
        Size: new FormControl('', Validators.compose([Validators.maxLength(10)])),
        URL: new FormControl('', Validators.compose([Validators.maxLength(150)])),
        isActive: new FormControl('true'),
    });

    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });
    set records(value: RecordsTable) {
        this._records.next(value);
    }
    get records(): RecordsTable {
        return this._records.value;
    }
    records$(): Observable<RecordsTable> {
        return this._records.asObservable();
    }

    constructor(
        injector: Injector,
        private _router: Router,
        private _documentServicesBack: DocumentsServiceProxy,
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.getInformation();
    }

    getInformation(data?: PageStatus) {
        this._getInformation({ first: 0, page: 0, pageCount: 5, rows: 0 });
    }

    _getInformation(data: PageStatus) {
        const sub = this._documentServicesBack
            .getAllPaged(data.filter, data.sorting, data.pageCount, data.page)
            .subscribe(
                (res: any) => {
                    console.log(res.items);
                    this.records = { countTotal: res.totalCount, records: res.items };
                },
                (err) => {
                    console.error('_documentServices.getAllPaged()');
                }
            );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * add button on click action
     */
    addOnClick(): void {
        // this.DocumentsUpsert.show();
    }
    editOnClick(data: DocumentsModel): void {
        this._router.navigate(['app', 'content', 'documents', data.id, 'edit']);
    }

    deleteOnClick(data: DocumentsModel): void {
        //revisar el tipo
        abp.message.confirm(
            this.l('MsgConfirmDeleteDocument'),
            this.l('TitleConfirmDeleteDocument'),
            (result: boolean) => {
                if (result) {
                    this.deleteDocument(data.id);
                }
            }
        );
    }
    deleteDocument(id?: number) {
        this.documentUpsert._deleteDocumentById(id);
        // const sub = this._questionCategoryServiceProxy.delete(id).subscribe((res: any) => {//revisar
        //     this._getInformation();
        //     abp.notify.success(this.l('SuccessfullyDeleted'));
        // }, err => {
        //     console.error('_zipCodesServiceProxy.delete');
        // });
        // this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * filter button on click action
     */
    filterOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }

    // TODO: set this function in a shared place
    uppercaseFirstLetter(str: string) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    addDocument(): void {
        this.documentUpsert.addDocument();
    }
}
