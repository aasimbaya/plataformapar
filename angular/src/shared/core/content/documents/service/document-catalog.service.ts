import { Injectable } from "@angular/core";
import { GetAllDropdownDocumentServiceProxy } from "@shared/service-proxies/service-proxies";
import { Subscription } from "rxjs";

export interface List {
    name: string;
    id: number
}

export static class DocumentCatalogService {
    subscriptions: Subscription[] = [];
    constructor(private _documentCatalogosServices: GetAllDropdownDocumentServiceProxy) {

    }
        
    allEstructures: List[] = [];
    allLocations: List[] = [];
    allLanguages: List[] = [];

    loadEstructure(){
        const sub = this._documentCatalogosServices.getAllDocumentEstructure().subscribe((res: List[]) => {
        this.allEstructures = res;
      }, err => {
        console.error('_documentCatalogosServices.getAllDocumentEstructure');
      });
      this.subscriptions = [...this.subscriptions, sub];        

    }

    loadLocations(){
        const sub = this._documentCatalogosServices.getAllDocumentLocation().subscribe((res: List[]) => {
            this.allLocations = res;
        }, err => {
            console.error('_documentCatalogosServices.getAllDocumentLocation');
        });
        this.subscriptions = [...this.subscriptions, sub];        
    }

    loadLenguages(){
        const sub = this._documentCatalogosServices.getAllDocumentLanguage().subscribe((res: List[]) => {
            this.allLanguages = res;
        }, err => {
            console.error('_documentCatalogosServices.getAllDocumentLanguage');
        });
        this.subscriptions = [...this.subscriptions, sub];        
    }
}