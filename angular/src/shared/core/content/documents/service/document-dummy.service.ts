import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PageStatus, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {
  constructor(
    private _http: HttpClient
  ) { }
  getDocumentsData(pag: PageStatus): Observable<RecordsTable> {

    const data = {
      countTotal: 10,
      records: [//revisar, migrar a tabla generica con jsons, y separar en un componente generico
        {
          id: 1,
          name: 'Documento 1',
          size: '10 MB',
          extension: 'PDF',
          structure: 'Empresa',
          location: 'Documentos',
          language: 'Español',
          isActive: true,
          creationTime: new Date("2022-03-07"),
        },
        {
            id: 1,
            name: 'Documento 1',
            size: '10 MB',
            extension: 'PDF',
            structure: 'Empresa',
            location: 'Documentos',
            language: 'Español',
            isActive: true,
            creationTime: new Date("2022-03-07"),
          },
          {
            id: 2,
            name: 'Documento 1',
            size: '10 MB',
            extension: 'PDF',
            structure: 'Empresa',
            location: 'Documentos',
            language: 'Español',
            isActive: true,
            creationTime: new Date("2022-03-07"),
          },
          {
            id: 3,
            name: 'Documento 1',
            size: '10 MB',
            extension: 'PDF',
            structure: 'Empresa',
            location: 'Documentos',
            language: 'Español',
            isActive: true,
            creationTime: new Date("2022-03-07"),
          },
          {
            id: 4,
            name: 'Documento 1',
            size: '10 MB',
            extension: 'PDF',
            structure: 'Empresa',
            location: 'Documentos',
            language: 'Español',
            isActive: true,
            creationTime: new Date("2022-03-07"),
          },
          {
            id: 5,
            name: 'Documento 1',
            size: '10 MB',
            extension: 'PDF',
            structure: 'Empresa',
            location: 'Documentos',
            language: 'Español',
            isActive: true,
            creationTime: new Date("2022-03-07"),
          },
          {
            id: 6,
            name: 'Documento 1',
            size: '10 MB',
            extension: 'PDF',
            structure: 'Empresa',
            location: 'Documentos',
            language: 'Español',
            isActive: true,
            creationTime: new Date("2022-03-07"),
          },
      ]
    }
    return of(data);

  }
}
