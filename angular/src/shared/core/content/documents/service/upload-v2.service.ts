import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';

@Injectable({
  providedIn: 'root',
})
export class UploadS3V2Service {
  constructor() {}

  uploadFile(file: any) {
    const contentType = file.type;
    const bucket = new S3({
      accessKeyId: 'AKIAZ4EJ2EZMEAQECLSM',
      secretAccessKey: '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',
      signatureVersion: 'v4',
      region: 'us-east-2',
    });
    // this function converts the generic JS ISO8601 date format to the specific format the AWS API wants
    const getAmzDate = (dateStr: any) => {
        var chars = [':', '-'];
        for (var i = 0; i < chars.length; i++) {
            while (dateStr.indexOf(chars[i]) != -1) {
                dateStr = dateStr.replace(chars[i], '');
            }
        }
        dateStr = dateStr.split('.')[0] + 'Z';
        return dateStr;
    };

    // get the various date formats needed to form our request
    var amzDate = getAmzDate(new Date().toISOString());

    const params = {
      // Bucket: 'pardevaequalestest',
      Bucket: 'purpleappfood123',
      Key: file.name,
      Body: file,
      // ACL: 'public-read',
      ContentType: contentType,
    };

    // bucket.abortMultipartUpload;
    bucket
      //for upload progress
      // .upload(params)
      // .on('httpUploadProgress', function (evt) {
      //     console.log(evt.loaded + ' of ' + evt.total + ' Bytes');
      // })
      .putObject(params)
      .on('build', function (req: any) {
        console.log('on Build');
        // "x-amz-metadata-directive": "REPLACE"
        req.httpRequest.headers['X-Amz-Metadata-Directive'] = "REPLACE";
        req.httpRequest.headers['X-Amz-Date'] = 'hola';
        req.httpRequest.headers['x-amz-date'] = amzDate;
      })
      .on('httpHeaders', function (statusCode: any, headers: any) {
        console.log('on httpHeaders');
        console.log(headers);
      })
      .on('send', function (req: any) {
        console.log('on send');
        // console.log(req.request.httpRequest.headers);
      })
      .send(function (err: any, data: any) {
        console.log('in send', data);
        if (err) {
          console.log('There was an error uploading your file: ', err);
          return false;
        }
        console.log('Successfully uploaded file.', data);
        return true;
      });
  }
}
