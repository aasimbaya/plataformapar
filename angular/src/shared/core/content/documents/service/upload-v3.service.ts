import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { S3Client, PutObjectCommand, GetBucketAclCommand } from '@aws-sdk/client-s3';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';
// import { environment } from './environments/environment';

@Injectable({
    providedIn: 'root',
})
export class UploadS3V3Service {
    private bucket: any;
    region: string = 'us-east-2';

    constructor(private http: HttpClient) {
        this.bucket = new S3Client({
            credentials: {
                accessKeyId: 'AKIAXIQUAFWXP65EQVUB',
                secretAccessKey: '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',
            },
            region: 'us-east-2',
        });
    }

    async uploadMultiFile(file: File) {
        const params = {
            Bucket: 'purpleappfood123',
            Key: Math.random() * 10000000000000000 + '_' + file.name,
            Body: file,
        };
        console.log(params, 'params');
        try {
            const command = new PutObjectCommand(params);
            const response = await this.bucket.send(command);
        } catch (error) {
            console.log('FAILURE', error);
        }
        return `https://${params.Bucket}.s3${this.region}.amazonaws.com/${params.Key}`;
    }

    async listBuckets() {
        try {
            const config = {
                credentials: {
                    accessKeyId: 'AKIAZ4EJ2EZMD2YD3HVO',
                    secretAccessKey: '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',
                },
                region: 'us-east-2',
            };
            const client = new S3Client(config);
            const command = new GetBucketAclCommand({ Bucket: 'purpleappfood123' });
            const response = await client.send(command);
            console.log(response, 'response');
        } catch (error) {
            console.log('FAILURE', error);
        }
    }

    async uploadFile(file: File) {
        // const contentType = file.type;
        // console.log(contentType, 'contentType');
        const params = {
            Bucket: 'purpleappfood123',
            // Bucket: 'pardevaequalestest',
            Key: Math.random() * 10000000000000000 + '_' + file.name,
            Body: file,
            // ACL: 'public-read',
            // ContentType: contentType,
        };
        // console.log(params, 'params');
        // // this function converts the generic JS ISO8601 date format to the specific format the AWS API wants
        // const getAmzDate = (dateStr: any) => {
        //     var chars = [':', '-'];
        //     for (var i = 0; i < chars.length; i++) {
        //         while (dateStr.indexOf(chars[i]) != -1) {
        //             dateStr = dateStr.replace(chars[i], '');
        //         }
        //     }
        //     dateStr = dateStr.split('.')[0] + 'Z';
        //     return dateStr;
        // };

        // // get the various date formats needed to form our request
        // var amzDate = getAmzDate(new Date().toISOString());

        // try {
        //     const command = new PutObjectCommand(params);
        //     this.bucket.middlewareStack.add(
        //         (next, context) => async (args) => {
        //             debugger;
        //             args.request.headers['x-amz-date'] = amzDate;
        //             args.request.headers['X-Amz-Date'] = amzDate;
        //             // args.request['headers']['x-amz-date'] = amzDate;
        //             const result = await next(args);
        //             // result.response contains data returned from next middleware.
        //             return result;
        //         },
        //         {
        //             step: 'build',
        //             name: 'addFooMetadataMiddleware',
        //             tags: ['METADATA', 'FOO'],
        //         }
        //     );
        //     const response = await this.bucket.sen(command);
        //     console.log(response, 'response');
        // } catch (error) {
        //     console.log('FAILURE', error);
        // }
        const { S3 } = require('@aws-sdk/client-s3');
        const client = new S3({
            credentials: {
                accessKeyId: 'AKIAZ4EJ2EZMEAQECLSM',
                // accessKeyId: 'AKIAXIQUAFWXP65EQVUB',
                secretAccessKey: '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',
                // secretAccessKey: '6YO8rFANpKKU3Ay8RiOt1OhZTFqousI0NA5YSBOn',
            },
            region: 'us-west-2',
        });
        try {
            // Middleware added to client, applies to all commands.
            client.middlewareStack.add(
                (next, context) => async (args) => {
                    debugger;
                    args.request.headers['x-amz-date'] = '123123';
                    args.request.headers['x-amz-meta-foo'] = 'bar';
                    const result = await next(args);
                    // result.response contains data returned from next middleware.
                    return result;
                },
                {
                    step: 'build',
                    name: 'addFooMetadataMiddleware',
                    tags: ['METADATA', 'FOO'],
                }
            );

            await client.putObject(params);
        } catch (error) {
            console.log('FAILURE', error);
        }
    }

    async uploadFileWithPreSignedURL(file: File) {
        const contentType = file.type;

        const params = {
            Bucket: 'purpleappfood123',
            Key: file.name,
            ACL: 'public-read',
            ContentType: contentType,
        };

        const command = new PutObjectCommand(params);

        try {
            const preSignedURL = await getSignedUrl(this.bucket, command, { expiresIn: 3600 });

            console.log(preSignedURL);
            // this.http.put(preSignedURL, file).subscribe({
            //     next: (res) => {
            //         console.log('SUCCESS', res);
            //     },
            //     error: (err) => {
            //         console.log('FAILED', err);
            //     },
            //     complete: () => {
            //         console.log('DONE');
            //     },
            // });
        } catch (err) {
            console.log(err);
        }
    }

    //   try {
    //    const data =  await bucket.send(params)
    //   } catch(err) {
    //     console.log(err);
    //   }
    // bucket.config
    // bucket.upload(params, (err: any, data: any) => {
    //   if (err) {
    //     console.log('EROOR: ', JSON.stringify(err));
    //     return false;
    //   }
    //   console.log('File Uploaded.', data);
    //   return true;
    // });
}
