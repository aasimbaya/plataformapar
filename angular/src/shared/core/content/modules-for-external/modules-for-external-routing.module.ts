import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModulesForExternalComponent } from './modules-for-external/modules-for-external.component';

const routes: Routes = [
  {
    path: '',
    component: ModulesForExternalComponent,
    pathMatch: 'full',
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesForExternalRoutingModule { }
