import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { ModulesForExternalRoutingModule } from './modules-for-external-routing.module';
import { ModulesForExternalComponent } from './modules-for-external/modules-for-external.component';


@NgModule({
  declarations: [
    ModulesForExternalComponent
  ],
  imports: [
    CommonModule,
    ModulesForExternalRoutingModule,
    AppCommonModule,
    AdminSharedModule
  ]
})
export class ModulesForExternalModule { }
