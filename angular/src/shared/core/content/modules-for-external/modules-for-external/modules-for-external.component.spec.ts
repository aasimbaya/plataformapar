import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulesForExternalComponent } from './modules-for-external.component';

describe('ModulesForExternalComponent', () => {
  let component: ModulesForExternalComponent;
  let fixture: ComponentFixture<ModulesForExternalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModulesForExternalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModulesForExternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
