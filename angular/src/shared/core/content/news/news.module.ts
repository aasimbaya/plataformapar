import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { NewsRoutingModule } from './news-routing.module';
import { NewsComponent } from './news/news.component';


@NgModule({
  declarations: [
    NewsComponent
  ],
  imports: [
    CommonModule,
    NewsRoutingModule,
    AppCommonModule,
    AdminSharedModule
  ]
})
export class NewsModule { }
