import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { DynamicGraficComponent } from './dynamic-grafic/dynamic-grafic.component';
import { DynamicTableComponent } from './dynamic-table/dynamic-table.component';
import { DynamicModalComponent } from './dynamic-modal/dynamic-modal.component';
import { ModalFooterComponent } from './dynamic-modal/components/modal-footer/modal-footer.component';
import { ModalBodyComponent } from './dynamic-modal/components/modal-body/modal-body.component';
import { ModalHeaderComponent } from './dynamic-modal/components/modal-header/modal-header.component';
import { DynamicSelectTableComponent } from './dynamic-select-table/dynamic-select-table.component';
import { DynamicInputComponent } from './dynamic-input/dynamic-input.component';
import { DynamicRadioBooleanComponent } from './dynamic-input/dynamic-radio-boolean/dynamic-radio-boolean.component';
import { DynamicSelectmultipleComponent } from './dynamic-input/dynamic-selectmultiple/dynamic-selectmultiple.component';
import { DynamicTextareaComponent } from './dynamic-input/dynamic-textarea/dynamic-textarea.component';
import { DynamicTextComponent } from './dynamic-input/dynamic-text/dynamic-text.component';
import { DynamicSelectsingleComponent } from './dynamic-input/dynamic-selectsingle/dynamic-selectsingle.component';
import { DynamicSidebarComponent } from './dynamic-sidebar/dynamic-sidebar.component';

@NgModule({
    declarations: [
        DynamicGraficComponent,
        DynamicTableComponent,
        DynamicModalComponent,
        ModalFooterComponent,
        ModalBodyComponent,
        ModalHeaderComponent,
        DynamicSelectTableComponent,
        DynamicInputComponent,
        DynamicRadioBooleanComponent,
        DynamicSelectmultipleComponent,
        DynamicRadioBooleanComponent,
        DynamicTextareaComponent,
        DynamicTextComponent,
        DynamicSelectsingleComponent,
        DynamicSidebarComponent
    ],
    imports: [AppSharedModule, AdminSharedModule],
    exports: [
        DynamicGraficComponent,
        DynamicTableComponent,
        DynamicModalComponent,
        ModalFooterComponent,
        ModalBodyComponent,
        ModalHeaderComponent,
        DynamicSelectTableComponent,
        DynamicInputComponent,
        DynamicRadioBooleanComponent,
        DynamicSelectmultipleComponent,
        DynamicRadioBooleanComponent,
        DynamicTextareaComponent,
        DynamicTextComponent,
        DynamicSelectsingleComponent
    ],
})
export class CoreModule {}
