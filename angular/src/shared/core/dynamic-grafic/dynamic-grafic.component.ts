import { Component, Injector, ViewChild, OnInit, Input } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import { Table } from 'primeng/table';
import { filter as _filter } from 'lodash-es';
import { finalize } from 'rxjs/operators';
import { Subscription, Subject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { LazyLoadEvent } from 'primeng/api';
import { debounce as _debounce } from 'lodash-es';
import { AppConsts } from '@shared/AppConsts';
import { Paginator } from 'primeng/paginator';
import { ConfigGrafic } from './dynamic-grafic.model';

interface matrizGrafic {
    key: string;
    value: number;
    text: number | string;
    color: string;
}
@Component({
    selector: 'dynamic-grafic',
    templateUrl: './dynamic-grafic.component.html',
    styleUrls: ['./dynamic-grafic.component.less'],

    animations: [appModuleAnimation()],
})
export class DynamicGraficComponent extends AppComponentBase implements OnInit {
    @Input() configGrafic: Observable<ConfigGrafic>;
    @Input() data: Observable<any[]>;
    @Input() descriptionGrafic:string;

    configGraficFix: ConfigGrafic;
    dataFix: any[] = [];
    matriz: matrizGrafic[] = [];

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    subscriptions: Subscription[] = [];
    // branches: any[];
    // countries: CustomerCountryLookupTableDto[] = [];
    // arAccounts: any[];
    // warehouses: any[];
    // currencies: any[];
    regexInputs = "[^\']";
    filters: { filterText: string } = { filterText: '' };
    destroy$ = new Subject();

    tableInformation: any[] = [];

    constructor(
        injector: Injector,
        private _router: Router,
    ) {
        super(injector);
    }
    ngOnInit(): void {
        const ConfigSub = this.configGrafic?.subscribe((value => {
            this.configGraficFix = value;
            this._buildGrafic();
        }))
        const valueSub = this.data?.subscribe((value => {
            this.dataFix = value;
            this._buildGrafic();
        }))
        this.subscriptions = [...this.subscriptions, valueSub, ConfigSub];
    }
    private _buildGrafic(): void {
        this.configGraficFix;
        this.dataFix;
        this.matriz = this.dataFix.reduce((ant, value) => {
            const data = {
                ...this._getDataForShow(value)
            };
            ant.push(data);
            return ant;
        }, []);
    }
    private _getDataForShow(data: any): any {
        const newData = {
            key: data[this.configGraficFix.yKey]
        };
        this.configGraficFix.xKey.forEach(element => {
            const value = this.configGraficFix.propertyValue ? data[element][this.configGraficFix.propertyValue] : data[element];
            if (value == -1) {
                newData[element] = {
                    value: -1,
                    text: 'N/A',
                    color: '#c5c9d7'
                }
            }else if (value) {
                newData[element] = {
                    value: value,
                    text: this.configGraficFix.callFunValue ? this.configGraficFix.callFunValue(data[element]) : value,
                    color: this._getColorCell(value)
                }
            }
        });
        return newData;
    }
    private _getColorCell(value: number): any {
        return this.configGraficFix.rangeConfig.find(e => e.min <= value && e.max >= value)?.color || "white";
    }
    /**
     * add button on click action
     */
    addOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
    }
    /**
     * filter button on click action
     */
    filterOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
    }

    /**
     * close button on click action
     */
    closeOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration']);
    }
    /**
     * view button on click action
     */
    viewOnClick(zipCode1: string): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', zipCode1, 'view']);
    }

    /**
     * edit button on click action
     */
    editOnClick(zipCode1: string): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', zipCode1, 'edit']);
    }
    /**
     * delete button on click action
     */
    deleteOnClick(zipCode1: string): void {//revisar el tipo
        abp.message.confirm(
            this.l('MsgConfirmDeleteZipCode'),
            this.l('TitleConfirmDeleteZipCode'),
            (result: boolean) => {
                if (result) {
                    this._delete(zipCode1);
                    // this.usersService.deleteUser(user.id).subscribe(() => {
                    //     abp.notify.success(this.l('SuccessfullyDeleted'));
                    //     this.refresh();
                    // });
                }
            }
        );
    }
    /**
     * zip code search filter on input action
     */
    zipCodeSearchOnInput = _debounce(this.getZipCodes, AppConsts.SearchBarDelayMilliseconds);
    /**
     * get paged zipcode list
     */
    getZipCodes(event?: LazyLoadEvent): void {
        if (this.primengTableHelper.shouldResetPaging(event)) {//revisar
            this.paginator.changePage(0);
            return;
        }
        //this.primengTableHelper.showLoadingIndicator();
        // this._ZipCodeServiceProxy.getAllPaged(
        //     this.filters.filterText,
        //     this.primengTableHelper.getSorting(this.dataTable),
        //     this.primengTableHelper.getMaxResultCount(this.paginator, event),
        //     this.primengTableHelper.getSkipCount(this.paginator, event)
        // ).pipe(
        //     takeUntil(this.destroy$),
        //     finalize(() => this.primengTableHelper.hideLoadingIndicator())
        // ).subscribe((x: PagedResultDtoOfZipCodeListDto) => {
        //     this.primengTableHelper.totalRecordsCount = x.totalCount;
        //     this.primengTableHelper.records = x.items;
        // });

    }

    /**
     * action for delete zip code
     */
    private _delete(code: string) {
        // const sub = this._ZipCodeServiceProxy.delete(code).subscribe((res: any) => {
        //     this.getZipCodes();
        //     abp.notify.success(this.l('SuccessfullyDeleted'));
        // }, err => {
        //     console.error("_ZipCodeServiceProxy.delete");
        // });
        // this.subscriptions = [...this.subscriptions, sub];
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe())
        this.destroy$.next();
    }
}
