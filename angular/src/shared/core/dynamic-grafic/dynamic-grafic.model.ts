/*
* Interface to configure grafic
*/
export interface ConfigGrafic {
    yKey: string;
    xKey: string[];
    propertyValue?:string;
    aditionalLabel?:string;
    rangeConfig?: RangeConfig[];
    callFunValue?:Function;
}
/*
* Interface to configure range grafic 
*/
export interface RangeConfig {
    min: number;
    max: number;
    color: string;
    label: string;
}
