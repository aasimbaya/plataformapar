import { Component, Injector, Input, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

import { Router } from '@angular/router';

@Component({
    selector: 'dynamic-input',
    templateUrl: './dynamic-input.component.html',
    styleUrls: ['./dynamic-input.component.less'],
})
export class DynamicInputComponent extends AppComponentBase implements OnInit {

    @Input() questionsConfig: any[];

    public type: string;
    
    private IsBoolean: boolean = false;
    private IsMultiple: boolean = false;
    private IsSingle: boolean = false;
    private IsOpen: boolean = false;

    constructor(injector: Injector, private _router: Router) {
        super(injector);
    }

    ngOnInit(): void {
        console.log(this.questionsConfig);
        /*if (this.questionsConfig.type==QuestionTypeEnum.open.toString()){
            this.type =  QuestionTypeObject.open.toString();
            this.IsOpen = true;
        }else if (this.questionsConfig.type==QuestionTypeEnum.boolean.toString()){
            this.type = QuestionTypeObject.boolean.toString();
            this.IsBoolean = true;
        }else if (this.questionsConfig.type==QuestionTypeEnum.single.toString()) {
            this.type = QuestionTypeObject.single.toString();
            this.IsSingle = true;
        }else if (this.questionsConfig.type==QuestionTypeEnum.multiple.toString()){
            this.type = QuestionTypeObject.multiple.toString();
            this.IsMultiple = true;
        }*/
     }

    /*get _IsBoolean(): boolean {
        return this.IsBoolean;
    }

    get _IsMultiple(): boolean {
        return this.IsMultiple;
    }

    get _IsSingle(): boolean {
        return this.IsSingle;
    }

    get _IsOpen(): boolean {
        return this.IsOpen;
    }*/
   
}
