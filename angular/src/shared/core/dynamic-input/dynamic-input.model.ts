
export class QuestionConfig {
    question?: string;
    tooltip?: string;
    implementationTip?: string;
    type?: string;
    responses: string[];
    dependentQuestion: Array<QuestionConfig[]>;
}
