import { Component, Injector, ViewChild, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { filter as _filter } from 'lodash-es';
import { Router } from '@angular/router';
import { debounce as _debounce } from 'lodash-es';
import { QuestionConfig } from '../dynamic-input.model';

@Component({
    selector: 'dynamic-radio-boolean',
    templateUrl: './dynamic-radio-boolean.component.html'
})
export class DynamicRadioBooleanComponent extends AppComponentBase implements OnInit {

    @Input() questionsInput: QuestionConfig;
    
    constructor(injector: Injector, private _router: Router) {
        super(injector);
    }

    ngOnInit(): void {
        console.log("Componente radio buttom",this.questionsInput);
     }
   
}
