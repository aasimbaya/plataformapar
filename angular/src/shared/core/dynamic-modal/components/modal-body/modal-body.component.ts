import { Component, Injector, Input, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalBodyDescription, ModalBodyTitle } from '../../dynamic-modal.model';

@Component({
    selector: 'modal-body',
    templateUrl: './modal-body.component.html',
    styleUrls: ['./modal-body.component.less'],
})
export class ModalBodyComponent extends AppComponentBase implements OnInit {
    @Input() modalBodyTitle?: ModalBodyTitle;
    @Input() modalBodyDescription?: ModalBodyDescription;

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {}

    ngOnDestroy(): void {}
}
