import { Component, OnInit, Output, EventEmitter, Input, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable, Subscription } from 'rxjs';
import { ModalFooterButtoms } from '../../dynamic-modal.model';

@Component({
    selector: 'modal-footer',
    templateUrl: './modal-footer.component.html',
    styleUrls: ['./modal-footer.component.less'],
})
export class ModalFooterComponent extends AppComponentBase implements OnInit {
    @Input() modalFooterButtoms?: ModalFooterButtoms;
    @Input() events: Observable<void>;
    @Output() onClose = new EventEmitter();

    modal: ModalDirective;
    subscriptions: Subscription[] = [];
    destroy$: any;

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {
        // const events = this.events.subscribe(() => this.closeModal());
        // this.subscriptions = [...this.subscriptions, events];
    }

    closeModal(): void {
        this.onClose.emit();
        this.hideModal();
    }

    hideModal(): void {
        this.modal.hide();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
        this.destroy$.next();
    }

}
