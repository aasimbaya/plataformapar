import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable, Subscription } from 'rxjs';
import { ModalHeaderImage, ModalHeaderTitle } from '../../dynamic-modal.model';


@Component({
    selector: 'modal-header',
    templateUrl: './modal-header.component.html',
    styleUrls: ['./modal-header.component.less'],
})
export class ModalHeaderComponent extends AppComponentBase implements OnInit {
    @Input() modalHeaderTitle?: ModalHeaderTitle;
    @Input() modalHeaderImage?: ModalHeaderImage;
    @Input() events: Observable<void>;
    @Output() onClose = new EventEmitter();


    modal: ModalDirective;
    subscriptions: Subscription;
    destroy$: any;

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {
        // this.subscriptions = this.events.subscribe(() => this.closeModal());
    }

    closeModal(): void {
        this.onClose.emit();
        this.hideModal();
    }

    hideModal(): void {
        this.modal.hide();
    }

    ngOnDestroy(): void {
        this.subscriptions.unsubscribe();
        this.destroy$.next();
    }

}
