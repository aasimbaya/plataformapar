import { Component, OnInit, Injector, Input, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription, Observable } from 'rxjs';
import {
    ModalBodyDescription,
    ModalBodyTitle,
    ModalFooterButtoms,
    ModalHeaderImage,
    ModalHeaderTitle,
} from './dynamic-modal.model';

@Component({
    selector: 'dynamic-modal',
    templateUrl: './dynamic-modal.component.html',
    styleUrls: ['./dynamic-modal.component.less'],

})
export class DynamicModalComponent extends AppComponentBase implements OnInit {
    private subscriptions: Subscription[] = [];

    @Input() _ModalHeaderTitle?: ModalHeaderTitle;
    @Input() _ModalHeaderImage?: ModalHeaderImage;
    @Input() _ModalBodyTitle?: ModalBodyTitle;
    @Input() _ModalBodyDescription?: ModalBodyDescription;
    @Input() _ModalFooterButtoms?: ModalFooterButtoms;
    @Input() events: Observable<void>;
    @Output() onClose = new EventEmitter();

    modal: ModalDirective;

    destroy$: any;

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {
        // const events = this.events.subscribe();
        // this.subscriptions = [...this.subscriptions, events];
    }

    closeModal(): void {
        this.onClose.emit();
        this.hideModal();
    }

    hideModal(): void {
        this.modal.hide();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
        this.destroy$.next();
    }
}
