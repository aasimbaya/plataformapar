
// Interface to configure ModalHeaderTitle
export interface ModalHeaderTitle {
    isShow?: boolean;
    text?: string;
}
// Interface to ModalHeaderImage
export interface ModalHeaderImage {
    isShow?: boolean;
    src?: string;
    alt?: string;
}
// Interface to configure ModalBodyTitle
export interface ModalBodyTitle {
    isShow?: boolean;
    text: string;
}
// Interface to configure ModalBodyDescription
export interface ModalBodyDescription {
    isShow?: boolean;
    text: string;
}
//Interface to configure ModalFooterIputs visibilization
export interface ModalFooterButtoms{
    isSuccess?: boolean;
    isFaileru?: boolean;
    isSeeMore?: boolean;
    isOptional?: boolean;
    optionalParams?: optionalParams[];
}
// Interface to configure optionalParam implent in ModalFooterButtoms
export interface optionalParams {
    text?: boolean;
    type?: string //buttom or link (a)
}

