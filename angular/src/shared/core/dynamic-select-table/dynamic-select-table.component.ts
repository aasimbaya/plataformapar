import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';
import { Table, TableCheckbox, TableHeaderCheckbox } from 'primeng/table';
import { Observable, Subject, Subscription } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { debounce as _debounce } from 'lodash-es';
import {
    ConfigColumns,
    FilterDynamicTable,
    OutputAllSelection,
    OutputSelection,
    PageStatus,
    RecordsTable,
    ReloadTable,
} from './dynamic-select-table.model';

@Component({
    selector: 'app-dynamic-select-table',
    templateUrl: './dynamic-select-table.component.html',
    styleUrls: ['./dynamic-select-table.component.css'],
    animations: [appModuleAnimation()],
})
export class DynamicSelectTableComponent extends AppComponentBase implements OnInit {
    isAddClose: boolean = true;
    @Input() configColumns: ConfigColumns[];
    @Input() filtersByColumns: string[];
    @Input() showFilter: boolean = true;
    @Input() showBtn: boolean = true;
    @Input() filterPlaceholder: string;
    @Input() data: Observable<RecordsTable>;
    @Input() reload: Observable<ReloadTable>;
    @Input() executeFunction: Function;
    @Input() seletedIds: number[];
    @Output() pagingChange: EventEmitter<PageStatus> = new EventEmitter<PageStatus>();
    @Output() itemSelectedEvent: EventEmitter<any> = new EventEmitter<any[]>();
    @Output() changeSelecction: EventEmitter<OutputSelection> = new EventEmitter<OutputSelection>();
    @Output() changeAllSelecction: EventEmitter<OutputAllSelection> = new EventEmitter<OutputAllSelection>();
    @Output() isAddItemEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() findEvent: EventEmitter<string> = new EventEmitter<string>();
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    subscriptions: Subscription[] = [];
    regexInputs = "[^']";
    filters: FilterDynamicTable = { filterText: '' };
    destroy$ = new Subject();
    hideSearchPanel: boolean;
    hidePaginator: boolean;
    selectedProducts3: any[];

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {
        const valueSub = this.data?.subscribe((value) => {
            this.primengTableHelper.totalRecordsCount = value.countTotal;
            this.primengTableHelper.records = value.records;
            this._setValueSelected(value.records);
            const hide = value.records.length >= value.countTotal;
            this.hideSearchPanel = hide;
            this.hidePaginator = hide;
        });
        const reloadSub = this.reload?.subscribe((value) => {
            if (value.reset) {
                this.getData();
            } else if (value.reload) {
                this._getData();
            }
        });
        this.subscriptions = [...this.subscriptions, valueSub, reloadSub];
    }
    private _setValueSelected(data: any[]): void {
        const newValues = [...data].filter(
            (e) =>
                (this.seletedIds || []).some((id) => id == e.id) &&
                !(this.selectedProducts3 || []).some((sel) => sel.id == e.id)
        );
        this.selectedProducts3 = [...(this.selectedProducts3 || []), ...newValues];
    }
    selectRow(value: TableCheckbox, data: any): void {
        this.changeSelecction.emit({ value: value.checked, data });
    }
    selectAll(value: TableHeaderCheckbox): void {
        this.changeAllSelecction.emit({ value: value.checked, data: [...this.primengTableHelper.records] });
    }
    onSelectedItem(): void {
        this.itemSelectedEvent.emit(this.selectedProducts3);
        this.onSelectedItemClose();
    }

    onSelectedItemClose(): void {
        this.isAddItemEvent.emit(this.isAddClose);
    }
    searchOnInput = _debounce(this.getData, AppConsts.SearchBarDelayMilliseconds);

    getData(event?: LazyLoadEvent): void {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            //revisar
            this.paginator.changePage(0);
            return;
        }
        this._getData(event);
    }
    private _getData(event?: LazyLoadEvent): void {
        if (this.executeFunction) {
            this.primengTableHelper.showLoadingIndicator();
            const sub = this.executeFunction({
                filter: this.filters.filterText,
                pageCount: this.primengTableHelper.getMaxResultCount(this.paginator, event),
                page: this.primengTableHelper.getSkipCount(this.paginator, event),
                sorting: this.primengTableHelper.getSorting(this.dataTable),
                filtersByColumns: this.filters.filtersByColumns || '',
            })
                .pipe(
                    takeUntil(this.destroy$),
                    finalize(() => this.primengTableHelper.hideLoadingIndicator())
                )
                .subscribe(
                    (res: any) => {
                        this.primengTableHelper.totalRecordsCount = res.totalCount;
                        this.primengTableHelper.records = res.items;
                    },
                    (err) => {
                        console.error('this.executeFunction on dynamic table');
                    }
                );
            this.subscriptions = [...this.subscriptions, sub];
        } else {
            this.findEvent.emit(this.filters.filterText);
        }
    }
}
