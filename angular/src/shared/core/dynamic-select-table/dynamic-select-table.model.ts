/*
 * Interface to configure table columns
 */
export interface ConfigColumns {
    field: string;
    titleLabel: string;
    type: string;
    icon?: boolean;
}
/*
 * Table records
 */
export interface RecordsTable {
    countTotal: number;
    records: any[];
}
/*
 * Information of status table
 */
export interface PageStatus {
    first?: number;
    page?: number;
    pageCount?: number;
    rows?: number;
    sorting?: string;
    filter?: string;
    filtersByColumns?: string;
}
/*
 * Information of pagination table
 */
export interface FilterDynamicTable {
    filterText: string;
    filtersByColumns?: string;
}
/*
 * Parameter for relad data from extern
 */
export interface ReloadTable {
    reload?: boolean;
    reset?: boolean;
}
/*
 * Output selection
 */
export interface OutputSelection {
    value: boolean;
    data: any;
}
/*
 * Output selection
 */
export interface OutputAllSelection {
    value: boolean;
    data: any[];
}
