import { Component, Injector, Input, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'app-dynamic-sidebar',
    templateUrl: './dynamic-sidebar.component.html',
    animations: [appModuleAnimation()],
})
export class DynamicSidebarComponent extends AppComponentBase implements OnInit {
    visibleSidebar1;
    @Input() isVisible: boolean;
    @Input() titleTooltip: string;
    @Input() descriptionTooltip: string;
    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {}
}
