import { Component, Injector, ViewChild, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import { Table } from 'primeng/table';
import { filter as _filter } from 'lodash-es';
import { finalize, takeUntil } from 'rxjs/operators';
import { Subscription, Subject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { LazyLoadEvent } from 'primeng/api';
import { debounce as _debounce } from 'lodash-es';
import { AppConsts } from '@shared/AppConsts';
import { Paginator } from 'primeng/paginator';
import { ConfigColumns, FilterDynamicTable, PageStatus, RecordsTable, ReloadTable } from './dynamic-table.model';

@Component({
    selector: 'dynamic-table',
    templateUrl: './dynamic-table.component.html',
    styleUrls: ['./dynamic-table.component.less'],

    animations: [appModuleAnimation()],
})
export class DynamicTableComponent extends AppComponentBase implements OnInit, OnDestroy {
    @Input() configColumns: ConfigColumns[];
    @Input() filtersByColumns: ConfigColumns[];
    @Input() showFilter: boolean = true;
    @Input() filterPlaceholder: string;
    @Input() filterByPlaceholder: string;
    @Input() showEdit: boolean;
    @Input() showDuplicate: boolean;
    @Input() showView: boolean;
    @Input() showDelete: boolean;
    @Input() data: Observable<RecordsTable>;
    @Input() reload: Observable<ReloadTable>;
    @Input() executeFunction: Function;
    @Input() hideLoading: boolean;
    @Input() showActions: boolean = true;
    @Output() pagingChange: EventEmitter<PageStatus> = new EventEmitter<PageStatus>();
    @Output() findEvent: EventEmitter<string> = new EventEmitter<string>();
    @Output() editEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output() duplicateEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output() deleteEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output() viewEvent: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    subscriptions: Subscription[] = [];
    // branches: any[];
    // countries: CustomerCountryLookupTableDto[] = [];
    // arAccounts: any[];
    // warehouses: any[];
    // currencies: any[];
    regexInputs = "[^']";
    filters: FilterDynamicTable = { filterText: '' };
    destroy$ = new Subject();
    hideSearchPanel: boolean;
    hidePaginator: boolean;

    constructor(injector: Injector, private _router: Router) {
        super(injector);
    }
    ngOnInit(): void {
        const valueSub = this.data?.subscribe((value) => {
            this.primengTableHelper.totalRecordsCount = value.countTotal;
            this.primengTableHelper.records = value.records;
            const hide = value.records.length >= value.countTotal;
            this.hideSearchPanel = hide;
            this.hidePaginator = hide;
        });
        const reloadSub = this.reload?.subscribe((value) => {
            if (value.reset) {
                this.getData();
            } else if (value.reload) {
                this._getData();
            }
        });
        this.subscriptions = [...this.subscriptions, valueSub, reloadSub];
    }
    /**
     * add button on click action
     */
    addOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
    }
    /**
     * add button on click action
     */
    /**
     * filter button on click action
     */
    filterOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
    }

    /**
     * close button on click action
     */
    closeOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration']);
    }

    editOnClick(data: any): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', zipCode1, 'edit']);
        this.editEvent.emit(data);
    }

    /**
     * duplicate button on click action
     */
    duplicateOnClick(data: any): void {
        this.duplicateEvent.emit(data);
    }
    viewOnClick(data: any): void {
        this.viewEvent.emit(data);
    }

    deleteOnClick(data: any): void {
        //revisar el tipo
        this.deleteEvent.emit(data);
    }

    searchOnInput = _debounce(this.getData, AppConsts.SearchBarDelayMilliseconds);

    getData(event?: LazyLoadEvent): void {
        if (this.primengTableHelper.shouldResetPaging(event) && this.primengTableHelper.totalRecordsCount > 0) {
            //revisar
            this.paginator.changePage(0);
            return;
        }
        this._getData(event);
    }
    private _getData(event?: LazyLoadEvent): void {
        if (this.executeFunction) {
            this._showLoading();
            const sub = this.executeFunction({
                filter: this.filters.filterText,
                pageCount: this.primengTableHelper.getMaxResultCount(this.paginator, event),
                page: this.primengTableHelper.getSkipCount(this.paginator, event),
                sorting: this.primengTableHelper.getSorting(this.dataTable),
                filtersByColumns: this.filters.filtersByColumns || '',
            })
                .pipe(
                    takeUntil(this.destroy$),
                    finalize(() => this._hideLoading())
                )
                .subscribe(
                    (res: any) => {
                        this.primengTableHelper.totalRecordsCount = res.totalCount;
                        this.primengTableHelper.records = res.items;
                    },
                    (err) => {
                        console.error('this.executeFunction on dynamic table');
                    }
                );
            this.subscriptions = [...this.subscriptions, sub];
        } else {
            this.findEvent.emit(this.filters.filterText);
        }
        //this.primengTableHelper.showLoadingIndicator();//revisar como incluir lalogica del loading
        // this._ZipCodeServiceProxy.getAllPaged(
        //     this.filters.filterText,
        //this.primengTableHelper.getSorting(this.dataTable),
        //     this.primengTableHelper.getMaxResultCount(this.paginator, event),
        //     this.primengTableHelper.getSkipCount(this.paginator, event)
        // ).pipe(
        //     takeUntil(this.destroy$),
        //     finalize(() => this.primengTableHelper.hideLoadingIndicator())
        // ).subscribe((x: PagedResultDtoOfZipCodeListDto) => {
        //     this.primengTableHelper.totalRecordsCount = x.totalCount;
        //     this.primengTableHelper.records = x.items;
        // });
    }
    private _showLoading(): void {
        if (this.hideLoading) {
            return;
        }
        this.primengTableHelper.showLoadingIndicator();
    }
    private _hideLoading(): void {
        if (this.hideLoading) {
            return;
        }
        this.primengTableHelper.hideLoadingIndicator();
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
        this.destroy$.next();
    }
}
