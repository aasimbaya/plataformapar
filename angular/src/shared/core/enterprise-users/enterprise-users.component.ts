import { Component, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { filter as _filter } from 'lodash-es';
import { debounce as _debounce } from 'lodash-es';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';

interface OptionsMenu {
    index: string,
    titleLabel: string,
    active: boolean
}
@Component({
    selector: 'enterprise-users',
    templateUrl: './enterprise-users.component.html',
    styleUrls: ['./enterprise-users.component.less'],
    animations: [appModuleAnimation()],
})
export class EnterpriseUsersModule extends AppComponentBase {
    breadcrumbs: BreadcrumbItem[] = [
        new BreadcrumbItem(this.l('EnterpriseUsersModule'))
    ];
    
    constructor(
        injector: Injector,
    ) {
        console.log("Modulo enterprise users");
        super(injector);
    }

    ngOnDestroy(): void {
    }
}
