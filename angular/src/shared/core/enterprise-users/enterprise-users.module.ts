import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CoreModule } from '../core.module';

@NgModule({
    declarations: [],
    imports: [AppSharedModule, AdminSharedModule, CoreModule],
})
export class EnterpriseUsersModule {}
