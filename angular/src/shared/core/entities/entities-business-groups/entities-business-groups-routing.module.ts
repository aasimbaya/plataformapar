import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EntitiesBusinessGroupsComponent } from './entities-business-groups.component';
import { PageMode } from '@shared/AppEnums';

const routes: Routes = [
    {
        path: '',
        component: EntitiesBusinessGroupsComponent,
        pathMatch: 'full',
        },
        // {
        //     path: 'add',
        //     component: MasuringToolsCategoryUpsertComponent,
        //     data: { pageMode: PageMode.Add },
        //     pathMatch: 'full'
        // },
        // {
        //     path: ':id/edit',
        //     component: MasuringToolsCategoryUpsertComponent,
        //     data: { pageMode: PageMode.Edit },
        //     pathMatch: 'full'
        // },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class EntitiesBusinessGroupsRoutingModule {}
