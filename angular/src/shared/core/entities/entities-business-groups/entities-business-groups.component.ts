import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { filter as _filter } from 'lodash-es';
import { Subscription, BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { debounce as _debounce } from 'lodash-es';
import { ConfigColumns, PageStatus, RecordsTable } from '../../dynamic-table/dynamic-table.model';
import { EntitiesBusinessGroups } from './entities-business-groups.model';
import { QuestionCategoryServiceProxy } from '@shared/service-proxies/service-proxies';
import { MeasuringToolsService } from '@shared/core/measuring-tools/measuring-tools-category/services/measuring-tools.service';


@Component({
    templateUrl: './entities-business-groups.component.html',
    animations: [appModuleAnimation()],
})
export class EntitiesBusinessGroupsComponent extends AppComponentBase implements OnInit {
    columns: ConfigColumns[] = [
        {
            field: 'name',
            titleLabel: 'BusinessGroup',
            type: 'string'
        },
        {
            field: 'count',
            titleLabel: 'Companies',
            type: 'string'
        },
        {
            field: 'createdDate',
            titleLabel: 'DateCreatedOnTable',
            type: 'date',
        }
    ];

    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: []
    })
    set records(value: RecordsTable) { this._records.next(value) }
    get records(): RecordsTable { return this._records.value }
    records$(): Observable<RecordsTable> { return this._records.asObservable() }

    subscriptions: Subscription[] = [];
    constructor(
        injector: Injector,
        private _router: Router,
        private _measuringToolsService: MeasuringToolsService,
        private _questionCategoryServiceProxy: QuestionCategoryServiceProxy,
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.loadDataTable = this.loadDataTable.bind(this);
    }
    loadDataTable(data: PageStatus) {
        //        return this._questionCategoryServiceProxy.getAllPaged(data.filter, data.sorting, data.pageCount, data.page);
        return this._measuringToolsService.getDataBusinessGroups();
        // const sub = this._measuringToolsService.getDataUserAequales().subscribe((res: RecordsTable) => {
        //     this.records = res;
        // }, err => {
        //     console.error("_measuringToolsService.getData()");
        // });
        // this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * add button on click action
     */
    addOnClick(): void {
        this._router.navigate(['app', 'measuring-tools', 'category', 'add']);
    }
    editOnClick(data: EntitiesBusinessGroups): void {
        this._router.navigate(['app', 'measuring-tools', 'category', data.id, 'edit']);
    }

    deleteOnClick(data: EntitiesBusinessGroups): void {
        abp.message.confirm(
            this.l('MsgConfirmDeleteZipCode'),
            this.l('TitleConfirmDeleteZipCode'),
            (result: boolean) => {
                if (result) {
                    this.delete(data.id);
                }
            }
        );
    }
    private delete(id: number) {
        // const sub = this._questionCategoryServiceProxy.delete(id).subscribe((res: any) => {
        //     this._getInformation();
        //     abp.notify.success(this.l('SuccessfullyDeleted'));
        // }, err => {
        //     console.error('_zipCodesServiceProxy.delete');
        // });
        // this.subscriptions = [...this.subscriptions, sub];
    }
    /**
     * filter button on click action
     */
    filterOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
    }

    /**
     * close button on click action
     */
    closeOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration']);
    }




    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe())
    }
}
