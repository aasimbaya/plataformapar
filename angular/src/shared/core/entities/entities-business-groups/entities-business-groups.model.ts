export interface EntitiesBusinessGroups {
    id: number;
    name: string;
    count: string;
    createdDate: Date,
}
