import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CoreModule } from '@shared/core/core.module';
import { EntitiesBusinessGroupsRoutingModule } from './entities-business-groups-routing.module';
import { EntitiesBusinessGroupsComponent } from './entities-business-groups.component';
import { NgxDropzoneModule } from 'ngx-dropzone';

@NgModule({
    declarations: [EntitiesBusinessGroupsComponent],
    imports: [AppSharedModule, AdminSharedModule, EntitiesBusinessGroupsRoutingModule, CoreModule, NgxDropzoneModule],
})
export class EntitiesBusinessGroupsModule {}
