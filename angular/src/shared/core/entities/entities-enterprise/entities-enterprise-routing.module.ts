import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EntitiesEnterpriseComponent } from './entities-enterprise.component';
import { PageMode } from '@shared/AppEnums';
import { EntitiesEnterpriseUpsertComponent } from './entities-enterprise-upsert/entities-enterprise-upsert.component';

const routes: Routes = [
    {
        path: '',
        component: EntitiesEnterpriseComponent,
        pathMatch: 'full',
        },
        {
            path: 'add',
            component: EntitiesEnterpriseUpsertComponent,
            data: { pageMode: PageMode.Add },
            pathMatch: 'full'
        },
        {
            path: ':id/edit',
            component: EntitiesEnterpriseUpsertComponent,
            data: { pageMode: PageMode.Edit },
            pathMatch: 'full'
        },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class EntitiesEnterpriseRoutingModule {}
