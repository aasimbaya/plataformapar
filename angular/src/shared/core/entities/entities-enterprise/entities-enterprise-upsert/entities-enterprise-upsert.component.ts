import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { isEmpty as _isEmpty } from 'lodash-es';
import { AppComponentBase } from '@shared/common/app-component-base';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';

import { ActivatedRoute, Router } from '@angular/router';
import { PageMode } from '@shared/AppEnums';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
    UpsertEnterpriseDto,
    UpsertPersonalDto,
    UpsertQuestionCategoryDto,
    UpsertQuestionCategoryTypeDto,
    UpsertUserEnterpriseWrapperDto,
    UserEnterpriseServiceProxy,
    UpsertUsersByEnterpriseDto,
} from '@shared/service-proxies/service-proxies';
import { OptionsMenu } from '@shared/core/premium-dashboard/premium-dashboard.model';
import { appModuleAnimation } from '@shared/animations/routerTransition';

/**
 * Main component for zip code edit
 */
@Component({
    selector: 'entities-enterprise-upsert',
    styleUrls: ['./entities-enterprise-upsert.component.less'],
    templateUrl: './entities-enterprise-upsert.component.html',
    animations: [appModuleAnimation()],
})
export class EntitiesEnterpriseUpsertComponent extends AppComponentBase implements OnInit, OnDestroy {
    breadcrumbs: BreadcrumbItem[] = [];
    loading: boolean = false;
    pageMode: PageMode;
    subscriptions: Subscription[] = [];
    form: FormGroup;
    id = 0;
    isNew = true;
    optionsTypeCategory: UpsertQuestionCategoryTypeDto[] = [];

    filter: string = '';
    optionsMenu: OptionsMenu[] = [
        {
            index: 'companyInformation',
            titleLabel: 'entities_enterprise_editor_tab_enterpriseinformation',
            active: true,
        },
        {
            index: 'plans',
            titleLabel: 'entities_enterprise_editor_tab_plans',
            active: false,
        },
        {
            index: 'users',
            titleLabel: 'entities_enterprise_editor_tab_users',
            active: false,
        },
    ];
    optionSelectedMenu = 'companyInformation';
    onlyAlphaNumeric = '[A-Za-z0-9 ]+$';
    onlyEmail = '[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';
    constructor(
        injector: Injector,
        private _userEnterpriseServiceProxy: UserEnterpriseServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _router: Router
    ) {
        super(injector);
    }

    /**
     * OnInit
     */
    ngOnInit(): void {
        this.pageMode = this._activatedRoute.snapshot.data.pageMode;
        const id = this._activatedRoute.snapshot.params.id;
        this._init(id);
    }

    private async _init(id) {
        if (id) {
            this.breadcrumbs = [new BreadcrumbItem(this.l('entities_enterprise_editor_title_update'))];
            this.isNew = false;
            this._getInformation(id);
        } else {
            this.breadcrumbs = [new BreadcrumbItem(this.l('entities_enterprise_editor_title_create'))];
            this.isNew = true;
            this._formInitialize(new UpsertUserEnterpriseWrapperDto());
        }
    }
    private async _getInformation(id?: number) {
        const sub = this._userEnterpriseServiceProxy.get(id).subscribe(
            async (res: UpsertUserEnterpriseWrapperDto) => {
                this._formInitialize(res);
            },
            (err) => {
                console.error('_questionCategoryServiceProxy.get');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }
    selectMenu(item: OptionsMenu): void {
        this.optionSelectedMenu = item.index;
    }
    private async _formInitialize(data: UpsertUserEnterpriseWrapperDto) {
        const { user, rrhh, ceo } = data;
        this.id = user?.id || 0;
        this.form = new FormGroup({
            information: new FormGroup({
                id: new FormControl(user?.id || 0),
                name: new FormControl(user?.name || '', [
                    Validators.required,
                    Validators.pattern(this.onlyAlphaNumeric),
                ]),
                idNumber: new FormControl(user?.idNumber || '', [
                    Validators.required,
                    Validators.pattern(this.onlyAlphaNumeric),
                ]),
                countryId: new FormControl(user?.countryId || '', [Validators.required]),
                regionId: new FormControl(user?.regionId || ''),
                cityId: new FormControl(user?.cityId || ''),
                sectorId: new FormControl(user?.sectorId || '', [Validators.required]),
                lineBusinessId: new FormControl(user?.lineBusinessId || '', [Validators.required]),
                userId: new FormControl(user?.userId || '', [Validators.required]),
                employeesNumber: new FormControl(user?.employeesNumber || '', Validators.required),
                isInternational: new FormControl(user?.isInternational || false),
                hasInclusion: new FormControl(user?.hasInclusion || false),
                enterpriseTypeId: new FormControl(user?.enterpriseTypeId || '', [Validators.required]),

                logoCompany: new FormControl(''),

                nameHHRR: new FormControl(rrhh?.name || '', [Validators.pattern(this.onlyAlphaNumeric)]),
                emailHHRR: new FormControl(rrhh?.email || '', [Validators.pattern(this.onlyEmail)]),
                positionIdHHRR: new FormControl(rrhh?.positionId || ''),
                cellPhoneHHRR: new FormControl(rrhh?.cellPhone || ''),

                nameCeo: new FormControl(ceo?.name || '', [Validators.pattern(this.onlyAlphaNumeric)]),
                emailCeo: new FormControl(ceo?.email || '', [Validators.pattern(this.onlyEmail)]),
                positionIdCeo: new FormControl(ceo?.positionId || ''),
                cellPhoneCeo: new FormControl(ceo?.cellPhone || ''),
                // last: new FormControl('Drew', Validators.required)
            }),
            plans: new FormGroup({
                listPlansId: new FormControl('1'),
                // last: new FormControl('Drew', Validators.required)
            }),
            users: new FormGroup({
                listUsersId: new FormControl('', [Validators.required]),
                // last: new FormControl('Drew', Validators.required)
            }),
        });
    }

    closeOnClick(): void {
        if (!this.form.pristine) {
            abp.message.confirm(
                this.l('entities_enterprise_editor_confirm_close_mgs'),
                this.l('entities_enterprise_editor_confirm_close_title'),
                (result: boolean) => {
                    if (result) {
                        this._redirectToMeasuringToolsCategoryListPage();
                    }
                }
            );
        } else {
            this._redirectToMeasuringToolsCategoryListPage();
        }
    }

    saveOnClick(): void {
        const { valid } = this.form;
        if (valid) {
            // this.sanitizeData();
            if (this.isNew) {
                this._add();
            } else {
                this._edit();
            }
        } else {
            this.form.markAllAsTouched();
        }
    }
    onFormSubmit($event, form) {
        $event.preventDefault;
    }
    private _edit() {
        const sub = this._userEnterpriseServiceProxy
            .update(this.id, this._getUpsertUserEnterpriseWrapperDto())
            .subscribe(
                (res: any) => {
                    abp.notify.success(this.l('entities_enterprise_editor_success_updated'));
                    this._redirectToMeasuringToolsCategoryListPage();
                },
                (err) => {
                    console.error('_userEnterpriseServiceProxy.update');
                }
            );
        this.subscriptions = [...this.subscriptions, sub];
    }

    private _add() {
        const sub = this._userEnterpriseServiceProxy.create(this._getUpsertUserEnterpriseWrapperDto()).subscribe(
            (res: any) => {
                abp.notify.success(this.l('entities_enterprise_editor_success_created'));
                this._redirectToMeasuringToolsCategoryListPage();
            },
            (err) => {
                console.error('_userEnterpriseServiceProxy.create');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }
    private _getUpsertUserEnterpriseWrapperDto(): UpsertUserEnterpriseWrapperDto {
        const { value } = this.form;
        const { information, plans, users } = value;
        const {
            id,
            name,
            idNumber,
            countryId,
            regionId,
            cityId,
            sectorId,
            lineBusinessId,
            employeesNumber,
            isInternational,
            hasInclusion,
            enterpriseTypeId,
            logoCompany,
            nameHHRR,
            emailHHRR,
            positionIdHHRR,
            cellPhoneHHRR,
            nameCeo,
            emailCeo,
            positionIdCeo,
            cellPhoneCeo,
        } = information;
        const data = new UpsertUserEnterpriseWrapperDto();

        const user = new UpsertEnterpriseDto();
        user.id = id;
        user.name = name;
        user.idNumber = idNumber;
        user.countryId = countryId;
        user.regionId = regionId;
        user.cityId = cityId;
        user.sectorId = sectorId;
        user.lineBusinessId = lineBusinessId;
        user.employeesNumber = employeesNumber;
        user.isInternational = isInternational;
        user.hasInclusion = hasInclusion;
        user.enterpriseTypeId = enterpriseTypeId;
        user.listPlansId = plans.listPlansId;
        user.userId = users.userId;
        user.listUsersId = users.listUsersId.map((e) => e.id).join(',');
        data.user = user;

        const rrhh = new UpsertPersonalDto();
        rrhh.name = nameHHRR;
        rrhh.email = emailHHRR;
        rrhh.positionId = positionIdHHRR;
        rrhh.cellPhone = cellPhoneHHRR?.e164Number;
        data.rrhh = rrhh;

        const ceo = new UpsertPersonalDto();
        ceo.name = nameCeo;
        ceo.email = emailCeo;
        ceo.positionId = positionIdCeo;
        ceo.cellPhone = cellPhoneCeo?.e164Number;
        data.ceo = ceo;

        return data;
    }

    // private sanitizeData() {
    //     const {
    //         zipCode1,
    //         city,
    //         state,
    //         county,
    //         branch,
    //         taxCode,
    //         group1,
    //         group2,
    //         group3,
    //         group4,
    //         group5,
    //         group6,
    //         latitude,
    //         longitude,
    //         areaCode,
    //         timeZone,
    //         elevation,
    //     } = this.upsertZipCodeDto;
    //     this.upsertZipCodeDto.zipCode1 = zipCode1.toUpperCase().trim();
    //     this.upsertZipCodeDto.city = this.getTrim(city);
    //     this.upsertZipCodeDto.state = this.getTrim(state);
    //     this.upsertZipCodeDto.county = this.getTrim(county);
    //     this.upsertZipCodeDto.taxCode = this.getTrim(taxCode);
    //     this.upsertZipCodeDto.group1 = this.getTrim(group1);
    //     this.upsertZipCodeDto.group2 = this.getTrim(group2);
    //     this.upsertZipCodeDto.group3 = this.getTrim(group3);
    //     this.upsertZipCodeDto.group4 = this.getTrim(group4);
    //     this.upsertZipCodeDto.group5 = this.getTrim(group5);
    //     this.upsertZipCodeDto.group6 = this.getTrim(group6);
    //     this.upsertZipCodeDto.latitude = this.getCoordinate(latitude?.toString());
    //     this.upsertZipCodeDto.longitude = this.getCoordinate(longitude?.toString());
    //     this.upsertZipCodeDto.areaCode = this.getTrim(areaCode);
    //     this.upsertZipCodeDto.timeZone = this.getCoordinate(timeZone?.toString());
    //     this.upsertZipCodeDto.elevation = this.getCoordinate(elevation?.toString());
    // }

    private _redirectToMeasuringToolsCategoryListPage(): void {
        this._router.navigate(['app', 'entities', 'enterprise']);
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
