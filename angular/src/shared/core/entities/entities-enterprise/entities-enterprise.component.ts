import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { filter as _filter } from 'lodash-es';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { debounce as _debounce } from 'lodash-es';
import { ConfigColumns, PageStatus, ReloadTable } from '../../dynamic-table/dynamic-table.model';
import { EnterpriseListItemDto, UserEnterpriseServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './entities-enterprise.component.html',
    animations: [appModuleAnimation()],
})
export class EntitiesEnterpriseComponent extends AppComponentBase implements OnInit {
    columns: ConfigColumns[] = [
        {
            field: 'name',
            titleLabel: 'entities_enterprise_grid_name',
            type: 'string',
        },
        {
            field: 'enterpriseGroupName',
            titleLabel: 'entities_enterprise_grid_group',
            type: 'string',
        },
        {
            field: 'creationTime',
            titleLabel: 'entities_enterprise_grid_creationtime',
            type: 'date',
        },
    ];

    subscriptions: Subscription[] = [];
    private _reload: BehaviorSubject<ReloadTable> = new BehaviorSubject({});
    set reload(value: ReloadTable) {
        this._reload.next(value);
    }
    get reload(): ReloadTable {
        return this._reload.value;
    }
    reload$(): Observable<ReloadTable> {
        return this._reload.asObservable();
    }
    constructor(
        injector: Injector,
        private _router: Router,
        private _userEnterpriseServiceProxy: UserEnterpriseServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.loadDataTable = this.loadDataTable.bind(this);
    }
    loadDataTable(data: PageStatus) {
        return this._userEnterpriseServiceProxy.getAllPaged(data.filter, data.sorting, data.pageCount, data.page);
    }

    joinOnClick(): void {}
    addOnClick(): void {
        this._router.navigate(['app', 'entities', 'enterprise', 'add']);
    }
    editOnClick(data: EnterpriseListItemDto): void {
        this._router.navigate(['app', 'entities', 'enterprise', data.id, 'edit']);
    }

    deleteOnClick(data: EnterpriseListItemDto): void {
        abp.message.confirm(
            this.l('entities_enterprise_confirm_delete_mgs'),
            this.l('entities_enterprise_confirm_delete_title'),
            (result: boolean) => {
                if (result) {
                    this.delete(data.id);
                }
            }
        );
    }
    private delete(id: number) {
        const sub = this._userEnterpriseServiceProxy.delete(id).subscribe(
            (res: any) => {
                this._getInformation({ reset: true });
                abp.notify.success(this.l('entities_enterprise_success_delete'));
            },
            (err) => {
                console.error('_userEnterpriseServiceProxy.delete');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }
    private _getInformation(data: ReloadTable): void {
        this.reload = data;
    }
    /**
     * filter button on click action
     */
    filterOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
    }

    /**
     * close button on click action
     */
    closeOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration']);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
