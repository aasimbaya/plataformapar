export interface EntityUserPlanList {
    id: number;
    name: string;
    period: Date;
    amount: string;
    discount: string;
    active: boolean;
}

