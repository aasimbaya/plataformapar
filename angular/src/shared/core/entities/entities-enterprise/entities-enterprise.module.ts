import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CoreModule } from '@shared/core/core.module';
import { EntitiesEnterpriseRoutingModule } from './entities-enterprise-routing.module';
import { EntitiesEnterpriseComponent } from './entities-enterprise.component';
import { EntitiesEnterpriseUpsertComponent } from './entities-enterprise-upsert/entities-enterprise-upsert.component';
import { TabEnterpriseUsersComponent } from './tab-enterprise-users/tab-enterprise-users.component';
import { TabEnterprisePlansComponent } from './tab-enterprise-plans/tab-enterprise-plans.component';
import { TabEnterpriseInformationComponent } from './tab-enterprise-information/tab-enterprise-information.component';
import { InputsModule } from '@shared/core/inputs/inputs.module';
import { ModalSelectUsersComponent } from './modal-select-users/modal-select-users.component';
import { UsersModule } from '@app/admin/users/users.module';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';

@NgModule({
    declarations: [
        EntitiesEnterpriseComponent,
        EntitiesEnterpriseUpsertComponent,
        TabEnterpriseUsersComponent,
        TabEnterprisePlansComponent,
        TabEnterpriseInformationComponent,
        ModalSelectUsersComponent,
    ],
    imports: [
        AppSharedModule,
        AdminSharedModule,
        EntitiesEnterpriseRoutingModule,
        CoreModule,
        InputsModule,
        UsersModule,
        NgxIntlTelInputModule
    ],
})
export class EntitiesEnterpriseModule {}
