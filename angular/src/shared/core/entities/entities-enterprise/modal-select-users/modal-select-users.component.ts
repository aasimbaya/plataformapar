import { Component, Input, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    ConfigColumns,
    OutputAllSelection,
    OutputSelection,
    PageStatus,
    RecordsTable,
} from '@shared/core/dynamic-select-table/dynamic-select-table.model';
import {
    PagedResultDtoOfUpsertUsersByEnterpriseDto,
    UserEnterpriseServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { debounce as _debounce } from 'lodash-es';
import { AppConsts } from '@shared/AppConsts';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'modal-select-users',
    templateUrl: './modal-select-users.component.html',
    animations: [appModuleAnimation()],
})
export class ModalSelectUsersComponent extends AppComponentBase implements OnInit {
    @Input() selectedProducts: number[];
    columns: ConfigColumns[] = [
        {
            field: 'name',
            titleLabel: 'name',
            type: 'string',
        },
        {
            field: 'emailAddress',
            titleLabel: 'email',
            type: 'string',
        },
    ];
    @ViewChild('upsertElementModal', { static: true }) modal: ModalDirective;
    @Output() returnItemSelectedEvent: EventEmitter<OutputSelection> = new EventEmitter<OutputSelection>();
    @Output() returnAllItemsSelectedEvent: EventEmitter<OutputAllSelection> = new EventEmitter<OutputAllSelection>();
    @Output() createUser: EventEmitter<boolean> = new EventEmitter<boolean>();
    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });
    set records(value: RecordsTable) {
        this._records.next(value);
    }
    get records(): RecordsTable {
        return this._records.value;
    }
    records$(): Observable<RecordsTable> {
        return this._records.asObservable();
    }
    subscriptions: Subscription[] = [];
    filter = '';
    constructor(injector: Injector, private _userEnterpriseServiceProxy: UserEnterpriseServiceProxy) {
        super(injector);
    }

    ngOnInit(): void {
        this.getData();
    }
    addOnClick(): void {
        this.createUser.emit(true);
    }

    searchOnInput = _debounce(this.getData, AppConsts.SearchBarDelayMilliseconds);
    getData(): void {
        this.getDestinaryList({ first: 0, page: 0, pageCount: 10, rows: 0 });
    }

    private getDestinaryList(data: PageStatus): void {
        const sub = this._userEnterpriseServiceProxy
            .getUsers(this.filter, data.sorting, data.pageCount, data.page)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe((res: PagedResultDtoOfUpsertUsersByEnterpriseDto) => {
                this.records = { countTotal: res.totalCount, records: res.items };
            });

        this.subscriptions = [...this.subscriptions, sub];
    }

    onSelectedItemClose(data: boolean): void {
        if (data) {
            this.modal.hide();
        }
    }

    onSelectedItem(data: OutputSelection): void {
        this.returnItemSelectedEvent.emit(data);
    }
    onSelectedAllItems(data: OutputAllSelection): void {
        this.returnAllItemsSelectedEvent.emit(data);
    }

    onShown(): void {}

    show(id?: number): void {
        const self = this;
        self.modal.show();
    }

    close(): void {
        this.modal.hide();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }
}
