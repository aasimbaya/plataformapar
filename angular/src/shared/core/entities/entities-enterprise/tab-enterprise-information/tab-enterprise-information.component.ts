import { Component, Injector, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { InputDropdown, ReloadCombo } from '@shared/core/inputs/input-dropdown/input-dropdown.model';
import {
    DropdownItemDto,
    GetAllDropdownServiceProxy,
    UpsertUsersByEnterpriseDto,
} from '@shared/service-proxies/service-proxies';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';

@Component({
    selector: 'tab-enterprise-information',
    templateUrl: './tab-enterprise-information.component.html',
})
export class TabEnterpriseInformationComponent extends AppComponentBase implements OnInit, OnDestroy {
    @Input() form: FormGroup;
    @Input() listUsersId: Observable<UpsertUsersByEnterpriseDto[]>;
    optionsSector: DropdownItemDto[] = [];
    optionsBusiness: DropdownItemDto[] = [];
    optionsUsers: DropdownItemDto[] = [];
    optionsPositions: DropdownItemDto[] = [];
    collaborators: DropdownItemDto[] = [];
    optionsEnterpriseTypeId = [];
    subscriptions: Subscription[] = [];
    onlyCountries = AppConsts.onlyCountries;
    private _reloadRegion: BehaviorSubject<ReloadCombo> = new BehaviorSubject({});
    set reloadRegion(value: ReloadCombo) {
        this._reloadRegion.next(value);
    }
    get reloadRegion(): ReloadCombo {
        return this._reloadRegion.value;
    }
    reloadRegion$(): Observable<ReloadCombo> {
        return this._reloadRegion.asObservable();
    }

    private _reloadCity: BehaviorSubject<ReloadCombo> = new BehaviorSubject({});
    set reloadCity(value: ReloadCombo) {
        this._reloadCity.next(value);
    }
    get reloadCity(): ReloadCombo {
        return this._reloadCity.value;
    }
    reloadCity$(): Observable<ReloadCombo> {
        return this._reloadCity.asObservable();
    }
    constructor(injector: Injector, private _getAllDropdownServiceProxy: GetAllDropdownServiceProxy) {
        super(injector);
    }
    ngOnInit(): void {
        this.loadDataCountry = this.loadDataCountry.bind(this);
        this.loadDataRegion = this.loadDataRegion.bind(this);
        this.loadDataCity = this.loadDataCity.bind(this);
        this.loadCombos();
        this.optionsEnterpriseTypeId = AppConsts.OptionsEnterpriseTypeId.map((e) => {
            return { ...e, name: this.l(e.name) };
        });
    }
    loadDataCountry(data: InputDropdown) {
        return this._getAllDropdownServiceProxy.getAllCountries(data.filter, data.id);
    }
    loadDataRegion(data: InputDropdown) {
        return this._getAllDropdownServiceProxy.getAllRegions(
            data.filter,
            data.id,
            this.form.controls.countryId.value || 0
        );
    }
    loadDataCity(data: InputDropdown) {
        return this._getAllDropdownServiceProxy.getAllCities(
            data.filter,
            data.id,
            this.form.controls.regionId.value || 0
        );
    }
    loadRegion() {
        this.form.controls.regionId.reset();
        this.reloadRegion = { reset: true };
        this.loadCity();
    }
    loadCity() {
        this.form.controls.cityId.reset();
        this.reloadCity = { reset: true };
    }
    private loadCombos(): void {
        const subCollaborators = this._getAllDropdownServiceProxy.getAllCollaborators().subscribe(
            (res: DropdownItemDto[]) => {
                this.collaborators = res;
            },
            (err) => {
                console.error('_getAllDropdownServiceProxy.getAllCollaborators');
            }
        );
        const subSector = this._getAllDropdownServiceProxy.getAllSectors().subscribe(
            (res: DropdownItemDto[]) => {
                this.optionsSector = res;
            },
            (err) => {
                console.error('_getAllDropdownServiceProxy.getAllSectors');
            }
        );
        const subLineB = this._getAllDropdownServiceProxy.getAllLineBusiness().subscribe(
            (res: DropdownItemDto[]) => {
                this.optionsBusiness = res;
            },
            (err) => {
                console.error('_getAllDropdownServiceProxy.getAllLineBusiness');
            }
        );
        const subPosition = this._getAllDropdownServiceProxy.getAllPositions().subscribe(
            (res: DropdownItemDto[]) => {
                this.optionsPositions = res;
            },
            (err) => {
                console.error('_getAllDropdownServiceProxy.getAllLineBusiness');
            }
        );
        const subListUsersId = this.listUsersId?.subscribe(
            async (res: UpsertUsersByEnterpriseDto[]) => {
                this.optionsUsers = res.map(
                    (e) => new DropdownItemDto({ id: e.id, name: e.emailAddress, code: '', type: '' })
                );
            },
            (err) => {
                console.error('_questionCategoryServiceProxy.get');
            }
        );
        this.subscriptions = [
            ...this.subscriptions,
            subCollaborators,
            subSector,
            subLineB,
            subPosition,
            subListUsersId,
        ];
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
