import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus } from '@shared/core/dynamic-table/dynamic-table.model';
import { MeasuringToolsService } from '@shared/core/measuring-tools/measuring-tools-category/services/measuring-tools.service';
import { EntityUserPlanList } from '../entities-enterprise.model';

@Component({
    selector: 'tab-enterprise-plans',
    templateUrl: './tab-enterprise-plans.component.html',
})
export class TabEnterprisePlansComponent extends AppComponentBase implements OnInit {
    @Input() form: FormGroup;

    columns: ConfigColumns[] = [
        {
            field: 'name',
            titleLabel: 'Plan',
            type: 'string',
        },
        {
            field: 'period',
            titleLabel: 'Period',
            type: 'date',
        },
        {
            field: 'amount',
            titleLabel: 'Amount',
            type: 'string',
        },
        {
            field: 'discount',
            titleLabel: 'Discount',
            type: 'string',
        },
        {
            field: 'active',
            titleLabel: 'Active',
            type: 'boolean',
        },
    ];
    constructor(injector: Injector, private _measuringToolsService: MeasuringToolsService) {
        super(injector);
    }
    ngOnInit(): void {
        this.loadDataTable = this.loadDataTable.bind(this);
    }
    loadDataTable(data: PageStatus) {
        //        return this._questionCategoryServiceProxy.getAllPaged(data.filter, data.sorting, data.pageCount, data.page);
        return this._measuringToolsService.getDataCompaniesPlans();
        // const sub = this._measuringToolsService.getDataUserAequales().subscribe((res: RecordsTable) => {
        //     this.records = res;
        // }, err => {
        //     console.error("_measuringToolsService.getData()");
        // });
        // this.subscriptions = [...this.subscriptions, sub];
    }
    addOnClick(): void {
        // this._router.navigate(['app', 'entities-users', 'companies', 'add']);
    }
    editOnClick(data: EntityUserPlanList): void {
        // this._router.navigate(['app', 'entities-users', 'companies', data.id, 'edit']);
    }

    deleteOnClick(data: EntityUserPlanList): void {
        //revisar el tipo
        // abp.message.confirm(
        //     this.l('MsgConfirmDeleteZipCode'),
        //     this.l('TitleConfirmDeleteZipCode'),
        //     (result: boolean) => {
        //         if (result) {
        //             this.delete(data.id);
        //         }
        //     }
        // );
    }
}
