import { Component, Injector, Input, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CreateOrEditUserModalComponent } from '@app/admin/users/create-or-edit-user-modal.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { OutputAllSelection, OutputSelection } from '@shared/core/dynamic-select-table/dynamic-select-table.model';
import { ConfigColumns, PageStatus, ReloadTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { MeasuringToolsService } from '@shared/core/measuring-tools/measuring-tools-category/services/measuring-tools.service';
import {
    UpsertUsersByEnterpriseDto,
    UserEditDto,
    UserEnterpriseServiceProxy,
    UserListDto,
} from '@shared/service-proxies/service-proxies';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModalSelectUsersComponent } from '../modal-select-users/modal-select-users.component';

@Component({
    selector: 'tab-enterprise-users',
    templateUrl: './tab-enterprise-users.component.html',
})
export class TabEnterpriseUsersComponent extends AppComponentBase {
    @Input() form: FormGroup;
    @Input() id: number;
    @ViewChild('ModalSelectUsersComponent', { static: true }) modalSelectUsersComponent: ModalSelectUsersComponent;
    @ViewChild('createOrEditUserModal', { static: true }) createOrEditUserModal: CreateOrEditUserModalComponent;

    columns: ConfigColumns[] = [
        {
            field: 'name',
            titleLabel: 'entities_enterprise_editor_users_grid_name',
            type: 'string',
        },
        {
            field: 'surname',
            titleLabel: 'entities_enterprise_editor_users_grid_lastname',
            type: 'string',
        },
        {
            field: 'emailAddress',
            titleLabel: 'entities_enterprise_editor_users_grid_emailaddress',
            type: 'string',
        },
        {
            field: 'creationTime',
            titleLabel: 'entities_enterprise_editor_users_grid_creationtime',
            type: 'date',
        },
    ];
    subscriptions: Subscription[] = [];
    selectedIds: number[] = [];
    private _reload: BehaviorSubject<ReloadTable> = new BehaviorSubject({});
    set reload(value: ReloadTable) {
        this._reload.next(value);
    }
    get reload(): ReloadTable {
        return this._reload.value;
    }
    reload$(): Observable<ReloadTable> {
        return this._reload.asObservable();
    }

    private _data: BehaviorSubject<UpsertUsersByEnterpriseDto[]> = new BehaviorSubject([]);
    set data(value: UpsertUsersByEnterpriseDto[]) {
        this._data.next(value);
    }
    get data(): UpsertUsersByEnterpriseDto[] {
        return this._data.value;
    }
    data$(): Observable<UpsertUsersByEnterpriseDto[]> {
        return this._data.asObservable();
    }
    constructor(injector: Injector, private _userEnterpriseServiceProxy: UserEnterpriseServiceProxy) {
        super(injector);
    }
    ngOnInit(): void {
        this.loadDataTable = this.loadDataTable.bind(this);
        this.loadData();
        const sub = this.data$().subscribe((res: UpsertUsersByEnterpriseDto[]) => {
            this.selectedIds = res.map((e) => e.id);
            this.form.controls.listUsersId.setValue(res);
        });
        this.subscriptions = [...this.subscriptions, sub];
    }
    loadData(data?: UserEditDto) {
        const sub = this._userEnterpriseServiceProxy.getUsersByEnterpriseId(this.id).subscribe(
            (res: UpsertUsersByEnterpriseDto[]) => {
                this.data = [...res, ...this.data].reduce((ant, value) => {
                    if (!ant.some((e) => e.id != value.id)) {
                        ant.push(value);
                    }
                    return ant;
                }, this.getDataNew(data));
                this._getInformation();
            },
            (err) => {
                console.error('_userEnterpriseServiceProxy.getUsersByEnterpriseId');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }
    private getDataNew(data?: UserEditDto): UpsertUsersByEnterpriseDto[] {
        if (!data) {
            return [];
        }
        let result = new UpsertUsersByEnterpriseDto();
        result.init(data);
        return [result];
    }
    loadDataTable(data: PageStatus) {
        return this.data$().pipe(
            map((e: UpsertUsersByEnterpriseDto[]) => {
                const items = [...e].sort((a, b) => this._sortTable(a, b, data));
                const start = Number(data.pageCount) * Number(data.page);
                const end = Number(data.pageCount) + start;
                return {
                    totalCount: e.length,
                    items: items.filter((e, i) => i >= start && i < end),
                };
            })
        );
    }
    private _sortTable(a: UpsertUsersByEnterpriseDto, b: UpsertUsersByEnterpriseDto, data: PageStatus): number {
        const sort = data.sorting.split(' ')[0];
        const asc = data.sorting.indexOf('ASC') > -1;
        return asc ? ('' + a[sort]).localeCompare(b[sort]) : ('' + b[sort]).localeCompare(a[sort]);
    }
    private _getInformation(): void {
        this.reload = { reset: true };
    }
    addOnClick(): void {
        this.modalSelectUsersComponent.show();
    }
    onSelectedItem(data: OutputSelection): void {
        if (data.value) {
            const { id, name, surname, emailAddress, creationTime } = data.data;
            this.data = [
                ...this.data,
                new UpsertUsersByEnterpriseDto({ id, name, surname, emailAddress, creationTime }),
            ];
        } else {
            this.data = [...this.data].filter((e) => e.id !== data.data.id);
        }
    }
    onSelectedAllItems(data: OutputAllSelection): void {
        if (data.value) {
            const newData = [...data.data]
                .map((e) => {
                    const { id, name, surname, emailAddress, creationTime } = e;
                    return new UpsertUsersByEnterpriseDto({ id, name, surname, emailAddress, creationTime });
                })
                .filter((value) => !this.data.some((item) => item.id == value.id));
            this.data = [...this.data, ...newData];
        } else {
            const ids: number[] = data.data.map((e) => e.id);
            this.data = [...this.data].filter((e) => !ids.some((id) => id === e.id));
        }
    }
    editOnClick(data: UpsertUsersByEnterpriseDto): void {
        this.createOrEditUserModal.show(data.id);
    }

    deleteOnClick(data: UpsertUsersByEnterpriseDto): void {
        this.data = [...this.data].filter((e) => e.id !== data.id);
        this._getInformation();
    }
    createUser(): void {
        this.createOrEditUserModal.show();
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
