import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'business-groups',
        loadChildren: () =>
            import('./entities-business-groups/entities-business-groups.module').then(
                (m) => m.EntitiesBusinessGroupsModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
    {
        path: 'enterprise',
        loadChildren: () =>
            import('./entities-enterprise/entities-enterprise.module').then(
                (m) => m.EntitiesEnterpriseModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class EntitiesRoutingModule {}
