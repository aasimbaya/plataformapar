import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { EntitiesRoutingModule } from './entities-routing.module';
@NgModule({
    declarations: [
  ],
    imports: [AppSharedModule, AdminSharedModule, EntitiesRoutingModule],
})
export class EntitiesModule {}
