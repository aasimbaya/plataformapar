import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TenantSettingsServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileUploader } from 'ng2-file-upload';
import { UploadService } from '../services/upload.service';

@Component({
  selector: 'app-form-template',
  templateUrl: './form-template.component.html',
  styleUrls: ['./form-template.component.css'],
  animations: [appModuleAnimation()],
})
export class FormTemplateComponent extends AppComponentBase implements OnInit {

    @ViewChild('uploaInputLabel') uploadInputLabel: ElementRef;

    form: FormGroup;
    formDates: FormGroup;
    uploadFile: FileUploader;
    files: File[] = [];
    allIputTypes: string[] = [
        "SINGLE_LINE_STRING",
        "COMBOBOX",
        "CHECKBOX",
        "MULTISELECTCOMBOBOX"
    ]
    formTemplate = new FormGroup({
        required: new FormControl('', Validators.compose([Validators.required])),
        email: new FormControl('', Validators.compose([Validators.email])),
        maxLength: new FormControl('', Validators.compose([Validators.maxLength(10)])),
        minLength: new FormControl('', Validators.compose([Validators.minLength(5)])),
        alphabetOnly: new FormControl('', Validators.compose([Validators.pattern('[a-zA-Z]*')])),
        numberOnly: new FormControl('', Validators.compose([Validators.pattern('[0-9]*')])),
        alphaNumericOnly: new FormControl('', Validators.compose([Validators.pattern('[-_a-zA-Z0-9]*')])),
        floatNumber: new FormControl('', Validators.compose([Validators.pattern(/^\d+\,\d{2}$/)])),
        blankSpaces: new FormControl('', Validators.compose([Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/)])),
        selectRequired: new FormControl('', Validators.compose([Validators.required])),
        radioRequired: new FormControl('', Validators.compose([Validators.required])),
        overToday: new FormControl('',  (c: AbstractControl) => (new Date(c.value).getTime() < Date.now() ? { invalid: true } : null)),
        chooseDate: new FormControl('', Validators.compose([Validators.required])),
        over18yo: new FormControl('',  (c: AbstractControl) => ( Math.floor((new Date().getTime() - new Date(c.value).getTime()) / 3.15576e+10) < 18)
        ? { invalid: true } : null),
        currentYear: new FormControl('',  (c: AbstractControl) => ( new Date(c.value).getFullYear() != new Date().getFullYear() ? { invalid: true } : null)),
        textAreaRequired: new FormControl('', Validators.compose([Validators.required])),
        password: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/\d/),
                                                        Validators.pattern(/[A-Z]/), Validators.pattern(/[a-z]/),
                                                        Validators.pattern(/[^\p{L}\d\s@#]/u), Validators.minLength(8)])),
    })

  constructor(injector: Injector, private formBuilder: FormBuilder,
                private _tenantSettingsService: TenantSettingsServiceProxy,
                private uploadService: UploadService
                ) {
    super(injector);
   }

  ngOnInit(): void {
    this.moreThanOneChecked();
    this.betweenDates();
  }


  //Este metodo va en el onClick del boton de upload
  upload() {
/*     const file = this.selectedFiles.item(0); //Dependiendo del metodo para extraer el file
                                                //se asignara a la constante
    this.uploadService.uploadFile(file); */
    }
  onSelect(event) {
      console.log(event);
      this.files.push(...event.addedFiles);
  }

  onRemove(event) {
      console.log(event);
      this.files.splice(this.files.indexOf(event), 1);
  }
  onUploadInputChange(files: FileList) {
    this.uploadInputLabel.nativeElement.innerText = Array.from(files)
        .map((f) => f.name)
        .join(', ');
}

    uploadCustomCss(): void {
        this.uploadFile.uploadAll();
    }

//Este metodo es solo una referencia muy basica del boton de limpia de subir imagen
clearCustomCss(): void {
    this._tenantSettingsService.clearCustomCss().subscribe(() => {
        this.appSession.tenant.customCssId = null;

        let oldTenantCustomCss = document.getElementById('TenantCustomCss');
        if (oldTenantCustomCss) {
            oldTenantCustomCss.remove();
        }

        this.notify.info(this.l('ClearedSuccessfully'));
    });
}

  private betweenDates(): void {
    this.formDates = this.formBuilder.group({
        dateTo: ['', Validators.required ],
        dateFrom: ['', Validators.required ]
      }, {validator: this.dateLessThan('dateFrom', 'dateTo')});
  }

  private dateLessThan(from: string, to: string) {
        return (group: FormGroup): {[key: string]: any} => {
            let f = group.controls[from];
            let t = group.controls[to];
            if (f.value < t.value) {
            return {
                dates: false
            };
            }
            return {};
        }
    }

  private moreThanOneChecked(): void {
        this.form = this.formBuilder.group({
            checkbox1: [''],
            checkbox2: [''],
            checkbox3: [''],
        });

        this.form.setErrors({required: true});
        this.form.valueChanges.subscribe((newValue) => {
            if (newValue.checkbox1 === true && newValue.checkbox2 === true
                && newValue.checkbox3 === true) {
            this.form.setErrors(null);
            }
            if (newValue.checkbox1 === true && newValue.checkbox2 === true
                && newValue.checkbox3 === false) {
                this.form.setErrors(null);
            }
            if (newValue.checkbox1 === false && newValue.checkbox2 === true
                && newValue.checkbox3 === true) {
                this.form.setErrors(null);
            }
            if (newValue.checkbox1 === true && newValue.checkbox2 === false
                && newValue.checkbox3 === true) {
                this.form.setErrors(null);
            }
            if (newValue.checkbox1 === false && newValue.checkbox2 === false
                && newValue.checkbox3 === false) {
                this.form.setErrors({required: true});
            }
            if (newValue.checkbox1 === true && newValue.checkbox2 === false
                && newValue.checkbox3 === false) {
                this.form.setErrors({required: true});
            }
            if (newValue.checkbox1 === false && newValue.checkbox2 === true
                && newValue.checkbox3 === false) {
                this.form.setErrors({required: true});
            }
            if (newValue.checkbox1 === false && newValue.checkbox2 === false
                && newValue.checkbox3 === true) {
                this.form.setErrors({required: true});
            }
        });
    }
}
