import { NgModule } from '@angular/core';
import { FormTemplateRoutingModule } from './form-template-routing.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { FormTemplateComponent } from './form-template.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SubheaderModule } from '@app/shared/common/sub-header/subheader.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';



@NgModule({
  declarations: [FormTemplateComponent],
  imports: [
    AppSharedModule,
    FormTemplateRoutingModule,
    NgxDropzoneModule,
    FormsModule,
    ReactiveFormsModule,
    SubheaderModule,
    TooltipModule.forRoot(),
  ],
})
export class FormTemplateModule { }
