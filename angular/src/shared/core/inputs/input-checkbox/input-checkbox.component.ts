import { Component, Injector, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'input-checkbox',
    templateUrl: './input-checkbox.component.html',
    styleUrls: ['./input-checkbox.component.less'],
})
export class InputCheckboxComponent extends AppComponentBase {
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() title: string;
    @Input() placeholder: string;
    constructor(
        injector: Injector
    ) {
        super(injector);
    }

}
