import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'input-drag-file',
    templateUrl: './input-drag-file.component.html',
    styleUrls: ['./input-drag-file.component.less'],
})
export class InputDragFileComponent extends AppComponentBase implements OnInit {
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() title: string;
    files: File[] = [];
    constructor(injector: Injector) {
        super(injector);
    }
    ngOnInit(): void {
        //revisar, cargar archivo cuando venga la url de la imagen
    }
    onSelectFile(event) {
        debugger;
        this.form.controls[this.name].setValue({ ...event.addedFiles });
        this.files.push(...event.addedFiles);
    }
    onRemoveFile(event) {
        debugger;
        this.form.controls[this.name].reset();
        this.files.splice(this.files.indexOf(event), 1);
    }
}
