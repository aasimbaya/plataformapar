import { Component, EventEmitter, Injector, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DropdownItemDto } from '@shared/service-proxies/service-proxies';
import { Observable, Subscription } from 'rxjs';
import { ReloadCombo } from './input-dropdown.model';

@Component({
    selector: 'input-dropdown',
    templateUrl: './input-dropdown.component.html',
    styleUrls: ['./input-dropdown.component.less'],
})
export class InputDropdownComponent extends AppComponentBase implements OnInit, OnDestroy {
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() title: string;
    @Input() executeFunction: Function;
    @Input() placeholder: string;
    @Input() showSelectOption: boolean;
    @Input() reload: Observable<ReloadCombo>;
    @Output() changeValue: EventEmitter<boolean> = new EventEmitter<boolean>();

    subscriptions: Subscription[] = [];
    options: DropdownItemDto[] = [];
    constructor(injector: Injector) {
        super(injector);
    }
    ngOnInit(): void {
        this._getData('');
        const reloadSub = this.reload?.subscribe((value) => {
            if (value.reset) {
                this._getData('');
            } else if (value.reload) {
                this._getData('');
            }
        });
        this.subscriptions = [...this.subscriptions, reloadSub];
    }
    findValues(e: any): void {
        this.changeValue.emit(true);
    }
    private _getData(value: string): void {
        const sub = this.executeFunction({
            id: this.getId(),
            filter: value,
        }).subscribe(
            (res: DropdownItemDto[]) => {
                this.options = res;
            },
            (err) => {
                console.error('this.executeFunction on dynamic table');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }
    private getId(): number {
        return this.form.controls[this.name].value || 0;
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
