export interface InputDropdown {
    id: number;
    filter: string;
}

export interface ReloadCombo {
    reload?: boolean;
    reset?: boolean;
}
