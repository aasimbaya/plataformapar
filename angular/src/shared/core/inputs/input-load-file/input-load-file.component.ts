import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'input-load-file',
    templateUrl: './input-load-file.component.html',
    styleUrls: ['./input-load-file.component.less'],
})
export class InputLoadFileComponent extends AppComponentBase implements OnInit {
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() title: string;
    files: File[] = [];
    constructor(injector: Injector) {
        super(injector);
    }
    ngOnInit(): void {
        //revisar, cargar archivo cuando venga la url de la imagen
    }
}
