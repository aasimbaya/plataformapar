import { Component, Injector, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'input-number',
    templateUrl: './input-number.component.html',
    styleUrls: ['./input-number.component.less'],
})
export class InputNumberComponent extends AppComponentBase {
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() title: string;
    @Input() placeholder: string;
    constructor(
        injector: Injector
    ) {
        super(injector);
    }

}
