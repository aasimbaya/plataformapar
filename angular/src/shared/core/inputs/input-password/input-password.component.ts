import { Component, Injector, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'input-password',
    templateUrl: './input-password.component.html',
    styleUrls: ['./input-password.component.less'],
})
export class InputPasswordComponent extends AppComponentBase {
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() title: string;
    @Input() placeholder: string;
    type = "password";
    constructor(
        injector: Injector
    ) {
        super(injector);
    }
    change() {
        this.type = this.type === "text" ? "password" : "text";
    }
}
