import { Component, Injector, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
@Component({
    selector: 'input-phone',
    templateUrl: './input-phone.component.html',
    styleUrls: ['./input-phone.component.less'],
})
export class InputPhoneComponent extends AppComponentBase {
    @Input() form: FormGroup;
    @Input() controlName: string;
    @Input() title: string;
    @Input() country: string;
    options: any[] = [];
    separateDialCode = false;
    SearchCountryField = SearchCountryField;
    CountryISO = CountryISO;
    PhoneNumberFormat = PhoneNumberFormat;
    onlyCountries = AppConsts.onlyCountries;
    // preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
    constructor(injector: Injector) {
        super(injector);
    }
}
