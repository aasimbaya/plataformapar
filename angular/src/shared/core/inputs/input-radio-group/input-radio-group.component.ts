import { Component, Injector, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserRoleDto } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'input-radio-group',
    templateUrl: './input-radio-group.component.html',
    styleUrls: ['./input-radio-group.component.less'],
})
export class InputRadioGroupComponent extends AppComponentBase {
    @Input() form: FormGroup;
    @Input() radioList: UserRoleDto[];
    @Input() name: string;
    constructor(
        injector: Injector
    ) {
        super(injector);
    }

}
