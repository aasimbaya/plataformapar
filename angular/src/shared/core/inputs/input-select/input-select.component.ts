import { Component, Injector, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DropdownItemDto } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'input-select',
    templateUrl: './input-select.component.html',
    styleUrls: ['./input-select.component.less'],
})
export class InputSelectComponent extends AppComponentBase {
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() title: string;
    @Input() placeholder: string;
    @Input() options: DropdownItemDto[];
    @Input() showSelectOption: boolean;

    constructor(
        injector: Injector
    ) {
        super(injector);
    }

}
