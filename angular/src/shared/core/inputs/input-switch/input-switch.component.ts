import { Component, Injector, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'input-switch',
    templateUrl: './input-switch.component.html',
    styleUrls: ['./input-switch.component.less'],
})
export class InputSwitchComponent extends AppComponentBase {
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() title: string;
    @Input() placeholder: string;
    constructor(
        injector: Injector
    ) {
        super(injector);
    }

}
