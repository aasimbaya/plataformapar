import { Component, Injector, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'input-text',
    templateUrl: './input-text.component.html',
    styleUrls: ['./input-text.component.less'],
})
export class InputTextComponent extends AppComponentBase {
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() title: string;
    @Input() placeholder: string;
    @Input() maxlength: number = 150;
    constructor(
        injector: Injector
    ) {
        super(injector);
    }

}
