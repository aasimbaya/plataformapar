import { Component, Injector, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'input-textarea',
    templateUrl: './input-textarea.component.html',
    styleUrls: ['./input-textarea.component.less'],
})
export class InputTextareaComponent extends AppComponentBase {
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() title: string;
    @Input() placeholder: string;
    constructor(
        injector: Injector
    ) {
        super(injector);
    }

}
