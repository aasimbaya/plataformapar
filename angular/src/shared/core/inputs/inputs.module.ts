import { NgModule } from '@angular/core';

import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';

import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';

import { InputCheckboxComponent } from './input-checkbox/input-checkbox.component';
import { InputDragFileComponent } from './input-drag-file/input-drag-file.component';
import { InputPasswordComponent } from './input-password/input-password.component';
import { InputPhoneComponent } from './input-phone/input-phone.component';
import { InputRadioGroupComponent } from './input-radio-group/input-radio-group.component';
import { InputSelectComponent } from './input-select/input-select.component';
import { InputSwitchComponent } from './input-switch/input-switch.component';
import { InputTextComponent } from './input-text/input-text.component';
import { InputTextareaComponent } from './input-textarea/input-textarea.component';
import { InputNumberComponent } from './input-number/input-number.component';
import { InputDropdownComponent } from './input-dropdown/input-dropdown.component';
import { InputLoadFileComponent } from './input-load-file/input-load-file.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
    declarations: [
        InputPasswordComponent,
        InputDragFileComponent,
        InputPhoneComponent,
        InputTextComponent,
        InputSelectComponent,
        InputCheckboxComponent,
        InputTextareaComponent,
        InputSwitchComponent,
        InputRadioGroupComponent,
        InputNumberComponent,
        InputLoadFileComponent,
        InputDropdownComponent,
    ],
    imports: [AppSharedModule, AdminSharedModule, NgxDropzoneModule, NgxIntlTelInputModule, BsDropdownModule ],
    exports: [
        InputPasswordComponent,
        InputDragFileComponent,
        InputPhoneComponent,
        InputTextComponent,
        InputSelectComponent,
        InputCheckboxComponent,
        InputTextareaComponent,
        InputRadioGroupComponent,
        InputSwitchComponent,
        InputNumberComponent,
        InputLoadFileComponent,
        InputDropdownComponent,
    ],
})
export class InputsModule {}
