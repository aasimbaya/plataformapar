import { Component, Injector, OnInit } from '@angular/core';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'app-launch-call-page',
    templateUrl: './launch-call-page.component.html',
    styleUrls: ['./launch-call-page.component.css'],
    animations: [appModuleAnimation()],
})
export class LaunchCallPageComponent extends AppComponentBase implements OnInit {
    breadcrumbs: BreadcrumbItem[] = [new BreadcrumbItem(this.l('Questionnaire'))];
    bucketname: string = 'pardevaequalestest';
    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {}

    /**
     * Action to open or donwload file from aws bucket
     * @param FileFullName
     */
    downloadFile(FileFullName: string): void {
        window.open(`https://${this.bucketname}.s3.us-east-2.amazonaws.com/${FileFullName}`);
    }
}
