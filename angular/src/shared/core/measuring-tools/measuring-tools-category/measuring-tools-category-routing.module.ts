import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MasuringToolsCategoryUpsertComponent } from './measuring-tools-category-upsert/measuring-tools-category-upsert.component';
import { MeasuringToolsCategoryComponent } from './measuring-tools-category.component';
import { PageMode } from '@shared/AppEnums';

const routes: Routes = [
    {
        path: '',
        component: MeasuringToolsCategoryComponent,
        pathMatch: 'full',
        },
        {
            path: 'add',
            component: MasuringToolsCategoryUpsertComponent,
            data: { pageMode: PageMode.Add },
            pathMatch: 'full'
        },
        {
            path: ':id/edit',
            component: MasuringToolsCategoryUpsertComponent,
            data: { pageMode: PageMode.Edit },
            pathMatch: 'full'
        },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MeasuringToolsCategoryRoutingModule {}
