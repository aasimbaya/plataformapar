import { Component, Injector } from '@angular/core';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PageMode } from '@shared/AppEnums';
import { ConfigColumns, PageStatus, RecordsTable, ReloadTable } from '@shared/core/dynamic-table/dynamic-table.model';
import {
    PagedResultDtoOfQuestionsByCategoryListItemDto,
    QuestionCategoryServiceProxy,
    QuestionsBankServiceProxy,
    QuestionsByCategoryListItemDto,
    UpsertQuestionCategoryDto,
    UpsertQuestionCategoryTypeDto
} from '@shared/service-proxies/service-proxies';
import { QuestionByCategory } from '../measuring-tools-category.model';
import { MeasuringToolsService } from '../services/measuring-tools.service';

/**
 * Main component for zip code edit
 */
@Component({
    selector: 'measuring-tools-category-upsert',
    styleUrls: ['./measuring-tools-category-upsert.component.less'],
    templateUrl: './measuring-tools-category-upsert.component.html',
})
export class MasuringToolsCategoryUpsertComponent extends AppComponentBase {
    private _questions: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });
    set questions(value: RecordsTable) {
        this._questions.next(value);
    }
    get questions(): RecordsTable {
        return this._questions.value;
    }
    questions$(): Observable<RecordsTable> {
        return this._questions.asObservable();
    }
    flatHasQuestions = false;
    breadcrumbs: BreadcrumbItem[] = [];
    loading: boolean = false;
    pageMode: PageMode;
    subscriptions: Subscription[] = [];
    form: FormGroup;
    isNew = true;
    optionsTypeCategory: UpsertQuestionCategoryTypeDto[] = [];

    private _reload: BehaviorSubject<ReloadTable> = new BehaviorSubject({});
    set reload(value: ReloadTable) {
        this._reload.next(value);
    }
    get reload(): ReloadTable {
        return this._reload.value;
    }
    reload$(): Observable<ReloadTable> {
        return this._reload.asObservable();
    }
    pageStatus: PageStatus = {
        first: 0,
        page: 0,
        pageCount: 10,
        rows: 0,
    };
    filter: string = '';
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _measuringToolsService: MeasuringToolsService,
        private _questionCategoryServiceProxy: QuestionCategoryServiceProxy,
        private _formBuilder: FormBuilder,
        private _questionsBankService:  QuestionsBankServiceProxy
    ) {
        super(injector);
    }

    /**
     * OnInit
     */
    ngOnInit(): void {
        this.pageMode = this._activatedRoute.snapshot.data.pageMode;
        const id = this._activatedRoute.snapshot.params.id;
        this._init(id);
        this.loadDataTable = this.loadDataTable.bind(this);
    }

    private async _init(id) {
        this.getQuestionCategoryType();
        if (id) {
            this.breadcrumbs = [new BreadcrumbItem(this.l('measuringtools_categories_editor_subtitle_update'))];
            this.isNew = false;
            this.getQuestionCategory(id);
        } else {
            this.breadcrumbs = [new BreadcrumbItem(this.l('measuringtools_categories_editor_subtitle_create'))];
            this.isNew = true;
            this.form = await this._formInitialize(new UpsertQuestionCategoryDto());
        }
    }
    private getQuestionCategoryType() {
        const sub = this._questionCategoryServiceProxy.getTypeCategories('').subscribe(
            (res: UpsertQuestionCategoryTypeDto[]) => {
                this.optionsTypeCategory = res;
            },
            (err) => {
                console.error('_questionCategoryServiceProxy.getTypeCategories');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }
    private async getQuestionCategory(id: number) {
        const sub = this._questionCategoryServiceProxy.get(id).subscribe(
            async (res: UpsertQuestionCategoryDto) => {
                this.form = await this._formInitialize(res);
            },
            (err) => {
                console.error('_questionCategoryServiceProxy.get');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }
    private async _formInitialize(data: UpsertQuestionCategoryDto) {
        return this._formBuilder.group({
            id: [data.id || 0, []],
            name: [data.name || '', [Validators.required]],
            questionCategoryTypeId: [data.questionCategoryTypeId || '', [Validators.required]],
            description: [data.description || '', []],
            isActive: [data.isActive || false, []],
            icon: [data.icon || null, []],
        });
    }

    loadCategory(pag: PageStatus): void {
        this.pageStatus = pag;
        this.loadQuestions();
    }
    editOnClick(data: QuestionByCategory): void {
        //revisar el tipo
        debugger;
    }
    findEvent(data: string): void {
        //revisar el tipo
        this.filter = data;
        this.loadQuestions();
    }
    loadQuestions(): void {
        const id = this.form.controls.id.value;
        if (id) {
            const sub = this._questionCategoryServiceProxy
                .getAllQuestionsByCategoryIdPaged(
                    id,
                    this.filter,
                    this.pageStatus.sorting,
                    this.pageStatus.pageCount,
                    this.pageStatus.page
                )
                .subscribe(
                    (res: PagedResultDtoOfQuestionsByCategoryListItemDto) => {
                        this.questions = { countTotal: res.totalCount, records: res.items };
                        this.flatHasQuestions = res.items.length > 0;
                    },
                    (err) => {
                        console.error('_questionCategoryServiceProxy.getData()');
                    }
                );
            this.subscriptions = [...this.subscriptions, sub];
        }
    }
    closeOnClick(): void {
        if (!this.form.pristine) {
            abp.message.confirm(
                this.l('measuringtools_categories_editor_confirm_close_mgs'),
                this.l('measuringtools_categories_editor_confirm_close_title'),
                (result: boolean) => {
                    if (result) {
                        this._redirectToMeasuringToolsCategoryListPage();
                    }
                }
            );
        } else {
            this._redirectToMeasuringToolsCategoryListPage();
        }
    }

    saveOnClick(): void {
        const { valid } = this.form;
        if (valid) {
            // this.sanitizeData();
            if (this.isNew) {
                this._add();
            } else {
                this._edit();
            }
        } else {
            this.form.markAllAsTouched();
        }
    }
    onFormSubmit($event, form) {
        $event.preventDefault;
    }
    private _edit() {
        const sub = this._questionCategoryServiceProxy
            .update(this.form.controls.id.value, this._getUpsertQuestionCategoryDto())
            .subscribe(
                (res: any) => {
                    abp.notify.success(this.l('measuringtools_categories_editor_success_updated'));
                    this._redirectToMeasuringToolsCategoryListPage();
                },
                (err) => {
                    console.error('_questionCategoryServiceProxy.update');
                }
            );
        this.subscriptions = [...this.subscriptions, sub];
    }

    private _add() {
        const sub = this._questionCategoryServiceProxy.create(this._getUpsertQuestionCategoryDto()).subscribe(
            (res: any) => {
                abp.notify.success(this.l('measuringtools_categories_editor_success_created'));
                this._redirectToMeasuringToolsCategoryListPage();
            },
            (err) => {
                console.error('_questionCategoryServiceProxy.create');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }
    private _getUpsertQuestionCategoryDto(): UpsertQuestionCategoryDto {
        const { value } = this.form;
        const { name, description, isActive, questionCategoryTypeId, icon } = value;
        const data = new UpsertQuestionCategoryDto();
        data.name = name;
        data.description = description;
        data.isActive = isActive;
        data.questionCategoryTypeId = questionCategoryTypeId;
        data.icon = icon;
        return data;
    }

    // private sanitizeData() {
    //     const {
    //         zipCode1,
    //         city,
    //         state,
    //         county,
    //         branch,
    //         taxCode,
    //         group1,
    //         group2,
    //         group3,
    //         group4,
    //         group5,
    //         group6,
    //         latitude,
    //         longitude,
    //         areaCode,
    //         timeZone,
    //         elevation,
    //     } = this.upsertZipCodeDto;
    //     this.upsertZipCodeDto.zipCode1 = zipCode1.toUpperCase().trim();
    //     this.upsertZipCodeDto.city = this.getTrim(city);
    //     this.upsertZipCodeDto.state = this.getTrim(state);
    //     this.upsertZipCodeDto.county = this.getTrim(county);
    //     this.upsertZipCodeDto.taxCode = this.getTrim(taxCode);
    //     this.upsertZipCodeDto.group1 = this.getTrim(group1);
    //     this.upsertZipCodeDto.group2 = this.getTrim(group2);
    //     this.upsertZipCodeDto.group3 = this.getTrim(group3);
    //     this.upsertZipCodeDto.group4 = this.getTrim(group4);
    //     this.upsertZipCodeDto.group5 = this.getTrim(group5);
    //     this.upsertZipCodeDto.group6 = this.getTrim(group6);
    //     this.upsertZipCodeDto.latitude = this.getCoordinate(latitude?.toString());
    //     this.upsertZipCodeDto.longitude = this.getCoordinate(longitude?.toString());
    //     this.upsertZipCodeDto.areaCode = this.getTrim(areaCode);
    //     this.upsertZipCodeDto.timeZone = this.getCoordinate(timeZone?.toString());
    //     this.upsertZipCodeDto.elevation = this.getCoordinate(elevation?.toString());
    // }

    private _redirectToMeasuringToolsCategoryListPage(): void {
        //revisar
        this._router.navigate(['app', 'measuring-tools', 'category']);
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }

    showQuestionsAssociated(): boolean {
        return this.pageMode != PageMode.Add;
    }

    loadDataTable(data: PageStatus) {
        return this._questionCategoryServiceProxy.getAllQuestionsByCategoryIdPaged(
            this._activatedRoute.snapshot.params.id,
            data.filter,
            data.sorting,
            data.pageCount,
            data.page
        );
    }

    deleteOnClick(data: QuestionsByCategoryListItemDto): void {
        abp.message.confirm(this.l('MsgConfirmDeleteAssociation'), this.l('TitleConfirmDeleteAssociation'), (result: boolean) => {
            if (result) {
                this.delete(data.id);

            }
        });
    }

    /**
     * Method delete a category question
     */
     private delete(id: number) {
        const sub = this._questionsBankService.delete(id).subscribe(
            (res: any) => {
                this._getInformation({ reset: true });
                abp.notify.success(this.l('SuccessfullyDeleted'));
            },
            (err) => {
                console.error('_formServiceProxy.delete');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    private _getInformation(data: ReloadTable): void {
        this.reload = data;
    }
    columns: ConfigColumns[] = [
        {
            field: 'order',
            titleLabel: 'Order',
            type: 'string',
        },
        {
            field: 'question',
            titleLabel: 'Question',
            type: 'string',
        },
    ];
}
