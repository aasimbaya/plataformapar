import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { filter as _filter } from 'lodash-es';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { debounce as _debounce } from 'lodash-es';
import { ConfigColumns, PageStatus, ReloadTable } from '../../dynamic-table/dynamic-table.model';
import { MeasuringToolCategory } from './measuring-tools-category.model';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';
import { QuestionCategoryServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './measuring-tools-category.component.html',
    animations: [appModuleAnimation()],
})
export class MeasuringToolsCategoryComponent extends AppComponentBase implements OnInit, OnDestroy {
    breadcrumbs: BreadcrumbItem[] = [new BreadcrumbItem(this.l('measuringtools_categories_subtitle'))];
    columns: ConfigColumns[] = [
        {
            field: 'name',
            titleLabel: 'CategoryMeasuringTools',
            type: 'string',
        },
        {
            field: 'questionCategoryTypeName',
            titleLabel: 'measuringtools_categories_grid_type',
            type: 'string',
        },
        {
            field: 'isActive',
            titleLabel: 'measuringtools_categories_grid_active',
            type: 'boolean',
        },
        {
            field: 'questionsRelations',
            titleLabel: 'measuringtools_categories_grid_relations',
            type: 'number',
        },
        {
            field: 'lastModificationTime',
            titleLabel: 'measuringtools_categories_grid_lastmodificationtime',
            type: 'date',
        },
    ];

    subscriptions: Subscription[] = [];
    private _reload: BehaviorSubject<ReloadTable> = new BehaviorSubject({});
    set reload(value: ReloadTable) {
        this._reload.next(value);
    }
    get reload(): ReloadTable {
        return this._reload.value;
    }
    reload$(): Observable<ReloadTable> {
        return this._reload.asObservable();
    }
    constructor(
        injector: Injector,
        private _router: Router,
        private _questionCategoryServiceProxy: QuestionCategoryServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.loadDataTable = this.loadDataTable.bind(this);
    }

    loadDataTable(data: PageStatus) {
        return this._questionCategoryServiceProxy.getAllPaged(data.filter, data.sorting, data.pageCount, data.page);
    }
    /**
     * add button on click action
     */
    addOnClick(): void {
        this._router.navigate(['app', 'measuring-tools', 'category', 'add']);
    }
    editOnClick(data: MeasuringToolCategory): void {
        this._router.navigate(['app', 'measuring-tools', 'category', data.id, 'edit']);
    }

    deleteOnClick(data: MeasuringToolCategory): void {
        //revisar el tipo
        abp.message.confirm(
            this.l('measuringtools_categories_confirm_delete_mgs'),
            this.l('measuringtools_categories_confirm_delete_title'),
            (result: boolean) => {
                if (result) {
                    this.delete(data.id);
                }
            }
        );
    }
    private delete(id: number) {
        const sub = this._questionCategoryServiceProxy.delete(id).subscribe(
            (res: any) => {
                this.getCategories({ reset: true });
                abp.notify.success(this.l('measuringtools_categories_success_delete'));
            },
            (err) => {
                console.error('_questionCategoryServiceProxy.delete');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }
    getCategories(data: ReloadTable): void {
        this.reload = data;
    }
    /**
     * filter button on click action
     */
    filterOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
    }

    /**
     * close button on click action
     */
    closeOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration']);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
