export interface MeasuringToolCategory {
    id: number;
    category: string;
    type: string;
    active: boolean;
    associatedQuestions: Number;
    latestModificatedDate: Date,
}
export interface QuestionByCategory {
    id: Number;
    order: Number;
    question: string;
}
