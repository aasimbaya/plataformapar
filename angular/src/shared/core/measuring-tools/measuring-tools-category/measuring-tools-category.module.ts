import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CoreModule } from '@shared/core/core.module';
import { MeasuringToolsCategoryRoutingModule } from './measuring-tools-category-routing.module';
import { MasuringToolsCategoryUpsertComponent } from './measuring-tools-category-upsert/measuring-tools-category-upsert.component';
import { MeasuringToolsCategoryComponent } from './measuring-tools-category.component';
import { InputsModule } from '@shared/core/inputs/inputs.module';

@NgModule({
    declarations: [MeasuringToolsCategoryComponent, MasuringToolsCategoryUpsertComponent],
    imports: [AppSharedModule, AdminSharedModule, MeasuringToolsCategoryRoutingModule, CoreModule, InputsModule],
})
export class MeasuringCategoryToolsModule {}
