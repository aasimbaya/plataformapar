import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of, BehaviorSubject, Observable } from 'rxjs';
import { MeasuringToolCategory } from '../measuring-tools-category.model';
import { PageStatus, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';

@Injectable({
    providedIn: 'root',
})
export class MeasuringToolsService {
    private baseUrl = 'https://pokeapi.co';
    constructor(private _http: HttpClient) {}
    getData(pag: PageStatus): Observable<RecordsTable> {
        const data = {
            countTotal: 20,
            records: [
                //revisar, migrar a tabla generica con jsons, y separar en un componente generico
                {
                    id: 1,
                    category: 'categoria de prueba',
                    type: 'obligatorio',
                    active: true,
                    associatedQuestions: 6,
                    latestModificatedDate: new Date('2022-03-07'),
                },
                {
                    id: 2,
                    category: 'otra categoria',
                    type: 'opcional',
                    active: false,
                    associatedQuestions: 2,
                    latestModificatedDate: new Date('2021-04-17'),
                },
                {
                    id: 3,
                    category: 'otra categoria2',
                    type: 'obligatorio',
                    active: true,
                    associatedQuestions: 23,
                    latestModificatedDate: new Date('2021-04-18'),
                },
                {
                    id: 4,
                    category: 'otra categoria3',
                    type: 'opcional',
                    active: false,
                    associatedQuestions: 0,
                    latestModificatedDate: new Date('2022-04-18'),
                },
            ],
        };
        return of(data);
    }
    getQuestions(pag: PageStatus, idCategory: Number, find: string): Observable<RecordsTable> {
        const data = {
            countTotal: 2,
            records: [
                //revisar, migrar a tabla generica con jsons, y separar en un componente generico
                {
                    id: 1,
                    order: 1,
                    question: 'pregunta 1',
                },
                {
                    id: 2,
                    order: 2,
                    question: 'pregunta 2',
                },
            ],
        };
        return of(data);
    }


    getDataUserGrouping(): Observable<any> {
        const data = {
            totalCount: 20,
            items: [
                {
                    id: 1,
                    name: 'categoria de prueba',
                    count: 5,
                    createDate: new Date('2022-03-07'),
                },
                {
                    id: 2,
                    name: 'categoria de 2',
                    count: 6,
                    createDate: new Date('2022-03-08'),
                },
            ],
        };
        return of(data);
    }
    getDataBusinessGroups(): Observable<any> {
        const data = {
            totalCount: 20,
            items: [
                {
                    id: 1,
                    name: 'categoria de prueba',
                    count: 5,
                    createdDate: new Date('2022-03-07'),
                },
                {
                    id: 2,
                    name: 'categoria de 2',
                    count: 6,
                    createdDate: new Date('2022-03-08'),
                },
            ],
        };
        return of(data);
    }
    getDataCompaniesPlans(): Observable<any> {
        const data = {
            totalCount: 20,
            items: [
                {
                    id: 1,
                    name: 'categoria de prueba',
                    period: new Date('2022-03-07'),
                    amount: '$200',
                    discount: '$10',
                    active: false,
                },
                {
                    id: 2,
                    name: 'categoria de 2',
                    period: new Date('2022-02-07'),
                    amount: '$300',
                    discount: '$40',
                    active: true,
                },
            ],
        };
        return of(data);
    }
}
