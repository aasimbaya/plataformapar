import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LaunchCallPageComponent } from './launch-call-page/launch-call-page.component';

const routes: Routes = [
    {
        path: 'category',
        loadChildren: () =>
            import('./measuring-tools-category/measuring-tools-category.module').then(
                (m) => m.MeasuringCategoryToolsModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
    {
        path: 'questionnaire',
        loadChildren: () =>
            import('./questionnaire/questionnaire.module').then(
                (m) => m.MeasuringQuestionnaireToolsModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
    {
        path: 'special-distintion',
        loadChildren: () =>
            import('./special-distintion/special-distintion.module').then(
                (m) => m.SpecialDistintionModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
    { path: 'launch-call-page', component: LaunchCallPageComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MeasuringToolsRoutingModule {}
