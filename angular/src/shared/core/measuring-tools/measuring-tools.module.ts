import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { MeasuringToolsRoutingModule } from './measuring-tools-routing.module';
import { LaunchCallPageComponent } from './launch-call-page/launch-call-page.component';
@NgModule({
    declarations: [
    LaunchCallPageComponent
  ],
    imports: [AppSharedModule, AdminSharedModule, MeasuringToolsRoutingModule],
})
export class MeasuringToolsModule {}
