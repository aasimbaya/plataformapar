import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import { filter as _filter } from 'lodash-es';
import { Subscription, BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { debounce as _debounce } from 'lodash-es';
import { ConfigColumns, PageStatus, ReloadTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';
import { Questionnaire } from '../questionnaire.model';
import {
    FormServiceProxy,
    FormDto,
    FormNameDto,
    UpsertFormDto,
} from '@shared/service-proxies/service-proxies';
import { TypeForms } from '@shared/core/shared/type-forms';

const QUESTIONNAIRE_COLLUMS_SCHEMA = [
    {
        field: 'title',
        titleLabel: 'Questionnaire',
        type: 'string',
    },
    {
        field: 'typeName',
        titleLabel: 'TypeQuestionnaireName',
        type: 'string',
    },
    {
        field: 'isActive',
        titleLabel: 'IsActive',
        type: 'boolean',
    },
    {
        field: 'lastModificationTime',
        titleLabel: 'LastModificationTime',
        type: 'date',
    },
];

/**
 * Main page of a Questionnaire List
 */
@Component({
    selector: 'app-questionnaire-list',
    templateUrl: './list.component.html',
    animations: [appModuleAnimation()],
})
export class QuestionnaireListComponent extends AppComponentBase implements OnInit {
    breadcrumbs: BreadcrumbItem[] = [new BreadcrumbItem(this.l('Questionnaire'))];

    columns: ConfigColumns[] = QUESTIONNAIRE_COLLUMS_SCHEMA;

    private _reload: BehaviorSubject<ReloadTable> = new BehaviorSubject({});
    set reload(value: ReloadTable) {
        this._reload.next(value);
    }
    get reload(): ReloadTable {
        return this._reload.value;
    }
    reload$(): Observable<ReloadTable> {
        return this._reload.asObservable();
    }

    subscriptions: Subscription[] = [];
    pageStatus: PageStatus = { first: 0, page: 0, pageCount: 10, rows: 0 };
    filter: { text: string } = { text: '' };
    idTypeQuestionnaire: number;
    upsertQuestionnaireDto: UpsertFormDto = new UpsertFormDto();

    /**
     * Constructor
     */
    constructor(injector: Injector, private _router: Router, private _formService: FormServiceProxy) {
        super(injector);
    }

    /**
     * OnInit
     */
    ngOnInit(): void {
        this.getFormNameId();
        this.loadDataTable = this.loadDataTable.bind(this);
    }

    private getFormNameId(): void {
        const sub = this._formService.getFormNameByName(TypeForms.questionnaries).subscribe(
            (res: FormNameDto) => {
                this.idTypeQuestionnaire = res.id;
            },
            (err) => {
                console.error('_formService.getFormNameByName');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Add button on click action
     */
    addOnClick(): void {
        this._router.navigate(['app', 'measuring-tools', 'questionnaire', 'add']);
    }

    /**
     * Delete button on click action
     */
    deleteOnClick(data: FormDto): void {
        abp.message.confirm(
            this.l('MsgConfirmDeleteQuestionnaire'),
            this.l('TitleConfirmDeleteQuestionnaire'),
            (result: boolean) => {
                if (result) {
                    this.deleteQuestionnaire(data.id, data.title);
                }
            }
        );
    }

    /**
     * Duplicate button on click action
     */
    duplicateOnClick(data: Questionnaire): void {
        this.getQuestionnaireToDuplicate(data.id);
    }

    /**
     * Edit button on click action
     */
    editOnClick(data: Questionnaire): void {
        this._router.navigate(['app', 'measuring-tools', 'questionnaire', data.id, 'edit']);
    }

    /**
     * Filter button on click action
     */
    filterOnClick(): void {
        //TODO: set accion on this btn
        debugger;
    }

    /**
     * Get all grid list data
     */
    loadDataTable(data: PageStatus) {
        return this._formService.getAllPaged(
            data.filter,
            TypeForms.questionnaries,
            data.sorting,
            data.pageCount,
            data.page
        );
    }

    /**
     * Action to reload questionnarie list
     */
    realoadQuestionnaires(data: ReloadTable): void {
        this.reload = data;
    }

    /**
     * Get a questionnaire by id and insert
     */
    private getQuestionnaireToDuplicate(id: number) {
        const sub = this._formService.getFormById(id).subscribe(
            async (res: UpsertFormDto) => {
                this.setValues(res);
                this.duplicateQuestionnaire();
            },
            (err) => {
                console.error('_formService.getFormById');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Set values to the model to proceed add or edit a questionnaire
     */
    private setValues(res): void {
        const { title, isActive, typeId } = res;
        this.upsertQuestionnaireDto.title = title;
        this.upsertQuestionnaireDto.description = '';
        this.upsertQuestionnaireDto.isActive = isActive;
        this.upsertQuestionnaireDto.typeId = typeId;
        this.upsertQuestionnaireDto.formNameId = this.idTypeQuestionnaire;
    }

    /**
     * Create a new questionnaire
     */
    duplicateQuestionnaire(): void {
        const sub = this._formService.create(this.upsertQuestionnaireDto).subscribe(
            (res: any) => {
                this.realoadQuestionnaires({ reload: true });
                abp.notify.success(this.l('SavedSuccessfully'));
            },
            (err) => {
                console.error('_formService.create');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Delete form by Id
     */
    private deleteQuestionnaire(itemId: number, itemName: string): void {
        const sub = this._formService.deleteFormById(itemId).subscribe(
            () => {
                this.realoadQuestionnaires({ reload: true });
                abp.notify.success(this.l('SuccessfullyDeleted'));
            },
            () => {
                console.error('_formService.delete');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * OnDestroy
     */
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }
}
