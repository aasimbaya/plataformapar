import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus, RecordsTable } from '@shared/core/dynamic-select-table/dynamic-select-table.model';
import { TypeForms } from '@shared/core/shared/type-forms';
import { FormServiceProxy, PagedResultDtoOfFormDto, PagedResultDtoOfQuestionCategoryListItemDto, QuestionCategoryServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-modal-add-category',
  templateUrl: './modal-add-category.component.html',
})
export class ModalAddCategoryComponent extends AppComponentBase implements OnInit {
    @ViewChild('addCategory', { static: true }) modal: ModalDirective;
    @Output() returnItemSelectedEvent: EventEmitter<any[]> = new EventEmitter<any[]>();
    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });
    set records(value: RecordsTable) {
        this._records.next(value);
    }
    get records(): RecordsTable {
        return this._records.value;
    }
    records$(): Observable<RecordsTable> {
        return this._records.asObservable();
    }
    subscriptions: Subscription[] = [];
    @Input() selectedProducts: number[];

    constructor(injector: Injector, private _questionCategoryServiceProxy: QuestionCategoryServiceProxy) {
        super(injector);
    }

    ngOnInit(): void {
        this.getCategoryList({ first: 0, page: 0, pageCount: 10, rows: 0 });
    }

            /**
     * Method that load all the category list
     */
    private getCategoryList(pag: PageStatus): void {
        const sub = this._questionCategoryServiceProxy.getAllPaged(pag.filter,
        pag.sorting, pag.pageCount, pag.page)
        .subscribe(
            (res: PagedResultDtoOfQuestionCategoryListItemDto) => {
                this.records = { countTotal: res.totalCount, records: res.items };
            },
            (err) => {
                console.error('_formService.getAllPaged()');
            }
        );
    this.subscriptions = [...this.subscriptions, sub];
    }

    onSelectedItemClose(data:boolean): void {
        if(data){
            this.modal.hide();
        }
    }

    onSelectedItem(data: any): void {
        this.returnItemSelectedEvent.emit(data)
    }

    onShown(): void {}

    show(id?: number): void {
        const self = this;
        self.modal.show();
    }

     close(): void {
        this.modal.hide();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }

    columns: ConfigColumns[] = [
        {
            field: 'name',
            titleLabel: 'measuringtools_categories_add',
            type: 'string',
        },
        {
            field: 'questionsRelations',
            titleLabel: 'AssociatedQuestions',
            type: 'string',
        },
    ];
}
