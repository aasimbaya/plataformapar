import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus, RecordsTable } from '@shared/core/dynamic-select-table/dynamic-select-table.model';
import { TypeForms } from '@shared/core/shared/type-forms';
import { FormServiceProxy, PagedResultDtoOfFormDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable, Subscription } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Component({
    selector: 'app-modal-add-special-distinction',
    templateUrl: './modal-add-special-distinction.component.html',
    animations: [appModuleAnimation()],
})
export class ModalAddSpecialDistinctionComponent extends AppComponentBase implements OnInit {
    @ViewChild('addSpecialDistinction', { static: true }) modal: ModalDirective;
    @Output() returnItemSelectedEvent: EventEmitter<any[]> = new EventEmitter<any[]>();
    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });
    set records(value: RecordsTable) {
        this._records.next(value);
    }
    get records(): RecordsTable {
        return this._records.value;
    }
    records$(): Observable<RecordsTable> {
        return this._records.asObservable();
    }
    subscriptions: Subscription[] = [];
    @Input() selectedProducts: number[];

    constructor(injector: Injector, private _formService: FormServiceProxy) {
        super(injector);
    }

    ngOnInit(): void {
        this.getSpecialDistintionList({ first: 0, page: 0, pageCount: 10, rows: 0 });
    }

    /**
     * Method that load all the special distintion lists
     */
    private getSpecialDistintionList(pag: PageStatus): void {
        const sub = this._formService
            .getAllPaged(pag.filter, TypeForms.specialDistintion, pag.sorting, pag.pageCount, pag.page)
            .subscribe(
                (res: PagedResultDtoOfFormDto) => {
                    this.records = { countTotal: res.totalCount, records: res.items };
                },
                (err) => {
                    console.error('_formService.getAllPaged()');
                }
            );
        this.subscriptions = [...this.subscriptions, sub];
    }

    onSelectedItemClose(data: boolean): void {
        if (data) {
            this.modal.hide();
        }
    }

    onSelectedItem(data: any): void {
        this.returnItemSelectedEvent.emit(data);
    }

    onShown(): void {}

    show(id?: number): void {
        const self = this;
        self.modal.show();
    }

    close(): void {
        this.modal.hide();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }

    columns: ConfigColumns[] = [
        {
            field: 'title',
            titleLabel: 'SpecialDistintionAdd',
            type: 'string',
        },
        {
            field: 'associatedQuestions',
            titleLabel: 'AssociatedQuestions',
            type: 'string',
        },
    ];
}
