import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { QuestionCategoryServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable, Subscription } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Component({
    selector: 'app-modal-show-category',
    templateUrl: './modal-show-category.component.html',
    animations: [appModuleAnimation()],
})
export class ModalShowCategoryComponent extends AppComponentBase implements OnInit {
    @ViewChild('showCategory', { static: true }) modal: ModalDirective;
    @Input() categoryId: number;
    @Input() categoryTitle: string;
    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });
    set records(value: RecordsTable) {
        this._records.next(value);
    }
    get records(): RecordsTable {
        return this._records.value;
    }
    records$(): Observable<RecordsTable> {
        return this._records.asObservable();
    }
    subscriptions: Subscription[] = [];

    constructor(injector: Injector, private _questionCategoryServiceProxy: QuestionCategoryServiceProxy) {
        super(injector);
    }

    ngOnInit(): void {
        this.loadDataTable = this.loadDataTable.bind(this);
    }

    onShown(): void {}

    show(id?: number): void {
        const self = this;
        self.modal.show();
    }

    close(): void {
        this.modal.hide();
    }

    /**
     * Method that load the associated questions
     */
    loadDataTable(data: PageStatus) {
        return this._questionCategoryServiceProxy.getAllQuestionsByCategoryIdPaged(
            this.categoryId,
            data.filter,
            data.sorting,
            data.pageCount,
            data.page
        );
    }
    columns: ConfigColumns[] = [
        {
            field: 'question',
            titleLabel: 'Question',
            type: 'string',
        },
    ];
}
