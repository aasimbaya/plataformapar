import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionnaireUpsertComponent } from './upsert/upsert.component';
import { QuestionnaireListComponent } from './list/list.component';
import { PageMode } from '@shared/AppEnums';

const routes: Routes = [
    {
        path: '',
        component: QuestionnaireListComponent,
        pathMatch: 'full',
    },
    {
        path: 'add',
        component: QuestionnaireUpsertComponent,
        data: { pageMode: PageMode.Add },
        pathMatch: 'full',
    },
    {
        path: ':id/edit',
        component: QuestionnaireUpsertComponent,
        data: { pageMode: PageMode.Edit },
        pathMatch: 'full',
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class QuestionnaireRoutingModule {}
