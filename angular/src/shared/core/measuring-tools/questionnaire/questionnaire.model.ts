export interface Questionnaire {
    id: number;
    questionaireName: string;
    type: string;
    active: boolean;
    latestModificatedDate: Date;
}
export interface QuestionForQuestionnaire {
    id: Number;
    order: Number;
    question: string;
}
