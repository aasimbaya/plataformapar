import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CoreModule } from '@shared/core/core.module';
import { FormServiceProxy, GetAllDropdownServiceProxy } from '@shared/service-proxies/service-proxies';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { QuestionnaireListComponent } from './list/list.component';
import { QuestionnaireRoutingModule } from './questionnaire-routing.module';
import { QuestionnaireUpsertComponent } from './upsert/upsert.component';
import { ModalAddSpecialDistinctionComponent } from './modals/modal-add-special-distinction/modal-add-special-distinction.component';
import { ModalAddCategoryComponent } from './modals/modal-add-category/modal-add-category.component';
import { ModalShowCategoryComponent } from './modals/modal-show-category/modal-show-category.component';

@NgModule({
    declarations: [QuestionnaireListComponent, QuestionnaireUpsertComponent, ModalAddSpecialDistinctionComponent, ModalAddCategoryComponent, ModalShowCategoryComponent],
    imports: [AppSharedModule, AdminSharedModule, QuestionnaireRoutingModule, CoreModule, NgxDropzoneModule],
    providers: [FormServiceProxy, GetAllDropdownServiceProxy],
})
export class MeasuringQuestionnaireToolsModule {}
