import { Component, Injector, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';
import { PageMode } from '@shared/AppEnums';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus, RecordsTable, ReloadTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
// import { QuestionForQuestionnaire } from '../questionnaire.model';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { TypeForms } from '@shared/core/shared/type-forms';
import {
    FormNameDto,
    FormServiceProxy,
    GetAllDropdownServiceProxy,
    UpsertFormDto,
} from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { typeAssociation } from '../../../shared/type-forms';
import { ModalAddSpecialDistinctionComponent } from '../modals/modal-add-special-distinction/modal-add-special-distinction.component';
import { forEach, forEachRight } from 'lodash-es';
import * as Push from 'push.js';
import { constants } from 'zlib';
import { ModalAddCategoryComponent } from '../modals/modal-add-category/modal-add-category.component';
import { ModalShowCategoryComponent } from '../modals/modal-show-category/modal-show-category.component';

const QUESTIONS_COLLUMS_SCHEMA = [
    {
        field: 'question',
        titleLabel: 'Question',
        type: 'string',
    },
];

/**
 * Main component for questionnaire code create/edit
 */
@Component({
    selector: 'app-questionnaire-upsert',
    templateUrl: './upsert.component.html',
    animations: [appModuleAnimation()],
})
export class QuestionnaireUpsertComponent extends AppComponentBase {
    breadcrumbs: BreadcrumbItem[] = [];

    columns: ConfigColumns[] = QUESTIONS_COLLUMS_SCHEMA;

    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });

    set records(value: RecordsTable) {
        this._records.next(value);
    }
    get records(): RecordsTable {
        return this._records.value;
    }
    records$(): Observable<RecordsTable> {
        return this._records.asObservable();
    }

    loading: boolean = false;
    pageMode: PageMode;

    id: number;
    upsertQuestionnaireDto: UpsertFormDto = new UpsertFormDto();
    idTypeQuestionnaire: number;
    formQuestionnaire = new FormGroup({
        title: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(50)])),
        typeId: new FormControl('', Validators.compose([Validators.required])),
        isActive: new FormControl(''),
    });

    allIputTypes: any;
    subscriptions: Subscription[] = [];
    private _reloadSpecialDistinction: BehaviorSubject<ReloadTable> = new BehaviorSubject({});
    set reloadSpecialDistinction(value: ReloadTable) {
        this._reloadSpecialDistinction.next(value);
    }
    get reloadSpecialDistinction(): ReloadTable {
        return this._reloadSpecialDistinction.value;
    }
    reloadSpecialDistinction$(): Observable<ReloadTable> {
        return this._reloadSpecialDistinction.asObservable();
    }

    private _reloadCategory: BehaviorSubject<ReloadTable> = new BehaviorSubject({});
    set reloadCategory(value: ReloadTable) {
        this._reloadCategory.next(value);
    }
    get reloadCategory(): ReloadTable {
        return this._reloadSpecialDistinction.value;
    }
    reloadCategory$(): Observable<ReloadTable> {
        return this._reloadCategory.asObservable();
    }

    @ViewChild('modalAddSpecialDistinctionComponent', { static: true })
    modalAddSpecialDistinctionComponent: ModalAddSpecialDistinctionComponent;
    @ViewChild('modalAddCategoryComponent', { static: true })
    modalAddCategoryComponent: ModalAddCategoryComponent;
    @ViewChild('modalShowCategoryComponent', { static: true })
    modalShowCategoryComponent: ModalShowCategoryComponent;
    selectedSpecialDistinction: number[] = [];
    selectedCategory: number[] = [];
    category: any;
    categoryId: number;
    categoryTitle: string;

    public dateRange: DateTime[] = [this._dateTimeService.getStartOfDay(), this._dateTimeService.getEndOfDay()];

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _formService: FormServiceProxy,
        private _getAllDropdownService: GetAllDropdownServiceProxy,
        private _dateTimeService: DateTimeService
    ) {
        super(injector);
    }

    /**
     * OnInit
     */
    ngOnInit(): void {
        this._init();
        this.loadDataTableSpecialDistintion = this.loadDataTableSpecialDistintion.bind(this);
        this.loadDataTableCategory = this.loadDataTableCategory.bind(this);
    }

    /**
     * OnInit async method
     */
    private async _init() {
        this.formQuestionnaire.get('isActive').setValue(true);
        this.pageMode = this._activatedRoute.snapshot.data.pageMode;
        this.id = this._activatedRoute.snapshot.params.id;
        this.getQuestionnaireType();
        this.getFormNameId();

        if (this.id) {
            this.getQuestionnaire(this.id);
            this.breadcrumbs = [new BreadcrumbItem(this.l('EditQuestionnaire'))];
        } else {
            this.breadcrumbs = [new BreadcrumbItem(this.l('NewQuestionnaire'))];
        }
    }

    /**
     * Get all the thematics to fill the select
     */
    private getQuestionnaireType(): void {
        this._getAllDropdownService.getAllQuestionnaireType().subscribe((data) => {
            this.allIputTypes = data;
        });
    }

    /**
     * Create a new questionnaire
     */
    newQuestionnaire(): void {
        this.setValues();
        console.log(this.upsertQuestionnaireDto);
        const sub = this._formService.create(this.upsertQuestionnaireDto).subscribe(
            (res: any) => {
                abp.notify.success(this.l('SavedSuccessfully'));
                this._redirectToMeasuringToolsQuestionnaireListPage();
            },
            (err) => {
                console.error('_formService.create');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Update a questionnaire
     */
    updateQuestionnaire(): void {
        this.setValues();
        this.upsertQuestionnaireDto.id = this.id;
        console.log(this.upsertQuestionnaireDto);
        const sub = this._formService.update(this.upsertQuestionnaireDto).subscribe(
            (res: any) => {
                abp.notify.success(this.l('SavedSuccessfully'));
                this._redirectToMeasuringToolsQuestionnaireListPage();
            },
            (err) => {
                console.error('_formService.update');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Set values to the model to proceed add or edit a questionnaire
     */
    private setValues(): void {
        const { value } = this.formQuestionnaire;
        const { title, isActive, typeId } = value;
        this.upsertQuestionnaireDto.title = title;
        this.upsertQuestionnaireDto.description = '';
        this.upsertQuestionnaireDto.isActive = isActive;
        this.upsertQuestionnaireDto.typeId = typeId == '' ? 0 : typeId;
        this.upsertQuestionnaireDto.formNameId = this.idTypeQuestionnaire;
        this.upsertQuestionnaireDto.startDate = this._dateTimeService.getStartOfDayForDate(this.dateRange[0]);
        this.upsertQuestionnaireDto.endDate = this._dateTimeService.getStartOfDayForDate(this.dateRange[1]);
    }

    /**
     * Method helper to disabled or not the save button
     */
    allowSave(): boolean {
        return this.formQuestionnaire.get('title').invalid || this.formQuestionnaire.get('typeId').invalid;
    }

    /**
     * Get a questionnaire by id
     */
    private getQuestionnaire(id: number) {
        const sub = this._formService.getFormById(id).subscribe(
            async (res: UpsertFormDto) => {
                this.loadValues(res);
                this.loadSelectedSpecialDistinction(res);
                this.loadSelectedCategory(res);
            },
            (err) => {
                console.error('_formService.getFormById');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    loadSelectedSpecialDistinction(res): void {
        this.upsertQuestionnaireDto = res;
        let array = res.specialDistinctionIdArray.split(',');
        this.selectedSpecialDistinction = array.map((str) => {
            return Number(str);
        });
    }

    loadSelectedCategory(res): void {
        this.upsertQuestionnaireDto = res;
        let array = res.questionCategoryIdArray.split(',');
        this.selectedCategory = array.map((str) => {
            return Number(str);
        });
    }

    /**
     * Method that redirect to save or update
     */
    saveOnClick(): void {
        if (this.id) {
            this.updateQuestionnaire();
        } else {
            this.newQuestionnaire();
        }
    }

    /**
     * Set the values of a response to the model
     */

    private loadValues(res): void {
        this.formQuestionnaire.controls['title'].setValue(res.title);
        this.formQuestionnaire.controls['isActive'].setValue(res.isActive);
        this.formQuestionnaire.controls['typeId'].setValue(res.typeId);
        this.dateRange = [res.startDate, res.endDate];
    }

    /**
     * Get the id of the form
     */
    private getFormNameId(): void {
        const sub = this._formService.getFormNameByName(TypeForms.questionnaries).subscribe(
            (res: FormNameDto) => {
                this.idTypeQuestionnaire = res.id;
            },
            (err) => {
                console.error('_formService.getFormNameByName');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Action to close and back to questionnaire list page.
     */
    closeOnClick(): void {
        if (!this.formQuestionnaire.pristine) {
            abp.message.confirm(this.l('MsgConfirmClose'), this.l('TitleConfirmClose'), (res: boolean) => {
                if (res) {
                    this._redirectToMeasuringToolsQuestionnaireListPage();
                }
            });
        } else {
            this._redirectToMeasuringToolsQuestionnaireListPage();
        }
    }

    /**
     * Redirect to Questionnaire list page.
     */
    private _redirectToMeasuringToolsQuestionnaireListPage(): void {
        this._router.navigate(['app', 'measuring-tools', 'questionnaire']);
    }

    /**
     *  Destroyed customized behavior when a component is destroyed.
     */
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }

    /**
     * Method that load the associated special distinction
     */
    loadDataTableSpecialDistintion(data: PageStatus) {
        return this._formService.getAllPagedAssociated(
            data.filter,
            this.id,
            typeAssociation.specialDistintion,
            data.sorting,
            data.pageCount,
            data.page
        );
    }

    /**
     * Method that load the associated category
     */
    loadDataTableCategory(data: PageStatus) {
        return this._formService.getAllPagedAssociated(
            data.filter,
            this.id,
            typeAssociation.category,
            data.sorting,
            data.pageCount,
            data.page
        );
    }

    /**
     * Method update associations
     */
    updateAssociated(data: UpsertFormDto, type: number): void {
        const sub = this._formService.update(data).subscribe(
            (res: any) => {
                if (type === typeAssociation.specialDistintion) {
                    this._getInformationSpecialDistinction({ reset: true });
                }
                if (type === typeAssociation.category) {
                    this._getInformationCategory({ reset: true });
                }
            },
            (err) => {
                console.error('_formService.update');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    onSelectedSpecialDistinction(data: any): void {
        var newAssociated = [];
        forEachRight(data, function (value: any) {
            newAssociated.push(value.id);
        });
        this.upsertQuestionnaireDto.specialDistinctionIdArray = String(newAssociated);
        this.upsertQuestionnaireDto.id = this.id;
        this.updateAssociated(this.upsertQuestionnaireDto, typeAssociation.specialDistintion);
    }

    onSelectedCategory(data: any): void {
        var newAssociated = [];
        forEachRight(data, function (value: any) {
            newAssociated.push(value.id);
        });
        this.upsertQuestionnaireDto.questionCategoryIdArray = String(newAssociated);
        this.upsertQuestionnaireDto.id = this.id;
        this.updateAssociated(this.upsertQuestionnaireDto, typeAssociation.category);
    }

    deleteSpecialDistintionOnClick(data): void {
        abp.message.confirm(
            this.l('MsgConfirmDeleteAssociation'),
            this.l('TitleConfirmDeleteAssociation'),
            (result: boolean) => {
                if (result) {
                    this.delete(data.id, typeAssociation.specialDistintion);
                }
            }
        );
    }

    deleteCategoryOnClick(data): void {
        abp.message.confirm(
            this.l('MsgConfirmDeleteAssociation'),
            this.l('TitleConfirmDeleteAssociation'),
            (result: boolean) => {
                if (result) {
                    this.delete(data.id, typeAssociation.category);
                }
            }
        );
    }

    delete(id: number, type: number): void {
        if (type === typeAssociation.specialDistintion) {
            var array = this.upsertQuestionnaireDto.specialDistinctionIdArray.split(',');
            const index = array.indexOf(id.toString());
            array.splice(index, 1);
            this.upsertQuestionnaireDto.specialDistinctionIdArray = String(array);
            this.updateAssociated(this.upsertQuestionnaireDto, typeAssociation.specialDistintion);
        }
        if (type === typeAssociation.category) {
            var array = this.upsertQuestionnaireDto.questionCategoryIdArray.split(',');
            const index = array.indexOf(id.toString());
            array.splice(index, 1);
            this.upsertQuestionnaireDto.questionCategoryIdArray = String(array);
            this.updateAssociated(this.upsertQuestionnaireDto, typeAssociation.category);
        }
    }

    addSpecialDistinctionOnClick(): void {
        this.modalAddSpecialDistinctionComponent.show();
    }

    addCategoryOnClick(): void {
        this.modalAddCategoryComponent.show();
    }

    showCategoryOnClick(data): void {
        debugger;
        this.categoryId = data.id;
        this.categoryTitle = data.title;
        this.modalShowCategoryComponent.show();
    }

    private _getInformationSpecialDistinction(data: ReloadTable): void {
        this.reloadSpecialDistinction = data;
    }

    private _getInformationCategory(data: ReloadTable): void {
        this.reloadCategory = data;
    }

    showAssociated(): boolean {
        return this.pageMode != PageMode.Add;
    }

    columnsSpecialDistintion: ConfigColumns[] = [
        {
            field: 'title',
            titleLabel: 'SpecialDistintionAdd',
            type: 'string',
        },
        {
            field: 'associatedQuestions',
            titleLabel: 'AssociatedQuestions',
            type: 'string',
        },
    ];

    columnsCategory: ConfigColumns[] = [
        {
            field: 'title',
            titleLabel: 'measuringtools_categories_add',
            type: 'string',
        },
        {
            field: 'associatedQuestions',
            titleLabel: 'AssociatedQuestions',
            type: 'string',
        },
    ];
}
