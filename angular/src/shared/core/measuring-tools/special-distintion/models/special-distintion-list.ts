import { stringList } from 'aws-sdk/clients/datapipeline';

export interface SpecialDistintionList {
    id: number;
    name: string;
    active: boolean;
    associatedQuestion: number;
    lastUpdatedOn: Date;
}
