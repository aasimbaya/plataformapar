import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PageStatus, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpecialDistintionService {
    constructor(private _http: HttpClient) {}

    getData(pag: PageStatus): Observable<RecordsTable> {
        const data = {
            countTotal: 4,
            records: [
                {
                    id: 1,
                    name: 'Mauricio Marcano',
                    email: '0508mauricio@gmail.com',
                },
                {
                    id: 2,
                    name: 'Pedro Cadevilla',
                    email: 'pedrocadevilla@gmail.com',
                }
            ],
        };
        return of(data);
    }

    getAssociatedQuestions(pag: PageStatus): Observable<RecordsTable> {
        const data = {
            countTotal: 4,
            records: [
                {
                    id: 1,
                    order: 1,
                    question: 'Pregunta 1'

                },
            ],
        };
        return of(data);
    }
}
