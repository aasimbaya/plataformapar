import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus, RecordsTable, ReloadTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { FormServiceProxy } from '@shared/service-proxies/service-proxies';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { SpecialDistintionList } from '../models/special-distintion-list';
import { TypeForms } from '@shared/core/shared/type-forms';

@Component({
    selector: 'app-special-distintion-list',
    templateUrl: './special-distintion-list.component.html',
    animations: [appModuleAnimation()],
})

/**
 * Main page of a Special Distintion
 */
export class SpecialDistintionListComponent extends AppComponentBase implements OnInit {
    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });
    set records(value: RecordsTable) {
        this._records.next(value);
    }
    get records(): RecordsTable {
        return this._records.value;
    }
    records$(): Observable<RecordsTable> {
        return this._records.asObservable();
    }
    subscriptions: Subscription[] = [];

    private _reload: BehaviorSubject<ReloadTable> = new BehaviorSubject({});
    set reload(value: ReloadTable) {
        this._reload.next(value);
    }
    get reload(): ReloadTable {
        return this._reload.value;
    }
    reload$(): Observable<ReloadTable> {
        return this._reload.asObservable();
    }
    pageStatus: PageStatus = { filter: '', page: 0, pageCount: 10, rows: 0 };

    constructor(injector: Injector, private _formService: FormServiceProxy, private router: Router) {
        super(injector);
    }

    ngOnInit(): void {
        this.loadDataTable = this.loadDataTable.bind(this);
    }

    /**
     * Return all the special distintions
     */

    loadDataTable(data: PageStatus) {
        return this._formService.getAllPaged(
            data.filter,
            TypeForms.specialDistintion,
            data.sorting,
            data.pageCount,
            data.page
        );
    }

    /**
     * Method when click edit on the list
     */
    editOnClick(data: SpecialDistintionList): void {
        this.router.navigate(['app', 'measuring-tools', 'special-distintion', data.id, 'edit']);
    }

    /**
     * Method for add a new special distintion
     */
    addOnClick(): void {
        this.router.navigate(['app', 'measuring-tools', 'special-distintion', 'add']);
    }

    /**
     * Method message for deleted a special distintion
     */

    deleteOnClick(data: SpecialDistintionList): void {
        abp.message.confirm(this.l('MsgConfirmDeleteFormSpecial'), this.l('TitleConfirmDeleteFormSpecial'), (result: boolean) => {
            if (result) {
                this.delete(data.id);
            }
        });
    }

    /**
     * Method delete a special distintion
     */
    private delete(id: number) {
        const sub = this._formService.deleteFormById(id).subscribe(
            (res: any) => {
                this._getInformation({ reset: true });
                abp.notify.success(this.l('SuccessfullyDeleted'));
            },
            (err) => {
                console.error('_formServiceProxy.delete');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    private _getInformation(data: ReloadTable): void {
        this.reload = data;
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }

    columns: ConfigColumns[] = [
        {
            field: 'title',
            titleLabel: 'TitleSpecialDistintion',
            type: 'string',
        },
        {
            field: 'isActive',
            titleLabel: 'IsActive',
            type: 'boolean',
        },
        {
            field: 'associatedQuestions',
            titleLabel: 'AssociatedQuestions',
            type: 'string',
        },
        {
            field: 'lastModificationTime',
            titleLabel: 'LastModificationTime',
            type: 'date',
        },
    ];
}
