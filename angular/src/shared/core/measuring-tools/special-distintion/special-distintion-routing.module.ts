import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageMode } from '@shared/AppEnums';
import { SpecialDistintionUpsertComponent } from './special-distintion-upsert/special-distintion-upsert.component';
import { SpecialDistintionListComponent } from './special-distintion-list/special-distintion-list.component';

const routes: Routes = [
    {
        path: '',
        component: SpecialDistintionListComponent,
        pathMatch: 'full',
        },
        {
            path: 'add',
            component: SpecialDistintionUpsertComponent,
            data: { pageMode: PageMode.Add },
            pathMatch: 'full'
        },
        {
            path: ':id/edit',
            component: SpecialDistintionUpsertComponent,
            data: { pageMode: PageMode.Edit },
            pathMatch: 'full'
        },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecialDistintionRoutingModule {
}
