import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ConfigColumns, PageStatus, RecordsTable, ReloadTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { FormNameDto, FormServiceProxy, GetAllDropdownServiceProxy, PagedResultDtoOfQuestionsSpecialDistintionListItemDto, QuestionsBankServiceProxy, QuestionsSpecialDistintionListItemDto } from '@shared/service-proxies/service-proxies';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { SpecialDistintionService } from '../service/special-distintion.service';
import { UpsertFormDto } from '../../../../service-proxies/service-proxies';
import { PageMode } from '@shared/AppEnums';
import { TypeForms } from '@shared/core/shared/type-forms';

@Component({
    selector: 'app-special-distintion-upsert',
    templateUrl: './special-distintion-upsert.component.html',
    styleUrls: ['./special-distintion-upsert.component.css'],
    animations: [appModuleAnimation()],
})

/**
 * Class to create and update a Special Distintion
 */
export class SpecialDistintionUpsertComponent extends AppComponentBase implements OnInit {
    allIputTypes: any;
    files: File[] = [];
    upsertSpecialDistintion: UpsertFormDto = new UpsertFormDto();
    id: number;
    pageMode: PageMode;
    formSpecialDistintion = new FormGroup({
        title: new FormControl('', Validators.compose([Validators.required, Validators.maxLength(50)])),
        thematicId: new FormControl('', Validators.compose([Validators.required])),
        isActive: new FormControl(''),
        description: new FormControl('', Validators.compose([Validators.maxLength(150)])),
        icon: new FormControl('', Validators.compose([Validators.required])),
    });

    private _records: BehaviorSubject<RecordsTable> = new BehaviorSubject({
        countTotal: 0,
        records: [],
    });
    idTypeSpecialDistintion: number;
    set records(value: RecordsTable) {
        this._records.next(value);
    }
    get records(): RecordsTable {
        return this._records.value;
    }
    records$(): Observable<RecordsTable> {
        return this._records.asObservable();
    }

    private _reload: BehaviorSubject<ReloadTable> = new BehaviorSubject({});
    set reload(value: ReloadTable) {
        this._reload.next(value);
    }
    get reload(): ReloadTable {
        return this._reload.value;
    }
    reload$(): Observable<ReloadTable> {
        return this._reload.asObservable();
    }


    subscriptions: Subscription[] = [];

    constructor(
        injector: Injector,
        private _specialDistintionService: SpecialDistintionService,
        private _formService: FormServiceProxy,
        private _dropdownService: GetAllDropdownServiceProxy,
        private router: Router,
        private _activatedRoute: ActivatedRoute,
        private _questionsBankService:  QuestionsBankServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.formSpecialDistintion.get('isActive').setValue(true);
        this.pageMode = this._activatedRoute.snapshot.data.pageMode;
        this.id = this._activatedRoute.snapshot.params.id;
        this.getFormNameId();
        this.getSpecialDistintionList({ first: 0, page: 0, pageCount: 10, rows: 0 });
        this.getThematics();
        this.getSpecialDistintion(this.id);
        this.loadDataTable = this.loadDataTable.bind(this);
    }

    loadDataTable(data: PageStatus) {
        debugger
        return this._questionsBankService.getAllPagedQuestionsSpecialDistition(
            data.filter,
            this.id,
            data.sorting,
            data.pageCount,
            data.page
        );
    }

    /**
     * Get all the thematics to fill the select
     */

    private getThematics(): void {
        this._dropdownService.getAllThematics().subscribe((data) => {
            this.allIputTypes = data;
            console.log('temativas', data);
            console.log('temativas2',  this.allIputTypes);
        });
    }

    /**
     * Method to get all the associated questions
     */
    private getSpecialDistintionList(pag: PageStatus): void {
        const sub = this._specialDistintionService.getAssociatedQuestions(pag).subscribe(
            (res: RecordsTable) => {
                this.records = res;
            },
            (err) => {
                console.error('_measuringToolsService.getData()');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Create a new special distintion
     */
    newSpecialDistintion(): void {
        this.setValues();
        const sub = this._formService.create(this.upsertSpecialDistintion).subscribe(
            (res: any) => {
                abp.notify.success(this.l('SavedSuccessfully'));
                this.goBack();
            },
            (err) => {
                console.error('_formServiceProxy.createOrEdit');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Update a special distintion
     */
    updateSpecialDistintion(): void {
        this.setValues();
        this.upsertSpecialDistintion.id = this.id;
        const sub = this._formService.update(this.upsertSpecialDistintion).subscribe(
            (res: any) => {
                abp.notify.success(this.l('SuccessfullyUpdated'));
                this.goBack();
            },
            (err) => {
                console.error('_formServiceProxy.createOrEdit');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Set values to the model to proceed add or edit a special distintion
     */
    private setValues(): void {
        const { value } = this.formSpecialDistintion;
        const { title, isActive, description, thematicId } = value;
        this.upsertSpecialDistintion.title = title;
        this.upsertSpecialDistintion.isActive = isActive;
        this.upsertSpecialDistintion.description = description;
        this.upsertSpecialDistintion.thematicId = thematicId;
        this.upsertSpecialDistintion.formNameId = this.idTypeSpecialDistintion;
    }

    /**
     * Method helper to disabled or not the save button
     */
    allowSave(): boolean {
        return this.formSpecialDistintion.get('title').invalid || this.formSpecialDistintion.get('thematicId').invalid;
    }

    /**
     * Get a special distintion by id
     */
    private getSpecialDistintion(id: number) {
        const sub = this._formService.getFormById(id).subscribe(
            async (res: UpsertFormDto) => {
                this.loadValues(res);
            },
            (err) => {
                console.error('_formServiceProxy.get');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Method that redirect to save or update
     */
    save(): void {
        if (this.id) {
            this.updateSpecialDistintion();
        } else {
            this.newSpecialDistintion();
        }
    }

    /**
     * Set the values of a response to the model
     */

    private loadValues(res): void {
        this.formSpecialDistintion.controls['title'].setValue(res.title);
        this.formSpecialDistintion.controls['isActive'].setValue(res.isActive);
        this.formSpecialDistintion.controls['description'].setValue(res.description);
        this.formSpecialDistintion.controls['thematicId'].setValue(res.thematicId);
    }

    /**
     * Get the id of the form
     */
    private getFormNameId(): void {
        const sub = this._formService.getFormNameByName(TypeForms.specialDistintion).subscribe(
            (res: FormNameDto) => {
                this.idTypeSpecialDistintion = res.id;
            },
            (err) => {
                console.error('_formServiceProxy.get');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * Paginator method
     */
    pagingChange(event: PageStatus) {
        this.getSpecialDistintionList(event);
    }

    /**
     * Method for the dropzone upload
     */
    onSelect(event) {
        this.files.push(...event.addedFiles);
    }

    /**
     * Method for the dropzone upload
     */
    onRemove(event) {
        this.files.splice(this.files.indexOf(event), 1);
    }

    /**
     * Return to the previews page
     */
    goBack(): void {
        this.router.navigate(['app', 'measuring-tools', 'special-distintion']);
    }

    deleteOnClick(data: QuestionsSpecialDistintionListItemDto): void {
        abp.message.confirm(this.l('MsgConfirmDeleteAssociation'), this.l('TitleConfirmDeleteAssociation'), (result: boolean) => {
            if (result) {
                this.delete(data.id);

            }
        });
    }

    /**
     * Method delete a special distintion question
     */
     private delete(id: number) {
        const sub = this._questionsBankService.delete(id).subscribe(
            (res: any) => {
                this._getInformation({ reset: true });
                abp.notify.success(this.l('SuccessfullyDeleted'));
            },
            (err) => {
                console.error('_formServiceProxy.delete');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }
        /**
     * Return if the questions should be show
     */
    showQuestionsAssociated(): boolean {
        return this.pageMode != PageMode.Add;
    }

    private _getInformation(data: ReloadTable): void {
        this.reload = data;
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }

    columns: ConfigColumns[] = [
        {
            field: 'order',
            titleLabel: 'Order',
            type: 'string',
        },
        {
            field: 'question',
            titleLabel: 'Question',
            type: 'string',
        },
    ];
}
