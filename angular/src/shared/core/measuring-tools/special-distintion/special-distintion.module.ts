import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecialDistintionRoutingModule } from './special-distintion-routing.module';
import { SpecialDistintionListComponent } from './special-distintion-list/special-distintion-list.component';
import { SpecialDistintionUpsertComponent } from './special-distintion-upsert/special-distintion-upsert.component';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { CoreModule } from '@shared/core/core.module';
import { NgxDropzoneModule } from 'ngx-dropzone';


@NgModule({
  declarations: [SpecialDistintionListComponent, SpecialDistintionUpsertComponent],
  imports: [
    AppSharedModule, AdminSharedModule, CommonModule,
    SpecialDistintionRoutingModule, CoreModule, NgxDropzoneModule
  ]
})
export class SpecialDistintionModule { }
