import { Component, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { filter as _filter } from 'lodash-es';
import { debounce as _debounce } from 'lodash-es';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';

interface OptionsMenu {
    index: string,
    titleLabel: string,
    active: boolean
}
@Component({
    selector: 'player-container',
    templateUrl: './player-container.component.html',
    styleUrls: ['./player-container.component.less'],
    animations: [appModuleAnimation()],
})
export class PlayerContainerComponent extends AppComponentBase {
    breadcrumbs: BreadcrumbItem[] = [
        new BreadcrumbItem(this.l('DashboardCompanyBusiness'))
    ];
    optionOpened = "";
    height = 250;
    width = 500;
    constructor(
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnDestroy(): void {
    }
    close(): void {
        this.optionOpened = "";
        this.height = 250;
        this.width = 500;
    }
    open(option: string): void {
        if (this.optionOpened != '') return;
        this.optionOpened = option;
        this.height = 500;
        this.width = 750;
    }
}
