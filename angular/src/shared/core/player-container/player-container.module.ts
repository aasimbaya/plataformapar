import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { PlayerContainerRoutingModule } from './player-container-routing.module';
import { PlayerContainerComponent } from './player-container.component';
import { YouTubePlayerModule } from "@angular/youtube-player";
import { VideoFileComponent } from './video-file/video-file.component';
import { VideoYoutuberComponent } from './video-youtube/video-youtube.component';

@NgModule({
    declarations: [PlayerContainerComponent, VideoFileComponent, VideoYoutuberComponent],
    imports: [AppSharedModule, AdminSharedModule, PlayerContainerRoutingModule, YouTubePlayerModule],
})
export class PlayerContainerModule {}
