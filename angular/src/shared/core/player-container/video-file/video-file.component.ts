import { Component, Injector, Input } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
@Component({
    selector: 'video-file',
    styleUrls: ['./video-file.component.less'],
    templateUrl: './video-file.component.html',
    animations: [appModuleAnimation()],
})
export class VideoFileComponent extends AppComponentBase {
    @Input() src: string;
    @Input() height: number;
    @Input() width: number;

    constructor(
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnDestroy(): void {
    }
}
