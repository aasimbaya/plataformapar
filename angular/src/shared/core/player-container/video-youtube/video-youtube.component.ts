import { Component, Injector, Input } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
@Component({
    selector: 'video-youtube',
    templateUrl: './video-youtube.component.html',
    animations: [appModuleAnimation()],
})
export class VideoYoutuberComponent extends AppComponentBase {
    @Input() videoId: string;
    @Input() suggestedQuality: string;
    @Input() height: number;
    @Input() width: number;
    @Input() startSeconds: number;
    @Input() endSeconds: number;
    constructor(
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnDestroy(): void {
    }
}
