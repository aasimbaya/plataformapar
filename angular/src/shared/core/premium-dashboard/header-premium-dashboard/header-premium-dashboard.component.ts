import { Component, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

interface OptionsMenu {
    index: string,
    titleLabel: string,
    active: boolean
}
@Component({
    selector: 'header-premium-dashboard',
    templateUrl: './header-premium-dashboard.component.html',
    styleUrls: ['./header-premium-dashboard.component.less'],
    animations: [appModuleAnimation()],
})
export class HeaderPremiumDashboardComponent extends AppComponentBase {
    
    constructor(
        injector: Injector,
    ) {
        super(injector);
    }
    
    ngOnDestroy(): void {
    }
}
