export interface Pokedex {
    count:    number;
    next:     string;
    previous: string;
    results:  Result[];
}

export interface Result {
    name: string;
    url:  string;
}
export interface OptionsMenu {
    index: string,
    titleLabel: string,
    active: boolean
}