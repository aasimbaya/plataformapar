import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CoreModule } from '../core.module';
import { DynamicGraficComponent } from '../dynamic-grafic/dynamic-grafic.component';
import { HeaderPremiumDashboardComponent } from './header-premium-dashboard/header-premium-dashboard.component';
import { PremiumDashboardRoutingModule } from './premium-dashboard-routing.module';
import { PremiumDashboardComponent } from './premium-dashboard.component';

@NgModule({
    declarations: [PremiumDashboardComponent, HeaderPremiumDashboardComponent],
    imports: [AppSharedModule, AdminSharedModule, PremiumDashboardRoutingModule, CoreModule],
})
export class PremiumDashboardModule {}
