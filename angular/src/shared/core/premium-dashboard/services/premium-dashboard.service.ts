import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of, BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PremiumDashboardService {
  constructor(
  ) { }
  getDataScore(): Observable<any[]> {
    const data: any[] =
      [
        {        
          nameCompany:"Company 1",
          ScoreTotal: 80,
          ObejtiveManagement: 73,
          TalentManagement: 49,
          Culture: 80,
          Estructure: 36
        },
        {
          nameCompany:"Company 2",
          ScoreTotal: 60,
          ObejtiveManagement: 57,
          TalentManagement: 26,
          Culture: 62,
          Estructure: 61
        },
      ];

    return of(data);
  }
  getDataRepresentation(): Observable<any[]> {
    const data: any[] =
      [
        {        
          nameCompany:"Company 1",
          JuntaDirectiva: 63,
          PrimerNivl: -1,
          Segundo: 43,
          TErcero: 73,
          Cuarto: 36
        },
        {
          nameCompany:"Company 2",
          JuntaDirectiva: 60,
          PrimerNivl: -1,
          Segundo: 47,
          TErcero: 62,
          Cuarto: 61
        },
      ];

    return of(data);
  }
  getDataPromotions(): Observable<any[]> {
    const data: any[] =
      [
        {        
          nameCompany:"Company 1",
          Total: 62,
          JuntaDirectiva: 63,
          PrimerNivl: 73,
          Segundo: 49,
          TErcero: 70,
          Cuarto: 26
        },
        {
          nameCompany:"Company 2",
          Total: 80,
          JuntaDirectiva: 60,
          PrimerNivl: 57,
          Segundo: 47,
          TErcero: 62,
          Cuarto: 61
        },
      ];

    return of(data);
  }
  getDataHiring(): Observable<any[]> {
    const data: any[] =
      [
        {        
          nameCompany:"Company 1",
          Total: 62,
          JuntaDirectiva: 63,
          PrimerNivl: 73,
          Segundo: 36,
          TErcero: 70,
          Cuarto: 36
        },
        {
          nameCompany:"Company 2",
          Total: 63,
          JuntaDirectiva: 60,
          PrimerNivl: 57,
          Segundo: 80,
          TErcero: 62,
          Cuarto: 61
        },
      ];

    return of(data);
  }
  getDataRetreats(): Observable<any[]> {
    const data: any[] =
      [
        {        
          nameCompany:"Company 1",
          Total: 62,
          JuntaDirectiva: 80,
          PrimerNivl: 73,
          Segundo: 36,
          TErcero: 70,
          Cuarto: 2
        },
        {
          nameCompany:"Company 2",
          Total: 63,
          JuntaDirectiva: 60,
          PrimerNivl: 57,
          Segundo: 80,
          TErcero: 62,
          Cuarto: 4
        },
      ];

    return of(data);
  }
  getDataSalary(): Observable<any[]> {
    const data: any[] =
      [
        {        
          nameCompany:"Company 1",
          Total: {gender:'h', value:70},
          JuntaDirectiva: {gender:'h', value:70},
          PrimerNivl: {gender:'m', value:70},
          Segundo: {gender:'h', value:45},
          TErcero: {gender:'h', value:70},
          Cuarto: {gender:'m', value:56}
        },
        {        
          nameCompany:"Company 1",
          Total: {gender:'h', value:63},
          JuntaDirectiva: {gender:'h', value:60},
          PrimerNivl: {gender:'m', value:57},
          Segundo: {gender:'h', value:45},
          TErcero: {gender:'h', value:70},
          Cuarto: {gender:'m', value:80}
        }
      ];

    return of(data);
  }
}
