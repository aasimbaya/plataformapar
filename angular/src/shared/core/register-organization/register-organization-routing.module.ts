import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterOrganizationComponent } from './register-organization.component';

const routes: Routes = [
    {
        path: '',
        component: RegisterOrganizationComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RegisterOrganizationRoutingModule {}
