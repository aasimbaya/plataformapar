import { AfterViewInit, Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { map as _map, filter as _filter } from 'lodash-es';
import {
    CreateOrUpdateAnonymousUserInput,
    DropdownItemDto,
    GetAllDropdownServiceProxy,
    ValidatorServiceProxy,
    OrganizationUnitDto,
    PasswordComplexitySetting,
    SendPasswordResetCodeInput,
    TenantServiceProxy,
    UpsertTenantDto,
    UserAnonymousServiceProxy,
    UserEditDto,
    UserRoleDto,
    UpsertEnterpriseDto,
    UpsertEnterpriseGroupDto,
    UpsertPersonalDto,
    UpsertCountryDto,
    IPasswordComplexitySetting,
    UpsertOrganizationDataInputDto,
} from '@shared/service-proxies/service-proxies';
import { finalize, take } from 'rxjs/operators';
import { StepperComponent } from '@shared/core/stepper/stepper/stepper.component';
import { TenantNameEnum } from '../shared/tenant-name-enum';
import { LazyLoadEvent } from 'primeng/api';
import { Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoginService } from '@account/login/login.service';
import { TradeAssociationAllowedCountriesEnum } from '../shared/enumerators';
import { environment } from 'environments/environment';
import { CountryISO, PhoneNumberFormat, SearchCountryField } from 'ngx-intl-tel-input';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    templateUrl: './register-organization.component.html',
    styleUrls: ['./register-organization.component.less'],
    animations: [accountModuleAnimation()],
})
export class RegisterOrganizationComponent extends AppComponentBase implements OnInit, AfterViewInit {
    @ViewChild(StepperComponent) myStepper!: StepperComponent;
    @ViewChild('submitDataAccess') SubmitDataAccess: ElementRef<HTMLButtonElement>;
    model: SendPasswordResetCodeInput = new SendPasswordResetCodeInput();

    saving = false;
    emailDomainsBlacList: DropdownItemDto[] = [];
    // Data validations
    onlyAlphaNumeric = '[A-Za-z0-9 ]+$';
    onlyNumeric = '[0-9]+$';
    onlyAlphaSpace = '[a-zA-Z\u00C0-\u017Fs ]+';
    validationOrganizationId = this.onlyNumeric;
    // Phone numbers resources
    SearchCountryField = SearchCountryField;
    CountryISO = CountryISO;
    PhoneNumberFormat = PhoneNumberFormat;
    phoneLengthByCountry = 0;

    // Default stepper configuration
    stepperSetup = {
        stepUserType: {
            title: this.l('UserType'),
            isValid: true,
            hidden: false,
        },
        stepAccessData: {
            title: this.l('AccessData'),
            isValid: false,
            hidden: false,
        },
        stepYourGroupData: {
            title: this.l('YourGroupData'),
            isValid: false,
            hidden: true,
        },
        stepYourOrganizationsGroupData: {
            title: this.l('YourOrganizationsGroupData'),
            isValid: false,
            hidden: true,
        },
        stepYourOrganizationData: {
            title: this.l('YourOrganizationData'),
            isValid: false,
            hidden: false,
        },
        stepContactData: {
            title: this.l('ContactData'),
            isValid: false,
            hidden: false,
        },
        stepTermsAndConditions: {
            title: this.l('AdditionalInformation'),
            isValid: false,
            hidden: false,
        },
    };

    // Tenant object
    tenant: UpsertTenantDto;

    // Organization type: Enterprise by default
    isOrganizationTypeGroup = false;

    // User data properties
    canChangeUserName = true;
    isTwoFactorEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.TwoFactorLogin.IsEnabled');
    isLockoutEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.UserLockOut.IsEnabled');
    passwordComplexity: IPasswordComplexitySetting = {
        requireDigit: true,
        requiredLength: 8,
        requireLowercase: true,
        requireNonAlphanumeric: true,
        requireUppercase: true,
    };

    passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting(this.passwordComplexity);
    user: UserEditDto = new UserEditDto();
    roles: UserRoleDto[];
    sendActivationEmail = true;
    setRandomPassword = false;
    passwordComplexityInfo = '';
    profilePicture: string;
    allowedUserNameCharacters = '';
    isSMTPSettingsProvided = false;
    allOrganizationUnits: OrganizationUnitDto[];
    memberedOrganizationUnits: string[];
    userPasswordRepeat = '';
    // Organization group data
    enterpriseGroup: UpsertEnterpriseGroupDto = new UpsertEnterpriseGroupDto();
    countries: UpsertCountryDto[] = [];
    regions: DropdownItemDto[] = [];
    cities: DropdownItemDto[] = [];
    sectors: DropdownItemDto[] = [];
    selectedGroupCountry: UpsertCountryDto;
    selectedGroupCountryId: number;
    selectedGroupRegionId: number;
    selectedGroupCityId: number;
    selectedGroupSectorId: number;

    // Organization data
    enterprisesList: UpsertEnterpriseDto[] = [];
    enterpriseListToSave: any[] = [];
    singleEnterprise: UpsertEnterpriseDto = new UpsertEnterpriseDto();
    selectedEnterpriseCountry: UpsertCountryDto;
    selectedEnterpriseCountryAb: string;
    selectedEnterpriseCountryId: number;
    selectedEnterpriseRegionId: number;
    selectedEnterpriseCityId: number;
    selectedEnterpriseSectorId: number;
    collaborators: DropdownItemDto[] = [];
    selectedEnterpriseCollaboratorId: number;

    // Contact data
    showPersonDataForm: boolean = false;
    surveyedPersonData: UpsertPersonalDto = new UpsertPersonalDto();
    ceoPersonData: UpsertPersonalDto = new UpsertPersonalDto();
    hrPersonData: UpsertPersonalDto = new UpsertPersonalDto();
    personalPositions: DropdownItemDto[] = [];
    personalTypes: DropdownItemDto[] = [];
    personalForm: FormGroup;

    // Finish Register
    receivePromoAccepted = false;
    termsConditionsAccepted = false;
    isPartOfEconomicGroupAcepted = false;
    isPartOfTradeAssociationAccepted = false;
    tradeAssociations: DropdownItemDto[] = [];
    selectedTradeAssociationId: number;
    economicGroupName: string;
    showTradeAsociationOptions: boolean = false;
    PolicyUseDataAndInformationClients = environment.PolicyUseDataAndInformationClients;
    TermsAndConditions = environment.TermsAndConditions;
    isAcceptedTerms: boolean = false;

    organizationDataInput : UpsertOrganizationDataInputDto = new UpsertOrganizationDataInputDto();

    optionsEnterpriseTypeId = [];
    onlyCountries = AppConsts.onlyCountries;
    constructor(
        injector: Injector,
        private _userService: UserAnonymousServiceProxy,
        private _tenantService: TenantServiceProxy,
        private _dropDownsService: GetAllDropdownServiceProxy,
        private _validatorServiceProxy: ValidatorServiceProxy,
        private _router: Router,
        public loginService: LoginService
    ) {
        super(injector);
        this.optionsEnterpriseTypeId = AppConsts.OptionsEnterpriseTypeId.map((e) => {
            return { ...e, name: this.l(e.name) };
        });
    }

    ngAfterViewInit(): void {
        this.getEmailDomainsBlackList();
        this.getTenantByName(TenantNameEnum.Enterprise);
        this.getCountries();
        this.getSectors();
        this.getPersonalTypes();
        this.getPositions();
        this.getCollaborators();
        this.enterpriseGroup.enterpriseNumber = 1;
        this.singleEnterprise.employeesNumber = 1;
        this.personalForm = new FormGroup({
            surveyedName: new FormControl(this.surveyedPersonData?.name || '', [
                Validators.required,
                Validators.pattern(this.onlyAlphaSpace),
            ]),
            surveyedEmail: new FormControl(this.surveyedPersonData?.email || '', [Validators.required]),
            surveyedPositionId: new FormControl(this.surveyedPersonData?.positionId || '', [Validators.required]),
            surveyedPhone: new FormControl(this.surveyedPersonData?.cellPhone || '', [Validators.required]),
            hrName: new FormControl(this.hrPersonData?.name || '', [
                Validators.required,
                Validators.pattern(this.onlyAlphaSpace),
            ]),
            hrEmail: new FormControl(this.hrPersonData?.email || '', [Validators.required]),
            hrPositionId: new FormControl(this.hrPersonData?.positionId || '', [Validators.required]),
            hrPhone: new FormControl(this.hrPersonData?.cellPhone || '', [Validators.required]),
            ceoName: new FormControl(this.ceoPersonData?.name || '', [
                Validators.required,
                Validators.pattern(this.onlyAlphaSpace),
            ]),
            ceoEmail: new FormControl(this.ceoPersonData?.email || '', [Validators.required]),
        });
    }

    ngOnInit(): void {
        this.getPositions = this.getPositions.bind(this);
    }

    /**
     * Allow set valid status for next step
     *
     * @param currentStep
     * @return
     */
    async stepChanged(steps: any) {
        this.saving = true;
        let currentStep = steps.findIndex((x) => x._isActive);
        if (currentStep === 0) {
            this.stepperSetup.stepAccessData.isValid = true;
        }

        if (currentStep === 1) {
            this.stepperSetup.stepAccessData.isValid = true;
            if (!this.isEmailDomainValid(this.user.emailAddress)) {
                this.notify.error(this.l('EmailDomainAllowed'));
                return;
            }

            this._userService.anonymousUserExist(this.user.emailAddress).subscribe((exist) => {
                if (!exist) {
                    let defaultRol: UserRoleDto = new UserRoleDto();
                    defaultRol.roleName = this.getRoleByTenantCurrent();
                    this.roles = [];
                    this.roles.push(defaultRol);

                    if (!this.isOrganizationTypeGroup) {
                        this.enterprisesList.push(this.singleEnterprise);
                    }

                    let input = new CreateOrUpdateAnonymousUserInput();
                    input.tenantId = this.tenant.id;
                    input.user = this.user;
                    input.user.userName = input.user.name; // Passing by validations only, username generated in the BL
                    input.sendActivationEmail = this.sendActivationEmail;
                    input.assignedRoleNames = this.roles.map(role => role.roleName);
                    input.assignedRoleNames.length == 0 ? [this.getRoleByTenantCurrent()] : input.assignedRoleNames;

                    //Associate user for register organization
                    this.organizationDataInput.anonymousUserInput = input;

                    this.surveyedPersonData.name = `${this.user.name} ${this.user.surname}`;
                    this.surveyedPersonData.email = `${this.user.emailAddress}`;
                    this.saving = false;
                    this.MoveToNextStep();
                } else {
                    this.saving = false;
                    return this.showIdNumberExistsMessage(this.l('EmailExist'));
                }
            });
        }
        if (currentStep === 2) {   
            this.enterpriseListToSave = [];

            if (!this.isOrganizationTypeGroup) {
                this.stepperSetup.stepYourGroupData.isValid = false;
                this.stepperSetup.stepYourOrganizationData.isValid = true;
                this.stepperSetup.stepContactData.isValid = true;
                if (
                    this.singleEnterprise.inclusionRegionalManagerEmail &&
                    !this.isEmailDomainValid(this.singleEnterprise.inclusionRegionalManagerEmail)
                ) {
                    this.notify.error(this.l('EmailDomainAllowed'));
                    return;
                }

                const idNumberExists = await this.validateEnterpriseNumber(this.singleEnterprise.idNumber);
                if (idNumberExists) {
                    this.saving = false;
                    return this.showIdNumberExistsMessage(this.l('EnterpriseAlreadyCreated'));
                }

                if (!this.isOrganizationTypeGroup && this.singleEnterprise && this.singleEnterprise.name) {
                    this.singleEnterprise.sectorId = this.selectedEnterpriseSectorId;
                    this.singleEnterprise.countryId = this.selectedEnterpriseCountryId;
                    this.singleEnterprise.regionId = this.selectedEnterpriseRegionId;
                    this.singleEnterprise.cityId = this.selectedEnterpriseCityId;
                    this.singleEnterprise.isActive = true;
                    this.singleEnterprise.isPartOfEconomicGroup = false;
                    this.singleEnterprise.isPartOfTradeAssociation = false;
                    this.singleEnterprise.economicGroupName = '';
                    this.singleEnterprise.employeesNumber = this.selectedEnterpriseCollaboratorId;

                    this.enterpriseListToSave.push(this.singleEnterprise);
                    this.showPersonDataForm = true;
                }
                this.personalForm.controls['surveyedName'].setValue(this.organizationDataInput.anonymousUserInput.user.userName);
                this.personalForm.controls['surveyedEmail'].setValue(this.user.emailAddress);
            } else {
                this.stepperSetup.stepYourGroupData.isValid = true;
                this.stepperSetup.stepYourOrganizationData.isValid = false;
                this.stepperSetup.stepContactData.isValid = false;

                if (
                    this.enterpriseGroup.inclusionRegionalManagerEmail &&
                    !this.isEmailDomainValid(this.enterpriseGroup.inclusionRegionalManagerEmail)
                ) {
                    this.notify.error(this.l('EmailDomainAllowed'));
                    return;
                }

                this.enterpriseGroup.isActive = true;
                for (let i = 0; i < this.enterpriseGroup.enterpriseNumber; i++) {
                    let enterprise = new UpsertEnterpriseDto();
                    enterprise.isActive = true;
                    enterprise.employeesNumber = 0;
                    this.enterpriseListToSave.push(enterprise);
                }

                console.log(this.enterpriseListToSave);

                this.organizationDataInput.anonymousUserInput.enterpriseGroup = this.enterpriseGroup;
            }

            this.organizationDataInput.anonymousUserInput.enterprises = this.enterpriseListToSave;
            this.saving = false;
            this.MoveToNextStep();
        }
        if (currentStep === 3) {
            this.stepperSetup.stepYourOrganizationsGroupData.isValid = true;
            this.organizationDataInput.personsDataInput = [];

            if (!this.isOrganizationTypeGroup) {
                if (
                    !this.isEmailDomainValid(this.personalForm.value.surveyedEmail.toString()) ||
                    !this.isEmailDomainValid(this.personalForm.value.hrEmail.toString()) ||
                    !this.isEmailDomainValid(this.personalForm.value.ceoEmail.toString())
                ) {
                    this.notify.error(this.l('EmailDomainAllowed'));
                    return;
                }

                // Surveyed data
                this.surveyedPersonData.name = this.personalForm.value.surveyedName;
                this.surveyedPersonData.email = this.personalForm.value.surveyedEmail;
                this.surveyedPersonData.cellPhone = this.personalForm.value.surveyedPhone.e164Number;
                this.surveyedPersonData.positionId = this.personalForm.value.surveyedPositionId;
                this.ceoPersonData.enterpriseId = this.singleEnterprise.id;
                this.ceoPersonData.isActive = true;                
                // HR data
                this.hrPersonData.name = this.personalForm.value.hrName;
                this.hrPersonData.email = this.personalForm.value.hrEmail;
                this.hrPersonData.cellPhone = this.personalForm.value.hrPhone.e164Number;
                this.hrPersonData.positionId = this.personalForm.value.hrPositionId;
                this.hrPersonData.enterpriseId = this.singleEnterprise.id;
                this.hrPersonData.isActive = true;                
                // CEO data
                this.ceoPersonData.name = this.personalForm.value.ceoName;
                this.ceoPersonData.email = this.personalForm.value.ceoEmail;
                this.surveyedPersonData.enterpriseId = this.singleEnterprise.id;
                this.surveyedPersonData.isActive = true;
                
                this.organizationDataInput.personsDataInput.push(this.surveyedPersonData);
                this.organizationDataInput.personsDataInput.push(this.hrPersonData);
                this.organizationDataInput.personsDataInput.push(this.ceoPersonData);

                this.saving = false;
                this.MoveToNextStep();
            } else {
                for (let i = 0; i < this.enterpriseGroup.enterpriseNumber; i++) {
                    let enterprise = new UpsertEnterpriseDto();
                    enterprise.isActive = true;
                    this.enterpriseListToSave[i].cityId = this.enterpriseListToSave[i].selectedEnterpriseCity;
                    this.enterpriseListToSave[i].regionId = this.enterpriseListToSave[i].selectedEnterpriseRegionId;
                    this.enterpriseListToSave[i].countryId = this.enterpriseListToSave[i].country;
                    this.enterpriseListToSave[i].idNumber = this.enterpriseListToSave[i].OrganizationIdentification;
                    this.enterpriseListToSave[i].sectorId = this.enterpriseListToSave[i].selectedEnterpriseSectorId;
                    this.enterpriseListToSave[i].employeesNumber = this.enterpriseListToSave[i].selectedEnterpriseCollaboratorId;
                    this.enterpriseListToSave[i].enterpriseTypeId = this.enterpriseListToSave[i].enterpriseTypeId;
                }
                
                this.MoveToNextStep();
            }
        }
        if (currentStep === 4) {
            this.stepperSetup.stepTermsAndConditions.isValid = true;

            if (this.isOrganizationTypeGroup) {
                this.enterpriseGroup.receivePromoAccepted = this.receivePromoAccepted;
                this.enterpriseGroup.isPartOfEconomicGroup = this.isPartOfEconomicGroupAcepted;
                this.enterpriseGroup.isPartOfTradeAssociation = this.isPartOfTradeAssociationAccepted;
                if (this.enterpriseGroup.isPartOfEconomicGroup == true) {
                    this.enterpriseGroup.economicGroupName = this.economicGroupName;
                }
                if (this.enterpriseGroup.isPartOfTradeAssociation == true) {
                    this.enterpriseGroup.tradeAssociationId = this.selectedTradeAssociationId;
                }

                this.organizationDataInput.anonymousUserInput.enterpriseGroup = this.enterpriseGroup;

                this.saving = true;
            } else {
                this.singleEnterprise.receivePromoAccepted = this.receivePromoAccepted;
                this.singleEnterprise.isPartOfEconomicGroup = this.isPartOfEconomicGroupAcepted;
                this.singleEnterprise.isPartOfTradeAssociation = this.isPartOfTradeAssociationAccepted;

                //Index 0 for single enterprise
                this.organizationDataInput.anonymousUserInput.enterprises[0].receivePromoAccepted;
                this.organizationDataInput.anonymousUserInput.enterprises[0].isPartOfEconomicGroup;
                this.organizationDataInput.anonymousUserInput.enterprises[0].isPartOfTradeAssociation;
                
                if (this.singleEnterprise.isPartOfEconomicGroup == true) {
                    this.singleEnterprise.economicGroupName = this.economicGroupName;
                    this.organizationDataInput.anonymousUserInput.enterprises[0].economicGroupName = this.economicGroupName;
                }
                if (this.singleEnterprise.isPartOfTradeAssociation == true) {
                    this.singleEnterprise.tradeAssociationId = this.selectedTradeAssociationId;
                    this.organizationDataInput.anonymousUserInput.enterprises[0].tradeAssociationId = this.selectedTradeAssociationId;
                }
            }
        }
    }

    /**
     * Allow define tenant needed by organization type selected
     *
     * @param currentStep
     * @return
     */
    changeOrganizationType(group: boolean): void {
        this.enterprisesList = [];
        this.enterpriseListToSave = [];
        if (this.isOrganizationTypeGroup) {
            this.getTenantByName(TenantNameEnum.Extern);
            this.stepperSetup.stepYourGroupData.hidden = false;
            this.stepperSetup.stepYourOrganizationsGroupData.hidden = false;
            this.stepperSetup.stepContactData.hidden = true;
            this.stepperSetup.stepYourOrganizationData.hidden = true;
        } else {
            this.getTenantByName(TenantNameEnum.Enterprise);
            this.stepperSetup.stepYourGroupData.hidden = true;
            this.stepperSetup.stepYourOrganizationsGroupData.hidden = true;
            this.stepperSetup.stepYourOrganizationData.hidden = false;
            this.stepperSetup.stepContactData.hidden = false;
        }
    }

    /**
     * Allow get tenant by name
     *
     * @param currentStep
     * @return
     */
    getTenantByName(name: string) {
        this._tenantService.getTenantByName(name).subscribe((response) => {
            if (response != undefined) {
                this.tenant = response;
            } else {
                this.notify.error(this.l('TenantByNameNotFound'));
            }
        });
    }

    /**
     * Allow save user data
     *
     * @return
     */
    saveUser(): void {
        if (!this.isEmailDomainValid(this.user.emailAddress)) {
            this.notify.error(this.l('EmailDomainAllowed'));
            return;
        }

        this._userService.anonymousUserExist(this.user.emailAddress).subscribe((exist) => {
            if (!exist) {
                let defaultRol: UserRoleDto = new UserRoleDto();
                defaultRol.roleName = this.getRoleByTenantCurrent();
                this.roles = [];
                this.roles.push(defaultRol);

                if (!this.isOrganizationTypeGroup) {
                    this.enterprisesList.push(this.singleEnterprise);
                }

                let input = new CreateOrUpdateAnonymousUserInput();
                input.tenantId = this.tenant.id;
                input.user = this.user;
                input.user.userName = input.user.name; // Passing by validations only, username generated in the BL
                input.sendActivationEmail = this.sendActivationEmail;
                input.assignedRoleNames = _map(
                    _filter(this.roles, { isAssigned: true, inheritedFromOrganizationUnit: false }),
                    (role) => role.roleName
                );
                input.assignedRoleNames =
                    input.assignedRoleNames.length == 0 ? [this.getRoleByTenantCurrent()] : input.assignedRoleNames;
                this.saving = true;
                this._userService
                    .createOrUpdateAnonymousUser(input)
                    .pipe(
                        finalize(() => {
                            this.saving = false;
                        })
                    )
                    .subscribe((response) => {
                        this.user.id = response;
                        this.notify.success(this.l('SavedSuccessfully'));
                        this.surveyedPersonData.name = `${this.user.name} ${this.user.surname}`;
                        this.surveyedPersonData.email = `${this.user.emailAddress}`;
                        this.MoveToNextStep();
                    });
            } else {
                let message = `${this.l(
                    'RecoverAccountMessage'
                )} <a class="margin-left-2 icon-exclamation-pointer choose-image " href="/account/login"">${this.l(
                    'Here'
                )}</a>`;

                this.message.error(message, this.l('EmailExist'), { isHtml: true });
            }
        });
    }

    /**
     * Get role by tenant
     *
     * @return
     */
    getRoleByTenantCurrent(): string {
        if (this.tenant.name == TenantNameEnum.Extern) {
            return 'ExternalUser';
        }
        if (this.tenant.name == TenantNameEnum.Enterprise) {
            return 'Enterprise';
        }
        return AppConsts.userManagement.defaultAdminUserName;
    }

    /**
     * Move to next step
     *
     * @return
     */
    MoveToNextStep() {
        this.myStepper.next();
    }

    /**
     * Get all countries
     *
     * @return
     */
    getCountries() {
        this._dropDownsService
            .getAllCountries('', 0)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((response) => {
                this.countries = response.sort((a, b) => a.name.localeCompare(b.name));
            });
    }

    /**
     * Country Selection changed for Group
     *
     * @return
     */
    countryGroupSelectionChanged(event: any): void {
        this.selectedGroupCountryId = event.value.id;
        this.selectedGroupCountry = this.countries.find((p) => p.id == this.selectedGroupCountryId);
        this.selectedEnterpriseCountryAb = this.selectedGroupCountry.abreviation !== '' ? this.selectedGroupCountry.abreviation.toLowerCase() : '';
        this.showTradeAsociationOptions =
            this.selectedEnterpriseCountryAb ==
                TradeAssociationAllowedCountriesEnum.Mexico.toString().toLocaleLowerCase() ||
            this.selectedEnterpriseCountryAb ==
                TradeAssociationAllowedCountriesEnum.Colombia.toString().toLocaleLowerCase() ||
            this.selectedEnterpriseCountryAb ==
                TradeAssociationAllowedCountriesEnum.Peru.toString().toLocaleLowerCase();
        forkJoin([
            this._dropDownsService.getAllRegionsByCountryId(this.selectedGroupCountryId).pipe(
                tap((res) => {
                    this.regions = res.sort((a, b) => a.name.localeCompare(b.name));
                    this.cities = [];
                    this.selectedGroupRegionId = undefined;
                    this.selectedEnterpriseRegionId = undefined;
                    this.selectedGroupCityId = undefined;
                    this.selectedEnterpriseCityId = undefined;
                })
            ),
            this._dropDownsService.getAllTradeAssociationsByCountryId(this.selectedGroupCountryId).pipe(
                tap((res) => {
                    this.tradeAssociations = res;
                })
            ),
        ]).subscribe((allResults) => {});
    }

    /**
     * Region Selection changed for Group
     *
     * @return
     */
    regionGroupSelectionChanged(event?: LazyLoadEvent): void {
        this._dropDownsService
            .getAllCitiesByRegionId(this.selectedGroupRegionId)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((response) => {
                this.cities = response.sort((a, b) => a.name.localeCompare(b.name));
                this.selectedGroupCityId = undefined;
                this.selectedEnterpriseCityId = undefined;
            });
    }

    /**
     * Get all sectors
     *
     * @return
     */
    getSectors() {
        this._dropDownsService
            .getAllSectors()
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((response) => {
                this.sectors = response.sort((a, b) => a.name.localeCompare(b.name));
            });
    }

    /**
     * Allow save group data
     *
     * @return
     */
    saveGroup(): void {
        if (
            this.enterpriseGroup.inclusionRegionalManagerEmail &&
            !this.isEmailDomainValid(this.enterpriseGroup.inclusionRegionalManagerEmail)
        ) {
            this.notify.error(this.l('EmailDomainAllowed'));
            return;
        }

        this.enterpriseGroup.userId = this.user.id;
        this.enterpriseGroup.sectorId = this.selectedGroupSectorId;
        this.enterpriseGroup.countryId = this.selectedGroupCountryId;
        this.enterpriseGroup.regionId = this.selectedGroupRegionId;
        this.enterpriseGroup.cityId = this.selectedGroupCityId;
        this.enterpriseGroup.isActive = true;
        this.enterpriseGroup.isPartOfEconomicGroup = false;
        this.enterpriseGroup.isPartOfTradeAssociation = false;
        this.enterpriseGroup.economicGroupName = '';
        this.saving = true;
        this._userService
            .createOrUpdateEnterpriseGroup(this.enterpriseGroup)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((response) => {
                this.enterpriseGroup.id = response;
                this.notify.success(this.l('SavedSuccessfully'));
                this.enterprisesList = [];
                this.enterpriseListToSave = [];
                for (let i = 0; i < this.enterpriseGroup.enterpriseNumber; i++) {
                    let enterprise = new UpsertEnterpriseDto();
                    enterprise.isActive = true;
                    enterprise.employeesNumber = 0;
                    enterprise.userId = this.user.id;
                    enterprise.tenantId = this.tenant.id;
                    this.enterprisesList.push(enterprise);
                }
                this.MoveToNextStep();
            });
    }

    /**
     * Allow save the group of enterprises in gruop
     *
     */
    saveGroupedEnterprises() {
        let defaultRol: UserRoleDto = new UserRoleDto();
        defaultRol.roleName = AppConsts.userManagement.defaultAdminUserName;
        this.roles = [];
        this.roles.push(defaultRol);

        if (!this.isOrganizationTypeGroup) {
            this.enterprisesList = [];
            this.enterprisesList.push(this.singleEnterprise);
        }

        let country = this.countries.find((p) => p.id == this.selectedGroupCountryId);

        if (
            country.nicRucLength > 0 &&
            this.enterprisesList.find((p) => p.idNumber.length > country.nicRucLength) != undefined
        ) {
            this.notify.error(this.l('NicRucLengthError').replace('{0}', country.nicRucLength.toString()));
            return;
        }

        const observablesList = [];
        this.enterprisesList.forEach((ent) => {
            observablesList.push(this._userService.createOrUpdateEnterprise(ent));
        });

        forkJoin(observablesList).subscribe((response: UpsertEnterpriseDto[]) => {
            this.enterprisesList.forEach((e) => {
                let ent = response.find((p) => p.idNumber == e.idNumber);
                e.id = ent.id;
            });
            this.notify.success(this.l('SavedSuccessfully'));
            this.MoveToNextStep();
        });
    }

    /**
     * Country Selection changed for Enterprise
     *
     * @return
     */
    countryEnterpriseSelectionChanged(event): void {
        if (!event.value) {
            this.selectedEnterpriseCountryId = null;
            this.selectedEnterpriseCountryAb = null;
            return;
        }

        this.selectedEnterpriseCountryId = event.value.id;
        this.selectedEnterpriseCountry = this.countries.find((p) => p.id == this.selectedEnterpriseCountryId);
        this.selectedEnterpriseCountryAb = this.selectedEnterpriseCountry.abreviation !== '' ? this.selectedEnterpriseCountry.abreviation.toLowerCase() : '';
        this.showTradeAsociationOptions =
            this.selectedEnterpriseCountryAb ==
                TradeAssociationAllowedCountriesEnum.Mexico.toString().toLocaleLowerCase() ||
            this.selectedEnterpriseCountryAb ==
                TradeAssociationAllowedCountriesEnum.Colombia.toString().toLocaleLowerCase() ||
            this.selectedEnterpriseCountryAb ==
                TradeAssociationAllowedCountriesEnum.Peru.toString().toLocaleLowerCase();

        if (this.selectedEnterpriseCountry.code.split('-').length > 1) {
            this.validationOrganizationId = this.onlyAlphaNumeric;
        }
        forkJoin([
            this._dropDownsService.getAllRegionsByCountryId(this.selectedEnterpriseCountryId).pipe(
                tap((res) => {
                    this.regions = res.sort((a, b) => a.name.localeCompare(b.name));
                    this.cities = [];
                    this.selectedGroupRegionId = undefined;
                    this.selectedEnterpriseRegionId = undefined;
                    this.selectedGroupCityId = undefined;
                    this.selectedEnterpriseCityId = undefined;
                })
            ),
            this._dropDownsService.getAllTradeAssociationsByCountryId(this.selectedEnterpriseCountryId).pipe(
                tap((res) => {
                    this.tradeAssociations = res.sort((a, b) => a.name.localeCompare(b.name));
                })
            ),
        ]).subscribe((allResults) => {});
    }

    countryGroupEnterpriseSelectionChanged(event: any, entity: any): void {
        if (!event.value) {
            entity.selectedEnterpriseCountryId = null;
            entity.selectedEnterpriseCountryAb = null;
            return;
        }

        entity.selectedEnterpriseCountryId = event.value.id;
        entity.selectedEnterpriseCountry = this.countries.find((p) => p.id == entity.selectedEnterpriseCountryId);
        entity.selectedEnterpriseCountryAb = entity.selectedEnterpriseCountry.abreviation !== '' ? entity.selectedEnterpriseCountry.abreviation.toLowerCase() : '';

        if (entity.selectedEnterpriseCountry.code.split('-').length > 1) {
            entity.validationOrganizationId = entity.onlyAlphaNumeric;
        }
        forkJoin([
            this._dropDownsService.getAllRegionsByCountryId(entity.selectedEnterpriseCountryId).pipe(
                tap((res) => {
                    entity.regions = res.sort((a, b) => a.name.localeCompare(b.name));
                    entity.cities = [];
                    entity.selectedEnterpriseRegionId = undefined;
                    entity.selectedEnterpriseCityId = undefined;
                })
            ),
            this._dropDownsService.getAllTradeAssociationsByCountryId(entity.selectedEnterpriseCountryId).pipe(
                tap((res) => {
                    entity.tradeAssociations = res.sort((a, b) => a.name.localeCompare(b.name));
                })
            ),
        ]).subscribe((allResults) => {});
    }

    /**
     * Region Selection changed for Enterprise
     *
     * @return
     */
    regionEnterpriseSelectionChanged(event?: LazyLoadEvent): void {
        this._dropDownsService
            .getAllCitiesByRegionId(this.selectedEnterpriseRegionId)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((response) => {
                this.cities = response.sort((a, b) => a.name.localeCompare(b.name));
                this.selectedGroupCityId = undefined;
                this.selectedEnterpriseCityId = undefined;
            });
    }

    regionGroupEnterpriseSelectionChanged(entity: any, event?: LazyLoadEvent): void {
        this._dropDownsService
            .getAllCitiesByRegionId(entity.selectedEnterpriseRegionId)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((response) => {
                entity.cities = response.sort((a, b) => a.name.localeCompare(b.name));
                entity.selectedGroupCityId = undefined;
                entity.selectedEnterpriseCityId = undefined;
            });
    }

    cityEnterpriseSelectionChanged(event?: LazyLoadEvent): void {}

    /**
     * Validate if specific enterprise Id Number already exists
     * @param enterpriseIdNumber Enterprise identificator number
     * @returns
     */
    async validateEnterpriseNumber(enterpriseIdNumber: string): Promise<boolean> {
        return await this._validatorServiceProxy
            .validateEnterpriseIdNumberExists(enterpriseIdNumber)
            .toPromise()
            .then((exists) => {
                return exists;
            });
    }

    /**
     * Show message if enterprise exists
     */
    showIdNumberExistsMessage(title: string): void {
        let message = `${this.l(
            'RecoverAccountMessage'
        )} <a class="margin-left-2 icon-exclamation-pointer choose-image " href="/account/login">${this.l(
            'Here'
        )} </a>`;

        this.message.error(message, title, { isHtml: true });
    }

    /**
     * Allow save single enterprise
     *
     * @return
     */
    async saveSingleEnterprise() {
        if (
            this.singleEnterprise.inclusionRegionalManagerEmail &&
            !this.isEmailDomainValid(this.singleEnterprise.inclusionRegionalManagerEmail)
        ) {
            this.notify.error(this.l('EmailDomainAllowed'));
            return;
        }

        const idNumberExists = await this.validateEnterpriseNumber(this.singleEnterprise.idNumber);
        if (idNumberExists) {
            return this.showIdNumberExistsMessage(this.l('EnterpriseAlreadyCreated'));
        }

        if (!this.isOrganizationTypeGroup && this.singleEnterprise && this.singleEnterprise.name) {
            this.singleEnterprise.sectorId = this.selectedEnterpriseSectorId;
            this.singleEnterprise.countryId = this.selectedEnterpriseCountryId;
            this.singleEnterprise.regionId = this.selectedEnterpriseRegionId;
            this.singleEnterprise.cityId = this.selectedEnterpriseCityId;
            this.singleEnterprise.isActive = true;
            this.singleEnterprise.userId = this.user.id;
            this.singleEnterprise.tenantId = this.tenant.id;
            this.singleEnterprise.isPartOfEconomicGroup = false;
            this.singleEnterprise.isPartOfTradeAssociation = false;
            this.singleEnterprise.economicGroupName = '';
            this.singleEnterprise.employeesNumber = this.selectedEnterpriseCollaboratorId;

            let enterpriseListToSave: UpsertEnterpriseDto[] = [];
            enterpriseListToSave.push(this.singleEnterprise);
            let input = new CreateOrUpdateAnonymousUserInput();
            // Only for validations
            input.tenantId = this.tenant.id;
            input.user = this.user;
            input.sendActivationEmail = this.sendActivationEmail;
            input.assignedRoleNames = _map(
                _filter(this.roles, { isAssigned: true, inheritedFromOrganizationUnit: false }),
                (role) => role.roleName
            );
            // Data to save
            input.enterprises = enterpriseListToSave;
            this.saving = true;
            this._userService
                .createOrUpdateGroupedEnterprises(input)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                        this.swapShowPersonDataForm();
                    })
                )
                .subscribe((response) => {
                    if (!this.isOrganizationTypeGroup) {
                        this.singleEnterprise.id = response;
                    }
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.MoveToNextStep();
                });
        }
    }

    /**
     * Get all Personal types
     *
     * @return
     */
    getPersonalTypes() {
        this._dropDownsService
            .getAllPersonalTypes()
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((response) => {
                this.personalTypes = response;
                if (this.personalTypes && this.personalTypes.length > 0) {
                    this.ceoPersonData.personalTypeId = this.personalTypes.filter((p) => p.code === 'CEO')[0].id;
                    this.hrPersonData.personalTypeId = this.personalTypes.filter((p) => p.code === 'HR')[0].id;
                    this.surveyedPersonData.personalTypeId = this.personalTypes.filter((p) => p.code === 'COM')[0].id;
                }
            });
    }

    /**
     * Get all Personal types
     *
     * @return
     */
    getPositions() {
        this._dropDownsService
            .getAllPositions()
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((response) => {
                this.personalPositions = response.sort((a, b) => a.name.localeCompare(b.name));
            });
    }

    /**
     * Save contact persons
     *
     * @return
     */
    saveContactData() {
        if (
            !this.isEmailDomainValid(this.personalForm.value.surveyedEmail.toString()) ||
            !this.isEmailDomainValid(this.personalForm.value.hrEmail.toString()) ||
            !this.isEmailDomainValid(this.personalForm.value.ceoEmail.toString())
        ) {
            this.notify.error(this.l('EmailDomainAllowed'));
            return;
        }

        // Surveyed data
        this.surveyedPersonData.name = this.personalForm.value.surveyedName;
        this.surveyedPersonData.email = this.personalForm.value.surveyedEmail;
        this.surveyedPersonData.cellPhone = this.personalForm.value.surveyedPhone.e164Number;
        this.surveyedPersonData.positionId = this.personalForm.value.surveyedPositionId;
        this.ceoPersonData.enterpriseId = this.singleEnterprise.id;
        this.ceoPersonData.isActive = true;
        // HR data
        this.hrPersonData.name = this.personalForm.value.hrName;
        this.hrPersonData.email = this.personalForm.value.hrEmail;
        this.hrPersonData.cellPhone = this.personalForm.value.hrPhone.e164Number;
        this.hrPersonData.positionId = this.personalForm.value.hrPositionId;
        this.hrPersonData.enterpriseId = this.singleEnterprise.id;
        this.hrPersonData.isActive = true;
        // CEO data
        this.ceoPersonData.name = this.personalForm.value.ceoName;
        this.ceoPersonData.email = this.personalForm.value.ceoEmail;
        this.surveyedPersonData.enterpriseId = this.singleEnterprise.id;
        this.surveyedPersonData.isActive = true;

        this.saving = true;

        forkJoin([
            this._userService
                .createOrUpdatePersonalData(this.surveyedPersonData)
                .pipe(tap((res: number) => (this.surveyedPersonData.id = res))),
            this._userService
                .createOrUpdatePersonalData(this.hrPersonData)
                .pipe(tap((res: number) => (this.hrPersonData.id = res))),
            this._userService
                .createOrUpdatePersonalData(this.ceoPersonData)
                .pipe(tap((res: number) => (this.ceoPersonData.id = res))),
        ]).subscribe((allResults) => {
            this.saving = true;
            this.notify.success(this.l('SavedSuccessfully'));
            this.MoveToNextStep();
        });
    }

    /**
     * Finish registration process
     *
     * @return
     */
    finishRegister() {
        if (this.isOrganizationTypeGroup) {
            this.enterpriseGroup.receivePromoAccepted = this.receivePromoAccepted;
            this.enterpriseGroup.isPartOfEconomicGroup = this.isPartOfEconomicGroupAcepted;
            this.enterpriseGroup.isPartOfTradeAssociation = this.isPartOfTradeAssociationAccepted;
            if (this.enterpriseGroup.isPartOfEconomicGroup == true) {
                this.enterpriseGroup.economicGroupName = this.economicGroupName;
            }
            if (this.enterpriseGroup.isPartOfTradeAssociation == true) {
                this.enterpriseGroup.tradeAssociationId = this.selectedTradeAssociationId;
            }

            this.saving = true;
            this._userService
                .createOrUpdateEnterpriseGroup(this.enterpriseGroup)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((response) => {
                    this.enterpriseGroup.id = response;
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.automaticLogin();
                });
        } else {
            this.singleEnterprise.receivePromoAccepted = this.receivePromoAccepted;
            this.singleEnterprise.sectorId = this.selectedEnterpriseSectorId;
            this.singleEnterprise.countryId = this.selectedEnterpriseCountryId;
            this.singleEnterprise.regionId = this.selectedEnterpriseRegionId;
            this.singleEnterprise.cityId = this.selectedEnterpriseCityId;
            this.singleEnterprise.isActive = true;
            this.singleEnterprise.userId = this.user.id;
            this.singleEnterprise.isPartOfEconomicGroup = this.isPartOfEconomicGroupAcepted;
            this.singleEnterprise.isPartOfTradeAssociation = this.isPartOfTradeAssociationAccepted;
            if (this.singleEnterprise.isPartOfEconomicGroup == true) {
                this.singleEnterprise.economicGroupName = this.economicGroupName;
            }
            if (this.singleEnterprise.isPartOfTradeAssociation == true) {
                this.singleEnterprise.tradeAssociationId = this.selectedTradeAssociationId;
            }

            let enterpriseListToSave: UpsertEnterpriseDto[] = [];
            enterpriseListToSave.push(this.singleEnterprise);
            let input = new CreateOrUpdateAnonymousUserInput();
            // Only for validations
            input.tenantId = this.tenant.id;
            input.user = this.user;
            input.sendActivationEmail = this.sendActivationEmail;
            input.assignedRoleNames = _map(
                _filter(this.roles, { isAssigned: true, inheritedFromOrganizationUnit: false }),
                (role) => role.roleName
            );
            // Data to save
            input.enterprises = enterpriseListToSave;
            this.saving = true;
            this._userService
                .createOrUpdateGroupedEnterprises(input)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((response) => {
                    this.enterpriseGroup.id = response;
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.automaticLogin();
                });
        }
    }

    createOrganization() {
        this.saving = true;
            this._userService
                .createOrganization(this.organizationDataInput)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe((response) => {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.automaticLogin();
                });
    }

    /**
     * Get collaborators
     *
     * @return
     */
    getCollaborators() {
        this._dropDownsService
            .getAllCollaborators()
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((response) => {
                this.collaborators = response;
            });
    }

    /**
     * Get collaborators
     *
     * @return
     */
    getEmailDomainsBlackList() {
        this._dropDownsService
            .getEmailDomainsBlackList()
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe((response) => {
                this.emailDomainsBlacList = response;
            });
    }

    /**
     * Validate if email address is allowed
     *
     * @return true when email is valid
     */
    isEmailDomainValid(email: string): boolean {
        return this.emailDomainsBlacList.find((p) => email.endsWith(p.name)) == undefined;
    }

    /**
     * Is international group Check changed
     *
     * @return
     */
    changeGroupIsInternational(event) {
        this.enterpriseGroup.hasInclusion = false;
        this.enterpriseGroup.inclusionRegionalManagerName = undefined;
        this.enterpriseGroup.inclusionRegionalManagerEmail = undefined;
    }

    /**
     * Regional manager group exists check changed
     *
     * @return
     */
    changeGroupRegionalManagementExist(event) {
        this.enterpriseGroup.inclusionRegionalManagerName = undefined;
        this.enterpriseGroup.inclusionRegionalManagerEmail = undefined;
    }

    /**
     * Is international enterprise check changed
     *
     * @return
     */
    changeEnterpriseIsInternational(event) {
        this.singleEnterprise.hasInclusion = false;
        this.singleEnterprise.inclusionRegionalManagerName = undefined;
        this.singleEnterprise.inclusionRegionalManagerEmail = undefined;
    }

    /**
     * Regional manager enterprise exists check changed
     *
     * @return
     */
    changeEnterpriseRegionalManagementExist(event) {
        this.singleEnterprise.inclusionRegionalManagerName = undefined;
        this.singleEnterprise.inclusionRegionalManagerEmail = undefined;
    }

    /**
     * Automatic login process
     *
     * @return
     */
    automaticLogin(): void {
        this.loginService.authenticateModel.userNameOrEmailAddress = this.user.emailAddress;
        this.loginService.authenticateModel.password = this.user.password;

        let recaptchaCallback = (token: string) => {
            this.showMainSpinner();

            this.loginService.authenticate(
                () => {
                    this.hideMainSpinner();
                },
                null,
                token
            );
        };

        recaptchaCallback(null);
    }

    /**
     * Set opossite value to the variable
     *
     * @return
     */
    swapShowPersonDataForm() {
        this.showPersonDataForm = !this.showPersonDataForm;
    }

    redirectLogin() {
        this._router.navigate(['/account/login']);
    }
    isAcceptedTermsCheck(): boolean {
        return this.isAcceptedTerms;
    }

    print(val) {
        console.log(val);
    }
}
