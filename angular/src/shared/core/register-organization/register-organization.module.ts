import { AccountSharedModule } from '@account/shared/account-shared.module';
import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { StepperModule } from '@shared/core/stepper/stepper.module';
import { PasswordShowHideDirective } from '@shared/utils/show-hide.directive';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { PasswordModule } from 'primeng/password';
import { InputsModule } from '../inputs/inputs.module';
import { RegisterOrganizationRoutingModule } from './register-organization-routing.module';
import { RegisterOrganizationComponent } from './register-organization.component';

@NgModule({
    declarations: [RegisterOrganizationComponent, PasswordShowHideDirective],
    imports: [
        AppSharedModule,
        AccountSharedModule,
        PasswordModule,
        RegisterOrganizationRoutingModule,
        AdminSharedModule,
        StepperModule,
        InputsModule,
        NgxIntlTelInputModule
    ],
})
export class RegisterOrganizationModule {}
