import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { AppSessionService } from '@shared/common/session/app-session.service';
import { LocalizationService } from 'abp-ng2-module';
import { environment } from 'environments/environment';
import { SweetalertProvider } from './sweetalert.service';

@Injectable()
export class CookiesProvider {
    private showModalForEditEnterprise = false;
    constructor(
        injector: Injector,
        private _sweetalertProvider: SweetalertProvider,
        private _router: Router,
        public localization: LocalizationService
    ) {
        this._validAcceptCookies();
    }
    private _validAcceptCookies() {
        this._validCookiesSavedOnCache();
        this._setCookiesOnCache();
        if (!abp.utils.getCookieValue(environment.keyCookieAccept)) {
            this._sweetalertProvider.bottomDrawerAcceptCookies().then((result) => {
                if (result.isConfirmed) {
                    abp.utils.setCookieValue(environment.keyCookieAccept, 'true');
                    this._setCookiesOnCache();
                }
                if (result.isDismissed) {
                    window.open(environment.KnowMoreCookiesUrl, '_blank');
                    this._validAcceptCookies();
                }
            });
        }
    }
    private _setCookiesOnCache() {
        if (abp.utils.getCookieValue(environment.keyCookieAccept)) {
            localStorage.setItem(environment.keyCookieAccept, 'true');
        }
    }
    private _validCookiesSavedOnCache() {
        if (localStorage.getItem(environment.keyCookieAccept) && !abp.utils.getCookieValue(environment.keyCookieAccept)) {
            abp.utils.setCookieValue(environment.keyCookieAccept, 'true');
        }
    }
    validDateEnterprise(appSession: AppSessionService) {
        if (!this.showModalForEditEnterprise && appSession.enterprise) {
            const creation: any = appSession.enterprise.lastModificationTime;
            if (
                new Date(creation) < new Date('2022-09-08T00:00:00')
            ) {
                this.showModalForEditEnterprise = true;
                abp.message.confirm(
                    this.getTranslation('msgYouNeedUpdateYouEnterprise'),
                    this.getTranslation('titleYouNeedUpdateYouEnterprise'),
                    (result: boolean) => {
                        this.showModalForEditEnterprise = false;
                        abp.event.trigger('app.show.editCompanyModal')
                       // this._router.navigate(['app', 'entities', 'enterprise', appSession.enterprise.id, 'edit']);
                    }
                );
            }
        }
    }
    getTranslation(key: string): string {
        return this.localization.localize(key, AppConsts.localization.defaultLocalizationSourceName);
    }
}
