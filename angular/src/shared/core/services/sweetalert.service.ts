import { Injectable, Injector } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LocalizationService } from 'abp-ng2-module';
import Swal, { SweetAlertResult } from 'sweetalert2';

@Injectable()
export class SweetalertProvider {
    localizationSourceName = AppConsts.localization.defaultLocalizationSourceName;
    constructor(public localization: LocalizationService) {}
    bottomDrawerAcceptCookies(): Promise<SweetAlertResult<any>> {
        return Swal.fire({
            text: this.getTranslation('TitleAccordingCookies'),
            position: 'bottom',
            showClass: {
                popup: `
            animate__animated
            animate__fadeInUp
            animate__faster
          `,
            },
            hideClass: {
                popup: `
            animate__animated
            animate__fadeOutDown
            animate__faster
          `,
            },
            width: '100%',
            grow: 'row',
            showConfirmButton: true,
            showDenyButton: true,
            showCloseButton: false,
            showCancelButton: true,
            confirmButtonText: this.getTranslation('AcceptCookies'),
            denyButtonText: this.getTranslation('NotAcceptCookies'),
            cancelButtonText: this.getTranslation('KnowMoreCookies'),
            allowOutsideClick: false,
            customClass: {
                confirmButton: 'btn btn-customer',
                denyButton: 'btn btn-customer-light',
                cancelButton: 'btn btn-customer-light',
            },
        });
    }
    getTranslation(key: string): string {
        return this.localization.localize(key, this.localizationSourceName);
    }
}
