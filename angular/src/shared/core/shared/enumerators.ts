/**
* Countries allowed for Trade Association enum
*
*/
export enum TradeAssociationAllowedCountriesEnum {
    Mexico = 'ME',
    Colombia = 'CO',
    Peru = 'PE'
}
