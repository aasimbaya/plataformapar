export enum QuestionUseTypeEnum {
    All = 0,
    Category = 1,
    SpecialDistinction = 2
}

export enum QuestionTypeEnum {
    boolean = 'boolean',
    multiple = 'multipleselection',
    single = 'singleselection',
    open = 'openquestion'
}

export enum QuestionTypeObject {
    boolean = 'radio',
    multiple = 'checkbox',
    single = 'radio',
    open = 'text'
}
