/**
* Tenants enum
*
*/
export enum TenantNameEnum {
    Aequales = 'Aequales',
    Enterprise = 'Enterprise',
    Extern = 'Extern',
    Default = 'Default'
}
