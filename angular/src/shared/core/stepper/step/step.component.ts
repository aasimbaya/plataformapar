import { Component, Input, HostBinding } from '@angular/core';

export enum MoveTo {
  next,
  previous
}

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.less']
})
export class StepComponent {
  @Input() title: string;
  @Input() stepControl: boolean;

  @HostBinding('class.animate-next') moveToNext: boolean;
  @HostBinding('class.animate-previus') moveToPrevious: boolean;

  hidden = false;
  isComplete = false;
  private _isActive = false;

  set moveTo(value: MoveTo) {
    this.moveToNext = value === MoveTo.next;
    this.moveToPrevious = value === MoveTo.previous;
  }

  set isActive(isActive: boolean) {
    this._isActive = isActive;
  }

  get isActive(): boolean {
    return this._isActive;
  }

  get isValid(): boolean {
    return this.stepControl;
  }
}
