import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-stepper-overview',
  templateUrl: './stepper-overview.component.html',
  styleUrls: ['./stepper-overview.component.css']
})
export class StepperOverviewComponent implements OnInit {

  stepperSetup = {
    stepPokemon: {
      title: 'Search Pokemon',
      isValid: false
    },
    stepInput: {
      title: 'To subscribe',
      isValid: false
    },
    stepFinish: {
      title: 'Congratulations',
      isValid: true
    }
  }

  constructor() { }

  ngOnInit(): void {
  }

  form_one = new FormGroup({
  });
  form_tow = new FormGroup({
  });
  form_three = new FormGroup({
  });
  form_four = new FormGroup({
  });


  /*{ paso1: formulario1 }*/

}
