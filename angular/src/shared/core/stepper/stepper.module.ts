import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { StepperComponent } from './stepper/stepper.component';
import { StepComponent } from './step/step.component';
import { StepperOverviewComponent } from '../stepper/stepper-overview/stepper-overview.component';
import { UserDataComponent } from './user-data/user-data.component';
import { UserFormComponent } from './user-form/user-form.component';

@NgModule({
  declarations: [
    StepperComponent,
    StepComponent,
    StepperOverviewComponent,
    UserDataComponent,
    UserFormComponent
  ],
  exports: [
    StepperComponent,
    StepComponent,
    StepperOverviewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class StepperModule { }
