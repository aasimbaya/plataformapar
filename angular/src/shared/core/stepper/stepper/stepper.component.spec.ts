import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';

import { StepperComponent } from './stepper.component';
import { StepperModule } from '../stepper.module';
import { By } from '@angular/platform-browser';

@Component({
  selector: 'one-test-stepper-component',
  template: `
    <one-stepper #stepper>
      <one-step title="First Name" [stepControl]="form_one">
        <form [formGroup]="form_one">
          <div>
            <input placeholder="First Name" type="text" formControlName="firstName" />
          </div>
          <div class="actions">
            <button class="button success" (click)="stepper.next()">Next</button>
          </div>
        </form>
      </one-step>
      <one-step title="Last Name" [stepControl]="form_tow">
        <form [formGroup]="form_tow">
          <div>
            <input placeholder="Last Name" type="text" formControlName="lastName" />
          </div>
          <div class="actions">
            <button class="button reject" (click)="stepper.previous()">Previous</button>
            <button class="button success" (click)="stepper.next()">Next</button>
          </div>
        </form>
      </one-step>
      <one-step title="Summary">
        <div class="summary">
          Your name is: {{ form_one.value.firstName }}
          {{ form_tow.value.firstLastName }}
        </div>
        <div class="actions">
          <button class="button reject" (click)="stepper.previous()">Previous</button>
          <button class="button success" (click)="stepper.next()">Finish</button>
        </div>
      </one-step>
    </one-stepper>
  `
})
class TestStepperComponent {
  form_one = new FormGroup({
    firstName: new FormControl('', Validators.required)
  });
  form_tow = new FormGroup({
    lastName: new FormControl('', Validators.required)
  });
}

describe('TooltipRendererDirective', () => {
  let testComponent: TestStepperComponent;
  let testFixture: ComponentFixture<TestStepperComponent>;
  let component: StepperComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestStepperComponent],
      imports: [StepperModule, ReactiveFormsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    testFixture = TestBed.createComponent<TestStepperComponent>(TestStepperComponent);
    testComponent = testFixture.componentInstance;
    component = testFixture.debugElement.children[0].componentInstance;
    testFixture.detectChanges();
  });

  it('should create an instance of TestStepperComponent', () => {
    expect(testComponent).toBeTruthy();
  });

  it('should create an instance of StepperComponent', () => {
    expect(component).toBeTruthy();
  });

  describe('next method test suite', () => {
    it('should not change the next step when the isValid property of the activeStep is false', () => {
      const expectValue = 'First Name';

      component.next();
      expect(component.activeStep.title).toBe(expectValue);
    });

    it('should change the next step when the isValid property of the activeStep is true', () => {
      testComponent.form_one.patchValue({
        firstName: 'Test name'
      });
      const expectValue = 'Last Name';

      component.next();
      expect(component.activeStep.title).toBe(expectValue);
    });

    it(`should mark the completed stepper when you call the next method
          and the isValid property of the last step is true`, () => {
      testComponent.form_one.patchValue({
        firstName: 'Test name'
      });
      testComponent.form_tow.patchValue({
        lastName: 'Test last name'
      });

      component.next();
      component.next();
      component.next();
      expect(component.isCompleted).toBe(true);
    });
  });

  describe('previous method test suite', () => {
    it(`shouldn't change the previous step when you call
          the previous method and there is no previous step`, () => {
      testComponent.form_one.patchValue({
        firstName: 'Test name'
      });
      const expectValue = 'First Name';

      component.previous();
      expect(component.activeStep.title).toBe(expectValue);
    });

    it('should change the previous step when you call the previous method and there is a previous step', () => {
      testComponent.form_one.patchValue({
        firstName: 'Test name'
      });
      const expectValue = 'First Name';

      component.next();
      component.previous();
      expect(component.activeStep.title).toBe(expectValue);
    });
  });

  describe('goToStep method test suite', () => {
    it('Should call the goToStep method when click the label of the navigation item', async(() => {
      spyOn(component as any, 'goToStep').and.callThrough();
      testFixture.detectChanges();
      testFixture.whenRenderingDone().then(() => {
        const stepNavigateRef = testFixture.debugElement.query(By.css('.stepper-navigate'));
        stepNavigateRef.nativeElement.click();
        testFixture.detectChanges();
        expect((component as any).goToStep).toHaveBeenCalled();
      });
    }));

    it(`should change the previous step when you call the
          goToStep method and newStep is less than activeStep`, async(() => {
      testComponent.form_one.patchValue({
        firstName: 'Test name'
      });
      testComponent.form_tow.patchValue({
        lastName: 'Test lastname'
      });
      component.next();
      component.next();
      testFixture.detectChanges();

      const expectValue = 'Last Name';
      testFixture.whenRenderingDone().then(() => {
        const stepNavigateRef = testFixture.debugElement.query(By.css('.stepper-navigate:nth-child(2)'));
        stepNavigateRef.nativeElement.click();
        testFixture.detectChanges();
        expect(component.activeStep.title).toBe(expectValue);
      });
    }));

    it(`should change the previous step when you call the
          goToStep method and newStep is less than activeStep`, async(() => {
      testComponent.form_one.patchValue({
        firstName: 'Test name'
      });
      testComponent.form_tow.patchValue({
        lastName: 'Test lastname'
      });
      component.next();
      component.next();
      component.previous();
      component.previous();
      testFixture.detectChanges();

      const expectValue = 'Summary';
      testFixture.whenRenderingDone().then(() => {
        const stepNavigateRef = testFixture.debugElement.query(By.css('.stepper-navigate:nth-child(3)'));
        stepNavigateRef.nativeElement.click();
        testFixture.detectChanges();
        expect(component.activeStep.title).toBe(expectValue);
      });
    }));
  });
});
