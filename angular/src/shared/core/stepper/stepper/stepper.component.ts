import {
  Component,
  AfterContentInit,
  Output,
  EventEmitter,
  ContentChildren,
  QueryList,
  Input
} from '@angular/core';
import { StepComponent, MoveTo } from '../step/step.component';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.less']
})
export class StepperComponent implements AfterContentInit {
  @Input() stepperLabel: string;

  @Input() allowManualStepping: boolean = true;

  @Output()
  stepChanged: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  completeStepper: EventEmitter<void> = new EventEmitter<void>();

  @ContentChildren(StepComponent)
  stepsList: QueryList<StepComponent> = new QueryList<StepComponent>();

  private _isCompleted = false;

  ngAfterContentInit() {
    this.activeStep = this.stepsList.first;
  }

  get steps(): Array<StepComponent> {
    return this.stepsList
      .toArray()
      .filter((step: StepComponent) => !step.hidden);
  }

  get isCompleted(): boolean {
    return this._isCompleted;
  }

  get activeStep(): StepComponent {
    return this.steps.find((step: StepComponent) => step.isActive);
  }

  set activeStep(step: StepComponent) {
    if (step && step !== this.activeStep) {
      this.moveTo(step, this.activeStep);
      if (this.activeStep) {
        this.activeStep.isActive = false;
      }
      step.isActive = true;
      this.stepChanged.emit(this.getIndexStep(step));
    }
  }

  get nextStep(): StepComponent {
    if (this.getIndexStep(this.activeStep) < this.steps.length - 1) {
      return this.steps[this.getIndexStep(this.activeStep) + 1];
    }
    return null;
  }

  get previousStep(): StepComponent {
    if (this.getIndexStep(this.activeStep) > 0) {
      return this.steps[this.getIndexStep(this.activeStep) - 1];
    }
    return null;
  }

  goToStep(nextStep: StepComponent) {
    if (this.validToGo(nextStep) && this.allowManualStepping) {
      this.activeStep.isComplete = this.activeStep.isValid;
      this.activeStep = nextStep;
    }
  }

  validToGo(nextStep: StepComponent): boolean {
    return (
      (this.getIndexStep(nextStep) < this.getIndexStep(this.activeStep) || this.activeStep.isValid) &&
      nextStep !== this.activeStep &&
      (this.getIndexStep(nextStep) === 0 ||
        this.getIndexStep(nextStep) < this.getIndexStep(this.activeStep) ||
        (this.steps[this.getIndexStep(nextStep) - 1].isValid &&
          this.steps[this.getIndexStep(nextStep) - 1].isComplete))
    );
  }

  getIndexStep(step: StepComponent) {
    return this.steps.indexOf(step);
  }

  private complete() {
    this._isCompleted = true;
    this.completeStepper.emit();
  }

  next() {
    if (this.activeStep.isValid) {
      this.activeStep.isComplete = true;
      if (this.nextStep) {
        this.nextStep.isComplete = this.nextStep.isValid;
        this.activeStep = this.nextStep;
      } else {
        this.complete();
      }
    }
  }

  previous() {
    if (this.previousStep) {
      this.goToStep(this.previousStep);
    }
  }

  moveTo(newStep: StepComponent, currentStep: StepComponent) {
    if (this.getIndexStep(newStep) > this.getIndexStep(currentStep)) {
      newStep.moveTo = MoveTo.next;
    } else {
      newStep.moveTo = MoveTo.previous;
    }
  }
}
