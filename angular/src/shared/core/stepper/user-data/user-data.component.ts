import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { EMPTY, of, Subject } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.css']
})
export class UserDataComponent implements OnInit {
  @Output() isValid = new EventEmitter<boolean>();

  inputValue = new Subject();

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.searchValidPokemon();
  }

  searchValidPokemon() {
    this.inputValue.pipe(
      debounceTime(800),
      distinctUntilChanged(),
      switchMap(
        (name: string) => this.http.get(`https://pokeapi.co/api/v2/pokemon/${name}/`)
          .pipe(catchError(() => of(false)))
      )
    ).subscribe(res => {
      if (res) {
        this.isValid.emit(true);
      } else {
        this.isValid.emit(false);
      }
    });
  }

}
