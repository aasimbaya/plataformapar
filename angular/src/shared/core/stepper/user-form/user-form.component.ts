import { Component, OnInit, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  @Output() isValid = new EventEmitter<boolean>();
  nameP = '';
  constructor() { }

  ngOnInit(): void {
  }

  checking() {
    if (this.nameP) {
      this.isValid.emit(true)
    }
    else {
      this.isValid.emit(false);
    }
  }

}
