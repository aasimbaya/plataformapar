import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'aequales',
        loadChildren: () =>
            import('./users-aequales/users-aequales.module').then(
                (m) => m.UsersAequalesModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
    {
        path: 'groupings',
        loadChildren: () =>
            import('./users-groupings/users-groupings.module').then(
                (m) => m.UsersGroupingsModule
            ),
        // data: { permission: 'Pages.ActivityStatuses' }
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UsersAdminRoutingModule {}
