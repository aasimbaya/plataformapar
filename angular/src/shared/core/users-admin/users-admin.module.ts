import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { UsersAdminRoutingModule } from './users-admin-routing.module';
@NgModule({
    declarations: [
  ],
    imports: [AppSharedModule, AdminSharedModule, UsersAdminRoutingModule],
})
export class UsersAdminModule {}
