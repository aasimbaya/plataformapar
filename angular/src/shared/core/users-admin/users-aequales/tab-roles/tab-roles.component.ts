import { Component, Injector, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import { UserRoleDto } from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'tab-roles',
    templateUrl: './tab-roles.component.html',
})
export class TabRolesComponent extends AppComponentBase {
    @Input() form: FormGroup;
    @Input() roles: UserRoleDto[];

    constructor(
        injector: Injector
    ) {
        super(injector);
    }
}
