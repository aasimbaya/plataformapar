import { Component, Injector, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'tab-user-information',
    templateUrl: './tab-user-information.component.html',
})
export class TabUserInformationComponent extends AppComponentBase {
    @Input() form: FormGroup;

    constructor(injector: Injector) {
        super(injector);
    }
}
