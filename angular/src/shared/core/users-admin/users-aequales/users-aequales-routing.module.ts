import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersAequalesComponent } from './users-aequales.component';
import { PageMode } from '@shared/AppEnums';

const routes: Routes = [
    {
        path: '',
        component: UsersAequalesComponent,
        pathMatch: 'full',
        },
        // {
        //     path: 'add',
        //     component: MasuringToolsCategoryUpsertComponent,
        //     data: { pageMode: PageMode.Add },
        //     pathMatch: 'full'
        // },
        // {
        //     path: ':id/edit',
        //     component: MasuringToolsCategoryUpsertComponent,
        //     data: { pageMode: PageMode.Edit },
        //     pathMatch: 'full'
        // },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UsersAequalesRoutingModule {}
