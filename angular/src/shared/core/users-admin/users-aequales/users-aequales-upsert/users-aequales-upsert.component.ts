import { Component, OnInit, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ReloadTable } from '@shared/core/dynamic-table/dynamic-table.model';
import { OptionsMenu } from '@shared/core/premium-dashboard/premium-dashboard.model';
import {
    CreateOrUpdateRoleInput,
    RoleEditDto,
    RoleServiceProxy,
    UpsertUserAequalesDto,
    UserRoleDto,
    UsersAequalesServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'users-aequales-upsert',
    templateUrl: './users-aequales-upsert.component.html',
})
export class UsersAequalesUpsertComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<ReloadTable> = new EventEmitter<ReloadTable>();

    saving = false;

    role: RoleEditDto = new RoleEditDto();
    form: FormGroup;
    isNew = true;
    subscriptions: Subscription[] = [];
    optionsMenu: OptionsMenu[] = [
        {
            index: 'userInformation',
            titleLabel: 'users_aequeales_userinformationtab_title',
            active: true,
        },
        // {
        //     index: 'roles',
        //     titleLabel: 'Roles',
        //     active: false,
        // },
    ];

    optionSelectedMenu = 'userInformation';
    roles: UserRoleDto[] = [];
    constructor(
        injector: Injector,
        private _roleService: RoleServiceProxy,
        private _usersAequalesServiceProxy: UsersAequalesServiceProxy,
        private _formBuilder: FormBuilder
    ) {
        super(injector);
    }

    ngOnInit(): void {
        const sub = this._usersAequalesServiceProxy.getRoles('Aequales').subscribe(
            async (res: UserRoleDto[]) => {
                this.roles = res;
            },
            (err) => {
                console.error('_usersAequalesServiceProxy.getRoles');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    selectMenu(item: OptionsMenu): void {
        this.optionSelectedMenu = item.index;
    }
    show(id?: number): void {
        const self = this;

        self.modal.show();
        this._getInformation(id);
    }
    private async _getInformation(id?: number) {
        if (id) {
            const sub = this._usersAequalesServiceProxy.get(id).subscribe(
                async (res: UpsertUserAequalesDto) => {
                    this.form = await this._formInitialize(res);
                },
                (err) => {
                    console.error('_usersAequalesServiceProxy.get');
                }
            );
            this.subscriptions = [...this.subscriptions, sub];
        } else {
            this.form = await this._formInitialize(new UpsertUserAequalesDto());
        }
    }
    private async _formInitialize(user: UpsertUserAequalesDto) {
        const onlyLettersPattern = '[a-zA-Z ]*';
        const onlyLettersAndNumbersPattern = '[a-zA-Z0-9]*';
        this.isNew = (user.id || 0) === 0;
        const validator = this.isNew
            ? [Validators.required, Validators.pattern(onlyLettersAndNumbersPattern)]
            : [Validators.pattern(onlyLettersAndNumbersPattern)];
        return this._formBuilder.group(
            {
                id: [user.id || 0],
                name: [user.name || '', [Validators.required, Validators.pattern(onlyLettersPattern)]],
                surname: [user.surname || '', [Validators.required, Validators.pattern(onlyLettersPattern)]],
                emailAddress: [
                    user.emailAddress || '',
                    [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')],
                ],
                userName: [
                    user.userName || '',
                    [Validators.required, Validators.pattern(onlyLettersAndNumbersPattern)],
                ],
                password: ['', [...validator]],
                repeatPassword: ['', [...validator]],
                roleId: [
                    user.roleId || this.roles[0].roleId,
                    [Validators.required, Validators.pattern(onlyLettersAndNumbersPattern)],
                ],
                SetRandomPassword: [false, []],
                ShouldChangePasswordOnNextLogin: [false, []],
                OrganizationUnits: [[], []],
            },
            {
                // check whether our password and confirm password match
                validator: this.passwordMatchValidator,
            }
        );
    }
    passwordMatchValidator(control: AbstractControl) {
        const password: string = control.get('password').value; // get password from our password form control
        const repeatPassword: string = control.get('repeatPassword').value; // get password from our confirmPassword form control
        // compare is the password math
        if (password !== repeatPassword) {
            // if they don't match, set an error in our confirmPassword form control
            control.get('repeatPassword').setErrors({ NoPassswordMatch: true });
        }
    }

    onShown(): void {
        document.getElementById('name')?.focus();
    }
    closeOnClick(): void {
        if (!this.form.pristine) {
            abp.message.confirm(
                this.l('users_aequeales_confirm_close_mgs'),
                this.l('users_aequeales_confirm_close_title'),
                (result: boolean) => {
                    if (result) {
                        this.close();
                    }
                }
            );
        } else {
            this.close();
        }
    }

    saveOnClick(): void {
        const { valid } = this.form;
        if (valid) {
            // this.sanitizeData();
            if (this.isNew) {
                this._add();
            } else {
                this._edit();
            }
        } else {
            this.form.markAllAsTouched();
        }
    }
    private _add() {
        const sub = this._usersAequalesServiceProxy.create(this._getUpsertUserAequalesWrapperDto()).subscribe(
            (res: any) => {
                abp.notify.success(this.l('users_aequeales_success_created'));
                this.modalSave.emit({ reset: true });
                this.close();
            },
            (err) => {
                console.error('_usersAequalesServiceProxy.create');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }
    private _edit() {
        const sub = this._usersAequalesServiceProxy
            .update(this.form.controls.id.value, this._getUpsertUserAequalesWrapperDto())
            .subscribe(
                (res: any) => {
                    abp.notify.success(this.l('users_aequeales_success_updated'));
                    this.modalSave.emit({ reload: true });
                    this.close();
                },
                (err) => {
                    console.error('_usersAequalesServiceProxy.create');
                }
            );
        this.subscriptions = [...this.subscriptions, sub];
    }
    private _getUpsertUserAequalesWrapperDto(): UpsertUserAequalesDto {
        const { value } = this.form;
        const {
            name,
            surname,
            emailAddress,
            userName,
            password,
            roleId,
            setRandomPassword,
            shouldChangePasswordOnNextLogin,
            organizationUnits,
        } = value;
        const user = new UpsertUserAequalesDto();
        user.name = name;
        user.surname = surname;
        user.emailAddress = emailAddress;
        user.userName = userName;
        user.password = password;
        user.roleId = roleId;
        user.setRandomPassword = setRandomPassword;
        user.shouldChangePasswordOnNextLogin = shouldChangePasswordOnNextLogin;
        user.organizationUnits = organizationUnits;
        return user;
    }
    save(): void {
        const self = this;

        const input = new CreateOrUpdateRoleInput();
        input.role = self.role;
    }
    onFormSubmit($event, form) {
        $event.preventDefault;
    }
    private close(): void {
        this.form = null;
        this.optionSelectedMenu = 'userInformation';
        this.modal.hide();
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
