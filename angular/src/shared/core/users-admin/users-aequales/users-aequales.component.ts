import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { filter as _filter } from 'lodash-es';
import { Subscription, BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { debounce as _debounce } from 'lodash-es';
import { ConfigColumns, PageStatus, RecordsTable, ReloadTable } from '../../dynamic-table/dynamic-table.model';
import { IUserAequalesListItemDto, UsersAequalesServiceProxy } from '@shared/service-proxies/service-proxies';
import { UsersAequalesUpsertComponent } from './users-aequales-upsert/users-aequales-upsert.component';

@Component({
    templateUrl: './users-aequales.component.html',
    animations: [appModuleAnimation()],
})
export class UsersAequalesComponent extends AppComponentBase implements OnInit {
    columns: ConfigColumns[] = [
        {
            field: 'name',
            titleLabel: 'users_aequeales_grid_name',
            type: 'string',
        },
        {
            field: 'surname',
            titleLabel: 'users_aequeales_grid_lastname',
            type: 'string',
        },
        {
            field: 'emailAddress',
            titleLabel: 'users_aequeales_grid_emailuser',
            type: 'string',
        },
        {
            field: 'role',
            titleLabel: 'users_aequeales_grid_rol',
            type: 'string',
        },
        {
            field: 'creationTime',
            titleLabel: 'users_aequeales_grid_creationtime',
            type: 'date',
        },
    ];
    filtersByColumns: ConfigColumns[] = [
        {
            field: 'name',
            titleLabel: this.l('users_aequeales_grid_name'),
            type: 'string',
        },
        {
            field: 'surname',
            titleLabel: this.l('users_aequeales_grid_lastname'),
            type: 'string',
        },
    ];
    @ViewChild('entitiesUsersAequalesUpsert', { static: true })
    entitiesUsersAequalesUpsert: UsersAequalesUpsertComponent;

    subscriptions: Subscription[] = [];
    private _reload: BehaviorSubject<ReloadTable> = new BehaviorSubject({});
    set reload(value: ReloadTable) {
        this._reload.next(value);
    }
    get reload(): ReloadTable {
        return this._reload.value;
    }
    reload$(): Observable<ReloadTable> {
        return this._reload.asObservable();
    }
    constructor(
        injector: Injector,
        private _router: Router,
        private _usersAequalesServiceProxy: UsersAequalesServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.loadDataTable = this.loadDataTable.bind(this);
    }
    loadDataTable(data: PageStatus) {
        return this._usersAequalesServiceProxy.getAllPaged(
            data.filter,
            data.filtersByColumns,
            [],
            0,
            false,
            data.sorting,
            data.pageCount,
            data.page
        );
    }
    /**
     * add button on click action
     */
    addOnClick(): void {
        this.entitiesUsersAequalesUpsert.show();
    }
    editOnClick(data: IUserAequalesListItemDto): void {
        this.entitiesUsersAequalesUpsert.show(data.id);
    }

    deleteOnClick(data: IUserAequalesListItemDto): void {
        abp.message.confirm(
            this.l('users_aequeales_confirm_delete_mgs'),
            this.l('users_aequeales_confirm_delete_title'),
            (result: boolean) => {
                if (result) {
                    this.delete(data.id);
                }
            }
        );
    }
    private delete(id: number) {
        const sub = this._usersAequalesServiceProxy.delete(id).subscribe(
            (res: any) => {
                this.getInformation({ reset: true });
                abp.notify.success(this.l('users_aequeales_success_delete'));
            },
            (err) => {
                console.error('_usersAequalesServiceProxy.delete');
            }
        );
        this.subscriptions = [...this.subscriptions, sub];
    }

    /**
     * filter button on click action
     */
    filterOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration', 'zipCodes', 'add']);
    }
    getInformation(data: ReloadTable): void {
        this.reload = data;
    }
    /**
     * close button on click action
     */
    closeOnClick(): void {
        // this._router.navigate(['app', 'main', 'administration']);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }
}
