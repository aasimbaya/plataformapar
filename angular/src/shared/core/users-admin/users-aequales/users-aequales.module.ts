import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CoreModule } from '@shared/core/core.module';
import { UsersAequalesRoutingModule } from './users-aequales-routing.module';
import { UsersAequalesComponent } from './users-aequales.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { UsersAequalesUpsertComponent } from './users-aequales-upsert/users-aequales-upsert.component';
import { TabRolesComponent } from './tab-roles/tab-roles.component';
import { TabUserInformationComponent } from './tab-user-information/tab-user-information.component';
import { InputsModule } from '@shared/core/inputs/inputs.module';

@NgModule({
    declarations: [
        UsersAequalesComponent,
        UsersAequalesUpsertComponent,
        TabRolesComponent,
        TabUserInformationComponent,
    ],
    imports: [
        AppSharedModule,
        AdminSharedModule,
        UsersAequalesRoutingModule,
        CoreModule,
        NgxDropzoneModule,
        InputsModule,
    ],
})
export class UsersAequalesModule {}
