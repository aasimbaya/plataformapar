import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersGroupingsComponent } from './users-groupings.component';
import { PageMode } from '@shared/AppEnums';
import { UsersGroupingsUpsertComponent } from './users-groupings-upsert/users-groupings-upsert.component';

const routes: Routes = [
    {
        path: '',
        component: UsersGroupingsComponent,
        pathMatch: 'full',
        },
        {
            path: 'add',
            component: UsersGroupingsUpsertComponent,
            data: { pageMode: PageMode.Add },
            pathMatch: 'full'
        },
        {
            path: ':id/edit',
            component: UsersGroupingsUpsertComponent,
            data: { pageMode: PageMode.Edit },
            pathMatch: 'full'
        },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UsersGroupingsRoutingModule {}
