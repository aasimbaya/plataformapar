import { Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { isEmpty as _isEmpty } from 'lodash-es';
import { AppComponentBase } from '@shared/common/app-component-base';
import { BreadcrumbItem } from '@app/shared/common/sub-header/sub-header.component';

import { ActivatedRoute, Router } from '@angular/router';
import { PageMode } from '@shared/AppEnums';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ConfigColumns, PageStatus, RecordsTable } from '@shared/core/dynamic-table/dynamic-table.model';
import {
    PagedResultDtoOfQuestionsByCategoryListItemDto,
    QuestionCategoryServiceProxy,
    UpsertQuestionCategoryDto,
    UpsertQuestionCategoryTypeDto,
} from '@shared/service-proxies/service-proxies';
import { OptionsMenu } from '@shared/core/premium-dashboard/premium-dashboard.model';

/**
 * Main component for zip code edit
 */
@Component({
    selector: 'users-groupings-upsert',
    styleUrls: ['./users-groupings-upsert.component.less'],
    templateUrl: './users-groupings-upsert.component.html',
})
export class UsersGroupingsUpsertComponent extends AppComponentBase {
    breadcrumbs: BreadcrumbItem[] = [];
    loading: boolean = false;
    pageMode: PageMode;
    subscriptions: Subscription[] = [];
    form: FormGroup;
    isNew = true;
    optionsTypeCategory: UpsertQuestionCategoryTypeDto[] = [];
    files: File[] = [];

    filter: string = '';

    constructor(injector: Injector, private _activatedRoute: ActivatedRoute, private _router: Router) {
        super(injector);
    }

    /**
     * OnInit
     */
    ngOnInit(): void {
        this.pageMode = this._activatedRoute.snapshot.data.pageMode;
        const id = this._activatedRoute.snapshot.params.id;
        this._init(id);
    }

    private async _init(id) {
        // this.getQuestionCategoryType();
        if (id) {
            this.breadcrumbs = [new BreadcrumbItem(this.l('EditGroup'))];
            //     this.isNew = false;
            //     this.getQuestionCategory(id);
        } else {
            this.breadcrumbs = [new BreadcrumbItem(this.l('NewGroup'))];
            //     this.isNew = true;
            this._formInitialize(new UpsertQuestionCategoryDto());
        }
    }

    private async _formInitialize(data: UpsertQuestionCategoryDto) {
        this.form = new FormGroup({
            id: new FormControl(''),
            name: new FormControl(''),
            users: new FormGroup({
                usersId: new FormControl(''),
                // last: new FormControl('Drew', Validators.required)
            }),
            companies: new FormGroup({
                companiesId: new FormControl(''),
                // last: new FormControl('Drew', Validators.required)
            }),
        });
    }

    closeOnClick(): void {
        if (!this.form.pristine) {
            abp.message.confirm(
                this.l('MsgConfirmCloseZipCode'),
                this.l('TitleConfirmCloseZipCode'),
                (result: boolean) => {
                    if (result) {
                        this._redirectToMeasuringToolsCategoryListPage();
                    }
                }
            );
        } else {
            this._redirectToMeasuringToolsCategoryListPage();
        }
        this._redirectToMeasuringToolsCategoryListPage();
    }

    saveOnClick(): void {
        const { valid } = this.form;
        if (valid) {
            // this.sanitizeData();
            if (this.isNew) {
                this._add();
            } else {
                this._edit();
            }
        } else {
            this.form.markAllAsTouched();
        }
    }
    onFormSubmit($event, form) {
        $event.preventDefault;
    }
    private _edit() {
        // const sub = this._questionCategoryServiceProxy.update(this.form.controls.id.value, this._getUpsertQuestionCategoryDto()).subscribe((res: any) => {
        //     abp.notify.success(this.l('SuccessfullyUpdated'));
        //     this._redirectToMeasuringToolsCategoryListPage();
        // }, err => {
        //     console.error('_questionCategoryServiceProxy.update');
        // });
        // this.subscriptions = [...this.subscriptions, sub];
    }

    private _add() {
        // const sub = this._questionCategoryServiceProxy.create(this._getUpsertQuestionCategoryDto()).subscribe((res: any) => {
        //     abp.notify.success(this.l('SuccessfullyCreated'));
        //     this._redirectToMeasuringToolsCategoryListPage();
        // }, err => {
        //     console.error('_questionCategoryServiceProxy.create');
        // });
        // this.subscriptions = [...this.subscriptions, sub];
    }
    private _getUpsertQuestionCategoryDto(): UpsertQuestionCategoryDto {
        const { value } = this.form;
        const { name, description, isActive, questionCategoryTypeId, icon } = value;
        const data = new UpsertQuestionCategoryDto();
        data.name = name;
        data.description = description;
        data.isActive = isActive;
        data.questionCategoryTypeId = questionCategoryTypeId;
        data.icon = icon;
        return data;
    }

    // private sanitizeData() {
    //     const {
    //         zipCode1,
    //         city,
    //         state,
    //         county,
    //         branch,
    //         taxCode,
    //         group1,
    //         group2,
    //         group3,
    //         group4,
    //         group5,
    //         group6,
    //         latitude,
    //         longitude,
    //         areaCode,
    //         timeZone,
    //         elevation,
    //     } = this.upsertZipCodeDto;
    //     this.upsertZipCodeDto.zipCode1 = zipCode1.toUpperCase().trim();
    //     this.upsertZipCodeDto.city = this.getTrim(city);
    //     this.upsertZipCodeDto.state = this.getTrim(state);
    //     this.upsertZipCodeDto.county = this.getTrim(county);
    //     this.upsertZipCodeDto.taxCode = this.getTrim(taxCode);
    //     this.upsertZipCodeDto.group1 = this.getTrim(group1);
    //     this.upsertZipCodeDto.group2 = this.getTrim(group2);
    //     this.upsertZipCodeDto.group3 = this.getTrim(group3);
    //     this.upsertZipCodeDto.group4 = this.getTrim(group4);
    //     this.upsertZipCodeDto.group5 = this.getTrim(group5);
    //     this.upsertZipCodeDto.group6 = this.getTrim(group6);
    //     this.upsertZipCodeDto.latitude = this.getCoordinate(latitude?.toString());
    //     this.upsertZipCodeDto.longitude = this.getCoordinate(longitude?.toString());
    //     this.upsertZipCodeDto.areaCode = this.getTrim(areaCode);
    //     this.upsertZipCodeDto.timeZone = this.getCoordinate(timeZone?.toString());
    //     this.upsertZipCodeDto.elevation = this.getCoordinate(elevation?.toString());
    // }

    private _redirectToMeasuringToolsCategoryListPage(): void {
        //revisar
        this._router.navigate(['app', 'entities-users', 'groupings']);
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub?.unsubscribe());
    }
}
