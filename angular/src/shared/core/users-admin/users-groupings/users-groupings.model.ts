export interface EntityUserGrouping {
    id: number;
    name: string;
    count: number;
    createdDate: Date,
}