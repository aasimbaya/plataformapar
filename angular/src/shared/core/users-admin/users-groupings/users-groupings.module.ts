import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CoreModule } from '@shared/core/core.module';
import { UsersGroupingsRoutingModule } from './users-groupings-routing.module';
import { UsersGroupingsComponent } from './users-groupings.component';
import { UsersGroupingsUpsertComponent } from './users-groupings-upsert/users-groupings-upsert.component';
import { InputsModule } from '@shared/core/inputs/inputs.module';

@NgModule({
    declarations: [UsersGroupingsComponent, UsersGroupingsUpsertComponent],
    imports: [AppSharedModule, AdminSharedModule, UsersGroupingsRoutingModule, CoreModule, InputsModule],
})
export class UsersGroupingsModule {}
