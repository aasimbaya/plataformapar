import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appPasswordShowHide]',
})
export class PasswordShowHideDirective {
    isVisible: boolean = false;
    constructor(private el: ElementRef, private renderer: Renderer2) {
        this.createIcon();
    }

    createIcon() {
        const input = this.el.nativeElement.parentNode;
        const span = document.createElement('span');
        span.innerHTML = `<i class="pi pi-eye" style="font-size: 2rem"></i>`;
        this.renderer.setStyle(span, 'margin-top', '-2.5rem');
        this.renderer.setStyle(span, 'float', 'right');
        this.renderer.setStyle(span, 'margin-right', '1rem');
        span.addEventListener('click', (event) => {
            this.toggle(span);
        });
        input.appendChild(span);
    }

    toggle(span) {
        this.isVisible = !this.isVisible;
        if (this.isVisible) {
            this.renderer.setAttribute(this.el.nativeElement, 'type', 'text');

            span.innerHTML = `<i class="pi pi-eye-slash" style="font-size: 2rem"></i>`;
        } else {
            this.renderer.setAttribute(this.el.nativeElement, 'type', 'password');
            span.innerHTML = `<i class="pi pi-eye" style="font-size: 2rem"></i>`;
        }
    }
}
