﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace PARPlatform
{
    public class PARPlatformClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PARPlatformClientModule).GetAssembly());
        }
    }
}
