﻿using System.Collections.Generic;
using Abp.Auditing;

namespace PARPlatform.Auditing
{
    public interface IExpiredAndDeletedAuditLogBackupService
    {
        bool CanBackup();
        
        void Backup(List<AuditLog> auditLogs);
    }
}