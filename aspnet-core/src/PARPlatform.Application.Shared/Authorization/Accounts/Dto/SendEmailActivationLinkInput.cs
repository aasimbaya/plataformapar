﻿using System.ComponentModel.DataAnnotations;

namespace PARPlatform.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}