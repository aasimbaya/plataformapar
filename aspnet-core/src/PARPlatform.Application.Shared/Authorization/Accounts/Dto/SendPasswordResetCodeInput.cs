﻿using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Users;

namespace PARPlatform.Authorization.Accounts.Dto
{
    public class SendPasswordResetCodeInput
    {
        [MaxLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        [MaxLength(AbpUserBase.MaxEmailAddressLength)]
        public string Identification { get; set; }
    }
}