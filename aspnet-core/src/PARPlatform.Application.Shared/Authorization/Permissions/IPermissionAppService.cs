﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using PARPlatform.Authorization.Permissions.Dto;

namespace PARPlatform.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
