﻿using Abp.Runtime.Validation;
using PARPlatform.Dto;
using System.Collections.Generic;

namespace PARPlatform.Authorization.Roles.Dto
{
    public class GetRolesInput: PagedAndSortedInputDto,IShouldNormalize
    {
        public List<string> Permissions { get; set; }

    public string Filter { get; set; }

    public void Normalize()
    {
        if (string.IsNullOrEmpty(Sorting))
        {
            Sorting = "displayName";
        }
        if (Sorting.Contains("structure"))
        {
            Sorting = Sorting.Replace("structure", "tenantId");
        }
        Filter = Filter?.Trim();
    }
}
}
