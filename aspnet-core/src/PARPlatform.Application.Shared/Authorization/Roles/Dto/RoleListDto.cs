﻿using System;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;

namespace PARPlatform.Authorization.Roles.Dto
{
    public class RoleListDto : EntityDto, IHasCreationTime
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public bool IsStatic { get; set; }

        public bool IsDefault { get; set; }
        public int? TenantId { get; set; }
        public string Structure { get; set; }
        public int CountUsers { get; set; }
        public DateTime CreationTime { get; set; }
    }
}