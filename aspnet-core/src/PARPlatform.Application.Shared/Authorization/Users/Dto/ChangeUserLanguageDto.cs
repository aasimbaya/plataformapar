﻿using System.ComponentModel.DataAnnotations;

namespace PARPlatform.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
