﻿using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PARPlatform.Authorization.Users.Dto
{
    public class CreateOrUpdateAnonymousUserInput
    {
        [Required]
        public UserEditDto User { get; set; }

        [Required]
        public string[] AssignedRoleNames { get; set; }

        [Required]
        public int TenantId { get; set; }

        public bool SendActivationEmail { get; set; }

        public bool SetRandomPassword { get; set; }

        public List<long> OrganizationUnits { get; set; }

        public UpsertEnterpriseGroupDto EnterpriseGroup { get; set; }

        public List<UpsertEnterpriseDto> Enterprises { get; set; }

        public CreateOrUpdateAnonymousUserInput()
        {
            OrganizationUnits = new List<long>();
            Enterprises = new List<UpsertEnterpriseDto>();
        }
    }
}