﻿using System.Collections.Generic;
using PARPlatform.Authorization.Permissions.Dto;

namespace PARPlatform.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}