﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PARPlatform.Authorization.Users.Dto;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;

namespace PARPlatform.Authorization.Users
{
    public interface IUserAnonymousAppService : IApplicationService
    {
        /// <summary>
        /// Create or Update User entity from anonymous requested
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<long> CreateOrUpdateAnonymousUser(CreateOrUpdateAnonymousUserInput input);

        /// <summary>
        /// Create or update EnterpriseGroup entity from anonymous requested
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<long> CreateOrUpdateEnterpriseGroup(UpsertEnterpriseGroupDto input);

        /// <summary>
        /// Create or update Enterprise entity from anonymous requested
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<UpsertEnterpriseDto> CreateOrUpdateEnterprise(UpsertEnterpriseDto input);

        /// <summary>
        /// Create or update enterprises list
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<long> CreateOrUpdateGroupedEnterprises(CreateOrUpdateAnonymousUserInput input);

        /// <summary>
        /// Create or update Personal Data
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<long> CreateOrUpdatePersonalData(UpsertPersonalDto input);

        /// <summary>
        /// Create Create entities for Organization register
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<UpsertOrganizationDataInputDto> CreateOrganization(UpsertOrganizationDataInputDto input);

    }
}
