﻿using PARPlatform.Security;

namespace PARPlatform.Authorization.Users.Profile.Dto
{
    public class GetPasswordComplexitySettingOutput
    {
        public PasswordComplexitySetting Setting { get; set; }
    }
}
