﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PARPlatform.Configuration.Host.Dto;

namespace PARPlatform.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
