﻿using Abp.Auditing;
using PARPlatform.Configuration.Dto;

namespace PARPlatform.Configuration.Tenants.Dto
{
    public class TenantEmailSettingsEditDto : EmailSettingsEditDto
    {
        public bool UseHostDefaultEmailSettings { get; set; }
    }
}