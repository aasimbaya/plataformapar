﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PARPlatform.Configuration.Tenants.Dto;

namespace PARPlatform.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
