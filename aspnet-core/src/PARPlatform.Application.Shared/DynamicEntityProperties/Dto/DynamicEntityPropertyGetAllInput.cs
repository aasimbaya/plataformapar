﻿namespace PARPlatform.DynamicEntityProperties
{
    public class DynamicEntityPropertyGetAllInput
    {
        public string EntityFullName { get; set; }
    }
}
