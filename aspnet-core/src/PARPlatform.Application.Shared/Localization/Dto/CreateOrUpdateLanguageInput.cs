﻿using System.ComponentModel.DataAnnotations;

namespace PARPlatform.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}