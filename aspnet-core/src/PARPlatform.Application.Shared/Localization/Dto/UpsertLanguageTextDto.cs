﻿using System.ComponentModel.DataAnnotations;
using Abp.Localization;

namespace PARPlatform.Localization.Dto
{
    public class UpsertLanguageTextDto
    {
        [Required]
        [StringLength(ApplicationLanguageText.MaxSourceNameLength)]
        public string SourceName { get; set; }

        [Required]
        [StringLength(ApplicationLanguage.MaxNameLength)]
        public string LanguageName { get; set; }
       
        [Required]
        [StringLength(ApplicationLanguageText.MaxKeyLength)]
        public string Key { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(ApplicationLanguageText.MaxValueLength)]
        public string targetValue { get; set; }
    }
}