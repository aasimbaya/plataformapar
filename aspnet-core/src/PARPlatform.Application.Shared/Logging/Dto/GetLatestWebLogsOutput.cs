﻿using System.Collections.Generic;

namespace PARPlatform.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatestWebLogLines { get; set; }
    }
}
