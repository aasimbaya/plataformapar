﻿using Abp.Application.Services;
using PARPlatform.Dto;
using PARPlatform.Logging.Dto;

namespace PARPlatform.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
