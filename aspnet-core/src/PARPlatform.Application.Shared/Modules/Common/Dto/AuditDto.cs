﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.Common.Dto
{
    /// <summary>
    /// partial DTO for Audit
    /// </summary>
    public class AuditDto
    {
        public string CreatorUserName { get; set; }
        public DateTime CreationTime { get; set; }
        public string LastModifierUserName { get; set; }
        public DateTime? LastModificationTime { get; set; }
    }
}
