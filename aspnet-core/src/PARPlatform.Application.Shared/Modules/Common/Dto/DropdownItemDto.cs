﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.Common.Dto
{
    /// <summary>
    /// Dto Dropdown item
    /// </summary>
    public class DropdownItemDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
    }
}
