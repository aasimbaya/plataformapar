﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.Common.Dto
{
    /// <summary>
    /// Dto for Enumerable Dictionary data
    /// </summary>
    public class EnumDictionaryDataDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Subtype { get; set; }
        public string Abbreviation { get; set; }
    }
}
