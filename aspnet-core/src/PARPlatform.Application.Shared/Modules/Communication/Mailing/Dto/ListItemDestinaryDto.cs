﻿namespace PARPlatform.Modules.Communication.Mailing.Dto
{
    /// <summary>
    /// Dto list destinary
    /// </summary>
    public class ListItemDestinaryDto
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
    }
}
