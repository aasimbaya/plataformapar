﻿using PARPlatform.Modules.Common.Dto;

namespace PARPlatform.Modules.Configuration.Sector.Dtos
{
    /// <summary>
    /// DTO for Sector create or edit
    /// </summary>
    public class UpsertSectorDto : AuditDto
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string InnerType { get; set; }
        public bool IsActive { get; set; }
        public string Ciiu { get; set; }
    }
}

