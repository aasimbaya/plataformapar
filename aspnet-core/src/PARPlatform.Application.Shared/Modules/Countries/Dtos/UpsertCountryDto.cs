﻿namespace PARPlatform.Modules.Countries.Dtos
{
    /// <summary>
    /// DTO for Country consultation
    /// </summary>
    public class UpsertCountryDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Abreviation { get; set; }
        public bool IsActive { get; set; }
        public string Code { get; set; }
        public string Iso3Code { get; set; }
        public int NicRucLength { get; set; }
        public int PhoneLength { get; set; }
    }
}

