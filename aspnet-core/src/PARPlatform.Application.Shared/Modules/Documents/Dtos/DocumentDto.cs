﻿using System;

namespace PARPlatform.Modules.Documents.Dtos
{
    public class DocumentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Size { get; set; }

        public string Extension { get; set; }

        public string Estructure { get; set; }

        public string Location { get; set; }

        public string Language { get; set; }

        public bool IsActive { get; set; }

        public DateTime LastModificationTime { get; set; }

    }
}
