﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.Documents.Dtos
{
    /// <summary>
    /// Dto Dropdown Document Estructure
    /// </summary>
    public class DropdownDocumentEstructureDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Estructure { get; set; }

    }
}
