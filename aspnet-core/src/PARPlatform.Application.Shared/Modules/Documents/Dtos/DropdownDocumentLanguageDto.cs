﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.Documents.Dtos
{
    /// <summary>
    /// Dto Dropdown Document Language
    /// </summary>
    public class DropdownDocumentLanguageDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }

    }
}
