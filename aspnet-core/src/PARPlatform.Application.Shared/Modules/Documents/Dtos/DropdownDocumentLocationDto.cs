﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.Documents.Dtos
{
    /// <summary>
    /// Dto Dropdown Document Location
    /// </summary>
    public class DropdownDocumentLocationDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }

    }
}
