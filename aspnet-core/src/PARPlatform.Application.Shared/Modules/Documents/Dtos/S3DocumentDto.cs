﻿using Microsoft.AspNetCore.Http;

namespace PARPlatform.Modules.Documents.Dtos
{

    public class S3DocumentDto
    {
        public IFormFile File { get; set; }

    }
}