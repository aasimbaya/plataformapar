﻿using Abp.Domain.Entities.Auditing;
using Microsoft.AspNetCore.Http;
using PARPlatform.Modules.Common.Dto;


namespace PARPlatform.Modules.Documents.Dtos
{

    public class UpsertDocumentDto : AuditDto
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public string Size { get; set; }

        public long? EstructureId { get; set; }

        public virtual long? LanguageId { get; set; }

        public virtual long? LocationId { get; set; }

        public string URL { get; set; }

        public string Location { get; set; }

        public bool IsActive { get; set; }

    }
}