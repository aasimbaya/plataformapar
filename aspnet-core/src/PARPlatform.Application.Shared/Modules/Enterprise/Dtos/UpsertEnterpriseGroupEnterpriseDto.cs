﻿using Abp.Domain.Entities;
using PARPlatform.Modules.Common.Dto;

namespace PARPlatform.Modules.Enterprise.Dtos
{
    /// <summary>
    /// DTO for Enterprise Group Enterprise
    /// </summary>
    public class UpsertEnterpriseGroupEnterpriseDto : AuditDto
    {
        public long Id { get; set; }
        public virtual long? EnterpriseGroupId { get; set; }
        public virtual long? EnterpriseId { get; set; }
        public bool IsActive { get; set; }
    }
}

