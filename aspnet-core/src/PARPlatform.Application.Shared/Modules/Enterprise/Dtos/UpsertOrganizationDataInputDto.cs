﻿using PARPlatform.Authorization.Users.Dto;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.Enterprise.Dtos
{
    /// <summary>
    /// Dto to receive user, enterprise or enterprise group and additional information for register
    /// </summary>
    public class UpsertOrganizationDataInputDto
    {
        /// <summary>
        /// User information (Enterprises and Enterprise group)
        /// </summary>
        public CreateOrUpdateAnonymousUserInput AnonymousUserInput { get; set; }

        /// <summary>
        /// Data of survey person, CEO and Human resources
        /// </summary>
        public List<UpsertPersonalDto> PersonsDataInput { get; set; }
    }
}
