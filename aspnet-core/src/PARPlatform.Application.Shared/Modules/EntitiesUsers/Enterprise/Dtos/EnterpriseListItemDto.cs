﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos
{
    /// <summary>
    /// Dto for User Enterprise row item
    /// </summary>
    public class EnterpriseListItemDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string EnterpriseGroupName { get; set; }

        public string Sector { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime LastModificationTime { get; set; }
    }
}
