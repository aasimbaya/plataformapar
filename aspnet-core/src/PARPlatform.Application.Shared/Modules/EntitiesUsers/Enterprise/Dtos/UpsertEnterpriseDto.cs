﻿using PARPlatform.Modules.Common.Dto;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos
{
    /// <summary>
    /// DTO for User Enterprise create or edit
    /// </summary>
    public class UpsertEnterpriseDto : AuditDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string IdNumber { get; set; }
        public bool IsActive { get; set; }
        public long UserId { get; set; }
        public long CountryId { get; set; }
        public long RegionId { get; set; }
        public long CityId { get; set; }
        public long SectorId { get; set; }
        public int EmployeesNumber { get; set; }
        public bool HasInclusion { get; set; }
        public bool IsInternational { get; set; }
        public bool ReceivePromoAccepted { get; set; }
        public long LineBusinessId { get; set; }
        public string ListUsersId { get; set; }
        public string ListPlansId { get; set; }
        public int TenantId { get; set; }
        public bool IsPartOfEconomicGroup { get; set; }
        public string EconomicGroupName { get; set; }
        public bool IsPartOfTradeAssociation { get; set; }
        public long? TradeAssociationId { get; set; }
        public string InclusionRegionalManagerName { get; set; }
        public string InclusionRegionalManagerEmail { get; set; }
        public string EnterpriseTypeId { get; set; }
    }
}
