﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos
{
    /// <summary>
    /// DTO to manage the update enterprise user information from sidebar
    /// </summary>
    public class UpsertEnterpriseUserFromSidebarDto
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }
}
