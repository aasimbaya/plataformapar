﻿using PARPlatform.Modules.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos
{
    /// <summary>
    /// DTO for Personal
    /// </summary>
    public class UpsertPersonalDto : AuditDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string CellPhone { get; set; }
        public long? PositionId { get; set; }
        public long? PersonalTypeId { get; set; }
        public long? EnterpriseId { get; set; }
        public bool IsActive { get; set; }
    }
}
