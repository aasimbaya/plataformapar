﻿using PARPlatform.Modules.Common.Dto;

namespace PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos
{
    /// <summary>
    /// DTO for Personal create or edit
    /// </summary>
    public class UpsertPersonalTypeDto : AuditDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string IdNumber { get; set; }
        public bool IsActive { get; set; }
        public long UserId { get; set; }
        public long CountryId { get; set; }
        public long RegionId { get; set; }
        public long CityId { get; set; }
        public long SectorId { get; set; }
        public int EmployeesNumber { get; set; }
        public bool HasInclusion { get; set; }
        public bool IsInternational { get; set; }
        public bool ReceivePromoAccepted { get; set; }
        public long LineBusinessId { get; set; }
        public string UsersId { get; set; }
        public string PlansId { get; set; }
    }
}
