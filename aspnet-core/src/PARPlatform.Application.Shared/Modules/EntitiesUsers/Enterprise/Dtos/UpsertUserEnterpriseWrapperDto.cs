﻿using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;

namespace PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos
{
    /// <summary>
    /// Request DTO for create and update enterprise
    /// </summary>
    public class UpsertUserEnterpriseWrapperDto
    {
        public UpsertEnterpriseDto User { get; set; }
        public UpsertPersonalDto RRHH { get; set; }
        public UpsertPersonalDto CEO { get; set; }
    }
}
