﻿
using System;

namespace PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos
{
    /// <summary>
    /// Request DTO for get users by enterprise
    /// </summary>
    public class UpsertUsersByEnterpriseDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public DateTime CreationTime { get; set; }

    }
}
