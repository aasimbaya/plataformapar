﻿using PARPlatform.Modules.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos
{
    /// <summary>
    /// DTO to manage the object user aequales for create or edit purposes
    /// </summary>
    public class UpsertUserAequalesDto : AuditDto
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public int RoleId { get; set; }
        public string Password { get; set; }
        public bool SetRandomPassword { get; set; }
        public bool SendActivationEmail { get; set; }
        public bool ShouldChangePasswordOnNextLogin { get; set; }
        public List<long> OrganizationUnits { get; set; }
    }
}
