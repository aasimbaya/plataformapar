﻿using System;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos
{
    /// <summary>
    /// Dto for user aequales
    /// </summary> 
    public class UserAequalesListItemDto
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string Role { get; set; }
        public DateTime CreationTime { get; set; }

    }
}
