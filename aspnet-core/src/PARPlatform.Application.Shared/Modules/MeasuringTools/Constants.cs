﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.MeasuringTools
{
    public static class Constants
    {
        public const int SPECIAL_DISTINTION = 1;
        public const int QUESTIONNARIES = 2;

        #region Question Use Constants
        public const long USE_CATEGORY = 1;
        public const long USE_SPECIAL_DISTINCTION = 2;

        public const string NO_CATEGORY_DASH = "-";
        #endregion

        #region Question Type Constants
        public const long TYPE_BOOLEAN = 1;
        public const long TYPE_MULTIPLESELECTION = 2;
        public const long TYPE_SINGLESELECTION = 3;
        public const long TYPE_OPENQUESTION = 4;
        public const long TYPE_STRUCTURE = 5;
        #endregion

        #region [Associated]
        public const int ASSOCIATED_SPECIAL_DISTINCTION = 1;
        public const int ASSOCIATED_CATEGORY = 2;
        #endregion
    }
}
