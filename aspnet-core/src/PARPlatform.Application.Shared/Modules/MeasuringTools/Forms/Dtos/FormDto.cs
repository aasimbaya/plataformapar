﻿using Abp.Application.Services.Dto;
using System;

namespace PARPlatform.Modules.MeasuringTools.Forms.Dtos
{
    /// <summary>
    /// Dto Form
    /// </summary>
    public class FormDto 
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public int FormNameId { get; set; }
        public string Description { get; set; }
        public string ThematicName { get; set; }
        public string TypeName { get; set; }
        public bool IsActive { get; set; }
        public DateTime LastModificationTime { get; set; }
        public int? AssociatedQuestions { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
