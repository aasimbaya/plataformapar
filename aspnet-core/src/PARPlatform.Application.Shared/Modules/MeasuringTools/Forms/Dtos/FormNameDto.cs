﻿using PARPlatform.Modules.Common.Dto;

namespace PARPlatform.Modules.MeasuringTools.Forms.Dtos
{
    /// <summary>
    /// Dto FormName 
    /// </summary>
    public class FormNameDto 
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
