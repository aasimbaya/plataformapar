﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.MeasuringTools.Forms.Dtos
{
    public class ListItemAssociatedToFormDto
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public int? AssociatedQuestions { get; set; }
    }
}
