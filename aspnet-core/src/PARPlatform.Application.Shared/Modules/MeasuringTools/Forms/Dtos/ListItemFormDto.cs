﻿using System;

namespace PARPlatform.Modules.MeasuringTools.Forms.Dtos
{
    /// <summary>
    /// Dto list form
    /// </summary>
    public class ListItemFormDto
    {
        public long? Id { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
        public DateTime LastModificationTime { get; set; }
        public int? AssociatedQuestions { get; set; }
        public string TypeQuestionnaireName { get; set; }
    }
}
