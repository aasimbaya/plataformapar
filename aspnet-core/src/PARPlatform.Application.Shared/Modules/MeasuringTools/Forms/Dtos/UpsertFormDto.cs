﻿using PARPlatform.Modules.Common.Dto;
using System;

namespace PARPlatform.Modules.MeasuringTools.Forms.Dtos
{
    /// <summary>
    /// Dto upsert Form
    /// </summary>
    public class UpsertFormDto
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public int FormNameId { get; set; }
        public string Description { get; set; }
        public string ThematicName { get; set; }
        public long ThematicId { get; set; }
        public string TypeName { get; set; }
        public long TypeId { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string SpecialDistinctionIdArray { get; set; }
        public string QuestionCategoryIdArray { get; set; }
    }
}
