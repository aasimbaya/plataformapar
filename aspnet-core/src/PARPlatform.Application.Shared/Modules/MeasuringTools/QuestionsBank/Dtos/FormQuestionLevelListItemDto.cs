﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    /// <summary>
    /// Dto for list of level Items of a question from Question Bank
    /// </summary>
    public class FormQuestionLevelListItemDto
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<LevelQuestionDataDto> ChildrenQuestionData { get; set; }
    }
}
