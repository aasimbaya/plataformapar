﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    /// <summary>
    /// Dto for list of level Items of a question from Question Bank with detailed children question information
    /// </summary>
    public class FormQuestionLevelWithQuestionDataDto
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<QuestionnaireQuestionDetailDto> ChildrenQuestionData { get; set; }
    }
}
