﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    /// <summary>
    /// Dto for Question bank detailed data 
    /// </summary>
    public class LevelQuestionDataDto
    {
        public long Id { get; set; }
        public string ShortVersion { get; set; }
        public string Question { get; set; }
    }
}
