﻿namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    /// <summary>
    /// Dto for Question Use dropdown
    /// </summary>
    public class QuestionUseDropdownDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
