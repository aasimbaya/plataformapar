﻿namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    public enum QuestionUseType
    {
        All = 0,
        Category = 1,
        SpecialDistinction = 2
    }
}
