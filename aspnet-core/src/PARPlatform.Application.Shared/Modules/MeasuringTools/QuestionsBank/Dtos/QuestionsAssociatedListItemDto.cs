﻿namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    public class QuestionsAssociatedListItemDto
    {
        public int Id { get; set; }
        public int Order { get; set; }
        public string Question { get; set; }
    }
}
