﻿namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    /// <summary>
    /// Dto for Question Bank list Detailed information
    /// </summary>
    public class QuestionsBankDetailedListItemDto : QuestionsBankListItemDto
    {
        public long QuestionnaireTypeId { get; set; }
        public long UseId { get; set; }
        public long CategoryId { get; set; }
        public long ThemeId { get; set; }
        public long TypeQuestionId { get; set; }
    }
}
