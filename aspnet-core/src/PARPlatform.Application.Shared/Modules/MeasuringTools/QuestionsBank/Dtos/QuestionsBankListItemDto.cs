﻿using System;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    /// <summary>
    /// Dto for Question Bank list row
    /// </summary>
    public class QuestionsBankListItemDto
    {
        public long Id { get; set; }
        public string Question { get; set; }
        public string QuestionnaireType { get; set; }
        public string Use { get; set; }
        public string Category { get; set; }
        public string Theme { get; set; }
        public string TypeQuestion { get; set; }
        public DateTime LastModificationTime { get; set; }
    }
}
