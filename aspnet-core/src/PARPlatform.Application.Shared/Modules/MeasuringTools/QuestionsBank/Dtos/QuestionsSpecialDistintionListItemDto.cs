﻿namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    public class QuestionsSpecialDistintionListItemDto
    {
        public int Id { get; set; }
        public int Order { get; set; }
        public string Question { get; set; }
    }
}
