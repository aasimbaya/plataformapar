﻿using PARPlatform.Modules.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    public class UpsertAnswerDto
    {
        public long Id { get; set; }
        public string AnswerContent { get; set; }
        public long FormQuestionId { get; set; }
        public long? ValidationTypeId { get; set; }
        public decimal Score { get; set; }
        public decimal? Percentage { get; set; }
        public bool AutoNull { get; set; }
        public bool SpecializedResource { get; set; }
        public string PersonalId { get; set; }
    }
}
