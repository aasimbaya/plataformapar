﻿using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    /// <summary>
    /// DTO for Form Question Level create or edit
    /// </summary>
    public class UpsertFormQuestionLevel
    {
        public long? Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<long> ChildrenQuestion { get; set; }
        public bool IsActive { get; set; }
    }
}
