﻿using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos
{
    /// <summary>
    /// DTO for Questions bank create or edit
    /// </summary>
    public class UpsertQuestionsBankDto : AuditDto
    {
        public long? Id { get; set; }
        public string ShortVersion { get; set; }
        public long FormTypeId { get; set; }
        public long FormThematicId { get; set; }
        public long QuestionTypeId { get; set; }
        public long QuestionCategoryId { get; set; }
        public long? ParentId { get; set; }
        public string Question { get; set; }
        public string Tooltip { get; set; }
        public string FixedText { get; set; }
        public string Tag { get; set; }
        public bool IsActive { get; set; }
        public long UsageId { get; set; }
        public string ImplementationTip { get; set; }
        public int? TypeScore { get; set; }
        public bool IsNullable { get; set; }
        public bool IsCertified { get; set; }
        public bool IsPriority { get; set; }
        public List<long> ConditionalParentIds { get; set; }
        public decimal? Score { get; set; }
        public decimal? TotalScore { get; set; }
        public string Formula { get; set; }
        public long? ParentValidation { get; set; }
        public List<UpsertAnswerDto> Answers { get; set; }
        /// <summary>
        /// List of level data for save associated questions ids
        /// </summary>
        public List<UpsertFormQuestionLevel> FormQuestionLevelsForSave { get; set; }
        /// <summary>
        /// List of level data with Associated questions detailed information to show
        /// </summary>
        public List<FormQuestionLevelListItemDto> LoadFormQuestionLevelData { get; set; }
    }
}
