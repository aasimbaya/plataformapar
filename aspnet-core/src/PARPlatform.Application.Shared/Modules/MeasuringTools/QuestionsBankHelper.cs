﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.MeasuringTools
{
    /// <summary>
    /// Helper class with common methods shared between handlers
    /// </summary>
    public static class QuestionsBankHelper
    {
        /// <summary>
        /// Return new Tag for Question with specific Id
        /// </summary>
        /// <param name="id">Question Identificator</param>
        public static string CreateQuestionTag(long id)
        {
            return id.ToString() + "_" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss:FFFF");
        }

        /// <summary>
        /// Create new PersonalId unique identificator for answer
        /// </summary>
        /// <param name="index">Index of corresponding answer</param>
        /// <returns></returns>
        public static string CreateNewAnswerPersonalId(int index)
        {
            return DateTime.Now.ToString("yyyyMMddHHmmss") + "answer" + index.ToString();
        }
    }
}
