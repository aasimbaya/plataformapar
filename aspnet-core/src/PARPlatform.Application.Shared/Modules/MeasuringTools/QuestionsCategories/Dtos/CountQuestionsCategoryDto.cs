﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos
{
    /// <summary>
    /// Dto for count question category
    /// </summary> 
    public class CountQuestionsCategoryDto
    {
        public long Id { get; set; }
        public int Count { get; set; }
    }
}
