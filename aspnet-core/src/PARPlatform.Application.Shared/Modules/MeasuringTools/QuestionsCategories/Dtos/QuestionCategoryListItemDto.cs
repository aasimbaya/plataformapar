﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos
{
    /// <summary>
    /// Dto for question category
    /// </summary> 
    public class QuestionCategoryListItemDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public long QuestionCategoryTypeId { get; set; }
        public string QuestionCategoryTypeName { get; set; }
        public int QuestionsRelations { get; set; }
        public DateTime LastModificationTime { get; set; }
        public string Icon { get; set; }
    }
}
