﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos
{
    /// <summary>
    /// Dto for question category
    /// </summary> 
    public class QuestionsByCategoryListItemDto
    {
        public long Id { get; set; }
        public string Question { get; set; }
        public int Order { get; set; }
    }
}
