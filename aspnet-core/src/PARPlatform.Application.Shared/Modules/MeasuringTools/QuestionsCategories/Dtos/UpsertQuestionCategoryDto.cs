﻿using System;
using System.Collections.Generic;
using System.Text;
using PARPlatform.Modules.Common.Dto;
namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos
{
    /// <summary>
    /// DTO to manage the object question category for create or edit purposes
    /// </summary>
    public class UpsertQuestionCategoryDto : AuditDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public long QuestionCategoryTypeId { get; set; }
        public string Icon { get; set; }
    }
}
