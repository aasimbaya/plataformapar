﻿using System;
using System.Collections.Generic;
using System.Text;
using PARPlatform.Modules.Common.Dto;
namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos
{
    /// <summary>
    /// DTO to manage the object type question category for create or edit purposes
    /// </summary>
    public class UpsertQuestionCategoryTypeDto : AuditDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
