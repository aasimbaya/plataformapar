﻿using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Tenants.Dtos
{
    /// <summary>
    /// DTO for Tenant consultation
    /// </summary>
    public class UpsertTenantDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
