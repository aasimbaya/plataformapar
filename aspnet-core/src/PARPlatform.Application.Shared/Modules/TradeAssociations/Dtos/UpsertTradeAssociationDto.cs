﻿namespace PARPlatform.Modules.TradeAssociations.Dtos
{
    /// <summary>
    /// DTO for Trade association consultation
    /// </summary>
    public class UpsertTradeAssociationDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public long? CountryId { get; set; }
    }
}
