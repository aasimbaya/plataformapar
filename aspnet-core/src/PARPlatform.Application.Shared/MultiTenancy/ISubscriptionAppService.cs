﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace PARPlatform.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task DisableRecurringPayments();

        Task EnableRecurringPayments();
    }
}
