using System.Threading.Tasks;
using Abp.Application.Services;
using PARPlatform.Editions.Dto;
using PARPlatform.MultiTenancy.Dto;

namespace PARPlatform.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}