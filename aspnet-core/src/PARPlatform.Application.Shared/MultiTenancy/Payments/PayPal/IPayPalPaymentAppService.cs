﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PARPlatform.MultiTenancy.Payments.PayPal.Dto;

namespace PARPlatform.MultiTenancy.Payments.PayPal
{
    public interface IPayPalPaymentAppService : IApplicationService
    {
        Task ConfirmPayment(long paymentId, string paypalOrderId);

        PayPalConfigurationDto GetConfiguration();
    }
}
