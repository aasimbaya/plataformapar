﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PARPlatform.MultiTenancy.Payments.Dto;
using PARPlatform.MultiTenancy.Payments.Stripe.Dto;

namespace PARPlatform.MultiTenancy.Payments.Stripe
{
    public interface IStripePaymentAppService : IApplicationService
    {
        Task ConfirmPayment(StripeConfirmPaymentInput input);

        StripeConfigurationDto GetConfiguration();

        Task<SubscriptionPaymentDto> GetPaymentAsync(StripeGetPaymentInput input);

        Task<string> CreatePaymentSession(StripeCreatePaymentSessionInput input);
    }
}