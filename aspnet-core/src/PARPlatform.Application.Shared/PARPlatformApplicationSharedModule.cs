﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace PARPlatform
{
    [DependsOn(typeof(PARPlatformCoreSharedModule))]
    public class PARPlatformApplicationSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PARPlatformApplicationSharedModule).GetAssembly());
        }
    }
}