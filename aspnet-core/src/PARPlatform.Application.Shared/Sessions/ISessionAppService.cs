﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PARPlatform.Sessions.Dto;

namespace PARPlatform.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
