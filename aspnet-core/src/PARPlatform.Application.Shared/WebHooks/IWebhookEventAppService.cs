﻿using System.Threading.Tasks;
using Abp.Webhooks;

namespace PARPlatform.WebHooks
{
    public interface IWebhookEventAppService
    {
        Task<WebhookEvent> Get(string id);
    }
}
