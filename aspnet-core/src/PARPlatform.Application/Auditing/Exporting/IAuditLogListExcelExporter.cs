﻿using System.Collections.Generic;
using PARPlatform.Auditing.Dto;
using PARPlatform.Dto;

namespace PARPlatform.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);

        FileDto ExportToFile(List<EntityChangeListDto> entityChangeListDtos);
    }
}
