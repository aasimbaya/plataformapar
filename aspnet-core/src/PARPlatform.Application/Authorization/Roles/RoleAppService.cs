﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.Zero.Configuration;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Authorization.Permissions;
using PARPlatform.Authorization.Permissions.Dto;
using PARPlatform.Authorization.Roles.Dto;
using PARPlatform.Base;
using PARPlatform.MultiTenancy;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using PARPlatform.Authorization.Users;
using Abp.Authorization.Users;
using System;
using Abp.Collections.Extensions;

namespace PARPlatform.Authorization.Roles
{
    /// <summary>
    /// Application service that is used by 'role management' page.
    /// </summary>
    [AbpAuthorize(AppPermissions.Pages_Administration_Roles)]
    public class RoleAppService : PARPlatformAppServiceBase, IRoleAppService
    {
        private readonly RoleManager _roleManager;
        private readonly IRoleManagementConfig _roleManagementConfig;
        private readonly IRepository<Tenant> _tenantsRepository;
        private readonly IRepository<UserRole> _userRoleRepository;

        public RoleAppService(
            RoleManager roleManager,
            IRoleManagementConfig roleManagementConfig,
            IRepository<UserRole> userRoleRepository,
             IRepository<Tenant> tenantsRepository)
        {
            _roleManager = roleManager;
            _roleManagementConfig = roleManagementConfig;
            _tenantsRepository = tenantsRepository;
            _userRoleRepository = userRoleRepository;
            ;
        }

        [HttpPost]
        public async Task<ListResultDto<RoleListDto>> GetRoles(GetRolesInput input)
        {
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);

            IQueryable<Role> query = _roleManager.Roles;

            if (input.Permissions != null && input.Permissions.Any(p => !string.IsNullOrEmpty(p)))
            {
                input.Permissions = input.Permissions.Where(p => !string.IsNullOrEmpty(p)).ToList();

                var staticRoleNames = _roleManagementConfig.StaticRoles.Where(
                    r => r.GrantAllPermissionsByDefault &&
                         r.Side == AbpSession.MultiTenancySide
                ).Select(r => r.RoleName).ToList();

                foreach (var permission in input.Permissions)
                {
                    query = query.Where(r =>
                        r.Permissions.Any(rp => rp.Name == permission)
                            ? r.Permissions.Any(rp => rp.Name == permission && rp.IsGranted)
                            : staticRoleNames.Contains(r.Name)
                    );
                }
            }

            List<RoleListDto> roles;
            if (input.Sorting.Contains("countUsers"))
            {
              
                roles =
                   (from role in query
                    join userRoleR in _userRoleRepository.GetAll() on role.Id equals userRoleR.RoleId into userRoleCout
                    from userRole in userRoleCout.DefaultIfEmpty()
                    group role by new
                    {
                        role.Id,
                        role.Name,
                        role.DisplayName,
                        role.IsStatic,
                        role.IsDefault,
                        role.TenantId,
                        role.CreationTime,
                        count = userRole.Id != null ? true : false
                    }
                   into rolesUsers
                    //orderby questionsCategory.Key
                    select new RoleListDto
                    {
                        Id = rolesUsers.Key.Id,
                        Name = rolesUsers.Key.Name,
                        DisplayName = rolesUsers.Key.DisplayName,
                        IsStatic = rolesUsers.Key.IsStatic,
                        IsDefault = rolesUsers.Key.IsDefault,
                        TenantId = rolesUsers.Key.TenantId,
                        Structure = "",
                        CountUsers = (rolesUsers.Key.count) ? rolesUsers.Count() : 0,
                        CreationTime = rolesUsers.Key.CreationTime,
                    })
                   .ToList();
            }
            else
            {
                List<Role> rolesTemp = query.OrderBy(input.Sorting).PageBy(input).ToList();
                roles = ObjectMapper.Map<List<RoleListDto>>(rolesTemp);
            }
            List<Tenant> Tenant = _tenantsRepository.GetAll().ToList();
            foreach (var role in roles)
            {
                Tenant? tenant = Tenant.Where(item => item.Id == role.TenantId).FirstOrDefault();
                if (tenant != null)
                {
                    role.Structure = tenant.TenancyName;
                }
                role.CountUsers = (role.CountUsers > 0) ? role.CountUsers : _userRoleRepository.GetAll().WhereIf(true, us => us.RoleId == role.Id).Count();
            }

            return new ListResultDto<RoleListDto>(roles);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Roles_Create, AppPermissions.Pages_Administration_Roles_Edit)]
        public async Task<GetRoleForEditOutput> GetRoleForEdit(NullableIdDto input)
        {
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);

            var permissions = PermissionManager.GetAllPermissions();
            var grantedPermissions = new Permission[0];
            RoleEditDto roleEditDto;

            if (input.Id.HasValue) //Editing existing role?
            {
                var role = await _roleManager.GetRoleByIdAsync(input.Id.Value);
                CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
                grantedPermissions = (await _roleManager.GetGrantedPermissionsAsync(role)).ToArray();
                roleEditDto = ObjectMapper.Map<RoleEditDto>(role);
            }
            else
            {
                roleEditDto = new RoleEditDto();
            }

            return new GetRoleForEditOutput
            {
                Role = roleEditDto,
                Permissions = ObjectMapper.Map<List<FlatPermissionDto>>(permissions).OrderBy(p => p.DisplayName).ToList(),
                GrantedPermissionNames = grantedPermissions.Select(p => p.Name).ToList()
            };
        }

        public async Task CreateOrUpdateRole(CreateOrUpdateRoleInput input)
        {
            if (input.Role.Id.HasValue)
            {
                await UpdateRoleAsync(input);
            }
            else
            {
                await CreateRoleAsync(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Roles_Delete)]
        public async Task DeleteRole(EntityDto input)
        {
            var role = await _roleManager.GetRoleByIdAsync(input.Id);

            var users = await UserManager.GetUsersInRoleAsync(role.Name);
            foreach (var user in users)
            {
                CheckErrors(await UserManager.RemoveFromRoleAsync(user, role.Name));
            }

            CheckErrors(await _roleManager.DeleteAsync(role));
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Roles_Edit)]
        protected virtual async Task UpdateRoleAsync(CreateOrUpdateRoleInput input)
        {
            Debug.Assert(input.Role.Id != null, "input.Role.Id should be set.");
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            var role = await _roleManager.GetRoleByIdAsync(input.Role.Id.Value);
            role.DisplayName = input.Role.DisplayName;
            role.TenantId = input.Role.TenantId;
            role.IsDefault = input.Role.IsDefault;

            await UpdateGrantedPermissionsAsync(role, input.GrantedPermissionNames);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Roles_Create)]
        protected virtual async Task CreateRoleAsync(CreateOrUpdateRoleInput input)
        {
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            var role = new Role(input.Role.TenantId, input.Role.DisplayName) { IsDefault = input.Role.IsDefault };
            CheckErrors(await _roleManager.CreateAsync(role));
            await CurrentUnitOfWork.SaveChangesAsync(); //It's done to get Id of the role.
            await UpdateGrantedPermissionsAsync(role, input.GrantedPermissionNames);
        }

        private async Task UpdateGrantedPermissionsAsync(Role role, List<string> grantedPermissionNames)
        {
            var grantedPermissions = PermissionManager.GetPermissionsFromNamesByValidating(grantedPermissionNames);
            await _roleManager.SetGrantedPermissionsAsync(role, grantedPermissions);
        }
    }
}
