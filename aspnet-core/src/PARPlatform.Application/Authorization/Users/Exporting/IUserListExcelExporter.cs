using System.Collections.Generic;
using PARPlatform.Authorization.Users.Dto;
using PARPlatform.Dto;

namespace PARPlatform.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}