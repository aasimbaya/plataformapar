﻿using System.Collections.Generic;
using PARPlatform.Authorization.Users.Importing.Dto;
using PARPlatform.Dto;

namespace PARPlatform.Authorization.Users.Importing
{
    public interface IInvalidUserExporter
    {
        FileDto ExportToFile(List<ImportUserDto> userListDtos);
    }
}
