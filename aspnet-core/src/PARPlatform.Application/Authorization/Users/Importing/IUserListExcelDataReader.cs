﻿using System.Collections.Generic;
using PARPlatform.Authorization.Users.Importing.Dto;
using Abp.Dependency;

namespace PARPlatform.Authorization.Users.Importing
{
    public interface IUserListExcelDataReader: ITransientDependency
    {
        List<ImportUserDto> GetUsersFromExcel(byte[] fileBytes);
    }
}
