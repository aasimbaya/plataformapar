using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Microsoft.AspNetCore.Identity;
using PARPlatform.Authorization.Users.Dto;
using PARPlatform.MultiTenancy;
using PARPlatform.Authorization.Roles;
using PARPlatform.Notifications;
using Abp.Notifications;
using PARPlatform.Url;
using MediatR;
using PARPlatform.Modules.Enterprises.Commands;
using PARPlatform.Modules.EnterprisesGroup.Commands;
using Abp.Domain.Uow;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using PARPlatform.Modules.EnterpriseGroupEnterprises.Command;
using PARPlatform.Modules.Personals.Commands;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Helpers;

namespace PARPlatform.Authorization.Users
{
    [AbpAllowAnonymous]
    public class UserAnonymousAppService : PARPlatformAppServiceBase, IUserAnonymousAppService
    {
        private readonly AbpLoginResultTypeHelper _abpLoginResultTypeHelper;
        private readonly IUserLinkManager _userLinkManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<UserAccount, long> _userAccountRepository;
        private readonly LogInManager _logInManager;
        private readonly IUserPolicy _userPolicy;
        private readonly UserManager _userManager;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly RoleManager _roleManager;
        private readonly IUserEmailer _userEmailer;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IEnumerable<IPasswordValidator<User>> _passwordValidators;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        public IAppUrlService AppUrlService { get; set; }
        private readonly IMediator _mediator;
        private readonly IRepository<User, long> _userRepository;

        public UserAnonymousAppService(
            AbpLoginResultTypeHelper abpLoginResultTypeHelper,
            IUserLinkManager userLinkManager,
            IRepository<Tenant> tenantRepository,
            IRepository<UserAccount, long> userAccountRepository,
            LogInManager logInManager,
            IUserPolicy userPolicy,
            UserManager userManager,
            IPasswordHasher<User> passwordHasher,
            RoleManager roleManager, IUserEmailer userEmailer,
            INotificationSubscriptionManager notificationSubscriptionManager,
            IAppNotifier appNotifier,
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            IMediator mediator,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<User, long> userRepository)
        {
            _abpLoginResultTypeHelper = abpLoginResultTypeHelper;
            _userLinkManager = userLinkManager;
            _tenantRepository = tenantRepository;
            _userAccountRepository = userAccountRepository;
            _logInManager = logInManager;
            _userPolicy = userPolicy;
            _userManager = userManager;
            _passwordHasher = passwordHasher;
            _roleManager = roleManager;
            _userEmailer = userEmailer;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
            AppUrlService = NullAppUrlService.Instance;
            _passwordValidators = passwordValidators;
            _mediator = mediator;
            _unitOfWorkManager = unitOfWorkManager;
            _userRepository = userRepository;
        }

        public async Task<long> CreateOrUpdateAnonymousUser(CreateOrUpdateAnonymousUserInput input)
        {
            if (input.User.Id.HasValue)
            {
                return await UpdateAnonymousUserAsync(input);
            }
            else
            {
                return await CreateAnonymousUserAsync(input);
            }
        }

        [AbpAllowAnonymous]
        public async Task<long> CreateOrUpdateEnterpriseGroup(UpsertEnterpriseGroupDto input)
        {
            if (input.Id > 0)
            {
                var enterpriseGroupToSave = ObjectMapper.Map<UpdateEnterpriseGroupCommand>(input);
                var saved = await _mediator.Send(enterpriseGroupToSave);
                return saved.Id;
            }
            else
            {
                var enterpriseGroupToSave = ObjectMapper.Map<CreateEnterpriseGroupCommand>(input);
                var saved = await _mediator.Send(enterpriseGroupToSave);
                return saved.Id;
            }
        }

        [AbpAllowAnonymous]
        public async Task<UpsertEnterpriseDto> CreateOrUpdateEnterprise(UpsertEnterpriseDto input)
        {
            if (input.Id > 0)
            {
                UpdateEnterpriseCommand update = ObjectMapper.Map<UpdateEnterpriseCommand>(input);
                var saved = await _mediator.Send(update);
                return saved;
            }
            else
            {
                var create = ObjectMapper.Map<CreateEnterpriseCommand>(input);
                var saved = await _mediator.Send(create);
                return saved;
            }
        }

        [AbpAllowAnonymous]
        public async Task<long> CreateOrUpdateGroupedEnterprises(CreateOrUpdateAnonymousUserInput input)
        {
            UpsertEnterpriseDto saved = null;
            long result = 0;
            foreach (UpsertEnterpriseDto enterprise in input.Enterprises)
            {
                // Create enterprise
                if (enterprise.Id == 0)
                {
                    var enterpriseToSave = ObjectMapper.Map<CreateEnterpriseCommand>(enterprise);
                    saved = await _mediator.Send(enterpriseToSave);
                    result = saved.Id;
                }
                else
                {
                    var enterpriseToUpdate = ObjectMapper.Map<UpdateEnterpriseCommand>(enterprise);
                    saved = await _mediator.Send(enterpriseToUpdate);
                    result = saved.Id;
                }

                // Create relation between enterprise and group
                if (saved != null && input.EnterpriseGroup != null && input.EnterpriseGroup.Id > 0)
                {
                    CreateEnterpriseGroupEnterpriseCommand relation = new CreateEnterpriseGroupEnterpriseCommand
                    {
                        EnterpriseGroupId = input.EnterpriseGroup.Id,
                        EnterpriseId = saved.Id,
                        IsActive = true
                    };
                    await _mediator.Send(relation);
                }
            }
            return result;
        }

        [AbpAllowAnonymous]
        public async Task<long> CreateOrUpdatePersonalData(UpsertPersonalDto input)
        {
            if (input.Id > 0)
            {
                UpdatePersonalCommand update = ObjectMapper.Map<UpdatePersonalCommand>(input);
                var saved = await _mediator.Send(update);
                return saved.Id;
            }
            else
            {
                var create = ObjectMapper.Map<CreatePersonalCommand>(input);
                var saved = await _mediator.Send(create);
                return saved.Id;
            }
        }

        [Route("/api/services/app/UserAnonymous/AnonymousUserExist/{emailAddress?}")]
        public async Task<bool> AnonymousUserExist([FromRoute] string emailAddress)
        {
            // Get current users list in the system
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            User existingUser = await _userRepository.FirstOrDefaultAsync(p => p.EmailAddress.Equals(emailAddress));
            return existingUser != null;
        }

        /// <summary>
        /// Allow create a new user
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAllowAnonymous]
        protected virtual async Task<long> CreateAnonymousUserAsync(CreateOrUpdateAnonymousUserInput input)
        {
            // Get current users list in the system
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            List<User> existingUsers = _userRepository.GetAll().ToList();

            // Set the generated username  and tenant to the new user
            input.User.UserName = GetUsername(existingUsers, input.User);

            var user = ObjectMapper.Map<User>(input.User); //Passwords is not mapped (see mapping configuration)
            user.TenantId = input.TenantId;
            user.IsActive = true;

            //Set password
            if (input.SetRandomPassword)
            {
                var randomPassword = await _userManager.CreateRandomPassword();
                user.Password = _passwordHasher.HashPassword(user, randomPassword);
                input.User.Password = randomPassword;
            }
            else if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                foreach (var validator in _passwordValidators)
                {
                    CheckErrors(await validator.ValidateAsync(UserManager, user, input.User.Password));
                }

                user.Password = _passwordHasher.HashPassword(user, input.User.Password);
            }

            user.ShouldChangePasswordOnNextLogin = input.User.ShouldChangePasswordOnNextLogin;

            //Assign roles
            user.Roles = new Collection<UserRole>();
            foreach (var roleName in input.AssignedRoleNames)
            {
                var role = await _roleManager.GetRoleByNameAsync(roleName);
                user.Roles.Add(new UserRole(user.TenantId ?? AbpSession.TenantId, user.Id, role.Id));
            }

            CheckErrors(await UserManager.CreateAsync(user));
            await CurrentUnitOfWork.SaveChangesAsync(); //To get new user's Id.

            //Notifications
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.ToUserIdentifier());
            await _appNotifier.WelcomeToTheApplicationAsync(user);

            //Organization Units
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            //Send activation email
            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.User.Password
                );
            }

            return user.Id;
        }

        /// <summary>
        /// Allow update an existing user
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAllowAnonymous]
        protected virtual async Task<long> UpdateAnonymousUserAsync(CreateOrUpdateAnonymousUserInput input)
        {
            User user = null;

            // Get current users list in the system            
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            List<User> existingUsers = _userRepository.GetAll().ToList();

            // Get user to update
            user = await UserManager.FindByIdAsync(input.User.Id.Value.ToString());

            //Update user properties
            ObjectMapper.Map(input.User, user); //Passwords is not mapped (see mapping configuration)

            // Set the generated username  and tenant to the new user
            user.UserName = GetUsername(existingUsers, input.User);
            user.TenantId = input.TenantId;

            CheckErrors(await UserManager.UpdateAsync(user));

            if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                CheckErrors(await UserManager.ChangePasswordAsync(user, input.User.Password));
            }

            //Update roles
            CheckErrors(await UserManager.SetRolesAsync(user, input.AssignedRoleNames));

            //update organization units
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.User.Password
                );
            }

            return user.Id;
        }

        /// <summary>
        /// Retrieves the most acceptable username for the user
        /// </summary>
        /// <param name="userListToCompare">General list of users in the system</param>
        /// <param name="user">User Data</param>
        /// <returns></returns>
        private string GetUsername(List<User> userListToCompare, UserEditDto user)
        {
            string usernameGenerated = string.Empty;
            int index = 0;
            do
            {
                ++index;
                usernameGenerated = GenerateUserName(user, index);
            } while (userListToCompare.Any(p => p.UserName.Equals(usernameGenerated)));
            return usernameGenerated;
        }

        /// <summary>
        /// Allow generate the username from user data and an index of sequence (0) initialized inside of do-while bucle
        /// </summary>
        /// <param name="user"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private string GenerateUserName(UserEditDto user, int index)
        {
            string name = user.Name.Contains(' ') ? user.Name.Split(' ').First() : user.Name;
            string surname = user.Surname.Contains(' ') ? user.Surname.Split(' ').First() : user.Surname;

            name = name.RemoveAccents();
            surname = surname.RemoveAccents();

            if (index <= name.Length)
            {
                return $"{name.Substring(0, index).ToLower()}{surname.ToLower()}";
            }
            else
            {
                return $"{name.ToLower()}{surname.ToLower()}{index - name.Length}";
            }
        }

        /// <summary>
        /// Create Organization from register
        /// </summary>
        /// <param name="input">Data for create entities related to Organization</param>
        /// <returns></returns>
        [AbpAllowAnonymous]
        public async Task<UpsertOrganizationDataInputDto> CreateOrganization(UpsertOrganizationDataInputDto input)
        {
            long userId = await CreateAnonymousUserAsync(input.AnonymousUserInput);
            input.AnonymousUserInput.User.Id = userId;

            if (input.AnonymousUserInput.EnterpriseGroup != null)
            {
                input.AnonymousUserInput.EnterpriseGroup.UserId = userId;
                CreateEnterpriseGroupCommand enterpriseGroupToSave = ObjectMapper.Map<CreateEnterpriseGroupCommand>(input.AnonymousUserInput.EnterpriseGroup);
                UpsertEnterpriseGroupDto enterpriseGroup = await _mediator.Send(enterpriseGroupToSave);
                input.AnonymousUserInput.EnterpriseGroup.Id = enterpriseGroup.Id;                
            }

            foreach (UpsertEnterpriseDto enterprise in input.AnonymousUserInput.Enterprises)
            {
                enterprise.UserId = userId;
                enterprise.TenantId = input.AnonymousUserInput.TenantId;
                CreateEnterpriseCommand enterpriseToSave = ObjectMapper.Map<CreateEnterpriseCommand>(enterprise);
                UpsertEnterpriseDto saved = await _mediator.Send(enterpriseToSave);
                enterprise.Id = saved.Id;

                // Create relation between enterprise and group
                if (saved != null && input.AnonymousUserInput.EnterpriseGroup != null && input.AnonymousUserInput.EnterpriseGroup.Id > 0)
                {
                    CreateEnterpriseGroupEnterpriseCommand relation = new CreateEnterpriseGroupEnterpriseCommand
                    {
                        EnterpriseGroupId = input.AnonymousUserInput.EnterpriseGroup.Id,
                        EnterpriseId = saved.Id,
                        IsActive = true
                    };
                    await _mediator.Send(relation);
                }
            }

            foreach (UpsertPersonalDto personData in input.PersonsDataInput)
            {
                CreatePersonalCommand command = ObjectMapper.Map<CreatePersonalCommand>(personData);
                UpsertPersonalDto person = await _mediator.Send(command);
                personData.Id = person.Id;
            }

            return input;
        }
    }
}
