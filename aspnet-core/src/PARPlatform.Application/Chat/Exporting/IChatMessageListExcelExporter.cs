﻿using System.Collections.Generic;
using Abp;
using PARPlatform.Chat.Dto;
using PARPlatform.Dto;

namespace PARPlatform.Chat.Exporting
{
    public interface IChatMessageListExcelExporter
    {
        FileDto ExportToFile(UserIdentifier user, List<ChatMessageExportDto> messages);
    }
}
