using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.DynamicEntityProperties;
using Abp.EntityHistory;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using Abp.Webhooks;
using AutoMapper;
using IdentityServer4.Extensions;
using PARPlatform.Auditing.Dto;
using PARPlatform.Authorization.Accounts.Dto;
using PARPlatform.Authorization.Delegation;
using PARPlatform.Authorization.Permissions.Dto;
using PARPlatform.Authorization.Roles;
using PARPlatform.Authorization.Roles.Dto;
using PARPlatform.Authorization.Users;
using PARPlatform.Authorization.Users.Delegation.Dto;
using PARPlatform.Authorization.Users.Dto;
using PARPlatform.Authorization.Users.Importing.Dto;
using PARPlatform.Authorization.Users.Profile.Dto;
using PARPlatform.Chat;
using PARPlatform.Chat.Dto;
using PARPlatform.DynamicEntityProperties.Dto;
using PARPlatform.Editions;
using PARPlatform.Editions.Dto;
using PARPlatform.Friendships;
using PARPlatform.Friendships.Cache;
using PARPlatform.Friendships.Dto;
using PARPlatform.Localization.Dto;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Configuration.Sector.Dtos;
using PARPlatform.Modules.Countries.Dtos;
using PARPlatform.Modules.Documents.Commands;
using PARPlatform.Modules.Documents.Dtos;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.EnterpriseGroupEnterprises.Command;
using PARPlatform.Modules.Enterprises.Commands;
using PARPlatform.Modules.EnterprisesGroup.Commands;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Commands;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;
using PARPlatform.Modules.Language.Commands;
using PARPlatform.Modules.MeasuringTools;
using PARPlatform.Modules.MeasuringTools.Forms.Commands;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Commands;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using PARPlatform.Modules.Personals.Commands;
using PARPlatform.Modules.Sectors.Commands;
using PARPlatform.MultiTenancy;
using PARPlatform.MultiTenancy.Dto;
using PARPlatform.MultiTenancy.HostDashboard.Dto;
using PARPlatform.MultiTenancy.Payments;
using PARPlatform.MultiTenancy.Payments.Dto;
using PARPlatform.Notifications.Dto;
using PARPlatform.Organizations.Dto;
using PARPlatform.PAREntities;
using PARPlatform.Sessions.Dto;
using PARPlatform.WebHooks.Dto;

namespace PARPlatform
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            #region Forms 
            configuration.CreateMap<Form, CreateFormCommand>().ReverseMap();
            configuration.CreateMap<Form, UpsertFormDto>().ReverseMap();
            configuration.CreateMap<UpsertFormDto, CreateFormCommand>().ReverseMap();

            configuration.CreateMap<Form, UpdateFormCommand>().ReverseMap();
            configuration.CreateMap<UpsertFormDto, UpdateFormCommand>().ReverseMap();
            #endregion

            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

            //Chat
            configuration.CreateMap<ChatMessage, ChatMessageDto>();
            configuration.CreateMap<ChatMessage, ChatMessageExportDto>();

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();
            configuration.CreateMap<UserRole, UserListRoleDto>();



            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionCreateDto, SubscribableEdition>();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<SubscribableEdition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();


            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            configuration.CreateMap<User, ChatUserDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<Role, OrganizationUnitRoleListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();
            configuration.CreateMap<ImportUserDto, User>();

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();
            configuration.CreateMap<EntityChange, EntityChangeListDto>();
            configuration.CreateMap<EntityPropertyChange, EntityPropertyChangeDto>();

            //Friendship
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();

            //OrganizationUnit
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();

            //Webhooks
            configuration.CreateMap<WebhookSubscription, GetAllSubscriptionsOutput>();
            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOutput>()
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.WebhookName,
                    options => options.MapFrom(l => l.WebhookEvent.WebhookName))
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.Data,
                    options => options.MapFrom(l => l.WebhookEvent.Data));

            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOfWebhookEventOutput>();

            configuration.CreateMap<DynamicProperty, DynamicPropertyDto>().ReverseMap();
            configuration.CreateMap<DynamicPropertyValue, DynamicPropertyValueDto>().ReverseMap();
            configuration.CreateMap<DynamicEntityProperty, DynamicEntityPropertyDto>()
                .ForMember(dto => dto.DynamicPropertyName,
                    options => options.MapFrom(entity => entity.DynamicProperty.DisplayName.IsNullOrEmpty() ? entity.DynamicProperty.PropertyName : entity.DynamicProperty.DisplayName));
            configuration.CreateMap<DynamicEntityPropertyDto, DynamicEntityProperty>();

            configuration.CreateMap<DynamicEntityPropertyValue, DynamicEntityPropertyValueDto>().ReverseMap();

            //User Delegations
            configuration.CreateMap<CreateUserDelegationDto, UserDelegation>();

            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */

            configuration.CreateMap<EnumDictionary, EnumDictionaryDataDto>().ReverseMap();

            #region [MeasuringTools mappings]

            #region [Categories mappings]

            configuration.CreateMap<CreateQuestionCategoryCommand, UpsertQuestionCategoryDto>().ReverseMap();
            configuration.CreateMap<UpdateQuestionCategoryCommand, UpsertQuestionCategoryDto>().ReverseMap();

            configuration.CreateMap<QuestionCategory, QuestionCategoryListItemDto>()
                .ForMember(dto => dto.QuestionCategoryTypeName, opt => opt.MapFrom(a => a.QuestionCategoryTypeFk.Name));
            configuration.CreateMap<FormQuestion, QuestionsByCategoryListItemDto>();

            configuration.CreateMap<QuestionCategory, UpsertQuestionCategoryDto>()
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id))
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id));

            configuration.CreateMap<QuestionCategoryType, UpsertQuestionCategoryTypeDto>()
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id))
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id));

            configuration.CreateMap<UpsertQuestionCategoryDto, CreateQuestionCategoryCommand>();
            configuration.CreateMap<UpsertQuestionCategoryDto, UpdateQuestionCategoryCommand>();

            configuration.CreateMap<CreateQuestionCategoryCommand, QuestionCategory>()
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id));

            configuration.CreateMap<UpdateQuestionCategoryCommand, QuestionCategory>()
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id));

            configuration.CreateMap<UpsertQuestionCategoryDto, QuestionCategory>()
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id));

            #endregion

            #region [Form]
            configuration.CreateMap<Form, ListItemFormDto>().ReverseMap();
            configuration.CreateMap<FormName, FormNameDto>();
            configuration.CreateMap<FormNameDto, FormName>();
            configuration.CreateMap<Form, FormDto>().ReverseMap();
            configuration.CreateMap<ListItemAssociatedToFormDto, FormDto>().ReverseMap();
            configuration.CreateMap<ListItemAssociatedToFormDto, Form>().ReverseMap();
            configuration.CreateMap<ListItemAssociatedToFormDto, QuestionCategory>()
                .ForMember(dto => dto.Name, opt => opt.MapFrom(a => a.Title)).ReverseMap();
            #endregion

            #region [Questions Bank mappings]
            configuration.CreateMap<FormQuestion, QuestionsBankListItemDto>()
                .ForMember(dto => dto.Question, opt => opt.MapFrom(m => m.ShortVersion))
                .ForMember(dto => dto.QuestionnaireType, opt => opt.MapFrom(m => m.FormTypeFk.Title))
                .ForMember(dto => dto.Category, opt => opt.MapFrom(m => (m.UsageId == Constants.USE_SPECIAL_DISTINCTION) ? Constants.NO_CATEGORY_DASH : m.QuestionCategoryFk.Name))
                .ForMember(dto => dto.Theme, opt => opt.MapFrom(m => m.FormThematicFk.Title))
                .ForMember(dto => dto.TypeQuestion, opt => opt.MapFrom(m => m.QuestionTypeFk.Name))
                .ForMember(dto => dto.Use, opt => opt.MapFrom(m => m.UsageFk.Name))
                .ForMember(dto => dto.LastModificationTime, opt => opt.MapFrom(m => m.LastModificationTime.HasValue ? m.LastModificationTime.Value : m.CreationTime));

            configuration.CreateMap<FormQuestion, UpsertQuestionsBankDto>()
                .ForMember(dto => dto.QuestionCategoryId, opt => opt.MapFrom(entity => entity.UsageId == Constants.USE_SPECIAL_DISTINCTION ? entity.FormId : entity.QuestionCategoryId));


            configuration.CreateMap<UpsertQuestionsBankDto, FormQuestion>();
            configuration.CreateMap<CreateQuestionsBankCommand, UpsertQuestionsBankDto>().ReverseMap();
            configuration.CreateMap<UpdateQuestionsBankCommand, UpsertQuestionsBankDto>().ReverseMap();

            configuration.CreateMap<UpsertQuestionsBankDto, QuestionnaireQuestionDetailDto>().ReverseMap();

            configuration.CreateMap<CreateQuestionsBankCommand, FormQuestion>()
                .ForMember(entity => entity.Tag, opt => opt.MapFrom(dto => string.IsNullOrWhiteSpace(dto.Tag) ? "" : dto.Tag))
                .ForMember(entity => entity.QuestionCategoryId, opt =>
                {
                    opt.Condition(dto => dto.UsageId == Constants.USE_CATEGORY);
                    opt.MapFrom(dto => dto.QuestionCategoryId);
                })
                .ForMember(entity => entity.FormId, opt =>
                {
                    opt.Condition(dto => dto.UsageId == Constants.USE_SPECIAL_DISTINCTION);
                    opt.MapFrom(dto => dto.QuestionCategoryId);
                })
                .ReverseMap()
                .ForMember(dto => dto.QuestionCategoryId, opt => opt.MapFrom(entity => (entity.UsageId == Constants.USE_SPECIAL_DISTINCTION ? entity.FormId : entity.QuestionCategoryId)));

            configuration.CreateMap<UpdateQuestionsBankCommand, FormQuestion>()
                .ForMember(entity => entity.Tag, opt => opt.MapFrom(dto => string.IsNullOrWhiteSpace(dto.Tag) ? "" : dto.Tag))
                .ForMember(entity => entity.QuestionCategoryId, opt =>
                {
                    opt.Condition(dto => dto.UsageId == Constants.USE_CATEGORY);
                    opt.MapFrom(dto => dto.QuestionCategoryId);
                })
                .ForMember(entity => entity.FormId, opt =>
                {
                    opt.Condition(dto => dto.UsageId == Constants.USE_SPECIAL_DISTINCTION);
                    opt.MapFrom(dto => dto.QuestionCategoryId);
                })
                .ReverseMap()
                .ForMember(dto => dto.QuestionCategoryId, opt => opt.MapFrom(entity => (entity.UsageId == Constants.USE_SPECIAL_DISTINCTION ? entity.FormId : entity.QuestionCategoryId)));

            configuration.CreateMap<QuestionsSpecialDistintionListItemDto, FormQuestion>().ReverseMap();

            #region [Answers or Level mappings]

            configuration.CreateMap<UpsertAnswerDto, Answer>()
                .ForMember(dto => dto.PersonalId, options => options.Ignore())
                .ForMember(dto => dto.Id, options => options.Ignore())
                .ForMember(dto => dto.FormQuestionId, options => options.Ignore())
                .ReverseMap();

            configuration.CreateMap<UpsertFormQuestionLevel, FormQuestionLevel>()
                .ForMember(dto => dto.Id, options => options.Ignore())
                .ReverseMap();

            configuration.CreateMap<FormQuestion, LevelQuestionDataDto>();
            configuration.CreateMap<FormQuestion, QuestionnaireQuestionDetailDto>();
            configuration.CreateMap<FormQuestionLevel, FormQuestionLevelListItemDto>();
            configuration.CreateMap<FormQuestionLevel, FormQuestionLevelWithQuestionDataDto>();

            #endregion

            #endregion

            #region [Shared DropDownList mappings]

            configuration.CreateMap<FormType, DropdownItemDto>()
               .ForMember(dto => dto.Name, opt => opt.MapFrom(m => m.Title));
            configuration.CreateMap<FormThematic, DropdownItemDto>()
                .ForMember(dto => dto.Name, opt => opt.MapFrom(m => m.Title));
            configuration.CreateMap<Form, DropdownItemDto>()
                .ForMember(dto => dto.Name, opt => opt.MapFrom(m => m.Title));
            configuration.CreateMap<QuestionCategory, DropdownItemDto>();
            configuration.CreateMap<QuestionType, DropdownItemDto>();
            configuration.CreateMap<Usage, DropdownItemDto>();
            configuration.CreateMap<ValidationType, DropdownItemDto>();

            #endregion

            #endregion

            #region [Entities Users mappings]
            //Users Aequales
            configuration.CreateMap<CreateUserAequalesCommand, UpsertUserAequalesDto>().ReverseMap();
            configuration.CreateMap<UpdateUserAequalesCommand, UpsertUserAequalesDto>().ReverseMap();
            // configuration.CreateMap<Form, ListItemFormDto>().ReverseMap();
            // configuration.CreateMap<FormThematic, ListItemFormThematicDto>().ReverseMap();

            configuration.CreateMap<User, UserAequalesListItemDto>();

            configuration.CreateMap<User, UpsertUserAequalesDto>()
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id))
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id));


            configuration.CreateMap<UpsertUserAequalesDto, CreateUserAequalesCommand>();
            configuration.CreateMap<UpsertUserAequalesDto, UpdateUserAequalesCommand>();

            configuration.CreateMap<CreateUserAequalesCommand, User>();
            configuration.CreateMap<UpdateUserAequalesCommand, User>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());


            // configuration.CreateMap<UpsertQuestionCategoryDto, QuestionCategory>()
            //.ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id));

            //Users Enterprise
            configuration.CreateMap<CreateUserEnterpriseCommand, UpsertUserEnterpriseWrapperDto>().ReverseMap();
            configuration.CreateMap<UpdateUserEnterpriseCommand, UpsertUserEnterpriseWrapperDto>().ReverseMap();

            configuration.CreateMap<Enterprise, UpsertEnterpriseDto>();
            configuration.CreateMap<Enterprise, EnterpriseListItemDto>()
                .ForMember(dtoSector => dtoSector.Sector, opt => opt.MapFrom(m => m.SectorFk.Name));

            configuration.CreateMap<User, UpsertUsersByEnterpriseDto>();
            configuration.CreateMap<UpsertEnterpriseDto, Enterprise>()
                .ForMember(dto => dto.TenantId, options => options.Ignore())
                .ReverseMap();

            configuration.CreateMap<Enterprise, UpsertEnterpriseDto>()
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id))
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id));

            configuration.CreateMap<Personal, UpsertPersonalDto>()
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id))
                .ForMember(dto => dto.Id, opt => opt.MapFrom(a => a.Id));


            configuration.CreateMap<UpsertUserEnterpriseWrapperDto, CreateUserEnterpriseCommand>();
            configuration.CreateMap<UpsertUserEnterpriseWrapperDto, UpdateUserEnterpriseCommand>();

            configuration.CreateMap<UpsertEnterpriseDto, CreateEnterpriseCommand>();
            configuration.CreateMap<CreateEnterpriseCommand, Enterprise>()
                .ForMember(p => p.SectorId, opt => opt.MapFrom(f => f.SectorId == 0 ? null : f.SectorId))
                .ForMember(p => p.CountryId, opt => opt.MapFrom(f => f.CountryId == 0 ? null : f.CountryId))
                .ForMember(p => p.RegionId, opt => opt.MapFrom(f => f.RegionId == 0 ? null : f.RegionId))
                .ForMember(p => p.CityId, opt => opt.MapFrom(f => f.CityId == 0 ? null : f.CityId))
                .ForMember(p => p.TradeAssociationId, opt => opt.MapFrom(f => f.TradeAssociationId == 0 ? null : f.TradeAssociationId));

            configuration.CreateMap<UpsertEnterpriseDto, UpdateEnterpriseCommand>();
            configuration.CreateMap<UpdateEnterpriseCommand, Enterprise>()
                .ForMember(p => p.SectorId, opt => opt.MapFrom(f => f.SectorId == 0 ? null : f.SectorId))
                .ForMember(p => p.CountryId, opt => opt.MapFrom(f => f.CountryId == 0 ? null : f.CountryId))
                .ForMember(p => p.RegionId, opt => opt.MapFrom(f => f.RegionId == 0 ? null : f.RegionId))
                .ForMember(p => p.CityId, opt => opt.MapFrom(f => f.CityId == 0 ? null : f.CityId))
                .ForMember(p => p.TradeAssociationId, opt => opt.MapFrom(f => f.TradeAssociationId == 0 ? null : f.TradeAssociationId));


            //configuration.CreateMap<CreateUserAequalesCommand, User>();
            //configuration.CreateMap<UpdateUserAequalesCommand, User>()
            //    .ForMember(dto => dto.Password, options => options.Ignore())
            //    .ReverseMap()
            //    .ForMember(user => user.Password, options => options.Ignore());

            //Personal
            configuration.CreateMap<Personal, UpsertPersonalDto>()
                .ForMember(dto => dto.Id, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Id, options => options.Ignore());
            configuration.CreateMap<UpsertPersonalDto, Personal>()
                .ForMember(dto => dto.Id, options => options.Ignore())
                .ForMember(dto => dto.IsActive, options => options.Ignore())
                .ForMember(dto => dto.PersonalTypeId, options => options.Ignore())
                .ForMember(dto => dto.EnterpriseId, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Id, options => options.Ignore())
                .ReverseMap();

            #endregion

            #region [Enterprise Profile mappings]
            configuration.CreateMap<UpdateUserEnterpriseProfileCommand, User>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());

            configuration.CreateMap<UpdateUserEnterpriseProfileCommand, UpsertEnterpriseUserFromSidebarDto>().ReverseMap();

            #endregion

            configuration.CreateMap<CreateOrUpdateLanguageCommand, UpsertLanguageTextDto>().ReverseMap();
            configuration.CreateMap<ApplicationLanguageText, CreateOrUpdateLanguageCommand>().ReverseMap();
            configuration.CreateMap<ApplicationLanguageText, UpsertLanguageTextDto>();
            configuration.CreateMap<UpsertLanguageTextDto, ApplicationLanguageText>();
            configuration.CreateMap<ApplicationLanguageListDto, ApplicationLanguageText>();

            #region Enterprise Group Mapping
            configuration.CreateMap<UpsertEnterpriseGroupDto, EnterpriseGroup>();
            configuration.CreateMap<EnterpriseGroup, UpsertEnterpriseGroupDto>();
            configuration.CreateMap<CreateEnterpriseGroupCommand, EnterpriseGroup>();
            configuration.CreateMap<UpdateEnterpriseGroupCommand, UpsertEnterpriseGroupDto>();
            configuration.CreateMap<UpsertEnterpriseGroupDto, UpdateEnterpriseGroupCommand>();
            configuration.CreateMap<UpdateEnterpriseGroupCommand, EnterpriseGroup>()
                .ForMember(p => p.SectorId, opt => opt.MapFrom(f => f.SectorId == 0 ? null : f.SectorId))
                .ForMember(p => p.CountryId, opt => opt.MapFrom(f => f.CountryId == 0 ? null : f.CountryId))
                .ForMember(p => p.RegionId, opt => opt.MapFrom(f => f.RegionId == 0 ? null : f.RegionId))
                .ForMember(p => p.CityId, opt => opt.MapFrom(f => f.CityId == 0 ? null : f.CityId))
                .ForMember(p => p.TradeAssociationId, opt => opt.MapFrom(f => f.TradeAssociationId == 0 ? null : f.TradeAssociationId));
            configuration.CreateMap<UpsertEnterpriseGroupDto, CreateEnterpriseGroupCommand>();

            #endregion

            #region Enterprise Group Enterprise Mapping

            configuration.CreateMap<CreateEnterpriseGroupEnterpriseCommand, EnterpriseGroupEnterprise>();
            configuration.CreateMap<EnterpriseGroupEnterprise, UpsertEnterpriseGroupEnterpriseDto>();
            #endregion

            #region Personal Mapping

            configuration.CreateMap<UpsertPersonalDto, CreatePersonalCommand>();
            configuration.CreateMap<CreatePersonalCommand, Personal>();
            configuration.CreateMap<Personal, UpsertPersonalDto>();

            configuration.CreateMap<UpsertPersonalDto, UpdatePersonalCommand>();
            configuration.CreateMap<UpdatePersonalCommand, Personal>();
            #endregion

            #region Sector Mapping

            configuration.CreateMap<UpsertSectorDto, CreateSectorCommand>();
            configuration.CreateMap<CreateSectorCommand, Sector>();
            configuration.CreateMap<Sector, UpsertSectorDto>();
            configuration.CreateMap<UpsertSectorDto, UpdateSectorCommand>();
            configuration.CreateMap<UpdateSectorCommand, Sector>();
            #endregion

            #region Documents
            configuration.CreateMap<DocumentEstructure, DropdownDocumentEstructureDto>();
            configuration.CreateMap<DocumentLocation, DropdownDocumentLocationDto>();
            configuration.CreateMap<DocumentLanguage, DropdownDocumentLanguageDto>();
            configuration.CreateMap<Document, DocumentDto>()
               .ForMember(dtoEstructure => dtoEstructure.Estructure, opt => opt.MapFrom(m => m.EstructureFk.Name))
               .ForMember(dtoLanguage => dtoLanguage.Language, opt => opt.MapFrom(m => m.LanguageFk.Name))
               .ForMember(dtoLocation => dtoLocation.Location, opt => opt.MapFrom(m => m.LocationFk.Name));
            configuration.CreateMap<Document, UpsertDocumentDto>();
            configuration.CreateMap<CreateDocumentCommand, UpsertDocumentDto>();
            configuration.CreateMap<UpdateDocumentCommand, Document>();
            configuration.CreateMap<UpsertDocumentDto, CreateDocumentCommand>();
            configuration.CreateMap<UpsertDocumentDto, UpdateDocumentCommand>();
            configuration.CreateMap<CreateDocumentCommand, Document>();
            configuration.CreateMap<Document, CreateDocumentCommand>();
            configuration.CreateMap<Document, UpdateDocumentCommand>();
            #endregion

            #region Country Mapping

            configuration.CreateMap<Country, UpsertCountryDto>();
            configuration.CreateMap<UpsertCountryDto, Country>();
            #endregion

        }
    }
}
