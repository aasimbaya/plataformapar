﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using PARPlatform.Dto;

namespace PARPlatform.Gdpr
{
    public interface IUserCollectedDataProvider
    {
        Task<List<FileDto>> GetFiles(UserIdentifier user);
    }
}
