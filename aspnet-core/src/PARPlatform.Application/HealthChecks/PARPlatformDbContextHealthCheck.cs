﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using PARPlatform.EntityFrameworkCore;

namespace PARPlatform.HealthChecks
{
    public class PARPlatformDbContextHealthCheck : IHealthCheck
    {
        private readonly DatabaseCheckHelper _checkHelper;

        public PARPlatformDbContextHealthCheck(DatabaseCheckHelper checkHelper)
        {
            _checkHelper = checkHelper;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            if (_checkHelper.Exist("db"))
            {
                return Task.FromResult(HealthCheckResult.Healthy("PARPlatformDbContext connected to database."));
            }

            return Task.FromResult(HealthCheckResult.Unhealthy("PARPlatformDbContext could not connect to database"));
        }
    }
}
