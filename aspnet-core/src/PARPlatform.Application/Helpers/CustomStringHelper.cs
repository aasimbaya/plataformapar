﻿using System.Globalization;
using System.Text;

namespace PARPlatform.Helpers
{
    public static class CustomStringHelper
    {
        /// <summary>
        /// Remove string accents and return a new clean string
        /// </summary>
        /// <param name="text">Original string with accents</param>
        /// <returns>Clean string</returns>
        public static string RemoveAccents(this string text)
        {
            StringBuilder sbReturn = new StringBuilder();
            var arrayText = text.Normalize(NormalizationForm.FormD).ToCharArray();
            foreach (char letter in arrayText)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
                    sbReturn.Append(letter);
            }
            return sbReturn.ToString();
        }
    }
}
