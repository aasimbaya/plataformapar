﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PARPlatform.Install.Dto;

namespace PARPlatform.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}