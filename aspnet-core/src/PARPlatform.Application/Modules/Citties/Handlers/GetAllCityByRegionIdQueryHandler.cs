﻿using Abp.Application.Services.Dto;
using MediatR;
using PARPlatform.Base;
using PARPlatform.PAREntities;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Cities.Queries;

namespace PARPlatform.Modules.Cities.Handlers
{
    /// <summary>
    /// Get all cities by region Id
    /// </summary> 
    public class GetAllCityByRegionIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllCityByRegionIdQuery, List<DropdownItemDto>>
    {
        private readonly IRepository<City> _cityRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="regionRepository"></param>
        public GetAllCityByRegionIdQueryHandler(IRepository<City> cityRepository)
        {
            _cityRepository = cityRepository;
        }
        public Task<List<DropdownItemDto>> Handle(GetAllCityByRegionIdQuery query, CancellationToken cancellationToken)
        {

            IQueryable<DropdownItemDto> regions = _cityRepository.GetAll()
                .Where(p => p.RegionId == query.RegionId)
                .Select(t => new DropdownItemDto { Id = t.Id, Name = t.Name })
                .OrderBy(p => p.Name);

            return Task.FromResult(regions.ToList());
        }
    }
}

