﻿using Abp.Collections.Extensions;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Cities.Queries;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Cities.Handlers
{
    /// <summary>
    /// Handler to get all cities
    /// </summary>
    public class GetAllCityQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllCityQuery, List<DropdownItemDto>>
    {
        private readonly IRepository<City> _cityRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="cityRepository"></param>
        public GetAllCityQueryHandler(IRepository<City> cityRepository)
        {
            _cityRepository = cityRepository;
        }

        public async Task<List<DropdownItemDto>> Handle(GetAllCityQuery query, CancellationToken cancellationToken)
        {
            return _cityRepository.GetAll()
                .WhereIf(true, t =>
                (t.Name.Contains(query.Filter ?? "") || t.Id == query.Id) &&
                t.RegionId == query.RegionId
                )
                .Select(t => new DropdownItemDto { Id = t.Id, Name = t.Name }).ToList();
        }
    }
}

