﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Cities.Queries
{
    /// <summary>
    /// Get cities by filter
    /// </summary>
    public class GetAllCityByRegionIdQuery : IRequest<List<DropdownItemDto>>
    {
        public long RegionId { get; set; }
        public string Filter { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="id"></param>
        public GetAllCityByRegionIdQuery(long regionId)
        {
            RegionId = regionId;
        }
    }
}

