﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Cities.Queries
{
    /// <summary>
    /// Query to get all cities
    /// </summary>
    public class GetAllCityQuery : IRequest<List<DropdownItemDto>>
    {
        public string Filter { get; set; }
        public int Id { get; set; }
        public long RegionId { get; set; }
    }
}
