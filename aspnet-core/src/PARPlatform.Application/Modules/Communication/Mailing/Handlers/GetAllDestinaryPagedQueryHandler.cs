﻿using Abp.Application.Services.Dto;
using MediatR;
using PARPlatform.Authorization.Users;
using PARPlatform.Base;
using PARPlatform.Modules.Communication.Mailing.Dto;
using PARPlatform.Modules.Communication.Mailing.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Domain.Uow;

namespace PARPlatform.Modules.Communication.Mailing.Handlers
{
    /// <summary>
    /// Handler to get all destijnary paged
    /// </summary>
    public class GetAllDestinaryPagedQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllDestinaryPagedQuery, PagedResultDto<ListItemDestinaryDto>>
    {
        private readonly IEnterpriseRepository _enterpriseRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="formThematicRepository"></param>
        public GetAllDestinaryPagedQueryHandler(IEnterpriseRepository enterpriseRepository,
            IUserRepository userRepository, IUnitOfWorkManager unitOfWorkManager)
        {
            _enterpriseRepository = enterpriseRepository;
            _userRepository = userRepository;
            _unitOfWorkManager = unitOfWorkManager;

        }
        public async Task<PagedResultDto<ListItemDestinaryDto>> Handle(GetAllDestinaryPagedQuery query, CancellationToken cancellationToken)
        {
            List<ListItemDestinaryDto> listDestinary = new();
            var listEnterpise = await _enterpriseRepository.GetAllListAsync();

            for (int i = 0; i < listEnterpise.Count; i++)
            {
                var user = await _userRepository.GetAsync(listEnterpise[i].UserId);
                ListItemDestinaryDto item = new();
                item.Id = listEnterpise[i].Id;
                item.Name = listEnterpise[i].Name;
                item.EmailAddress = user.EmailAddress;
                listDestinary.Add(item);
            }

            var queryableList = listDestinary.AsQueryable();
            var listAux = queryableList.OrderBy(query.Sorting)
           .Skip(query.SkipCount)
           .Take(query.MaxResultCount);

            int total = queryableList.Count();
            var resultList = ObjectMapper.Map<List<ListItemDestinaryDto>>(listAux);

            return new PagedResultDto<ListItemDestinaryDto>(
               total,
               resultList
           );
        }
    }
}
