﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.Communication.Mailing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Communication.Mailing.Queries
{
    public class GetAllDestinaryPagedQuery : PagedAndSortedInputDto, IShouldNormalize, IRequest<PagedResultDto<ListItemDestinaryDto>>
    {
        public string Filter { get; set; }
        public string UserName { get; set; }
        public string EnterpriseId { get; set; }


        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name DESC";
            }
            if (string.IsNullOrEmpty(Filter))
            {
                Filter = "";
            }
        }
    }
}
