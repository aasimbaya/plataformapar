﻿using Abp.Collections.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.Countries.Dtos;
using PARPlatform.Modules.Countries.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Countries.Handlers
{
    /// <summary>
    /// Handler to get all countries
    /// </summary>
    public class GetAllCountryQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllCountryQuery, List<UpsertCountryDto>>
    {
        private readonly IRepository<Country> _countryRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="countryRepository"></param>
        public GetAllCountryQueryHandler(IRepository<Country> countryRepository)
        {
            _countryRepository = countryRepository;
        }

        public async Task<List<UpsertCountryDto>> Handle(GetAllCountryQuery query, CancellationToken cancellationToken)
        {
            List<Country> countries = await _countryRepository.GetAll()
                .Where(e => e.IsActive && !e.IsDeleted)
                .ToListAsync();

            List<UpsertCountryDto> result = ObjectMapper.Map<List<UpsertCountryDto>>(countries);

            return result;
        }
    }
}

