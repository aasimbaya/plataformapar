﻿using MediatR;
using PARPlatform.Modules.Countries.Dtos;
using System.Collections.Generic;

namespace PARPlatform.Modules.Countries.Queries
{
    /// <summary>
    /// Query to get all countries
    /// </summary>
    public class GetAllCountryQuery : IRequest<List<UpsertCountryDto>>
    {
        public string Filter { get; set; }
        public int Id { get; set; }
    }
}
