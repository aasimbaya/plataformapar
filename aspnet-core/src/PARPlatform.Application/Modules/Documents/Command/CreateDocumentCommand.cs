﻿using MediatR;
using PARPlatform.Modules.Documents.Dtos;

namespace PARPlatform.Modules.Documents.Commands
{

    /// <summary>
    /// Create Document Command
    /// </summary>
    public class CreateDocumentCommand : IRequest<UpsertDocumentDto>
    {
        public string Name { get; set; }

        public string Size { get; set; }

        public string Extension { get; set; }

        public long? EstructureId { get; set; }

        public virtual long? LanguageId { get; set; }

        public virtual long? LocationId { get; set; }

        public string URL { get; set; }

        public string Location { get; set; }

        public bool IsActive { get; set; }

    }
}
