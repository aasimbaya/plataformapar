﻿using MediatR;

namespace PARPlatform.Modules.Documents.Commands
{
    /// <summary>
    /// Delete document Command
    /// </summary>
    public class DeleteDocumentCommand : IRequest
    {
        public DeleteDocumentCommand(long id)
        {
            Id = id;
        }

        public long Id { get; set; }
    }
}
