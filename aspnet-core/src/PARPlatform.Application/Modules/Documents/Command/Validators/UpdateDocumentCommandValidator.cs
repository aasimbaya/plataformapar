﻿using Abp.Localization;
using Abp.Localization.Sources;
using FluentValidation;

namespace PARPlatform.Modules.Documents.Commands.Validators
{
    /// <summary>
    /// Create document command input parameters validator
    /// </summary>
    public class UpdateDocumentCommandValidator : AbstractValidator<UpdateDocumentCommand>
    {
        private readonly ILocalizationSource _localizationSource;

        /// <summary>
        /// Main constructor
        /// </summary>
        public UpdateDocumentCommandValidator(ILocalizationManager localizationManager)
        {

            _localizationSource = localizationManager.GetSource(PARPlatformConsts.LocalizationSourceName);

            RuleFor(x => x.Name)
                .MaximumLength(50)
                .NotNull().WithMessage(_localizationSource.GetString("Document_NameRequired"))
                .NotEmpty().WithMessage(_localizationSource.GetString("Document_NameEmpty"));


            RuleFor(x => x.URL)
                .MaximumLength(150)
                .NotNull().WithMessage(_localizationSource.GetString("Document_URLRequired"))
                .NotEmpty().WithMessage(_localizationSource.GetString("Document_URLEmpty"));

            RuleFor(x => x.LocationId)
                .NotNull()
                .NotNull().WithMessage(_localizationSource.GetString("Document_LocationRequired"))
                .NotEmpty().WithMessage(_localizationSource.GetString("Document_LocationEmpty"));

            RuleFor(x => x.LanguageId)
                .NotNull()
                .NotNull().WithMessage(_localizationSource.GetString("Document_LanguageRequired"))
                .NotEmpty().WithMessage(_localizationSource.GetString("Document_LanguageEmpty"));

            RuleFor(x => x.EstructureId)
                .NotNull()
                .NotNull().WithMessage(_localizationSource.GetString("Document_EstructureRequired"))
                .NotEmpty().WithMessage(_localizationSource.GetString("Document_EstructureEmpty"));
       
        }
    }
}
