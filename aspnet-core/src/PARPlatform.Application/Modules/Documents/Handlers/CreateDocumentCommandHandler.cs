﻿using Abp.Localization;
using Amazon.S3.Transfer;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Documents.Commands;
using PARPlatform.Modules.Documents.Commands.Validators;
using PARPlatform.Modules.Documents.Dtos;
using PARPlatform.Modules.Shared.AwsS3.Services;
using PARPlatform.PAREntities;
using PARPlatform.Support;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Documents.Handlers
{
    /// <summary>
    /// Used to handle create documents command
    /// </summary>
    public class CreateDocumentCommandHandler : UseCaseServiceBase, IRequestHandler<CreateDocumentCommand, UpsertDocumentDto>
    {
        private readonly IDocumentRepository _documentRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IMediator _mediator;

        /// <summary>
        /// Main Constructor
        /// </summary>
        public CreateDocumentCommandHandler(IDocumentRepository documentRepository,
            IMediator mediator,
            ILocalizationManager localizationManager)
        {
            _documentRepository = documentRepository;
            _mediator = mediator;
            _localizationManager = localizationManager;
        }

        /// <summary>
        /// Create new Document use case
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<UpsertDocumentDto> Handle(CreateDocumentCommand input, CancellationToken cancellationToken)
        {
            new CreateDocumentCommandValidator(_localizationManager).Validate(input).ThrowIfAreErrors();

            Document document = ObjectMapper.Map<Document>(input);
            document.Id = await _documentRepository.InsertAndGetIdAsync(document);
            return ObjectMapper.Map<UpsertDocumentDto>(document);
        }
    }
}
