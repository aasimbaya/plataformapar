﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Documents.Commands;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Documents.Handlers
{
    /// <summary>
    /// Delete document command handler
    /// </summary>
    public class DeleteDocumentCommandHandler : UseCaseServiceBase, IRequestHandler<DeleteDocumentCommand>
    {
        private readonly IDocumentRepository _documentRepository;


        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="documentRepository"></param>
        public DeleteDocumentCommandHandler(IDocumentRepository documentRepository)
        {
            _documentRepository = documentRepository;
        }
        /// <summary>
        /// Delete question category
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteDocumentCommand command, CancellationToken cancellationToken)
        {
            Document document = await _documentRepository.FirstOrDefaultAsync(x => x.Id == command.Id);
            document.IsDeleted = true;
            await _documentRepository.UpdateAsync(document);
            return Unit.Value;
        }
    }
}
