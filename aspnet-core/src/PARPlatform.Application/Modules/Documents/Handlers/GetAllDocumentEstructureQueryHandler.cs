﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.Documents.Dtos;
using PARPlatform.Modules.Documents.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Documents.Handlers
{
    /// <summary>
    /// Get All Levels of structure Question By Question Id Query Handler
    /// </summary>
    public class GetAllDocumentEstructureQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllDocumentEstructureQuery, List<DropdownDocumentEstructureDto>>
    {
        private readonly IDocumentEstructureRepository _documentEstructureRepository;

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="documentEstructureRepository"></param>
        public GetAllDocumentEstructureQueryHandler(
            IDocumentEstructureRepository documentEstructureRepository)
        {
            _documentEstructureRepository = documentEstructureRepository;
        }

        public async Task<List<DropdownDocumentEstructureDto>> Handle(GetAllDocumentEstructureQuery query, CancellationToken cancellationToken)
        {
            List<DocumentEstructure> dataDocumentEstucture = await _documentEstructureRepository.GetAll()
                .Where(e => e.IsActive && !e.IsDeleted)
                .ToListAsync();

            List<DropdownDocumentEstructureDto> result = ObjectMapper.Map<List<DropdownDocumentEstructureDto>>(dataDocumentEstucture);
            return result;
        }

    }
}
