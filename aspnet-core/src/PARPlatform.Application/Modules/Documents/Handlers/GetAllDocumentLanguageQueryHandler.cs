﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.Documents.Dtos;
using PARPlatform.Modules.Documents.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Documents.Handlers
{
    /// <summary>
    /// Get All Levels of structure Question By Question Id Query Handler
    /// </summary>
    public class GetAllDocumentLanguageQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllDocumentLanguageQuery, List<DropdownDocumentLanguageDto>>
    {
        private readonly IDocumentLanguageRepository _documentLanguageRepository;

        /// <summary>
        /// GetAll Document Language Query Main constructor
        /// </summary>
        /// <param name="documentLanguageRepository"></param>
        public GetAllDocumentLanguageQueryHandler(
            IDocumentLanguageRepository documentLanguageRepository)
        {
            _documentLanguageRepository = documentLanguageRepository;
        }

        public async Task<List<DropdownDocumentLanguageDto>> Handle(GetAllDocumentLanguageQuery query, CancellationToken cancellationToken)
        {
            List<DocumentLanguage> dataDocumentLanguage = await _documentLanguageRepository.GetAll()
                .Where(e => e.IsActive && !e.IsDeleted)
                .ToListAsync();

            List<DropdownDocumentLanguageDto> result = ObjectMapper.Map<List<DropdownDocumentLanguageDto>>(dataDocumentLanguage);
            return result;
        }

    }
}
