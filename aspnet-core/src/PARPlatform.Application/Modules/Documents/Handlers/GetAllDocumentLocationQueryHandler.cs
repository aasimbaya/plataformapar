﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.Documents.Dtos;
using PARPlatform.Modules.Documents.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Documents.Handlers
{
    /// <summary>
    /// Get All Levels of structure Question By Question Id Query Handler
    /// </summary>
    public class GetAllDocumentLocationQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllDocumentLocationQuery, List<DropdownDocumentLocationDto>>
    {
        private readonly IDocumentLocationRepository _documentLocationRepository;

        /// <summary>
        /// GetAll Document Location Query Main constructor
        /// </summary>
        /// <param name="documentLocationRepository"></param>
        public GetAllDocumentLocationQueryHandler(
            IDocumentLocationRepository documentLocationRepository)
        {
            _documentLocationRepository = documentLocationRepository;
        }

        public async Task<List<DropdownDocumentLocationDto>> Handle(GetAllDocumentLocationQuery query, CancellationToken cancellationToken)
        {
            List<DocumentLocation> dataDocumentLocation = await _documentLocationRepository.GetAll()
                .Where(e => e.IsActive && !e.IsDeleted)
                .ToListAsync();

            List<DropdownDocumentLocationDto> result = ObjectMapper.Map<List<DropdownDocumentLocationDto>>(dataDocumentLocation);
            return result;
        }

    }
}
