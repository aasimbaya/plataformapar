﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using Abp.ObjectMapping;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.Documents.Dtos;
using PARPlatform.Modules.Documents.Queries;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle query paged Question Bank
    /// </summary>
    public class GetAllPagedDocumentsQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedDocumentsQuery, PagedResultDto<DocumentDto>>
    {
        private readonly IDocumentRepository _documentRepository;
        private readonly IDocumentEstructureRepository _documentEstructureRepository;
        private readonly IDocumentLocationRepository _documentLocationRepository;
        private readonly IDocumentLanguageRepository _documentLanguageRepository;
        private readonly IObjectMapper _objectMapper;

        /// <summary>
        /// Main Constructor
        /// </summary>
        public GetAllPagedDocumentsQueryHandler(
            IDocumentRepository documentRepository,
            IDocumentEstructureRepository documentEstructureRepository,
            IDocumentLocationRepository documentLocationRepository,
            IDocumentLanguageRepository documentLanguageRepository,
            IObjectMapper objectMapper)
        {
            _documentRepository = documentRepository;
            _documentEstructureRepository = documentEstructureRepository;
            _documentLocationRepository = documentLocationRepository;
            _documentLanguageRepository = documentLanguageRepository;
            _objectMapper = objectMapper;
        }

        /// <summary>
        /// Get paged Documents list 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<DocumentDto>> Handle(GetAllPagedDocumentsQuery query, CancellationToken cancellationToken)
        {
            var dbQuery = _documentRepository.GetAll()
                    .Where(x=>x.IsDeleted==false)
                    .Include(estructure => estructure.EstructureFk)
                    .Include(location => location.LocationFk)
                    .Include(language => language.LanguageFk);

            var resultCount = await dbQuery.CountAsync();
            List<Document> results = await dbQuery
                .OrderBy(query.Sorting)
                .Skip(query.SkipCount)
                .Take(query.MaxResultCount)
                .ToListAsync();

            return new PagedResultDto<DocumentDto>(
                resultCount,
                _objectMapper.Map<List<DocumentDto>>(results)
            );
        }
    }
}
