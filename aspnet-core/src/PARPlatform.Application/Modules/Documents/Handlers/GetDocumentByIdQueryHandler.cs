﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Documents.Dtos;
using PARPlatform.Modules.Documents.Queries;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Documents.Handlers
{
    /// <summary>
    /// Get document by id
    /// </summary>
    public class GetDocumentIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetDocumentByIdQuery, UpsertDocumentDto>
    {
        private readonly IDocumentRepository _documentRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="documentRepository"></param>
        public GetDocumentIdQueryHandler(IDocumentRepository documentRepository)
        {
            _documentRepository = documentRepository;
        }

        /// <summary>
        /// Handles request for getting question category details data
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertDocumentDto> Handle(GetDocumentByIdQuery input, CancellationToken cancellationToken)
        {
            List<Document> document = _documentRepository.GetAll().Where(e => e.Id == input.Id).ToList();
            UpsertDocumentDto result = ObjectMapper.Map<UpsertDocumentDto>(document.First());
            return result;
        }
    }
}
