﻿using Abp.Localization;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Documents.Dtos;
using PARPlatform.Modules.Documents.Commands;
using PARPlatform.Modules.Documents.Commands.Validators;
using PARPlatform.PAREntities;
using PARPlatform.Support;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Documents.Handlers
{
    /// <summary>
    /// Used to handle create documents command
    /// </summary>
    public class UpdateDocumentCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateDocumentCommand, UpsertDocumentDto>
    {
        private readonly IDocumentRepository _documentRepository;
        private readonly IMediator _mediator;
        private readonly ILocalizationManager _localizationManager;


        /// <summary>
        /// Main Constructor
        /// </summary>
        public UpdateDocumentCommandHandler(IDocumentRepository documentRepository,
            IMediator mediator,
            ILocalizationManager localizationManager)
        {
            _documentRepository = documentRepository;
            _mediator = mediator;
            _localizationManager = localizationManager;
        }

        /// <summary>
        /// Create new Document use case 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertDocumentDto> Handle(UpdateDocumentCommand input, CancellationToken cancellationToken)
        {
            new UpdateDocumentCommandValidator(_localizationManager).Validate(input).ThrowIfAreErrors();

            Document document = await _documentRepository.FirstOrDefaultAsync(e => e.Id == input.Id);

            ObjectMapper.Map(input, document);
            document = await _documentRepository.UpdateAsync(document);
            return ObjectMapper.Map<UpsertDocumentDto>(document);
        }
    }
}
