﻿using MediatR;
using PARPlatform.Modules.Documents.Dtos;
using System.Collections.Generic;

namespace PARPlatform.Modules.Documents.Queries
{
    public class GetAllDocumentLanguageQuery : IRequest<List<DropdownDocumentLanguageDto>>
    {
    }
}
