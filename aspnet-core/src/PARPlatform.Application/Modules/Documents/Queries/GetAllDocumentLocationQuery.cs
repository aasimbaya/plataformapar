﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Documents.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Documents.Queries
{
    public class GetAllDocumentLocationQuery : IRequest<List<DropdownDocumentLocationDto>>
    {
    }
}
