﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.Documents.Dtos;
using System;

namespace PARPlatform.Modules.Documents.Queries
{
    /// <summary>
    /// Get All Documents Paged Query
    /// </summary>
    public class GetAllPagedDocumentsQuery : PagedAndSortedInputDto, IShouldNormalize,IRequest<PagedResultDto<DocumentDto>>
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
}
