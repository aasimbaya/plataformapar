﻿using MediatR;
using PARPlatform.Modules.Documents.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Documents.Queries
{
    /// <summary>
    /// Get document query by Id query
    /// </summary>
    public class GetDocumentByIdQuery : IRequest<UpsertDocumentDto>
    {
        public long Id { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="id"></param>
        public GetDocumentByIdQuery(long id)
        {
            Id = id;
        }
    }
}
