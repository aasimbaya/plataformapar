﻿using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Modules.Enterprise.Dtos;

namespace PARPlatform.Modules.EnterpriseGroupEnterprises.Command
{
    /// <summary>
    /// Command for create Enterprise group Enterprise
    /// </summary>
    public class CreateEnterpriseGroupEnterpriseCommand : IRequest<UpsertEnterpriseGroupEnterpriseDto>, ICustomValidate
    {        
        public long Id { get; set; }
        public virtual long? EnterpriseGroupId { get; set; }
        public virtual long? EnterpriseId { get; set; }
        public bool IsActive { get; set; }
        public void AddValidationErrors(CustomValidationContext context)
        {
        }
    }
}

