﻿using Abp.Domain.Repositories;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.EnterpriseGroupEnterprises.Command;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.EnterpriseGroupEnterprises.Handlers
{
    public class CreateEnterpriseGroupEnterpriseCommandHandler : UseCaseServiceBase, IRequestHandler<CreateEnterpriseGroupEnterpriseCommand, UpsertEnterpriseGroupEnterpriseDto>
    {
        private readonly IRepository<EnterpriseGroupEnterprise, long> _enterpriseGroupEnterpriseRepository;        

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="enterpriseGroupEnterpriseRepository"></param>
        public CreateEnterpriseGroupEnterpriseCommandHandler(IRepository<EnterpriseGroupEnterprise, long> enterpriseGroupEnterpriseRepository)
        {
            _enterpriseGroupEnterpriseRepository = enterpriseGroupEnterpriseRepository;            
        }
        /// <summary>
        /// Create enterprise group enterprise use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseGroupEnterpriseDto> Handle(CreateEnterpriseGroupEnterpriseCommand input, CancellationToken cancellationToken)
        {
            var data = ObjectMapper.Map<EnterpriseGroupEnterprise>(input);
            await _enterpriseGroupEnterpriseRepository.InsertAsync(data);            
            return ObjectMapper.Map<UpsertEnterpriseGroupEnterpriseDto>(data);
        }
    }
}

