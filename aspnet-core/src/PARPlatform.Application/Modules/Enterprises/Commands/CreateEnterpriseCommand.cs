﻿using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;

namespace PARPlatform.Modules.Enterprises.Commands
{
    /// <summary>
    /// Command for create Enterprise
    /// </summary>
    public class CreateEnterpriseCommand : IRequest<UpsertEnterpriseDto>, ICustomValidate
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string IdNumber { get; set; }
        public bool IsActive { get; set; }
        public virtual long UserId { get; set; }
        public virtual long? CountryId { get; set; }
        public virtual long? RegionId { get; set; }
        public virtual long? CityId { get; set; }
        public virtual long? SectorId { get; set; }
        public int EmployeesNumber { get; set; }
        public bool HasInclusion { get; set; }
        public bool IsInternational { get; set; }
        public bool ReceivePromoAccepted { get; set; }
        public int TenantId { get; set; }
        public bool IsPartOfEconomicGroup { get; set; }
        public string EconomicGroupName { get; set; }
        public bool IsPartOfTradeAssociation { get; set; }
        public long? TradeAssociationId { get; set; }
        public string InclusionRegionalManagerName { get; set; }
        public string InclusionRegionalManagerEmail { get; set; }
        public void AddValidationErrors(CustomValidationContext context)
        {
        }
    }
}
