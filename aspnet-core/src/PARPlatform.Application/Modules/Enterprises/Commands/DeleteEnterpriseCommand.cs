﻿using MediatR;

namespace PARPlatform.Modules.Enterprises.Commands
{
    /// <summary>
    /// Command for delete Enterprise
    /// </summary>
    public class DeleteEnterpriseCommand : IRequest<Unit>
    {
        public DeleteEnterpriseCommand(long id)
        {
            Id = id;
        }

        public long Id { get; set; }
    }
}
