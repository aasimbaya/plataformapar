﻿using MediatR;
using PARPlatform;
using PARPlatform.Base;
using PARPlatform.EntityFrameworkCore;
using PARPlatform.Modules.Enterprises.Commands;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatenterprise.Modules.Enterprises.Handlers
{
    public class CreateEnterpriseCommandHandler : UseCaseServiceBase, IRequestHandler<CreateEnterpriseCommand, UpsertEnterpriseDto>
    {
        private readonly IEnterpriseRepository _enterpriseRepository;
        private readonly PARPlatformDbContext _dbContext;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="enterpriseRepository"></param>
        public CreateEnterpriseCommandHandler(IEnterpriseRepository enterpriseRepository, PARPlatformDbContext dbContext)
        {
            _enterpriseRepository = enterpriseRepository;
            _dbContext = dbContext;
        }
        /// <summary>
        /// Create Enterprise use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseDto> Handle(CreateEnterpriseCommand input, CancellationToken cancellationToken)
        {
            Enterprise newEnterprise = ObjectMapper.Map<Enterprise>(input);
            newEnterprise.Id = await _enterpriseRepository.InsertAndGetIdAsync(newEnterprise);
            return ObjectMapper.Map<UpsertEnterpriseDto>(newEnterprise);
        }
    }
}
