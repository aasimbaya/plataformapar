﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Enterprises.Commands;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Enterprises.Handlers
{
    /// <summary>
    /// Delete Enterprise command handler
    /// </summary>
    public class DeleteEnterpriseCommandHandler : IRequestHandler<DeleteEnterpriseCommand, Unit>
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        public DeleteEnterpriseCommandHandler(IEnterpriseRepository enterpriseRepository)
        {
            _enterpriseRepository = enterpriseRepository;

        }
        public async Task<Unit> Handle(DeleteEnterpriseCommand request, CancellationToken cancellationToken)
        {
            var enterpriseDeleted = await _enterpriseRepository.GetAsync(request.Id);
            enterpriseDeleted.IsDeleted = true;

            await _enterpriseRepository.DeleteAsync(enterpriseDeleted);

            return Unit.Value;
        }
    }
}
