﻿using Abp.Application.Services.Dto;
using MediatR;
using PARPlatform;
using PARPlatform.Base;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.Enterprises.Queries;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatenterprise.Modules.Enterprises.Handlers
{
    /// <summary>
    /// Get paged Enterprise data query handler
    /// </summary>
    public class GetAllPagedEnterpriseQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedEnterpriseQuery, PagedResultDto<UpsertEnterpriseDto>>
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        /// <summary>
        /// handler get all enterprise paged
        /// </summary>
        /// <param name="enterpriseRepository"></param>
        /// <param name="enterpriseTypeEnterpriseRepository"></param>
        /// <param name="enterpriseTypeRepository"></param>
        /// <param name="enterpriseThematicEnterpriseRepository"></param>
        /// <param name="enterpriseThematicRepository"></param>
        public GetAllPagedEnterpriseQueryHandler(IEnterpriseRepository enterpriseRepository)
        {
            _enterpriseRepository = enterpriseRepository;
        }

        /// <summary>
        /// Use case list enterprises
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<UpsertEnterpriseDto>> Handle(GetAllPagedEnterpriseQuery query, CancellationToken cancellationToken)
        {
            string search = query.Filter == null ? "" : query.Filter;
            List<UpsertEnterpriseDto> listFiltered = new();

            listFiltered = ObjectMapper.Map<List<UpsertEnterpriseDto>>(_enterpriseRepository.GetAll().ToList());
            listFiltered = listFiltered.Where(x => x.Name.Contains(search)).ToList();

            var queryableList = listFiltered.AsQueryable();
            queryableList = queryableList.OrderBy(query.Sorting)
           .Skip(query.SkipCount)
           .Take(query.MaxResultCount);

            int total = queryableList.Count();
            var resultList = ObjectMapper.Map<List<UpsertEnterpriseDto>>(queryableList);
            List<UpsertEnterpriseDto> result = ObjectMapper.Map<List<UpsertEnterpriseDto>>(queryableList);

            return new PagedResultDto<UpsertEnterpriseDto>(
                total,
                result
            );
        }
    }
}
