﻿using MediatR;
using PARPlatform.Base;
using System.Threading;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Modules.Enterprises.Queries;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;

namespace PARPlatform.Modules.Enterprises.Handlers
{
    /// <summary>
    /// Get enterprise by filter
    /// </summary> 
    public class GetEnterpriseByFilterQueryHandler : UseCaseServiceBase, IRequestHandler<GetEnterpriseByFilterQuery, UpsertEnterpriseDto>
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="enterpriseRepository"></param>
        public GetEnterpriseByFilterQueryHandler(IEnterpriseRepository enterpriseRepository)
        {
            _enterpriseRepository = enterpriseRepository;
        }

        /// <summary>
        /// Get enterprise query
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseDto> Handle(GetEnterpriseByFilterQuery input, CancellationToken cancellationToken)
        {
            var query = _enterpriseRepository.FirstOrDefault(p => p.Name.Equals(input.Filter) || p.IdNumber.Equals(input.Filter));

            UpsertEnterpriseDto result = ObjectMapper.Map<UpsertEnterpriseDto>(query);

            return result;
        }

    }
}

