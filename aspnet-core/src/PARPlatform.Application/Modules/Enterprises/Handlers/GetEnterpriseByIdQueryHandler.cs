﻿using MediatR;
using PARPlatform;
using PARPlatform.Base;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.Enterprises.Queries;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatenterprise.Modules.Enterprises.Handlers
{
    /// <summary>
    /// Get Enterprise by id handler
    /// </summary>
    public class GetEnterpriseByIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetEnterpriseByIdQuery, UpsertEnterpriseDto>
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="enterpriseRepository"></param>
        public GetEnterpriseByIdQueryHandler(IEnterpriseRepository enterpriseRepository)
        {
            _enterpriseRepository = enterpriseRepository;
        }

        /// <summary>
        /// Use case list enterprises
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseDto> Handle(GetEnterpriseByIdQuery query, CancellationToken cancellationToken)
        {
            var enterprise = await _enterpriseRepository.GetAsync(query.Id);

            var enterpriseDto = ObjectMapper.Map<UpsertEnterpriseDto>(enterprise);

            return enterpriseDto;
        }
    }
}
