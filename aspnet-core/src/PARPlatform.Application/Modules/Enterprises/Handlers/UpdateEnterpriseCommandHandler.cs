﻿using MediatR;
using PARPlatform;
using PARPlatform.Base;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.Enterprises.Commands;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatenterprise.Modules.Enterprises.Handlers
{
    /// <summary>
    /// Update Enterprise command handler
    /// </summary>
    public class UpdateEnterpriseCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateEnterpriseCommand, UpsertEnterpriseDto>
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="enterpriseRepository"></param>
        public UpdateEnterpriseCommandHandler(IEnterpriseRepository enterpriseRepository)
        {
            _enterpriseRepository = enterpriseRepository;
        }

        /// <summary>
        /// Update enterprise use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseDto> Handle(UpdateEnterpriseCommand command, CancellationToken cancellationToken)
        {
            var enterprise = await _enterpriseRepository.GetAsync(command.Id);
            ObjectMapper.Map(command, enterprise);

            await _enterpriseRepository.UpdateAsync(enterprise);

            return ObjectMapper.Map<UpsertEnterpriseDto>(enterprise);
        }
    }
}
