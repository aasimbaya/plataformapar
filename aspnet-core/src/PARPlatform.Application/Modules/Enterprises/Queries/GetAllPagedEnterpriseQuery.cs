﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;

namespace PARPlatform.Modules.Enterprises.Queries
{
    /// <summary>
    /// Get all paged enterprises query
    /// </summary>
    public class GetAllPagedEnterpriseQuery : PagedAndSortedInputDto, IShouldNormalize, IRequest<PagedResultDto<UpsertEnterpriseDto>>
    {

        public string Filter { get; set; }
        public int Type { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name DESC";
            }
            if (string.IsNullOrEmpty(Filter))
            {
                Filter = "";
            }
        }
    }
}
