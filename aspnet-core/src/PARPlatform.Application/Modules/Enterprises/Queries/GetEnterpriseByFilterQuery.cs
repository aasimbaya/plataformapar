﻿using MediatR;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;

namespace PARPlatform.Modules.Enterprises.Queries
{
    /// <summary>
    /// Get Enterprise by filter
    /// </summary>
    public class GetEnterpriseByFilterQuery : IRequest<UpsertEnterpriseDto>
    {
        public string Filter { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="filter"></param>
        public GetEnterpriseByFilterQuery(string filter)
        {
            Filter = filter;
        }
    }
}

