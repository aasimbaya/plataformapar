﻿using MediatR;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;

namespace PARPlatform.Modules.Enterprises.Queries
{
    /// <summary>
    /// Get enterprises by id query
    /// </summary>
    public class GetEnterpriseByIdQuery : IRequest<UpsertEnterpriseDto>
    {
        public long Id { get; set; }

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="id"></param>
        public GetEnterpriseByIdQuery(long id)
        {
            Id = id;
        }

    }
}
