﻿
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Modules.Enterprise.Dtos;

namespace PARPlatform.Modules.EnterprisesGroup.Commands
{
    /// <summary>
    /// Command for create Enterprise group
    /// </summary>
    public class CreateEnterpriseGroupCommand : IRequest<UpsertEnterpriseGroupDto>, ICustomValidate
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long EnterpriseNumber { get; set; }
        public bool IsActive { get; set; }
        public virtual long UserId { get; set; }
        public virtual long? CountryId { get; set; }
        public virtual long? RegionId { get; set; }
        public virtual long? CityId { get; set; }
        public virtual long? SectorId { get; set; }
        public bool HasInclusion { get; set; }
        public bool IsInternational { get; set; }
        public bool ReceivePromoAccepted { get; set; }
        public bool IsPartOfEconomicGroup { get; set; }
        public string EconomicGroupName { get; set; }
        public bool IsPartOfTradeAssociation { get; set; }
        public long? TradeAssociationId { get; set; }
        public string InclusionRegionalManagerName { get; set; }
        public string InclusionRegionalManagerEmail { get; set; }
        public void AddValidationErrors(CustomValidationContext context)
        {
        }
    }
}
