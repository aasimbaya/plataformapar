﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using MediatR;
using PARPlatform;
using PARPlatform.Base;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.EnterprisesGroup.Commands;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatenterprise.Modules.EnterprisesGroup.Handlers
{
    public class CreateEnterpriseGroupCommandHandler : UseCaseServiceBase, IRequestHandler<CreateEnterpriseGroupCommand, UpsertEnterpriseGroupDto>
    {
        private readonly IRepository<EnterpriseGroup, long> _enterpriseGroupRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="enterpriseGroupRepository"></param>
        public CreateEnterpriseGroupCommandHandler(IRepository<EnterpriseGroup, long> enterpriseGroupRepository)
        {
            _enterpriseGroupRepository = enterpriseGroupRepository;
        }
        /// <summary>
        /// Create enterprise group use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseGroupDto> Handle(CreateEnterpriseGroupCommand input, CancellationToken cancellationToken)
        {
            var data = ObjectMapper.Map<EnterpriseGroup>(input);
            data.Id = await _enterpriseGroupRepository.InsertAndGetIdAsync(data);
            return ObjectMapper.Map<UpsertEnterpriseGroupDto>(data);
        }
    }
}

