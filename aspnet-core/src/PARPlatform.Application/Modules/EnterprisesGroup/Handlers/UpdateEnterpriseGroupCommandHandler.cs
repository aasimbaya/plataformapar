﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Enterprise.Dtos;
using PARPlatform.Modules.EnterprisesGroup.Commands;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.EnterprisesGroup.Handlers
{
    /// <summary>
    /// Update Enterprise group command handler
    /// </summary>
    public class UpdateEnterpriseGroupCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateEnterpriseGroupCommand, UpsertEnterpriseGroupDto>
    {
        private readonly IRepository<EnterpriseGroup> _enterpriseGroupRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="enterpriseGroupRepository"></param>
        public UpdateEnterpriseGroupCommandHandler(IRepository<EnterpriseGroup> enterpriseGroupRepository)
        {
            _enterpriseGroupRepository = enterpriseGroupRepository;
        }

        /// <summary>
        /// Update enterpriseGroup use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseGroupDto> Handle(UpdateEnterpriseGroupCommand command, CancellationToken cancellationToken)
        {
            var enterpriseGroup = await _enterpriseGroupRepository.FirstOrDefaultAsync(p => p.Id == command.Id);
            ObjectMapper.Map(command, enterpriseGroup);

            await _enterpriseGroupRepository.UpdateAsync(enterpriseGroup);

            return ObjectMapper.Map<UpsertEnterpriseGroupDto>(enterpriseGroup);
        }
    }
}

