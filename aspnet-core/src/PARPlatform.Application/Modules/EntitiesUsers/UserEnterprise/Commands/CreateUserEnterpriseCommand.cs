﻿using MediatR;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands
{
    /// <summary>
    /// Create user enterpise command 
    /// </summary>
    public class CreateUserEnterpriseCommand : IRequest<UpsertEnterpriseDto>
    {
        public UpsertEnterpriseDto User { get; set; }
        public UpsertPersonalDto RRHH { get; set; }
        public UpsertPersonalDto CEO { get; set; }
    }
}
