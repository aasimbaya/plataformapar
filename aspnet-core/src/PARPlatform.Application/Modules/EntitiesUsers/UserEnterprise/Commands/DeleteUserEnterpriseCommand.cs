﻿using MediatR;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands
{
    /// <summary>
    /// Delete user enterprise
    /// </summary>
    public class DeleteUserEnterpriseCommand : IRequest<UpsertEnterpriseDto>
    {
        public long Id { get; set; }
    }
}
