﻿using MediatR;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands
{
    /// <summary>
    /// Update Enterprise Data Command
    /// </summary>
    public class UpdateEnterpriseDataSidebarCommand : IRequest<UpsertEnterpriseDto>
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string IdNumber { get; set; }
        public long CountryId { get; set; }
        public long RegionId { get; set; }
        public long CityId { get; set; }
    }
}
