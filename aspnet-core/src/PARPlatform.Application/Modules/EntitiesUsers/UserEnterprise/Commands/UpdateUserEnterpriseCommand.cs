﻿using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands
{
    /// <summary>
    /// Update user enterpise command 
    /// </summary>
    public class UpdateUserEnterpriseCommand : IRequest<UpsertEnterpriseDto>, ICustomValidate
    {
        public long Id { get; set; }
        public UpsertEnterpriseDto User { get; set; }
        public UpsertPersonalDto RRHH { get; set; }
        public UpsertPersonalDto CEO { get; set; }
        /// <summary>
        /// Validation command
        /// </summary>
        /// <param name="context"></param>
        public void AddValidationErrors(CustomValidationContext context)
        {
            if (Id == 0)
            {
                context.Results.Add(new System.ComponentModel.DataAnnotations.ValidationResult("IdIsRequired"));
            }
        }
    }
}
