﻿using MediatR;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands
{
    /// <summary>
    /// Update User Enterprise Profile Command
    /// </summary>
    public class UpdateUserEnterpriseProfileCommand : IRequest<UpsertEnterpriseUserFromSidebarDto>
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }
}
