﻿using FluentValidation;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands.Validators
{
    /// <summary>
    /// Update Enterprise Data from sidebar command input parameters validator
    /// </summary>
    public class UpdateEnterpriseDataSidebarCommandValidator : AbstractValidator<UpdateEnterpriseDataSidebarCommand>
    {
        /// <summary>
        /// Main constructor
        /// </summary>
        public UpdateEnterpriseDataSidebarCommandValidator()
        {
                
        }
    }
}
