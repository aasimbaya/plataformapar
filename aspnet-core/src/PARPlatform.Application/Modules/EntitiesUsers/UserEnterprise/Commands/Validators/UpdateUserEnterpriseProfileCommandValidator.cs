﻿using FluentValidation;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands.Validators
{
    /// <summary>
    /// Update Enterprise user profile from sidebar command input parameters validator
    /// </summary>
    public class UpdateUserEnterpriseProfileCommandValidator : AbstractValidator<UpdateUserEnterpriseProfileCommand>
    {
        /// <summary>
        /// Main constructor
        /// </summary>
        public UpdateUserEnterpriseProfileCommandValidator()
        {
                
        }
    }
}
