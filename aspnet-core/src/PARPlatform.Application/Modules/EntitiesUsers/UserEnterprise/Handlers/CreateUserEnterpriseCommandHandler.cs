﻿
using Abp.Domain.Uow;
using MediatR;
using PARPlatform.Authorization.Users;
using PARPlatform.Base;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Entity = PARPlatform.PAREntities;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Handlers
{
    /// <summary>
    /// Create user enterprise command handler
    /// </summary>
    public class CreateUserEnterpriseCommandHandler : UseCaseServiceBase, IRequestHandler<CreateUserEnterpriseCommand, UpsertEnterpriseDto>
    {
        private readonly IRepository<Entity.Enterprise> _enterpriseRepository;
        private readonly IRepository<Entity.PersonalType> _personalTypeRepository;
        private readonly IRepository<Entity.Personal> _personalRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="IRepository"></param>
        public CreateUserEnterpriseCommandHandler(
            IRepository<Entity.Enterprise> enterpriseRepository,
            IRepository<Entity.PersonalType> personalTypeRepository,
            IRepository<Entity.Personal> personalRepository)
        {
            _enterpriseRepository = enterpriseRepository;
            _personalTypeRepository = personalTypeRepository;
            _personalRepository = personalRepository;
        }

        /// <summary>
        /// Create user enterprise
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseDto> Handle(CreateUserEnterpriseCommand input, CancellationToken cancellationToken)
        {
            long[] usersId = input.User.ListUsersId.Split(',').Select(n => long.Parse(n)).ToArray();

            Entity.Enterprise enterprise = ObjectMapper.Map<Entity.Enterprise>(input.User);
            enterprise.UserId = usersId[0];
            enterprise.LastModificationTime = DateTime.Now;
            enterprise.IsActive = true;

            await _enterpriseRepository.InsertAsync(enterprise);
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            Entity.Personal hhrr = ObjectMapper.Map<Entity.Personal>(input.RRHH);
            Entity.PersonalType typeRrhh = await _personalTypeRepository.FirstOrDefaultAsync(e =>
               e.Abreviation == "HR");
            hhrr.PersonalTypeId = typeRrhh.Id;
            hhrr.EnterpriseId = enterprise.Id;
            hhrr.IsActive = true;
            await _personalRepository.InsertAsync(hhrr);

            Entity.Personal ceo = ObjectMapper.Map<Entity.Personal>(input.CEO);
            Entity.PersonalType typeCeo = await _personalTypeRepository.FirstOrDefaultAsync(e =>
               e.Abreviation == "CEO");
            ceo.PersonalTypeId = typeCeo.Id;
            ceo.EnterpriseId = enterprise.Id;
            hhrr.IsActive = true;
            await _personalRepository.InsertAsync(ceo);

            foreach (var id in usersId)
            {
                User user = await UserManager.FindByIdAsync(id.ToString());
                user.EnterpriseId = enterprise.Id;
                CheckErrors(await UserManager.UpdateAsync(user));
            }

            return input.User;
        }
    }
}
