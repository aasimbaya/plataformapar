﻿using Abp.Collections.Extensions;
using MediatR;
using PARPlatform.Authorization.Users;
using PARPlatform.Base;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Entity = PARPlatform.PAREntities;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Handlers
{
    /// <summary>
    /// Delete user enterprise command handler
    /// </summary>
    public class DeleteUserEnterpriseCommandHandler : UseCaseServiceBase, IRequestHandler<DeleteUserEnterpriseCommand, UpsertEnterpriseDto>
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Entity.Enterprise> _enterpriseRepository;
        private readonly IRepository<Entity.PersonalType> _personalTypeRepository;
        private readonly IRepository<Entity.Personal> _personalRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="IRepository"></param>
        public DeleteUserEnterpriseCommandHandler(
            IRepository<Entity.Enterprise> enterpriseRepository,
            IRepository<User> userRepository,
            IRepository<Entity.PersonalType> personalTypeRepository,
            IRepository<Entity.Personal> personalRepository)
        {
            _enterpriseRepository = enterpriseRepository;
            _personalTypeRepository = personalTypeRepository;
            _userRepository = userRepository;
            _personalRepository = personalRepository;
        }

        /// <summary>
        /// Delete user enterprise
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseDto> Handle(DeleteUserEnterpriseCommand command, CancellationToken cancellationToken)
        {

            Entity.Enterprise enterprise = await _enterpriseRepository.FirstOrDefaultAsync(e => e.Id == command.Id);
            await _enterpriseRepository.DeleteAsync(enterprise);

            List<User> deleteRelation = _userRepository.GetAll()
            .WhereIf(true, t => t.EnterpriseId == command.Id).ToList();
            foreach (var id in deleteRelation)
            {
                User user = await UserManager.FindByIdAsync(id.ToString());
                user.EnterpriseId = 0;
                CheckErrors(await UserManager.UpdateAsync(user));
            }
            return ObjectMapper.Map<UpsertEnterpriseDto>(enterprise);
        }
    }
}
