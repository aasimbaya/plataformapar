﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Entity = PARPlatform.PAREntities;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Handlers
{
    /// <summary>
    /// Get paged users enterprise
    /// </summary> 
    public class GetAllPagedUsersEnterpriseQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedUserEnterpriseQuery, PagedResultDto<EnterpriseListItemDto>>
    {
        private readonly IRepository<Entity.Enterprise> _enterprise;

        /// <summary>
        /// Base constructor
        /// </summary>
        public GetAllPagedUsersEnterpriseQueryHandler(
            IRepository<Entity.Enterprise> enterprise)
        {
            _enterprise = enterprise;
        }
        /// <summary>
        /// Get paged list of user enterprise
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<EnterpriseListItemDto>> Handle(GetAllPagedUserEnterpriseQuery query, CancellationToken cancellationToken)
        {
            IQueryable<Entity.Enterprise> dbQuery = _enterprise.GetAll()
           .WhereIf(!string.IsNullOrWhiteSpace(query.Filter), t => t.Name.Contains(query.Filter));

            int enterpriseCount = await dbQuery.CountAsync(cancellationToken);
            List<Entity.Enterprise> enterprise = await dbQuery.OrderBy(query.Sorting).PageBy(query).ToListAsync(cancellationToken);

            List<EnterpriseListItemDto> enterpriseDto = ObjectMapper.Map<List<EnterpriseListItemDto>>(enterprise);

            return new PagedResultDto<EnterpriseListItemDto>(
                enterpriseCount,
                enterpriseDto
            );
        }
    }
}
