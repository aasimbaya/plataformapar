﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Entity = PARPlatform.PAREntities;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Handlers
{
    /// <summary>
    /// Get paged users enterprise
    /// </summary> 
    public class GetAllUsersEnterpriseQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllUserEnterpriseQuery, List<EnterpriseListItemDto>>
    {
        private readonly IRepository<Entity.Enterprise> _enterprise;
        private readonly IRepository<Entity.EnterpriseGroup> _enterpriseGroup;
        private readonly IRepository<Entity.EnterpriseGroupEnterprise> _enterpriseGroupEnterprise;

        /// <summary>
        /// Base constructor
        /// </summary>
        public GetAllUsersEnterpriseQueryHandler(
            IRepository<Entity.Enterprise> enterprise, IRepository<Entity.EnterpriseGroup> enterpriseGroup, IRepository<Entity.EnterpriseGroupEnterprise> enterpriseGroupEnterprise)
        {
            _enterprise = enterprise;
            _enterpriseGroup = enterpriseGroup;
            _enterpriseGroupEnterprise = enterpriseGroupEnterprise;
        }
        /// <summary>
        /// Get paged list of user enterprise
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<EnterpriseListItemDto>> Handle(GetAllUserEnterpriseQuery query, CancellationToken cancellationToken)
        {
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);

            Entity.EnterpriseGroup enterpriseGroup = _enterpriseGroup.GetAll()
                .WhereIf(true, t => t.UserId == query.Id).First();

            List<long?> enterpriseGroupEnterprise = _enterpriseGroupEnterprise.GetAll()
                .WhereIf(true, t => t.EnterpriseGroupId == enterpriseGroup.Id).ToList().Select(geg => geg.EnterpriseId).ToList();

            List<Entity.Enterprise> enterprise = new List<Entity.Enterprise>();
            foreach (var enterpriseId in enterpriseGroupEnterprise)
            {
                enterprise.Add(_enterprise.GetAll().Where(x => x.Id == enterpriseId).First());
            }

            List<EnterpriseListItemDto> enterpriseDto = ObjectMapper.Map<List<EnterpriseListItemDto>>(enterprise);

            return enterpriseDto;
        }
    }
}
