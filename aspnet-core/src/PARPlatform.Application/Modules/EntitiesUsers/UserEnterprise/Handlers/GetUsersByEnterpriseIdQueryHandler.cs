﻿using Abp.Collections.Extensions;
using Abp.Domain.Uow;
using MediatR;
using PARPlatform.Authorization.Users;
using PARPlatform.Base;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Handlers
{
    /// <summary>
    /// Get users by enterprise given id
    /// </summary> 
    public class GetUsersByEnterpriseIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetUsersByEnterpriseIdQuery, List<UpsertUsersByEnterpriseDto>>
    {
        private readonly IRepository<User> _userRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        public GetUsersByEnterpriseIdQueryHandler(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }
        /// <summary>
        /// Handles request for getting user enterprise details data
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<UpsertUsersByEnterpriseDto>> Handle(GetUsersByEnterpriseIdQuery input, CancellationToken cancellationToken)
        {
            if (!AbpSession.TenantId.HasValue)
            {
                CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            }
            List<User> users = _userRepository.GetAll()
            .WhereIf(true, t => t.EnterpriseId == input.Id).ToList();
            return ObjectMapper.Map<List<UpsertUsersByEnterpriseDto>>(users);
        }
    }
}
