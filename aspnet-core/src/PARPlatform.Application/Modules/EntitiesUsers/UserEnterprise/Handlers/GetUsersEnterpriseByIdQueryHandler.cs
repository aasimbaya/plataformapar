﻿
using Abp.Domain.Uow;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Queries;
using System.Threading;
using System.Threading.Tasks;
using Entity = PARPlatform.PAREntities;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Handlers
{
    /// <summary>
    /// Get paged user enterprise
    /// </summary> 
    public class GetUsersEnterpriseByIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetUserEnterpriseByIdQuery, UpsertUserEnterpriseWrapperDto>
    {
        private readonly IRepository<Entity.Enterprise> _enterpriseRepository;
        private readonly IRepository<Entity.PersonalType> _personalTypeRepository;
        private readonly IRepository<Entity.Personal> _personalRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        public GetUsersEnterpriseByIdQueryHandler(
            IRepository<Entity.Enterprise> enterpriseRepository,
            IRepository<Entity.PersonalType> personalTypeRepository,
            IRepository<Entity.Personal> personalRepository)
        {
            _enterpriseRepository = enterpriseRepository;
            _personalTypeRepository = personalTypeRepository;
            _personalRepository = personalRepository;
        }
        /// <summary>
        /// Handles request for getting user enterprise details data
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertUserEnterpriseWrapperDto> Handle(GetUserEnterpriseByIdQuery input, CancellationToken cancellationToken)
        {
            Entity.Enterprise enterprise = await _enterpriseRepository.FirstOrDefaultAsync(e => e.Id == input.Id);
            UpsertUserEnterpriseWrapperDto result = new UpsertUserEnterpriseWrapperDto();
            result.User = ObjectMapper.Map<UpsertEnterpriseDto>(enterprise);
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            Entity.PersonalType typeRrhh = await _personalTypeRepository.FirstOrDefaultAsync(e =>
                e.Abreviation == "HR");
            Entity.Personal rrhh = await _personalRepository.FirstOrDefaultAsync(e =>
                e.EnterpriseId == enterprise.Id &&
                e.PersonalTypeId == typeRrhh.Id);
            result.RRHH = ObjectMapper.Map<UpsertPersonalDto>(rrhh);

            Entity.PersonalType typeCeo = await _personalTypeRepository.FirstOrDefaultAsync(e =>
               e.Abreviation == "CEO");
            Entity.Personal ceo = await _personalRepository.FirstOrDefaultAsync(e =>
               e.EnterpriseId == enterprise.Id &&
               e.PersonalTypeId == typeCeo.Id);
            result.CEO = ObjectMapper.Map<UpsertPersonalDto>(ceo);

            return result;
        }
    }
}
