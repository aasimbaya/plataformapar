﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Uow;
using MediatR;
using PARPlatform.Authorization.Users;
using PARPlatform.Base;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Handlers
{
    /// <summary>
    /// Get users
    /// </summary> 
    public class GetUsersQueryHandler : UseCaseServiceBase, IRequestHandler<GetUsersFilteredInput, PagedResultDto<UpsertUsersByEnterpriseDto>>
    {
        private readonly IRepository<User> _userRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        public GetUsersQueryHandler(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }
        /// <summary>
        /// Handles request for getting user enterprise details data
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<UpsertUsersByEnterpriseDto>> Handle(GetUsersFilteredInput query, CancellationToken cancellationToken)
        {
            if (!AbpSession.TenantId.HasValue)
            {
                CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            }

            IQueryable<User> dbQuery = _userRepository.GetAll()
            .WhereIf(!string.IsNullOrWhiteSpace(query.Filter), t => t.Name.Contains(query.Filter)).AsQueryable();

            List<User> users = await dbQuery.OrderBy(query.Sorting).PageBy(query).ToListAsync(cancellationToken);
            int usersCount = await dbQuery.CountAsync(cancellationToken);
            List<UpsertUsersByEnterpriseDto> result = ObjectMapper.Map<List<UpsertUsersByEnterpriseDto>>(users);

            return new PagedResultDto<UpsertUsersByEnterpriseDto>(
             usersCount,
             result
         );
        }
    }
}
