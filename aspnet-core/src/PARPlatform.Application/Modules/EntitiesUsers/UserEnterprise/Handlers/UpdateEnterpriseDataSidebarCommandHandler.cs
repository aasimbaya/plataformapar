﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands.Validators;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using PARPlatform.Support;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Handlers
{
    /// <summary>
    /// Update Enterprise Data Command Handler
    /// </summary>
    public class UpdateEnterpriseDataSidebarCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateEnterpriseDataSidebarCommand, UpsertEnterpriseDto>
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        /// <summary>
        /// Main constructor
        /// </summary>
        public UpdateEnterpriseDataSidebarCommandHandler(IEnterpriseRepository enterpriseRepository)
        {
            _enterpriseRepository = enterpriseRepository;
        }
        /// <summary>
        /// Update Enterprise data from sidebar use case 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseDto> Handle(UpdateEnterpriseDataSidebarCommand input, CancellationToken cancellationToken)
        {
            new UpdateEnterpriseDataSidebarCommandValidator().Validate(input).ThrowIfAreErrors();

            PAREntities.Enterprise enterprise = await _enterpriseRepository.GetAsync(input.Id);
            ObjectMapper.Map(input, enterprise);
            await _enterpriseRepository.UpdateAsync(enterprise);
            return ObjectMapper.Map<UpsertEnterpriseDto>(enterprise);
        }
    }
}
