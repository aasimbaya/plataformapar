﻿using Abp.Collections.Extensions;
using Abp.Domain.Uow;
using MediatR;
using PARPlatform.Authorization.Users;
using PARPlatform.Base;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Entity = PARPlatform.PAREntities;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Handlers
{
    /// <summary>
    /// Update user enterprise command handler
    /// </summary>
    public class UpdateUserEnterpriseCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateUserEnterpriseCommand, UpsertEnterpriseDto>
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Entity.Enterprise> _enterpriseRepository;
        private readonly IRepository<Entity.PersonalType> _personalTypeRepository;
        private readonly IRepository<Entity.Personal> _personalRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="IRepository"></param>
        public UpdateUserEnterpriseCommandHandler(
            IRepository<Entity.Enterprise> enterpriseRepository,
            IRepository<Entity.PersonalType> personalTypeRepository,
            IRepository<User> userRepository,
            IRepository<Entity.Personal> personalRepository)
        {
            _enterpriseRepository = enterpriseRepository;
            _personalTypeRepository = personalTypeRepository;
            _userRepository = userRepository;
            _personalRepository = personalRepository;
        }

        /// <summary>
        /// Update user enterprise
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseDto> Handle(UpdateUserEnterpriseCommand input, CancellationToken cancellationToken)
        {
            long[] usersId = input.User.ListUsersId.Split(',').Select(n => long.Parse(n)).ToArray();
            Entity.Enterprise enterprise = await _enterpriseRepository.FirstOrDefaultAsync(e => e.Id == input.Id);
            ObjectMapper.Map(input.User, enterprise);
            enterprise.UserId = usersId[0];
            enterprise.LastModificationTime = DateTime.Now;
            enterprise.IsActive = true;

            await _enterpriseRepository.UpdateAsync(enterprise);
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            Entity.PersonalType typeRrhh = await _personalTypeRepository.FirstOrDefaultAsync(e =>
               e.Abreviation == "HR");
            Entity.Personal hhrr = await _personalRepository.FirstOrDefaultAsync(e =>
                e.PersonalTypeId == typeRrhh.Id &&
                e.EnterpriseId == input.Id
                );
            ObjectMapper.Map(input.RRHH, hhrr);
            await _personalRepository.UpdateAsync(hhrr);

            Entity.PersonalType typeCeo = await _personalTypeRepository.FirstOrDefaultAsync(e =>
           e.Abreviation == "CEO");
            Entity.Personal ceo = await _personalRepository.FirstOrDefaultAsync(e =>
                e.PersonalTypeId == typeCeo.Id &&
                e.EnterpriseId == input.Id
                );
            ObjectMapper.Map(input.CEO, ceo);
            await _personalRepository.UpdateAsync(ceo);

            List<User> deleteRelation = _userRepository.GetAll()
            .WhereIf(true, t => t.EnterpriseId == enterprise.Id).ToList().Where(x => !usersId.Contains(x.Id)).ToList();
            foreach (var id in deleteRelation)
            {
                User user = await UserManager.FindByIdAsync(id.ToString());
                user.EnterpriseId = 0;
                CheckErrors(await UserManager.UpdateAsync(user));
            }
            foreach (var id in usersId)
            {
                User user = await UserManager.FindByIdAsync(id.ToString());
                user.EnterpriseId = enterprise.Id;
                CheckErrors(await UserManager.UpdateAsync(user));
            }
            return input.User;
        }
    }
}
