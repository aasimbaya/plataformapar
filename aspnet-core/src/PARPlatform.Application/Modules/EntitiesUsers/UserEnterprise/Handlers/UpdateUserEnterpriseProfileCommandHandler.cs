﻿using MediatR;
using PARPlatform.Authorization.Users;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands.Validators;
using PARPlatform.Support;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Handlers
{
    /// <summary>
    /// Update User Enterprise Profile Command Handler
    /// </summary>
    public class UpdateUserEnterpriseProfileCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateUserEnterpriseProfileCommand, UpsertEnterpriseUserFromSidebarDto>
    {
        /// <summary>
        /// Update User Enterprise Profile From sidebar use case 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertEnterpriseUserFromSidebarDto> Handle(UpdateUserEnterpriseProfileCommand input, CancellationToken cancellationToken)
        {
            new UpdateUserEnterpriseProfileCommandValidator().Validate(input).ThrowIfAreErrors();

            var user = await UserManager.FindByIdAsync(input.Id.ToString());

            //Update user properties
            ObjectMapper.Map(input, user); //Passwords is not mapped (see mapping configuration)

            CheckErrors(await UserManager.UpdateAsync(user));

            if (!String.IsNullOrEmpty(input.Password))
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                CheckErrors(await UserManager.ChangePasswordAsync(user, input.Password));
            }

            return ObjectMapper.Map<UpsertEnterpriseUserFromSidebarDto>(user);
        }
    }
}
