﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Queries
{
    /// <summary>
    /// Get user enterprise by filter
    /// </summary>
    public class GetAllPagedUserEnterpriseQuery : PagedAndSortedInputDto, IShouldNormalize, IRequest<PagedResultDto<EnterpriseListItemDto>>
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }            
            Filter = Filter?.Trim();
        }
    }
}
