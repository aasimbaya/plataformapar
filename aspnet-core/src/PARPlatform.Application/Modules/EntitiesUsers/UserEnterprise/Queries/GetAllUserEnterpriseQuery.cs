﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using System.Collections.Generic;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Queries
{
    /// <summary>
    /// Get user enterprise by filter
    /// </summary>
    public class GetAllUserEnterpriseQuery : IRequest<List<EnterpriseListItemDto>>
    {
        public long Id { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="id"></param>
        public GetAllUserEnterpriseQuery(long id)
        {
            Id = id;
        }
    }
}
