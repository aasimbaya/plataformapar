﻿using MediatR;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Queries
{
    /// <summary>
    /// Get user enterprise by fiidlter
    /// </summary>
    public class GetUserEnterpriseByIdQuery : IRequest<UpsertUserEnterpriseWrapperDto>
    {
        public long Id { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="id"></param>
        public GetUserEnterpriseByIdQuery(long id)
        {
            Id = id;
        }
    }
}
