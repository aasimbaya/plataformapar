﻿using MediatR;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using System.Collections.Generic;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Queries
{
    /// <summary>
    /// Get users by enterpriseId
    /// </summary>
    public class GetUsersByEnterpriseIdQuery : IRequest<List<UpsertUsersByEnterpriseDto>>
    {
        public long Id { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="id"></param>
        public GetUsersByEnterpriseIdQuery(long id)
        {
            Id = id;
        }
    }
}
