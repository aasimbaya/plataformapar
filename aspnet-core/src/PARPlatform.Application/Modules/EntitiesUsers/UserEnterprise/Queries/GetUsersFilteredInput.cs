﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;

namespace PARPlatform.Modules.EntitiesUsers.UserEnterprise.Queries
{
    /// <summary>
    /// Get users 
    /// </summary>
    public class GetUsersFilteredInput : PagedAndSortedInputDto, IShouldNormalize, IRequest<PagedResultDto<UpsertUsersByEnterpriseDto>>
    {
        public string Filter { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
            Filter = Filter?.Trim();
        }
    }
}
