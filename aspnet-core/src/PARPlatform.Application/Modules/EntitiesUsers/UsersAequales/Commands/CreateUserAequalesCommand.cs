﻿using MediatR;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Commands
{
    /// <summary>
    /// Create user aequales command 
    /// </summary>
    public class CreateUserAequalesCommand : IRequest<UpsertUserAequalesDto>
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public int RoleId { get; set; }
        public string Password { get; set; }
        public bool SetRandomPassword { get; set; }
        public bool SendActivationEmail { get; set; }
        public bool ShouldChangePasswordOnNextLogin { get; set; }
        public List<long> OrganizationUnits { get; set; }
    }
}
