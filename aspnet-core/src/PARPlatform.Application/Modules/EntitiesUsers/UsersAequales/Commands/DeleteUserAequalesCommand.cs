﻿using MediatR;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Commands
{
    /// <summary>
    /// Delete user  aequales
    /// </summary>
    public class DeleteUserAequalesCommand : IRequest<UpsertUserAequalesDto>
    {
        public long Id { get; set; }
    }
}
