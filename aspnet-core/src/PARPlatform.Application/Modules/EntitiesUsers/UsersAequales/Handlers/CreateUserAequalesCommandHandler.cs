﻿using Abp.Authorization.Users;
using Abp.Notifications;
using Abp.Runtime.Session;
using MediatR;
using Microsoft.AspNetCore.Identity;
using PARPlatform.Authorization.Roles;
using PARPlatform.Authorization.Users;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Commands;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;
using PARPlatform.Notifications;
using PARPlatform.Url;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Handlers
{
    /// <summary>
    /// Create user aequales command handler
    /// </summary>
    public class CreateUserAequalesCommandHandler : UseCaseServiceBase, IRequestHandler<CreateUserAequalesCommand, UpsertUserAequalesDto>
    {
        public IAppUrlService AppUrlService { get; set; }

        private readonly IUserPolicy _userPolicy;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IUserEmailer _userEmailer;
        private readonly IEnumerable<IPasswordValidator<User>> _passwordValidators;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly UserManager _userManager;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="IRepository"></param>
        public CreateUserAequalesCommandHandler(
            IUserEmailer userEmailer,
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            UserManager userManager,
            IPasswordHasher<User> passwordHasher, 
            INotificationSubscriptionManager notificationSubscriptionManager,
            IAppNotifier appNotifier,
            IUserPolicy userPolicy)
        {
            _userPolicy = userPolicy;
            _userEmailer = userEmailer;
            _passwordHasher = passwordHasher;
            _passwordValidators = passwordValidators;
            _userManager = userManager;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;

            AppUrlService = NullAppUrlService.Instance;
        }

        /// <summary>
        /// Create user aequales
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertUserAequalesDto> Handle(CreateUserAequalesCommand input, CancellationToken cancellationToken)
        {
            if (AbpSession.TenantId.HasValue)
            {
                await _userPolicy.CheckMaxUserCountAsync(AbpSession.GetTenantId());
            }
            User user = ObjectMapper.Map<User>(input); //Passwords is not mapped (see mapping configuration)
            user.TenantId = AbpSession.TenantId;

            //Set password
            if (input.SetRandomPassword)
            {
                var randomPassword = await _userManager.CreateRandomPassword();
                user.Password = _passwordHasher.HashPassword(user, randomPassword);
                input.Password = randomPassword;
            }
            else if (!String.IsNullOrEmpty(input.Password))
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                foreach (var validator in _passwordValidators)
                {
                    CheckErrors(await validator.ValidateAsync(UserManager, user, input.Password));
                }

                user.Password = _passwordHasher.HashPassword(user, input.Password);
            }

            user.ShouldChangePasswordOnNextLogin = input.ShouldChangePasswordOnNextLogin;

            user.Roles = new Collection<UserRole>();
            user.Roles.Add(new UserRole(AbpSession.TenantId, user.Id, input.RoleId));

            CheckErrors(await UserManager.CreateAsync(user));
            await CurrentUnitOfWork.SaveChangesAsync(); //To get new user's Id.

            //Notifications
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.ToUserIdentifier());
            await _appNotifier.WelcomeToTheApplicationAsync(user);

            //Organization Units
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            //Send activation email
            if (input.SendActivationEmail)//revisar de donde sale SendActivationEmail
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.Password
                );
            }
            return ObjectMapper.Map<UpsertUserAequalesDto>(user);
        }
    }
}
