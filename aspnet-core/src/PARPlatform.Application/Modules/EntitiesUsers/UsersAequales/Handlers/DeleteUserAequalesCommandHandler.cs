﻿using Abp.Runtime.Session;
using Abp.UI;
using MediatR;
using PARPlatform.Authorization.Users;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Commands;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Handlers
{
    /// <summary>
    /// Delete user aequales command handler
    /// </summary>
    public class DeleteUserAequalesCommandHandler : UseCaseServiceBase, IRequestHandler<DeleteUserAequalesCommand, UpsertUserAequalesDto>
    {
        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="questionCategoryRepository"></param>
        public DeleteUserAequalesCommandHandler()
        {
        }
        /// <summary>
        /// Delete user aequales
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertUserAequalesDto> Handle(DeleteUserAequalesCommand command, CancellationToken cancellationToken)
        {
            if (command.Id == AbpSession.GetUserId())
            {
                throw new UserFriendlyException(L("YouCanNotDeleteOwnAccount"));
            }
            User user = await UserManager.FindByIdAsync(command.Id.ToString());
            CheckErrors(await UserManager.DeleteAsync(user));
            return ObjectMapper.Map<UpsertUserAequalesDto>(user);
        }
    }
}
