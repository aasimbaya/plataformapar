﻿using Abp.Application.Services.Dto;
using Abp.Linq.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Authorization.Users;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Collections.Extensions;
using Abp.Zero.Configuration;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Authorization.Roles;
using PARPlatform.Authorization.Roles;
using Abp.Domain.Uow;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Handlers
{
    /// <summary>
    /// Get paged users aequales
    /// </summary> 
    public class GetAllPagedUsersAequalesQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedUserAequalesQuery, PagedResultDto<UserAequalesListItemDto>>
    {
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        public GetAllPagedUsersAequalesQueryHandler(
            IRepository<Role> roleRepository,
            IRepository<UserRole, long> userRoleRepository)
        {
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
        }
        /// <summary>
        /// Get paged list of user aequales
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<UserAequalesListItemDto>> Handle(GetAllPagedUserAequalesQuery input, CancellationToken cancellationToken)
        {
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);

            IQueryable<User> query;
            if (input.filtersByColumns == "surname")
            {
                query = UserManager.Users
                               .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                                   u =>
                                       u.Surname.Contains(input.Filter)
                               );
            }
            else
            {
                query = UserManager.Users
                             .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                                 u =>
                                     u.Name.Contains(input.Filter)
                             );
            }
            int userCount = await query.CountAsync(cancellationToken);

            List<User> users = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            List<Role> roles = _roleRepository.GetAll().ToList();
            List<UserAequalesListItemDto> userListDtos = ObjectMapper.Map<List<UserAequalesListItemDto>>(users);
            foreach (var user in userListDtos)
            {
                UserRole? roleUser = _userRoleRepository.GetAll().Where(relation => relation.UserId == user.Id).FirstOrDefault();
                if (roleUser != null)
                {
                    user.Role = roles.Where(r => r.Id == roleUser.RoleId).First().Name;
                }
            }
            return new PagedResultDto<UserAequalesListItemDto>(
                userCount,
                userListDtos
            );
        }
    }
}
