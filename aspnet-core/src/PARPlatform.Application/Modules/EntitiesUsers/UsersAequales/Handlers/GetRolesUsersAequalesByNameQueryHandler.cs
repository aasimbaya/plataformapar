﻿using MediatR;
using PARPlatform.Authorization.Users;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Zero.Configuration;
using PARPlatform.Authorization.Users.Dto;
using PARPlatform.Authorization.Roles;
using Abp.Domain.Repositories;
using Abp.Authorization.Users;
using Abp.Domain.Uow;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Handlers
{
    /// <summary>
    /// Get roles user aequales
    /// </summary> 
    public class GetRolesUsersAequalesByNameQueryHandler : UseCaseServiceBase, IRequestHandler<GetRolesUserAequalesByNameQuery, List<UserRoleDto>>
    {
        private readonly IRoleManagementConfig _roleManagementConfig;
        private readonly RoleManager _roleManager;
        private readonly IRepository<UserRole, long> _userRoleRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        public GetRolesUsersAequalesByNameQueryHandler(
            RoleManager roleManager,
            IRoleManagementConfig roleManagementConfig,
            IRepository<UserRole, long> userRoleRepository)
        {
            _roleManager = roleManager;
            _roleManagementConfig = roleManagementConfig;
            _userRoleRepository = userRoleRepository;
        }
        /// <summary>
        /// Handles request for getting roles user aequales
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<UserRoleDto>> Handle(GetRolesUserAequalesByNameQuery input, CancellationToken cancellationToken)
        {
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            List<UserRoleDto> userRoleDtos = _roleManager.Roles
                .Where(r => r.DisplayName == input.Name)
                .OrderBy(r => r.DisplayName)
                .Select(r => new UserRoleDto
                {
                    RoleId = r.Id,
                    RoleName = r.Name,
                    RoleDisplayName = r.DisplayName
                })
                .ToList();
            return userRoleDtos;
        }

    }
}
