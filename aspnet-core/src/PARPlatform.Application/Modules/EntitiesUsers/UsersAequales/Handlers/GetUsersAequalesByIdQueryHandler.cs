﻿using MediatR;
using PARPlatform.Authorization.Users;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Zero.Configuration;
using PARPlatform.Authorization.Users.Dto;
using PARPlatform.Authorization.Roles;
using Abp.Domain.Repositories;
using Abp.Authorization.Users;
using Abp.Domain.Uow;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Handlers
{
    /// <summary>
    /// Get paged user aequales
    /// </summary> 
    public class GetUsersAequalesByIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetUserAequalesByIdQuery, UpsertUserAequalesDto>
    {
        private readonly IRepository<UserRole, long> _userRoleRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        public GetUsersAequalesByIdQueryHandler(IRepository<UserRole, long> userRoleRepository)
        {
            _userRoleRepository = userRoleRepository;
        }
        /// <summary>
        /// Handles request for getting user aequales details data
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertUserAequalesDto> Handle(GetUserAequalesByIdQuery input, CancellationToken cancellationToken)
        {
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);

            List<User> filteredUA = UserManager.Users.
                Where(e => e.Id == input.Id)
                .ToList();
            UpsertUserAequalesDto result = ObjectMapper.Map<UpsertUserAequalesDto>(filteredUA.First());
            UserRole roleUser = _userRoleRepository.GetAll().Where(relation =>
            relation.UserId == result.Id
            ).First();
            result.RoleId = roleUser.RoleId;
            return result;
        }

    }
}
