﻿using Abp.Domain.Repositories;
using MediatR;
using Microsoft.AspNetCore.Identity;
using PARPlatform.Authorization.Roles;
using PARPlatform.Authorization.Users;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Commands;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;
using PARPlatform.Url;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Handlers
{
    /// <summary>
    /// Update user aequales command handler
    /// </summary>
    public class UpdateUserAequalesCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateUserAequalesCommand, UpsertUserAequalesDto>
    {
        public IAppUrlService AppUrlService { get; set; }

        private readonly IRepository<Role> _roleRepository;
        private readonly UserManager _userManager;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IUserEmailer _userEmailer;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="questionCategoryRepository"></param>
        public UpdateUserAequalesCommandHandler(
             UserManager userManager,
            IUserEmailer userEmailer,
            IPasswordHasher<User> passwordHasher,
            IRepository<Role> roleRepository)
        {
            _userEmailer = userEmailer;
            _roleRepository = roleRepository;
            _passwordHasher = passwordHasher;
            _userManager = userManager;

            AppUrlService = NullAppUrlService.Instance;
        }
        /// <summary>
        /// Update question category use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertUserAequalesDto> Handle(UpdateUserAequalesCommand input, CancellationToken cancellationToken)
        {
            User user = await UserManager.FindByIdAsync(input.Id.ToString());

            //Update user properties
            ObjectMapper.Map(input, user); //Passwords is not mapped (see mapping configuration)

            CheckErrors(await UserManager.UpdateAsync(user));

            if (input.SetRandomPassword)
            {
                var randomPassword = await _userManager.CreateRandomPassword();
                user.Password = _passwordHasher.HashPassword(user, randomPassword);
                input.Password = randomPassword;
            }
            else if (!String.IsNullOrEmpty(input.Password))
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                CheckErrors(await UserManager.ChangePasswordAsync(user, input.Password));
            }

            //Update roles
            string[] roles = _roleRepository.GetAll().Where(rol => rol.Id == input.RoleId).Select(item => item.Name).ToArray();
            CheckErrors(await UserManager.SetRolesAsync(user, roles));

            //update organization units
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.Password
                );
            }

            return ObjectMapper.Map<UpsertUserAequalesDto>(user);
        }
    }
}
