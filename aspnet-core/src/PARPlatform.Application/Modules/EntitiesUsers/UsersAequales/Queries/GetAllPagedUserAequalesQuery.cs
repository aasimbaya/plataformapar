﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Queries
{
    /// <summary>
    /// Get user aequales by filter
    /// </summary>
    public class GetAllPagedUserAequalesQuery : PagedAndSortedInputDto, IShouldNormalize, IRequest<PagedResultDto<UserAequalesListItemDto>>
    {
        public string Filter { get; set; }
        public string filtersByColumns { get; set; }

        public List<string> Permissions { get; set; }

        public int? Role { get; set; }

        public bool OnlyLockedUsers { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name,Surname";
            }
            if (Sorting.Contains("Role"))
            {
                Sorting = Sorting.Replace("Role", "RoleId");
            }
            Filter = Filter?.Trim();
        }
    }
}
