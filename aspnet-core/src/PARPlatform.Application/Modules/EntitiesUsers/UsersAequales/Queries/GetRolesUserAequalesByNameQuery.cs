﻿
using MediatR;
using PARPlatform.Authorization.Users.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Queries
{
    /// <summary>
    /// Get roles user aequales by filter
    /// </summary>
    public class GetRolesUserAequalesByNameQuery : IRequest<List<UserRoleDto>>
    {
        public string Name { get; set; }
    }
}
