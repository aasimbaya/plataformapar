﻿using MediatR;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;

namespace PARPlatform.Modules.EntitiesUsers.UsersAequales.Queries
{
    /// <summary>
    /// Get user aequales by id
    /// </summary>
    public class GetUserAequalesByIdQuery : IRequest<UpsertUserAequalesDto> 
    {
        public long Id { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="id"></param>
        public GetUserAequalesByIdQuery(long id)
        {
            Id = id;
        }
    }
}
