﻿using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Localization.Dto;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PARPlatform.Modules.Language.Commands
{

    /// <summary>
    /// Create Questions Bank Command
    /// </summary>
    public class CreateOrUpdateLanguageCommand : IRequest<UpsertLanguageTextDto>
    {
        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="languageText"></param>
        public CreateOrUpdateLanguageCommand(UpsertLanguageTextDto languageText)
        {
            LanguageText = languageText;
        }

        public UpsertLanguageTextDto LanguageText { get; set; }
        
    }
}
