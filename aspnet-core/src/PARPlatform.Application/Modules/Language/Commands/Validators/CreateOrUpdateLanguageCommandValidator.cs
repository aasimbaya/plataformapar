﻿using Abp.Localization;
using FluentValidation;

namespace PARPlatform.Modules.Language.Commands.Validators
{
    /// <summary>
    /// Create Language command input parameters validator
    /// </summary>
    public class CreateOrUpdateLanguageCommandValidator : AbstractValidator<CreateOrUpdateLanguageCommand>
    {
        /// <summary>
        /// Main constructor
        /// </summary>
        public CreateOrUpdateLanguageCommandValidator()
        {
            RuleFor(x => x.LanguageText.SourceName)
                .MaximumLength(ApplicationLanguageText.MaxSourceNameLength)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.LanguageText.LanguageName)
                .MaximumLength(ApplicationLanguage.MaxNameLength)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.LanguageText.Key)
                .MaximumLength(ApplicationLanguageText.MaxKeyLength)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.LanguageText.targetValue)
                    .MaximumLength(ApplicationLanguageText.MaxValueLength)
                    .NotNull()
                    .NotEmpty();

        }
    }
}
