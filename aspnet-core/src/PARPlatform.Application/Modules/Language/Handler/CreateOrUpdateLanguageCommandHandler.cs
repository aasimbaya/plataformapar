﻿using Abp.Domain.Uow;
using Abp.Localization;
using FluentValidation;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Localization.Dto;
using PARPlatform.Modules.Language.Commands;
using PARPlatform.Modules.Language.Commands.Validators;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands.Validators;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.PAREntities;
using PARPlatform.Support;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Language.Handlers
{
    /// <summary>
    /// Used to handle Create or update language Command
    /// </summary>
    public class CreateOrUpdateLanguageCommandHandler : UseCaseServiceBase, IRequestHandler<CreateOrUpdateLanguageCommand, UpsertLanguageTextDto>
    {
        private readonly IApplicationLanguageTextRepository _applicationLanguageTextRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;


        /// <summary>
        /// Main Constructor
        /// </summary>
        public CreateOrUpdateLanguageCommandHandler(IApplicationLanguageTextRepository applicationLanguageTextRepository,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _applicationLanguageTextRepository = applicationLanguageTextRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        /// <summary>
        /// Create new Question of language use case 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertLanguageTextDto> Handle(CreateOrUpdateLanguageCommand input, CancellationToken cancellationToken)
        {
            using (var uow = UnitOfWorkManager.Begin())
            {
                try
                {
                    new CreateOrUpdateLanguageCommandValidator().Validate(input).ThrowIfAreErrors();

                    ApplicationLanguageText data = await _applicationLanguageTextRepository
                        .FirstOrDefaultAsync(x => (x.Key == input.LanguageText.Key
                        && x.Source == input.LanguageText.SourceName
                        && x.LanguageName == input.LanguageText.LanguageName
                        && x.TenantId == AbpSession.TenantId));

                    if (data == null)
                    {
                        data = new ApplicationLanguageText()
                        {
                            Key = input.LanguageText.Key,
                            LanguageName = input.LanguageText.LanguageName,
                            Source = input.LanguageText.SourceName,
                            Value = input.LanguageText.targetValue,
                            TenantId = AbpSession.TenantId
                        };
                       
                        await _applicationLanguageTextRepository.InsertAndGetIdAsync(data);
                    }
                    else
                    {
                        data.Key = input.LanguageText.Key;
                        data.Value = input.LanguageText.targetValue;
                        await _applicationLanguageTextRepository.UpdateAsync(data);
                    }
                    return ObjectMapper.Map<UpsertLanguageTextDto>(data);
                }
                finally
                {
                    uow.Complete();
                }

            }

        }

    }
}
