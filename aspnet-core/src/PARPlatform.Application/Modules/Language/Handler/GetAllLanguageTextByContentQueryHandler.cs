﻿using Abp.Localization;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Language.Queries;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using PARPlatform.Modules.MeasuringTools.Forms.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Language.Handler
{
    /// <summary>
    /// Get Form by id handler
    /// </summary>
    public class GetAllLanguageTextByContentQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllLanguageTextByContent, List<ApplicationLanguageText>>
    {
        private readonly IApplicationLanguageTextRepository _applicationLanguageTextRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="formNameRepository"></param>
        public GetAllLanguageTextByContentQueryHandler(IApplicationLanguageTextRepository applicationLanguageTextRepository)
        {
            _applicationLanguageTextRepository = applicationLanguageTextRepository;
        }

        /// <summary>
        /// Use case get formName by Name
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<ApplicationLanguageText>> Handle(GetAllLanguageTextByContent query, CancellationToken cancellationToken)
        {
            var formLanguajeText = await _applicationLanguageTextRepository
                .GetAllListAsync(x => x.Value.ToUpper().Contains(query.Content.ToUpper())  
                    && x.TenantId == AbpSession.TenantId);
            return formLanguajeText;
        }
    }
}
