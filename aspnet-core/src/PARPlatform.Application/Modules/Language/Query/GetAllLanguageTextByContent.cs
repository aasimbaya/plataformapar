﻿using Abp.Localization;
using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Language.Queries
{
    /// <summary>
    /// Query to get all lLanguageText
    /// </summary>
    public class GetAllLanguageTextByContent : IRequest<List<ApplicationLanguageText>>
    {
        public string Content { get; set; }

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="content"></param>
        public GetAllLanguageTextByContent(string content)
        {
            Content = content;
        }
    }
}
