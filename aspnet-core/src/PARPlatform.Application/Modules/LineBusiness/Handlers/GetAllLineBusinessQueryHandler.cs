﻿
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.LineBusiness.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Entity = PARPlatform.PAREntities;

namespace PARPlatform.Modules.LineBusiness.Handlers
{
    /// <summary>
    /// Handler to get all setors
    /// </summary>
    public class GetAllLineBusinessQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllLineBusinessQuery, List<DropdownItemDto>>
    {
        private readonly IRepository<Entity.LineBusiness> _lineBusinessRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="countryRepository"></param>
        public GetAllLineBusinessQueryHandler(IRepository<Entity.LineBusiness> lineBusinessRepository)
        {
            _lineBusinessRepository = lineBusinessRepository;
        }
        /// <summary>
        /// Get all sectors
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<DropdownItemDto>> Handle(GetAllLineBusinessQuery request, CancellationToken cancellationToken)
        {
            var sectors = _lineBusinessRepository.GetAll()
                .Select(t => new DropdownItemDto { Id = t.Id, Name = t.Name });
            return ObjectMapper.Map(sectors, new List<DropdownItemDto>());
        }
    }
}
