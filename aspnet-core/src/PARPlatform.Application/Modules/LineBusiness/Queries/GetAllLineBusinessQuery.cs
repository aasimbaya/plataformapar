﻿
using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.LineBusiness.Queries
{
    /// <summary>
    /// Query to get all linebusiness
    /// </summary>
    public class GetAllLineBusinessQuery : IRequest<List<DropdownItemDto>>
    {
    }
}
