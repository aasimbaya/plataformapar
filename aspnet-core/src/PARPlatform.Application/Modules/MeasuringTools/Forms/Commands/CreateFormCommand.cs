﻿using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using System;

namespace PARPlatform.Modules.MeasuringTools.Forms.Commands
{
    /// <summary>
    /// Command for create Form
    /// </summary>
    public class CreateFormCommand : IRequest<UpsertFormDto>
    {
        public string Title { get; set; }
        public int FormNameId { get; set; }
        public string Description { get; set; }
        public string ThematicName { get; set; }
        public long ThematicId { get; set; }
        public string TypeName { get; set; }
        public long TypeId { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string SpecialDistinctionIdArray { get; set; }
        public string QuestionCategoryIdArray { get; set; }
       
    }
}
