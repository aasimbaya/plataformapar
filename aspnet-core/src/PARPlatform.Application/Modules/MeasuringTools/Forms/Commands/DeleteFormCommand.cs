﻿using MediatR;

namespace PARPlatform.Modules.MeasuringTools.Form.Commands
{
    /// <summary>
    /// Command for delete Form
    /// </summary>
    public class DeleteFormCommand : IRequest<Unit>
    {
        public DeleteFormCommand(long id)
        {
            Id = id;
        }

        public long Id { get; set; }
    }
}
