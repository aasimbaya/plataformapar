﻿using Abp.Localization;
using Abp.Localization.Sources;
using FluentValidation;
using PARPlatform.Modules.MeasuringTools.Forms.Commands;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands.Validators
{
    /// <summary>
    /// Create Questions Bank command input parameters validator
    /// </summary>
    public class CreateFormCommandValidator : AbstractValidator<CreateFormCommand>
    {
        private readonly ILocalizationSource _localizationSource;

        /// <summary>
        /// Main constructor
        /// </summary>
        public CreateFormCommandValidator(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(PARPlatformConsts.LocalizationSourceName);

            RuleFor(x => x.Title)
                .MaximumLength(150)
                .NotNull().WithMessage(_localizationSource.GetString("FormQuestionnaireTitle"))
                .NotEmpty().WithMessage(_localizationSource.GetString("FormQuestionnaireTitle"));
        }
    }
}
