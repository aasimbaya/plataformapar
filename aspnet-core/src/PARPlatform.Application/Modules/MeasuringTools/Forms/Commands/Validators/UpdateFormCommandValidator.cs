﻿using Abp.Localization;
using Abp.Localization.Sources;
using FluentValidation;
using PARPlatform.Modules.MeasuringTools.Forms.Commands;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands.Validators
{
    /// <summary>
    /// Update Questions Bank command input parameters validator
    /// </summary>
    public class UpdateFormCommandValidator : AbstractValidator<UpdateFormCommand>
    {
        private readonly ILocalizationSource _localizationSource;

        /// <summary>
        /// Main constructor
        /// </summary>
        public UpdateFormCommandValidator(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(PARPlatformConsts.LocalizationSourceName);

            RuleFor(x => x.Title)
                .MaximumLength(150)
                .NotNull().WithMessage(_localizationSource.GetString("FormQuestionnaireTitle"))
                .NotEmpty().WithMessage(_localizationSource.GetString("FormQuestionnaireTitle"));
        }
    }
}
