﻿using Abp.Localization;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.Forms.Commands;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands.Validators;
using PARPlatform.Support;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.Forms.Handlers
{
    /// <summary>
    /// Create Form command handler
    /// </summary>
    public class CreateFormCommandHandler : UseCaseServiceBase, IRequestHandler<CreateFormCommand, UpsertFormDto>
    {
        private readonly IFormRepository _formRepository;
        private readonly IFormTypeFormRepository _formTypeFormRepository;
        private readonly IFormThematicFormRepository _formThematicFormRepository;
        private readonly IFormTypeRepository _formTypeRepository;
        private readonly IFormThematicRepository _formThematicRepository;
        private readonly ILocalizationManager _localizationManager;
        

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="formRepository"></param>
        /// <param name="formTypeFormRepository"></param>
        /// <param name="formThematicFormRepository"></param>
        public CreateFormCommandHandler(IFormRepository formRepository,
                                        IFormTypeFormRepository formTypeFormRepository,
                                        IFormThematicFormRepository formThematicFormRepository,
                                        IFormTypeRepository formTypeRepository,
                                        IFormThematicRepository formThematicRepository,
                                        ILocalizationManager localizationManager)
        {
            _formRepository = formRepository;
            _formTypeFormRepository = formTypeFormRepository;
            _formThematicFormRepository = formThematicFormRepository;
            _formTypeRepository = formTypeRepository;
            _formThematicRepository = formThematicRepository;
            _localizationManager = localizationManager;
        }

        /// <summary>
        /// Create Form use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertFormDto> Handle(CreateFormCommand command, CancellationToken cancellationToken)
        {
            new CreateFormCommandValidator(_localizationManager).Validate(command).ThrowIfAreErrors();

            PAREntities.Form form = ObjectMapper.Map<PAREntities.Form>(command);
            form.LastModificationTime = form.CreationTime;
            form.Id = await _formRepository.InsertAndGetIdAsync(form);

            if (command.TypeId > 0)
            {
                PAREntities.FormType formType = await _formTypeRepository.GetAsync(command.TypeId);
                PAREntities.FormTypeForm formTypeForm = new()
                {
                    FormId = form.Id,
                    FormTypeId = formType.Id,
                    FormFk = form,
                    FormTypeFk = formType
                };
                formTypeForm.Id = await _formTypeFormRepository.InsertAndGetIdAsync(formTypeForm);
            }

            if (command.ThematicId > 0)
            {
                PAREntities.FormThematic formThematic = await _formThematicRepository.GetAsync(command.ThematicId);
                PAREntities.FormThematicForm formThematicForm = new()
                {
                    FormId = form.Id,
                    FormThematicId = formThematic.Id,
                    FormFk = form,
                    FormThematicFk = formThematic
                };
                formThematicForm.Id = await _formThematicFormRepository.InsertAndGetIdAsync(formThematicForm);

            }
            return ObjectMapper.Map<UpsertFormDto>(form);
        }
    }
}
