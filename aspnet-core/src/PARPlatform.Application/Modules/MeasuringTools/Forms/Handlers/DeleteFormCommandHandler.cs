﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.Form.Commands;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.Form.Handlers
{
    /// <summary>
    /// Delete Form command handler
    /// </summary>
    public class DeleteFormCommandHandler : IRequestHandler<DeleteFormCommand, Unit>
    {
        private readonly IFormRepository _formRepository;
        private readonly IFormTypeFormRepository _formTypeFormRepository;
        private readonly IFormThematicFormRepository _formThematicFormRepository;
        private readonly IQuestionsBankRepository _questionsBankRepository;

        public DeleteFormCommandHandler(IFormRepository formRepository,
            IFormTypeFormRepository formTypeFormRepository,
            IFormThematicFormRepository formThematicFormRepository,
            IQuestionsBankRepository questionsBankRepository)
        {
            _formRepository = formRepository;
            _formTypeFormRepository = formTypeFormRepository;
            _formThematicFormRepository = formThematicFormRepository;
            _questionsBankRepository = questionsBankRepository;

        }
        public async Task<Unit> Handle(DeleteFormCommand request, CancellationToken cancellationToken)
        {
            var formDeleted = await _formRepository.GetAsync(request.Id);
            formDeleted.IsDeleted = true;

            var formTypeForm = await _formTypeFormRepository.FirstOrDefaultAsync(x => x.FormId == request.Id);
            if (formTypeForm is not null)
            {
                formTypeForm.IsDeleted = true;
                await _formTypeFormRepository.DeleteAsync(formTypeForm);
            }

            var formThematicForm = await _formThematicFormRepository.FirstOrDefaultAsync(x => x.FormId == request.Id);
            if (formThematicForm is not null)
            {
                formThematicForm.IsDeleted = true;
                await _formThematicFormRepository.DeleteAsync(formThematicForm);
            }

            await _formRepository.DeleteAsync(formDeleted);

            var questionsList = _questionsBankRepository.GetAll().Where(x => x.FormId ==request.Id).ToList();
            if(questionsList.Count > 0)
            {
                for (int i = 0; i < questionsList.Count; i++)
                {
                    questionsList[i].FormId = null;
                    await _questionsBankRepository.UpdateAsync(questionsList[i]);
                }
            }

            return Unit.Value;
        }
    }
}
