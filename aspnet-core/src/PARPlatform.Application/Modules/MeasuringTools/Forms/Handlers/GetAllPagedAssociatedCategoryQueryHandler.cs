﻿using Abp.Application.Services.Dto;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using PARPlatform.Modules.MeasuringTools.Forms.Queries;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace PARPlatform.Modules.MeasuringTools.Forms.Handlers
{
    ///// <summary>
    ///// Get paged associated special distinction data query handler
    ///// </summary>
    //public class GetAllPagedAssociatedQuestionQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedAssociatedQuery, PagedResultDto<ListItemAssociatedToFormDto>>
    //{
    //    private readonly IFormRepository _formRepository;
    //    private readonly IQuestionsBankRepository _questionsBankRepository;
    //    private readonly IQuestionCategoryRepository _questionCategoryRepository;

    //    /// <summary>
    //    /// handler get all page
    //    /// </summary>
    //    /// <param name="formRepository"></param>
    //    /// <param name="questionsBankRepository"></param>
    //    /// 
    //    public GetAllPagedAssociatedQuestionQueryHandler(IFormRepository formRepository,
    //        IQuestionsBankRepository questionsBankRepository,
    //        IQuestionCategoryRepository questionCategoryRepository)
    //    {
    //        _formRepository = formRepository;
    //        _questionsBankRepository = questionsBankRepository;
    //        _questionCategoryRepository = questionCategoryRepository;
    //    }

    //    /// <summary>
    //    /// Handler associated special distinction data query handler
    //    /// </summary>
    //    public async Task<PagedResultDto<ListItemAssociatedToFormDto>> Handle(GetAllPagedAssociatedQuery query, CancellationToken cancellationToken)
    //    {
    //        List<ListItemAssociatedToFormDto> listCategory = new();
    //        List<ListItemAssociatedToFormDto> resultListItemFormDto = new();
    //        int total = 0;
    //        var questionnaire = await _formRepository.GetAsync(query.FormId);

    //        if (questionnaire.QuestionCategoryIdArray is not null)
    //        {
    //            var QuestionCategoryIdArray = questionnaire.QuestionCategoryIdArray.Split(',');

    //            for (int i = 0; i < QuestionCategoryIdArray.Count(); i++)
    //            {
    //                var category = (ObjectMapper.Map<ListItemAssociatedToFormDto>(await _formRepository.GetAsync(long.Parse(QuestionCategoryIdArray[i]))));
    //                var questions = ObjectMapper.Map<List<QuestionsSpecialDistintionListItemDto>>(_questionsBankRepository.GetAll().Where(x => x.FormId == long.Parse(QuestionCategoryIdArray[i])).ToList());
    //                category.AssociatedQuestions = 0;
    //                for (int j = 0; j < questions.Count; j++)
    //                {
    //                    category.AssociatedQuestions = j + 1;
    //                }

    //                listCategory.Add(category);
    //            }

    //            var queryableList = listCategory.AsQueryable();
    //            queryableList = queryableList.OrderBy(query.Sorting)
    //           .Skip(query.SkipCount)
    //           .Take(query.MaxResultCount);

    //            total = queryableList.Count();
    //            var resultList = ObjectMapper.Map<List<FormDto>>(queryableList);
    //            resultListItemFormDto = ObjectMapper.Map<List<ListItemAssociatedToFormDto>>(queryableList);

    //            return new PagedResultDto<ListItemAssociatedToFormDto>(
    //                total,
    //                resultListItemFormDto
    //            );
    //        }
    //        else
    //        {
    //            return new PagedResultDto<ListItemAssociatedToFormDto>(
    //                total,
    //                resultListItemFormDto
    //            );
    //        }
    //    }
    //}
}
