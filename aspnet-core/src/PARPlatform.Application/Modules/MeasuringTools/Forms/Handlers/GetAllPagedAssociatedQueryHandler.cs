﻿using Abp.Application.Services.Dto;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using PARPlatform.Modules.MeasuringTools.Forms.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.PAREntities;

namespace PARPlatform.Modules.MeasuringTools.Forms.Handlers
{
    /// <summary>
    /// Get paged associated data query handler
    /// </summary>
    public class GetAllPagedAssociatedQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedAssociatedQuery, PagedResultDto<ListItemAssociatedToFormDto>>
    {
        private readonly IFormRepository _formRepository;
        private readonly IQuestionsBankRepository _questionsBankRepository;
        private readonly IQuestionCategoryRepository _questionCategoryRepository;
        private readonly IRepository<FormQuestion> _formQuestionsRepository;

        public GetAllPagedAssociatedQueryHandler(IFormRepository formRepository,
            IQuestionsBankRepository questionsBankRepository,
            IQuestionCategoryRepository questionCategoryRepository,
             IRepository<FormQuestion> formQuestionsRepository)
        {
            _formRepository = formRepository;
            _questionsBankRepository = questionsBankRepository;
            _questionCategoryRepository = questionCategoryRepository;
            _formQuestionsRepository = formQuestionsRepository;
        }

        /// <summary>
        /// Handler associated data query handler
        /// </summary>
        public async Task<PagedResultDto<ListItemAssociatedToFormDto>> Handle(GetAllPagedAssociatedQuery query, CancellationToken cancellationToken)
        {
            if(query.AssociationType == Constants.ASSOCIATED_SPECIAL_DISTINCTION)
            {
                return await typeSpecialDistinction(query, cancellationToken);
            }
            if(query.AssociationType == Constants.ASSOCIATED_CATEGORY)
            {
                return await typeCategory(query, cancellationToken);
            }

            ListItemAssociatedToFormDto resultListItemFormDto = new();
            return new PagedResultDto<ListItemAssociatedToFormDto>(
                   0,
                   (IReadOnlyList<ListItemAssociatedToFormDto>)resultListItemFormDto
               ) ;
        }

        private async Task<PagedResultDto<ListItemAssociatedToFormDto>> typeSpecialDistinction(GetAllPagedAssociatedQuery query, CancellationToken cancellationToken)
        {
            List<ListItemAssociatedToFormDto> listSpecialDistinction = new();
            List<ListItemAssociatedToFormDto> resultListItemFormDto = new();
            int total = 0;
            var questionnaire = await _formRepository.GetAsync(query.FormId);

            if (questionnaire.SpecialDistinctionIdArray is not null)
            {
                var SpecialDistinctionIdArray = questionnaire.SpecialDistinctionIdArray.Split(',');

                for (int i = 0; i < SpecialDistinctionIdArray.Count(); i++)
                {
                    var specialDistinction = (ObjectMapper.Map<ListItemAssociatedToFormDto>(await _formRepository.GetAsync(long.Parse(SpecialDistinctionIdArray[i]))));
                    var questions = ObjectMapper.Map<List<QuestionsSpecialDistintionListItemDto>>(_questionsBankRepository.GetAll().Where(x => x.FormId == long.Parse(SpecialDistinctionIdArray[i])).ToList());
                    specialDistinction.AssociatedQuestions = 0;
                    for (int j = 0; j < questions.Count; j++)
                    {
                        specialDistinction.AssociatedQuestions = j + 1;
                    }

                    listSpecialDistinction.Add(specialDistinction);
                }

                var queryableList = listSpecialDistinction.AsQueryable();
                queryableList = queryableList.OrderBy(query.Sorting)
               .Skip(query.SkipCount)
               .Take(query.MaxResultCount);

                total = queryableList.Count();
                var resultList = ObjectMapper.Map<List<FormDto>>(queryableList);
                resultListItemFormDto = ObjectMapper.Map<List<ListItemAssociatedToFormDto>>(queryableList);

                return new PagedResultDto<ListItemAssociatedToFormDto>(
                    total,
                    resultListItemFormDto
                );
            }
            else
            {
                return new PagedResultDto<ListItemAssociatedToFormDto>(
                    total,
                    resultListItemFormDto
                );
            }
        }

        private async Task<PagedResultDto<ListItemAssociatedToFormDto>> typeCategory(GetAllPagedAssociatedQuery query, CancellationToken cancellationToken)
        {
            List<ListItemAssociatedToFormDto> listCategory = new();
            List<ListItemAssociatedToFormDto> resultListItemFormDto = new();
            int total = 0;
            var questionnaire = await _formRepository.GetAsync(query.FormId);

            if (questionnaire.QuestionCategoryIdArray is not null)
            {
                var QuestionCategoryIdArray = questionnaire.QuestionCategoryIdArray.Split(',');

                for (int i = 0; i < QuestionCategoryIdArray.Count(); i++)
                {
                    var category = (ObjectMapper.Map<ListItemAssociatedToFormDto>(await _questionCategoryRepository.GetAsync(long.Parse(QuestionCategoryIdArray[i]))));
                    var questions = ObjectMapper.Map<List<QuestionsSpecialDistintionListItemDto>>(_questionsBankRepository.GetAll().Where(x => x.FormId == long.Parse(QuestionCategoryIdArray[i])).ToList());
                    category.AssociatedQuestions = _formQuestionsRepository.GetAll().Where(question => question.QuestionCategoryId == category.Id).Count();
                    listCategory.Add(category);
                }

                var queryableList = listCategory.AsQueryable();
                queryableList = queryableList.OrderBy(query.Sorting)
               .Skip(query.SkipCount)
               .Take(query.MaxResultCount);

                total = queryableList.Count();
                var resultList = ObjectMapper.Map<List<FormDto>>(queryableList);
                resultListItemFormDto = ObjectMapper.Map<List<ListItemAssociatedToFormDto>>(queryableList);

                return new PagedResultDto<ListItemAssociatedToFormDto>(
                    total,
                    resultListItemFormDto
                );
            }
            else
            {
                return new PagedResultDto<ListItemAssociatedToFormDto>(
                    total,
                    resultListItemFormDto
                );
            }
        }
    }
}
