﻿using Abp.Application.Services.Dto;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using PARPlatform.Modules.MeasuringTools.Forms.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.Forms.Handlers
{
    /// <summary>
    /// Get paged Forms data query handler
    /// </summary>
    public class GetAllPagedFormsQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedFormsQuery, PagedResultDto<FormDto>>
    {
        private readonly IFormRepository _formRepository;
        private readonly IFormTypeFormRepository _formTypeFormRepository;
        private readonly IFormTypeRepository _formTypeRepository;
        private readonly IFormThematicFormRepository _formThematicFormRepository;
        private readonly IFormThematicRepository _formThematicRepository;
        private readonly IFormNameRepository _formNameRepository;
        private readonly IQuestionsBankRepository _questionsBankRepository;

        /// <summary>
        /// handler get all page
        /// </summary>
        /// <param name="formRepository"></param>
        /// <param name="formTypeFormRepository"></param>
        /// <param name="formTypeRepository"></param>
        /// <param name="formThematicFormRepository"></param>
        /// <param name="formThematicRepository"></param>
        /// <param name="formNameRepository"></param>
        /// <param name="questionsBankRepository"></param>
        /// 
        public GetAllPagedFormsQueryHandler(IFormRepository formRepository,
            IFormTypeFormRepository formTypeFormRepository,
            IFormTypeRepository formTypeRepository,
            IFormThematicFormRepository formThematicFormRepository,
            IFormThematicRepository formThematicRepository,
            IFormNameRepository formNameRepository,
            IQuestionsBankRepository questionsBankRepository)
        {
            _formRepository = formRepository;
            _formTypeFormRepository = formTypeFormRepository;
            _formTypeRepository = formTypeRepository;
            _formThematicFormRepository = formThematicFormRepository;
            _formThematicRepository = formThematicRepository;
            _formNameRepository = formNameRepository;
            _questionsBankRepository = questionsBankRepository;
        }

        /// <summary>
        /// Use case list forms
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<FormDto>> Handle(GetAllPagedFormsQuery query, CancellationToken cancellationToken)
        {
            string search = query.Filter == null ? "" : query.Filter;
            int total = 0;
            List<FormDto> resultListItemFormDto = new();
            List<FormDto> listFiltered = new();

            var formName = await _formNameRepository.FirstOrDefaultAsync(x => x.Name == query.Type);

            if (formName is not null)
            {
                listFiltered = ObjectMapper.Map<List<FormDto>>(_formRepository.GetAll().Where(x => x.FormNameId == formName.Id).ToList());
                listFiltered = listFiltered.Where(x => x.Title.Contains(search)).ToList();

                for (int i = 0; i < listFiltered.Count; i++)
                {
                    if (listFiltered[i].FormNameId == Constants.SPECIAL_DISTINTION)
                    {
                        var formThematicForm = await _formThematicFormRepository.FirstOrDefaultAsync(x => x.FormId == listFiltered[i].Id);
                        var formThematic = await _formThematicRepository.FirstOrDefaultAsync(x => x.Id == formThematicForm.FormThematicId);
                        listFiltered[i].ThematicName = formThematic.Title;
                        listFiltered[i].AssociatedQuestions = _questionsBankRepository.GetAll().Where(x => x.FormId == listFiltered[i].Id).ToList().Count();
                    }

                    if (listFiltered[i].FormNameId == Constants.QUESTIONNARIES)
                    {
                        var formTypeForm = await _formTypeFormRepository.FirstOrDefaultAsync(x => x.FormId == listFiltered[i].Id);
                        var formType = await _formTypeRepository.FirstOrDefaultAsync(x => x.Id == formTypeForm.FormTypeId);
                        listFiltered[i].TypeName = formType.Title;
                    }
                }

                var associatedQuestions = _questionsBankRepository.GetAll().Where(x => x.Id == 879).ToList();
                var queryableList = listFiltered.AsQueryable();
                queryableList = queryableList.OrderBy(query.Sorting)
               .Skip(query.SkipCount)
               .Take(query.MaxResultCount);

                total = queryableList.Count();
                var resultList = ObjectMapper.Map<List<FormDto>>(queryableList);
                resultListItemFormDto = ObjectMapper.Map<List<FormDto>>(queryableList);
                //resultListItemFormDto.Sort((x, y) => y.LastModificationTime.CompareTo(x.LastModificationTime));

                return new PagedResultDto<FormDto>(
                                total,
                                resultListItemFormDto
                            );
            }
            else
            {
                return new PagedResultDto<FormDto>(
                                               total,
                                               resultListItemFormDto
                                           );
            }
        }
    }
}
