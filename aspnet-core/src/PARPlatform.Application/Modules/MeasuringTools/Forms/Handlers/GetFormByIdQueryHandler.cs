﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using PARPlatform.Modules.MeasuringTools.Forms.Queries;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.Forms.Handlers
{
    /// <summary>
    /// Get Form by id handler
    /// </summary>
    public class GetFormByIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetFormByIdQuery, UpsertFormDto>
    {
        private readonly IFormRepository _formRepository;
        private readonly IFormThematicFormRepository _formThematicFormRepository;
        private readonly IFormTypeFormRepository _formTypeFormRepository;
        private readonly IFormThematicRepository _formThematicRepository;
        private readonly IFormTypeRepository _formTypeRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="formRepository"></param>
        public GetFormByIdQueryHandler(IFormRepository formRepository,
            IFormThematicFormRepository formThematicFormRepository,
            IFormTypeFormRepository formTypeFormRepository,
            IFormThematicRepository formThematicRepository,
            IFormTypeRepository formTypeRepository)
        {
            _formRepository = formRepository;
            _formThematicFormRepository = formThematicFormRepository;
            _formTypeFormRepository = formTypeFormRepository;
            _formThematicRepository = formThematicRepository;
            _formTypeRepository = formTypeRepository;
        }

        /// <summary>
        /// Use case list forms
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertFormDto> Handle(GetFormByIdQuery query, CancellationToken cancellationToken)
        {
            var form = await _formRepository.GetAsync(query.Id);
            var formThematicForm = await _formThematicFormRepository.FirstOrDefaultAsync(x => x.FormId == query.Id);
            var formTypeForm = await _formTypeFormRepository.FirstOrDefaultAsync(x => x.FormId == query.Id);

            var formDto = ObjectMapper.Map<UpsertFormDto>(form);

            if (formThematicForm is not null)
            {
                var formThematic = await _formThematicRepository.FirstOrDefaultAsync(x => x.Id == formThematicForm.FormThematicId);
                formDto.ThematicName = formThematic.Title;
                formDto.ThematicId = formThematic.Id;

            }
            if (formTypeForm is not null)
            {
                var formType = await _formTypeRepository.FirstOrDefaultAsync(x => x.Id == formTypeForm.FormTypeId);
                formDto.TypeName = formType.Title;
                formDto.TypeId = formType.Id;
            }
            return formDto;
        }
    }
}
