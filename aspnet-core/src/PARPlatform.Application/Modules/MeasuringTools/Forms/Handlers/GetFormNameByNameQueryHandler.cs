﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using PARPlatform.Modules.MeasuringTools.Forms.Queries;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.Forms.Handlers
{
    /// <summary>
    /// Get Form by id handler
    /// </summary>
    public class GetFormNameByNameQueryHandler : UseCaseServiceBase, IRequestHandler<GetFormNameByNameQuery, FormNameDto>
    {
        private readonly IFormNameRepository _formNameRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="formNameRepository"></param>
        public GetFormNameByNameQueryHandler(IFormNameRepository formNameRepository)
        {
            _formNameRepository = formNameRepository;
        }

        /// <summary>
        /// Use case get formName by Name
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<FormNameDto> Handle(GetFormNameByNameQuery query, CancellationToken cancellationToken)
        {
            var formName = await _formNameRepository.FirstOrDefaultAsync(x => x.Name == query.Name);
            var formNameDto = ObjectMapper.Map<FormNameDto>(formName);

            return formNameDto;
        }
    }
}
