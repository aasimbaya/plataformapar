﻿using Abp.Localization;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.Forms.Commands;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands.Validators;
using PARPlatform.Support;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.Forms.Handlers
{
    /// <summary>
    /// Update Form command handler
    /// </summary>
    public class UpdateFormCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateFormCommand, UpsertFormDto>
    {
        private readonly IFormRepository _formRepository;
        private readonly IFormTypeFormRepository _formTypeFormRepository;
        private readonly IFormThematicFormRepository _formThematicFormRepository;
        private readonly IFormTypeRepository _formTypeRepository;
        private readonly IFormThematicRepository _formThematicRepository;
        private readonly ILocalizationManager _localizationManager;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="formRepository"></param>
        /// <param name="formTypeFormRepository"></param>
        /// <param name="formThematicFormRepository"></param>
        public UpdateFormCommandHandler(IFormRepository formRepository,
                                        IFormTypeFormRepository formTypeFormRepository,
                                        IFormThematicFormRepository formThematicFormRepository,
                                        IFormTypeRepository formTypeRepository,
                                        IFormThematicRepository formThematicRepository,
                                        ILocalizationManager localizationManager)
        {
            _formRepository = formRepository;
            _formTypeFormRepository = formTypeFormRepository;
            _formThematicFormRepository = formThematicFormRepository;
            _formTypeRepository = formTypeRepository;
            _formThematicRepository = formThematicRepository;
            _localizationManager = localizationManager;
        }

        /// <summary>
        /// Update form use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertFormDto> Handle(UpdateFormCommand command, CancellationToken cancellationToken)
        {
            new UpdateFormCommandValidator(_localizationManager).Validate(command).ThrowIfAreErrors();

            var form = await _formRepository.GetAsync(command.Id);
            ObjectMapper.Map(command, form);

            await _formRepository.UpdateAsync(form);

            var formTypeForm = await _formTypeFormRepository.FirstOrDefaultAsync(x => x.FormId == form.Id);
            if (formTypeForm is not null)
            {
                var formType = await _formTypeRepository.FirstOrDefaultAsync(x => x.Id == formTypeForm.Id);
                formTypeForm.FormTypeId = command.TypeId;
                await _formTypeFormRepository.UpdateAsync(formTypeForm);
            }

            var formThematicForm = await _formThematicFormRepository.FirstOrDefaultAsync(x => x.FormId == form.Id);
            if (formThematicForm is not null)
            {
                formThematicForm.FormThematicId = command.ThematicId;
                await _formThematicFormRepository.UpdateAsync(formThematicForm);
            }
            return ObjectMapper.Map<UpsertFormDto>(form);
        }
    }
}
