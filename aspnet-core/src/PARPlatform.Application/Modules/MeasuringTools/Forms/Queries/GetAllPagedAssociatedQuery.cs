﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.Forms.Queries
{
    public class GetAllPagedAssociatedQuery : PagedAndSortedInputDto, IShouldNormalize, IRequest<PagedResultDto<ListItemAssociatedToFormDto>>
    {
        public string Filter { get; set; }
        public long FormId { get; set; }
        public int AssociationType { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Title";
            }
            if (string.IsNullOrEmpty(Filter))
            {
                Filter = "";
            }
        }
    }
}
