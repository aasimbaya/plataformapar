﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;

namespace PARPlatform.Modules.MeasuringTools.Forms.Queries
{
    /// <summary>
    /// Get all paged forms query
    /// </summary>
    public class GetAllPagedFormsQuery : PagedAndSortedInputDto, IShouldNormalize, IRequest<PagedResultDto<FormDto>>
    {

        public string Filter { get; set; }
        public string Type { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Title";
            }
            if (string.IsNullOrEmpty(Filter))
            {
                Filter = "";
            }
        }
    }
}
