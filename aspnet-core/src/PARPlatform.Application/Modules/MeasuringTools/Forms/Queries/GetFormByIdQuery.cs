﻿using MediatR;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;

namespace PARPlatform.Modules.MeasuringTools.Forms.Queries
{
    /// <summary>
    /// Get forms by id query
    /// </summary>
    public class GetFormByIdQuery : IRequest<UpsertFormDto>
    {
        public long Id { get; set; }

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="id"></param>
        public GetFormByIdQuery(long id)
        {
            Id = id;
        }

    }
}
