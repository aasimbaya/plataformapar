﻿using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MediatR;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.Forms.Queries
{
    public class GetFormNameByNameQuery : IRequest<FormNameDto>
    {
        public string Name { get; set; }

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="name"></param>
        public GetFormNameByNameQuery(string name)
        {
            Name = name;
        }

    }
}
