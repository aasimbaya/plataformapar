﻿using MediatR;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands
{
    /// <summary>
    /// Create Answers for questions bank Command
    /// </summary>
    public class CreateAnswersCommand : IRequest
    {
        public long FormQuestionId { get; set; }
        public List<UpsertAnswerDto> Answers { get; set; }
    }
}
