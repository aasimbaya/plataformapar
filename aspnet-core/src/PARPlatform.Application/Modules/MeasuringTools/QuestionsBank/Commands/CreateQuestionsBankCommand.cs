﻿using MediatR;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System.Collections.Generic;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands
{

    /// <summary>
    /// Create Questions Bank Command
    /// </summary>
    public class CreateQuestionsBankCommand : IRequest<UpsertQuestionsBankDto>
    {
        public string ShortVersion { get; set; }
        public long FormTypeId { get; set; }
        public long FormThematicId { get; set; }
        public long QuestionTypeId { get; set; }
        public long QuestionCategoryId { get; set; }
        public string Question { get; set; }
        public string Tooltip { get; set; }
        public string FixedText { get; set; }
        public string Tag { get; set; }
        public bool IsActive { get; set; }
        public long UsageId { get; set; }
        public string ImplementationTip { get; set; }
        public int? TypeScore { get; set; }
        public decimal? Score { get; set; }
        public decimal? TotalScore { get; set; }
        public bool IsNullable { get; set; }
        public bool IsCertified { get; set; }
        public bool IsPriority { get; set; }
        public List<long> ConditionalParentIds { get; set; }
        public List<UpsertAnswerDto> Answers { get; set; }
        public List<UpsertFormQuestionLevel> FormQuestionLevelsForSave { get; set; }
    }
}
