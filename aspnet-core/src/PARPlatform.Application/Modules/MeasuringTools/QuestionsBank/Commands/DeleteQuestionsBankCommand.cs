﻿using MediatR;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands
{
    /// <summary>
    /// Delete Questions Bank Command
    /// </summary>
    public class DeleteQuestionsBankCommand : IRequest
    {
        public long Id { get; set; }
    }
}
