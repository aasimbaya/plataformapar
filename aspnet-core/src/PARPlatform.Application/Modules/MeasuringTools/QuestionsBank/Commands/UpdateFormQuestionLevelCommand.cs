﻿using MediatR;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands
{
    /// <summary>
    /// Update Form Question Level Command
    /// </summary>
    public class UpdateFormQuestionLevelCommand : IRequest
    {
        public long FormQuestionId { get; set; }
        public List<UpsertFormQuestionLevel> FormQuestionLevelsForSave { get; set; }
    }
}
