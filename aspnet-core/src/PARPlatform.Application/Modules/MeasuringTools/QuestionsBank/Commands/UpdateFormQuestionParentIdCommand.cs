﻿using MediatR;
using System.Collections.Generic;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands
{
    /// <summary>
    /// Set FormQuestion parentId Command
    /// </summary>
    public class UpdateFormQuestionParentIdCommand : IRequest
    {
        public long FormQuestionId { get; set; }
        public List<long> FormQuestions { get; set; }
    }
}
