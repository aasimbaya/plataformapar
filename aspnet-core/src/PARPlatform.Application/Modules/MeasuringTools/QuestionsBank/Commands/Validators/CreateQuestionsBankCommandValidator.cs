﻿using Abp.Localization;
using Abp.Localization.Sources;
using FluentValidation;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands.Validators
{
    /// <summary>
    /// Create Questions Bank command input parameters validator
    /// </summary>
    public class CreateQuestionsBankCommandValidator : AbstractValidator<CreateQuestionsBankCommand>
    {
        private readonly ILocalizationSource _localizationSource;

        /// <summary>
        /// Main constructor
        /// </summary>
        public CreateQuestionsBankCommandValidator(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(PARPlatformConsts.LocalizationSourceName);

            RuleFor(x => x.Question)
                .MaximumLength(150)
                .NotNull().WithMessage(_localizationSource.GetString("QuestionsBank_QuestionRequired"))
                .NotEmpty().WithMessage(_localizationSource.GetString("QuestionsBank_QuestionRequired"));

            RuleFor(x => x.ShortVersion)
                .MaximumLength(150)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.ImplementationTip)
                .MaximumLength(150)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.QuestionTypeId)
                .NotNull()
                .NotEmpty()
                .NotEqual(0);

            RuleFor(x => x.QuestionCategoryId)
                .NotNull()
                .NotEmpty()
                .NotEqual(0);

            RuleFor(x => x.FormTypeId)
                .NotNull()
                .NotEmpty()
                .NotEqual(0);

            RuleFor(x => x.FormThematicId)
                .NotNull()
                .NotEmpty()
                .NotEqual(0);

            RuleFor(x => x.UsageId)
                .NotNull()
                .NotEmpty()
                .NotEqual(0);

            RuleFor(x => x.Answers)
                .NotEmpty().WithMessage(_localizationSource.GetString("QuestionsBank_AnswersRequired")).When(x => x.QuestionTypeId != Constants.TYPE_STRUCTURE);

            RuleForEach(x => x.Answers)
                .SetValidator(new UpsertAnswerValidator())
                .When(x => x.QuestionTypeId != Constants.TYPE_STRUCTURE);

            RuleFor(x => x.FormQuestionLevelsForSave)
                .NotEmpty().WithMessage(_localizationSource.GetString("QuestionsBank_LevelsRequired")).When(x => x.QuestionTypeId == Constants.TYPE_STRUCTURE);

            RuleForEach(x => x.FormQuestionLevelsForSave)
                .SetValidator(new UpsertFormQuestionLevelValidator(localizationManager))
                .When(x => x.QuestionTypeId == Constants.TYPE_STRUCTURE);
        }
    }
}
