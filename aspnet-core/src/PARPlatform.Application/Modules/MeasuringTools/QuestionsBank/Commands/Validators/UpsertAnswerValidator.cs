﻿using FluentValidation;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands.Validators
{
    /// <summary>
    /// Validator for Upsert Answer
    /// </summary>
    public class UpsertAnswerValidator : AbstractValidator<UpsertAnswerDto>
    {
        public UpsertAnswerValidator()
        {
            RuleFor(x => x.AnswerContent)
                .MaximumLength(150);

            RuleFor(x => x.Score)
                .NotNull()
                .NotEmpty();
        }
    }
}
