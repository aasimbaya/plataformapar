﻿using Abp.Localization;
using Abp.Localization.Sources;
using FluentValidation;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands.Validators
{
    /// <summary>
    /// Validator for Upsert FormQuestion level
    /// </summary>
    public class UpsertFormQuestionLevelValidator : AbstractValidator<UpsertFormQuestionLevel>
    {
        private readonly ILocalizationSource _localizationSource;

        /// <summary>
        /// Main cosntructor
        /// </summary>
        public UpsertFormQuestionLevelValidator(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(PARPlatformConsts.LocalizationSourceName);

            RuleFor(x => x.Title)
                .NotNull()
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.Description)
                .NotNull()
                .NotEmpty()
                .MaximumLength(150);

            RuleFor(x => x.ChildrenQuestion)
                .NotNull().WithMessage(_localizationSource.GetString("QuestionsBank_LevelWithQuestionRequired"))
                .NotEmpty().WithMessage(_localizationSource.GetString("QuestionsBank_LevelWithQuestionRequired"));
        }
    }
}
