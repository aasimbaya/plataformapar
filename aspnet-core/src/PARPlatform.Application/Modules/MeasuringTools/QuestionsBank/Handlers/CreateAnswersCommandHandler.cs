﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle Create Answers for Questions Bank Command
    /// </summary>
    public class CreateAnswersCommandHandler : UseCaseServiceBase, IRequestHandler<CreateAnswersCommand>
    {
        private readonly IAnswerRepository _answerRepository;

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="answerRepository"></param>
        public CreateAnswersCommandHandler(IAnswerRepository answerRepository)
        {
            _answerRepository = answerRepository;
        }

        /// <summary>
        /// Create new Question answers use case 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(CreateAnswersCommand input, CancellationToken cancellationToken)
        {
            int index = 1;
            foreach (var item in input.Answers)
            {
                var answerData = ObjectMapper.Map<Answer>(item);

                answerData.FormQuestionId = input.FormQuestionId;
                answerData.IsActive = true;
                answerData.PersonalId = QuestionsBankHelper.CreateNewAnswerPersonalId(index);
                index++;

                await _answerRepository.InsertAsync(answerData);
            }

            return Unit.Value;
        }
    }
}
