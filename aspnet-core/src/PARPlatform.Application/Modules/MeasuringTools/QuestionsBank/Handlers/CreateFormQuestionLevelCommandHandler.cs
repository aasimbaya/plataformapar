﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle Create FormQuestion Levels for questions bank Command
    /// </summary>
    public class CreateFormQuestionLevelCommandHandler : UseCaseServiceBase, IRequestHandler<CreateFormQuestionLevelCommand>
    {
        private readonly IFormQuestionLevelRepository _formQuestionLevelRepository;
        private readonly IMediator _mediator;

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="formQuestionLevelRepository"></param>
        public CreateFormQuestionLevelCommandHandler(
            IFormQuestionLevelRepository formQuestionLevelRepository,
            IMediator mediator)
        {
            _formQuestionLevelRepository = formQuestionLevelRepository;
            _mediator = mediator;
        }

        /// <summary>
        /// Create new Question Levels use case 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<Unit> Handle(CreateFormQuestionLevelCommand input, CancellationToken cancellationToken)
        {
            List<long> questionIds = new List<long>();

            foreach (var item in input.FormQuestionLevelsForSave)
            {
                questionIds.AddRange(item.ChildrenQuestion);

                var levelData = ObjectMapper.Map<FormQuestionLevel>(item);
                levelData.FormQuestionId = input.FormQuestionId;
                levelData.IsActive = true;
                await _formQuestionLevelRepository.InsertAsync(levelData);
            }

            var setParents = new UpdateFormQuestionParentIdCommand();
            setParents.FormQuestions = questionIds;
            setParents.FormQuestionId = input.FormQuestionId;
            await _mediator.Send(setParents);

            return Unit.Value;
        }
    }
}
