﻿using Abp.Localization;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands.Validators;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.PAREntities;
using PARPlatform.Support;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle Create Questions Bank Command
    /// </summary>
    public class CreateQuestionsBankCommandHandler : UseCaseServiceBase, IRequestHandler<CreateQuestionsBankCommand, UpsertQuestionsBankDto>
    {
        private readonly IQuestionsBankRepository _questionsBankRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IMediator _mediator;

        /// <summary>
        /// Main Constructor
        /// </summary>
        public CreateQuestionsBankCommandHandler(
            IQuestionsBankRepository questionsBankRepository,
            ILocalizationManager localizationManager,
            IMediator mediator)
        {
            _questionsBankRepository = questionsBankRepository;
            _localizationManager = localizationManager;
            _mediator = mediator;
        }

        /// <summary>
        /// Create new Question of Questions Bank use case 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertQuestionsBankDto> Handle(CreateQuestionsBankCommand input, CancellationToken cancellationToken)
        {
            new CreateQuestionsBankCommandValidator(_localizationManager).Validate(input).ThrowIfAreErrors();

            FormQuestion data = ObjectMapper.Map<FormQuestion>(input);

            data.Id = await _questionsBankRepository.InsertAndGetIdAsync(data);

            if (string.IsNullOrEmpty(input.Tag))
            {
                data.Tag = QuestionsBankHelper.CreateQuestionTag(data.Id);
                await _questionsBankRepository.UpdateAsync(data);
            }

            if (input.QuestionTypeId == Constants.TYPE_STRUCTURE)
            {
                var createLevels = new CreateFormQuestionLevelCommand();
                createLevels.FormQuestionLevelsForSave = input.FormQuestionLevelsForSave;
                createLevels.FormQuestionId = data.Id;
                await _mediator.Send(createLevels);
            }
            else
            {
                var createAnswers = new CreateAnswersCommand();
                createAnswers.Answers = input.Answers;
                createAnswers.FormQuestionId = data.Id;
                await _mediator.Send(createAnswers);
            }

            return ObjectMapper.Map<UpsertQuestionsBankDto>(data);
        }
    }
}
