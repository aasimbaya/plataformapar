﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle Delete Questions Bank Command
    /// </summary>
    public class DeleteQuestionsBankCommandHandler : UseCaseServiceBase, IRequestHandler<DeleteQuestionsBankCommand>
    {
        private readonly IQuestionsBankRepository _questionsBankRepository;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="questionsBankRepository"></param>
        public DeleteQuestionsBankCommandHandler(IQuestionsBankRepository questionsBankRepository)
        {
            _questionsBankRepository = questionsBankRepository;
        }

        /// <summary>
        /// Delete question from Questions Bank use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteQuestionsBankCommand command, CancellationToken cancellationToken)
        {
            FormQuestion question = await _questionsBankRepository.FirstOrDefaultAsync(x => x.Id == command.Id);
            question.IsDeleted = true;
            await _questionsBankRepository.UpdateAsync(question);
            return Unit.Value;
        }
    }
}
