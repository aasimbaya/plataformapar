﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Get All Levels of structure Question By Question Id Query Handler
    /// </summary>
    public class GetAllDocumentEstructureQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllLevelsByQuestionIdQuery, List<FormQuestionLevelListItemDto>>
    {
        private readonly IQuestionsBankRepository _questionsBankRepository;
        private readonly IFormQuestionLevelRepository _formQuestionLevelRepository;

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="questionsBankRepository"></param>
        public GetAllDocumentEstructureQueryHandler(
            IQuestionsBankRepository questionsBankRepository,
            IFormQuestionLevelRepository formQuestionLevelRepository)
        {
            _questionsBankRepository = questionsBankRepository;
            _formQuestionLevelRepository = formQuestionLevelRepository;
        }

        public async Task<List<FormQuestionLevelListItemDto>> Handle(GetAllLevelsByQuestionIdQuery query, CancellationToken cancellationToken)
        {
            List<FormQuestionLevel> formQuestionLevels = await _formQuestionLevelRepository.GetAll()
                .Where(e => e.FormQuestionId == query.FormQuestionId && e.IsActive && !e.IsDeleted)
                .ToListAsync();

            List<FormQuestionLevelListItemDto> levelsDto = new List<FormQuestionLevelListItemDto>();

            foreach (FormQuestionLevel item in formQuestionLevels)
            {
                List<FormQuestion> questions = await _questionsBankRepository.GetAllListAsync(e => item.ChildrenQuestion.Contains(e.Id) && e.IsActive && !e.IsDeleted);
                FormQuestionLevelListItemDto dto = ObjectMapper.Map<FormQuestionLevelListItemDto>(item);
                dto.ChildrenQuestionData = ObjectMapper.Map(questions, new List<LevelQuestionDataDto>());

                levelsDto.Add(dto);
            }

            return levelsDto;
        }
    }
}
