﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using Abp.ObjectMapping;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle query paged Question Bank
    /// </summary>
    public class GetAllPagedQuestionsBankQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedQuestionsBankQuery, PagedResultDto<QuestionsBankListItemDto>>
    {
        private readonly IQuestionsBankRepository _questionsBankRepository;
        private readonly IFormTypeRepository _formTypeRepository;
        private readonly IFormThematicRepository _formThematicRepository;
        private readonly IQuestionTypeRepository _questionTypeRepository;
        private readonly IQuestionCategoryRepository _questionCategoryRepository;
        private readonly IUsageRepository _usageRepository;
        private readonly IFormRepository _formRepository;
        private readonly IObjectMapper _objectMapper;

        /// <summary>
        /// Main Constructor
        /// </summary>
        public GetAllPagedQuestionsBankQueryHandler(
            IQuestionsBankRepository questionsBankRepository,
            IFormTypeRepository formTypeRepository,
            IFormThematicRepository formThematicRepository,
            IQuestionTypeRepository questionTypeRepository,
            IQuestionCategoryRepository questionCategoryRepository,
            IUsageRepository usageRepository,
            IFormRepository formRepository,
            IObjectMapper objectMapper)
        {
            _questionsBankRepository = questionsBankRepository;
            _formTypeRepository = formTypeRepository;
            _formThematicRepository = formThematicRepository;
            _questionTypeRepository = questionTypeRepository;
            _questionCategoryRepository = questionCategoryRepository;
            _usageRepository = usageRepository;
            _formRepository = formRepository;
            _objectMapper = objectMapper;
        }

        /// <summary>
        /// Get paged list of Question Bank
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<QuestionsBankListItemDto>> Handle(GetAllPagedQuestionsBankQuery query, CancellationToken cancellationToken)
        {
            var dbQuery = from formQuestion in _questionsBankRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(query.Filter), e => e.ShortVersion.Contains(query.Filter))
                            .WhereIf(query.Use != 0, e => e.UsageId == query.Use)
                            .WhereIf((query.IsForStructureQuestion.HasValue && query.IsForStructureQuestion.Value), e => !e.ParentId.HasValue && !e.FormId.HasValue)
                            .Where(e => e.IsDeleted == false)
                          join questionCategory in _questionCategoryRepository.GetAll() on formQuestion.QuestionCategoryId equals questionCategory.Id into formQuestionCategories
                          from formQuestionCategory in formQuestionCategories.DefaultIfEmpty()
                          join formType in _formTypeRepository.GetAll() on formQuestion.FormTypeId equals formType.Id into formQuestionFormTypes
                          from formQuestionFormType in formQuestionFormTypes.DefaultIfEmpty()
                          join formThematic in _formThematicRepository.GetAll() on formQuestion.FormThematicId equals formThematic.Id into formQuestionFormThematics
                          from formQuestionFormThematic in formQuestionFormThematics.DefaultIfEmpty()
                          join questionType in _questionTypeRepository.GetAll() on formQuestion.QuestionTypeId equals questionType.Id into formQuestionTypes
                          from formQuestionType in formQuestionTypes.DefaultIfEmpty()
                          join usage in _usageRepository.GetAll() on formQuestion.UsageId equals usage.Id into formQuestionUsages
                          from formQuestionUsage in formQuestionUsages.DefaultIfEmpty()
                          select new QuestionsBankListItemDto
                          {
                              Id = formQuestion.Id,
                              Question = formQuestion.ShortVersion,
                              QuestionnaireType = formQuestionFormType.Title,
                              Use = formQuestionUsage.Name,
                              Category = (formQuestion.UsageId == Constants.USE_CATEGORY ? formQuestionCategory.Name : Constants.NO_CATEGORY_DASH),
                              TypeQuestion = formQuestionType.Name,
                              Theme = formQuestionFormThematic.Title,
                              LastModificationTime = formQuestion.LastModificationTime.HasValue ? formQuestion.LastModificationTime.Value : formQuestion.CreationTime
                          };

            var resultCount = await dbQuery.CountAsync();
            List<QuestionsBankListItemDto> results = await dbQuery
                .OrderBy(query.Sorting)
                .Skip(query.SkipCount)
                .Take(query.MaxResultCount)
                .ToListAsync();

            return new PagedResultDto<QuestionsBankListItemDto>(
                resultCount,
                results
            );
        }
    }
}
