﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using Abp.ObjectMapping;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle query paged Question Bank
    /// </summary>
    public class GetAllPagedQuestionsSpecialDistintionQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedQuestionsSpecialDistintionQuery, PagedResultDto<QuestionsSpecialDistintionListItemDto>>
    {
        private readonly IQuestionsBankRepository _questionsBankRepository;

        /// <summary>
        /// Main Constructor
        /// </summary>
        public GetAllPagedQuestionsSpecialDistintionQueryHandler(
            IQuestionsBankRepository questionsBankRepository)
        {
            _questionsBankRepository = questionsBankRepository;
        }

        public async Task<PagedResultDto<QuestionsSpecialDistintionListItemDto>> Handle(GetAllPagedQuestionsSpecialDistintionQuery query, CancellationToken cancellationToken)
        {
            var questions =  ObjectMapper.Map<List<QuestionsSpecialDistintionListItemDto>>(_questionsBankRepository.GetAll().Where(x => x.FormId == query.FormId).ToList());

            for (int i = 0; i < questions.Count; i++)
            {
                questions[i].Order = i + 1;
            }

            var queryableList = questions.AsQueryable();
            queryableList = queryableList.OrderBy(query.Sorting)
           .Skip(query.SkipCount)
           .Take(query.MaxResultCount);

            int total = queryableList.Count();
            var resultList = ObjectMapper.Map<List<QuestionsSpecialDistintionListItemDto>>(queryableList);
            List<QuestionsSpecialDistintionListItemDto> resultListItemFormDto = ObjectMapper.Map<List<QuestionsSpecialDistintionListItemDto>>(queryableList);

            return new PagedResultDto<QuestionsSpecialDistintionListItemDto>(
                total,
                resultListItemFormDto
            );
        }
    }
}