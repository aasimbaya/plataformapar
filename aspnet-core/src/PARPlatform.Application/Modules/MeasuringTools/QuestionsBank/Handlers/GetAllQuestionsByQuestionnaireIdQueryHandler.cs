﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    public class GetAllQuestionsByQuestionnaireIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllQuestionsByQuestionnaireIdQuery, List<QuestionnaireQuestionDetailDto>>
    {
        private readonly IFormRepository _formRepository;
        private readonly IQuestionsBankRepository _questionsBankRepository;
        private readonly IMediator _mediator;

        /// <summary>
        /// Main constructor
        /// </summary>
        public GetAllQuestionsByQuestionnaireIdQueryHandler(
            IFormRepository formRepository,
            IQuestionsBankRepository questionsBankRepository,
            IMediator mediator)
        {
            _formRepository = formRepository;
            _questionsBankRepository = questionsBankRepository;
            _mediator = mediator;
        }

        /// <summary>
        /// Handle the request of detailed Question data by questionnaire Id
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<QuestionnaireQuestionDetailDto>> Handle(GetAllQuestionsByQuestionnaireIdQuery query, CancellationToken cancellationToken)
        {
            List<QuestionnaireQuestionDetailDto> questionsData = new List<QuestionnaireQuestionDetailDto>();
            List<long> categories = new List<long>();
            List<long> specialDistinctions = new List<long>();

            PAREntities.Form questionnaire = await _formRepository.GetAsync(query.QuestionnaireId);

            if (!string.IsNullOrEmpty(questionnaire.QuestionCategoryIdArray))
            {
                categories = questionnaire.QuestionCategoryIdArray.Split(',').ToList().Select(item => Convert.ToInt64(item)).ToList();
            }

            if (!string.IsNullOrEmpty(questionnaire.SpecialDistinctionIdArray))
            {
                specialDistinctions = questionnaire.SpecialDistinctionIdArray.Split(',').ToList().Select(item => Convert.ToInt64(item)).ToList();
            }

            List<FormQuestion> questionnaireQuestions = await _questionsBankRepository.GetAll()
                .Where(e => e.IsActive && !e.IsDeleted && ((e.QuestionCategoryId.HasValue && categories.Contains(e.QuestionCategoryId.Value)) || (e.FormId.HasValue && specialDistinctions.Contains(e.FormId.Value))))
                .ToListAsync();

            foreach (FormQuestion question in questionnaireQuestions)
            {
                var questionData = await _mediator.Send(new GetQuestionsBankByIdQuery(question.Id));
                questionsData.Add(ObjectMapper.Map<QuestionnaireQuestionDetailDto>(questionData));
            }

            List<QuestionnaireQuestionDetailDto> structureQuestionsData = questionsData.Where(e => e.QuestionTypeId == Constants.TYPE_STRUCTURE).ToList();

            foreach (QuestionnaireQuestionDetailDto item in structureQuestionsData)
            {
                var structureData = await _mediator.Send(new GetLevelsQuestionDetaislByQuestionIdQuery(item.Id));
                item.FormQuestionLevelData = structureData;
            }

            return questionsData;
        }
    }
}
