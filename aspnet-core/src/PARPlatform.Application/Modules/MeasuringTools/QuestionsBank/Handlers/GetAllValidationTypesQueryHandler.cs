﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle query Validation Type
    /// </summary>
    public class GetAllValidationTypesQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllValidationTypesQuery, List<DropdownItemDto>>
    {
        private readonly IValidationTypeRepository _validationTypeRepository;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="formTypeRepository"></param>
        public GetAllValidationTypesQueryHandler(IValidationTypeRepository validationTypeRepository)
        {
            _validationTypeRepository = validationTypeRepository;
        }

        /// <summary>
        /// Get all list of validation types
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<DropdownItemDto>> Handle(GetAllValidationTypesQuery request, CancellationToken cancellationToken)
        {
            List<ValidationType> validationTypes = await _validationTypeRepository.GetAllListAsync(e => !e.IsDeleted);

            return ObjectMapper.Map(validationTypes, new List<DropdownItemDto>());
        }
    }
}
