﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries;
using PARPlatform.Modules.Shared.Catalogue.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Get All Levels with questions detail of structure Question By Question Id Query Handler
    /// </summary>
    public class GetLevelsQuestionDetaislByQuestionIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetLevelsQuestionDetaislByQuestionIdQuery, List<FormQuestionLevelWithQuestionDataDto>>
    {
        private readonly IQuestionsBankRepository _questionsBankRepository;
        private readonly IFormQuestionLevelRepository _formQuestionLevelRepository;

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="questionsBankRepository"></param>
        public GetLevelsQuestionDetaislByQuestionIdQueryHandler(
            IQuestionsBankRepository questionsBankRepository,
            IFormQuestionLevelRepository formQuestionLevelRepository)
        {
            _questionsBankRepository = questionsBankRepository;
            _formQuestionLevelRepository = formQuestionLevelRepository;
        }

        /// <summary>
        /// Handles request of question levels with detailed children questions data from questions bank
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<FormQuestionLevelWithQuestionDataDto>> Handle(GetLevelsQuestionDetaislByQuestionIdQuery query, CancellationToken cancellationToken)
        {
            List<FormQuestionLevel> formQuestionLevels = await _formQuestionLevelRepository.GetAll()
                .Where(e => e.FormQuestionId == query.FormQuestionId && e.IsActive && !e.IsDeleted)
                .ToListAsync();

            List<FormQuestionLevelWithQuestionDataDto> levelsDto = new List<FormQuestionLevelWithQuestionDataDto>();

            foreach (FormQuestionLevel item in formQuestionLevels)
            {
                List<FormQuestion> questions = await _questionsBankRepository.GetAllListAsync(e => item.ChildrenQuestion.Contains(e.Id) && e.IsActive && !e.IsDeleted);
                FormQuestionLevelWithQuestionDataDto dto = ObjectMapper.Map<FormQuestionLevelWithQuestionDataDto>(item);
                dto.ChildrenQuestionData = ObjectMapper.Map(questions, new List<QuestionnaireQuestionDetailDto>());

                levelsDto.Add(dto);
            }

            return levelsDto;
        }
    }
}
