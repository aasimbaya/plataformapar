﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle Query of Get Questions Bank By Id
    /// </summary>
    public class GetQuestionsBankByIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetQuestionsBankByIdQuery, UpsertQuestionsBankDto>
    {
        private readonly IQuestionsBankRepository _questionsBankRepository;
        private readonly IAnswerRepository _answerRepository;
        private readonly IMediator _mediator;

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="questionCategoryRepository"></param>
        public GetQuestionsBankByIdQueryHandler(
            IQuestionsBankRepository questionsBankRepository,
            IAnswerRepository answerRepository,
            IMediator mediator)
        {
            _questionsBankRepository = questionsBankRepository;
            _answerRepository = answerRepository;
            _mediator = mediator;
        }

        /// <summary>
        /// Handles request of question details data from questions bank
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertQuestionsBankDto> Handle(GetQuestionsBankByIdQuery query, CancellationToken cancellationToken)
        {
            FormQuestion question = await _questionsBankRepository.GetAsync(query.Id);
            UpsertQuestionsBankDto formQuestionsDto = ObjectMapper.Map<UpsertQuestionsBankDto>(question);

            List<Answer> answers = await _answerRepository.GetAllListAsync(answer => answer.FormQuestionId == query.Id && answer.IsActive && !answer.IsDeleted);
            formQuestionsDto.Answers = ObjectMapper.Map<List<UpsertAnswerDto>>(answers);

            var queryLevels = new GetAllLevelsByQuestionIdQuery(query.Id);
            formQuestionsDto.LoadFormQuestionLevelData = await _mediator.Send(queryLevels);

            await SetAuditUsers(question, formQuestionsDto);
            return formQuestionsDto;
        }
    }
}
