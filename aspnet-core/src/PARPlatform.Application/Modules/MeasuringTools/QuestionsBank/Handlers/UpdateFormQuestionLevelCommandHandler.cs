﻿using Abp.Domain.Uow;
using Abp.Localization;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands.Validators;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.PAREntities;
using PARPlatform.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle Create or Update Questions Bank Levels Command
    /// </summary>
    public class UpdateFormQuestionLevelCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateFormQuestionLevelCommand>
    {
        private readonly IFormQuestionLevelRepository _formQuestionLevelRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IMediator _mediator;

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="formQuestionLevelRepository"></param>
        public UpdateFormQuestionLevelCommandHandler(
            IFormQuestionLevelRepository formQuestionLevelRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IMediator mediator)
        {
            _formQuestionLevelRepository = formQuestionLevelRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _mediator = mediator;
        }

        /// <summary>
        /// Create or Update FormQuestions levels for questions bank use case
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<Unit> Handle(UpdateFormQuestionLevelCommand command, CancellationToken cancellationToken)
        {
            using var uow = UnitOfWorkManager.Begin();
            try
            {
                List<FormQuestionLevel> currentLevels = await _formQuestionLevelRepository.GetAllListAsync(a => a.FormQuestionId == command.FormQuestionId);
                List<long> addQuestionIds = new List<long>();
                int index = 0;

                if (currentLevels.Count > 0)
                {
                    if (command.FormQuestionLevelsForSave.Count > currentLevels.Count)
                    {
                        foreach (FormQuestionLevel currLevel in currentLevels)
                        {
                            ObjectMapper.Map(command.FormQuestionLevelsForSave[index], currLevel);
                            currLevel.IsActive = true;
                            addQuestionIds.AddRange(currLevel.ChildrenQuestion);
                            await _formQuestionLevelRepository.UpdateAsync(currLevel);

                            index++;
                        }

                        //Add new levels associated with Question
                        List<UpsertFormQuestionLevel> insertLevels = command.FormQuestionLevelsForSave.GetRange(index, command.FormQuestionLevelsForSave.Count - index);
                        await CreateNewLevels(command.FormQuestionId, insertLevels);
                    }
                    else
                    {
                        //Update data of exist answers
                        foreach (FormQuestionLevel currLevel in currentLevels)
                        {
                            if (index < command.FormQuestionLevelsForSave.Count)
                            {
                                ObjectMapper.Map(command.FormQuestionLevelsForSave[index], currLevel);
                                addQuestionIds.AddRange(currLevel.ChildrenQuestion);
                                currLevel.IsActive = true;
                            }
                            else
                            {
                                currLevel.IsActive = false;
                            }

                            await _formQuestionLevelRepository.UpdateAsync(currLevel);

                            index++;
                        }
                    }

                    //Set Parents
                    var setParents = new UpdateFormQuestionParentIdCommand();
                    setParents.FormQuestions = addQuestionIds;
                    setParents.FormQuestionId = command.FormQuestionId;
                    await _mediator.Send(setParents);

                }
                else
                {
                    await CreateNewLevels(command.FormQuestionId, command.FormQuestionLevelsForSave);
                }
            }
            finally
            {
                await uow.CompleteAsync();
            }

            return Unit.Value;
        }

        /// <summary>
        /// Insert New level related to a Question
        /// </summary>
        /// <param name="formQuestionId">Question Identificator</param>
        /// <param name="FormQuestionLevelsForSave">Data for new Levels</param>
        /// <returns></returns>
        private async Task CreateNewLevels(long formQuestionId, List<UpsertFormQuestionLevel> FormQuestionLevelsForSave)
        {
            var createLevels = new CreateFormQuestionLevelCommand();
            createLevels.FormQuestionLevelsForSave = FormQuestionLevelsForSave;
            createLevels.FormQuestionId = formQuestionId;
            await _mediator.Send(createLevels);
        }
    }
}
