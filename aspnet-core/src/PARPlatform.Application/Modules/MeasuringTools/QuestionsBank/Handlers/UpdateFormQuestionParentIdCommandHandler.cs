﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle Update question parentid relationship Command
    /// </summary>
    public class UpdateFormQuestionParentIdCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateFormQuestionParentIdCommand>
    {
        private readonly IQuestionsBankRepository _questionsBankRepository;

        /// <summary>
        /// Main constructor
        /// </summary>
        public UpdateFormQuestionParentIdCommandHandler(IQuestionsBankRepository questionsBankRepository)
        {
            _questionsBankRepository = questionsBankRepository;
        }

        public async Task<Unit> Handle(UpdateFormQuestionParentIdCommand command, CancellationToken cancellationToken)
        {
            List<FormQuestion> questions = await _questionsBankRepository.GetAll()
                .Where(e => e.ParentId == command.FormQuestionId)
                .ToListAsync();

            List<FormQuestion> addQuestions = await _questionsBankRepository.GetAll()
                .Where(e => command.FormQuestions.Contains(e.Id))
                .ToListAsync();

            using var uow = UnitOfWorkManager.Begin();
            try
            {
                foreach (FormQuestion question in questions)
                {
                    question.ParentId = null;
                    await _questionsBankRepository.UpdateAsync(question);
                }

                foreach (FormQuestion addQuestion in addQuestions)
                {
                    addQuestion.ParentId = command.FormQuestionId;
                    await _questionsBankRepository.UpdateAsync(addQuestion);
                }
            }
            finally
            {
                await uow.CompleteAsync();
            }

            return Unit.Value;
        }
    }
}
