﻿using Abp.Domain.Uow;
using Abp.Localization;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands.Validators;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.PAREntities;
using PARPlatform.Support;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Handlers
{
    /// <summary>
    /// Used to handle Update Questions Bank Command
    /// </summary>
    public class UpdateQuestionsBankCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateQuestionsBankCommand, UpsertQuestionsBankDto>
    {
        private readonly IQuestionsBankRepository _questionsBankRepository;
        private readonly IAnswerRepository _answerRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IMediator _mediator;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="questionsBankRepository"></param>
        public UpdateQuestionsBankCommandHandler(
            IQuestionsBankRepository questionsBankRepository,
            IAnswerRepository answerRepository,
            ILocalizationManager localizationManager,
            IUnitOfWorkManager unitOfWorkManager,
            IMediator mediator)
        {
            _questionsBankRepository = questionsBankRepository;
            _answerRepository = answerRepository;
            _localizationManager = localizationManager;
            _unitOfWorkManager = unitOfWorkManager;
            _mediator = mediator;
        }

        /// <summary>
        /// Update Question of Questions Bank use case 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertQuestionsBankDto> Handle(UpdateQuestionsBankCommand command, CancellationToken cancellationToken)
        {
            new UpdateQuestionsBankCommandValidator(_localizationManager).Validate(command).ThrowIfAreErrors();

            FormQuestion formQuestion = await _questionsBankRepository.GetAsync(command.Id);


            ObjectMapper.Map(command, formQuestion);
            await _questionsBankRepository.UpdateAsync(formQuestion);


            //UpdateFormQuestionLevelCommand
            if (command.QuestionTypeId == Constants.TYPE_STRUCTURE)
            {
                var updateLevels = new UpdateFormQuestionLevelCommand();
                updateLevels.FormQuestionLevelsForSave = command.FormQuestionLevelsForSave;
                updateLevels.FormQuestionId = formQuestion.Id;
                await _mediator.Send(updateLevels);
            }
            else
            {
                using var uow = UnitOfWorkManager.Begin();
                try
                {
                    await UpdateAnswers(command.Id, command.QuestionTypeId, command.Answers);
                }
                finally
                {
                    await uow.CompleteAsync();
                }
            }

            return ObjectMapper.Map<UpsertQuestionsBankDto>(formQuestion);

        }

        /// <summary>
        /// Update answers information
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newAnswers"></param>
        /// <returns></returns>
        private async Task UpdateAnswers(long id, long questionTypeId, List<UpsertAnswerDto> newAnswers)
        {
            List<Answer> currentAnswers = await _answerRepository.GetAllListAsync(a => a.FormQuestionId == id);
            int index = 0;

            if (currentAnswers.Count > 0)
            {
                if (newAnswers.Count > currentAnswers.Count)
                {
                    foreach (Answer currAnswer in currentAnswers)
                    {
                        ObjectMapper.Map(newAnswers[index], currAnswer);
                        currAnswer.IsActive = true;
                        await _answerRepository.UpdateAsync(currAnswer);

                        index++;
                    }

                    //Add new answers associated with Question
                    for (int i = index; i < newAnswers.Count; i++)
                    {
                        await CreateNewAnswer(id, questionTypeId, newAnswers[i], i);
                    }
                }
                else
                {
                    //Update data of exist answers
                    foreach (Answer currAnswer in currentAnswers)
                    {
                        if (index < newAnswers.Count)
                        {
                            ObjectMapper.Map(newAnswers[index], currAnswer);
                            currAnswer.IsActive = true;
                        }
                        else
                        {
                            currAnswer.IsActive = false;
                        }

                        await _answerRepository.UpdateAsync(currAnswer);

                        index++;
                    }
                }
            }
            else
            {
                //Insert new answers associated with Question
                foreach (var item in newAnswers)
                {
                    await CreateNewAnswer(id, questionTypeId, item, index);
                    index++;
                }
            }
        }

        /// <summary>
        /// Insert New answer related to a Question
        /// </summary>
        /// <param name="id">Question Identificator</param>
        /// <param name="questionTypeId">Question Type Identificator</param>
        /// <param name="newAnswer">Data for new Answer</param>
        /// <param name="index">Index of answer for open questions or related to position</param>
        /// <returns></returns>
        private async Task CreateNewAnswer(long id, long questionTypeId, UpsertAnswerDto newAnswer, int index)
        {
            var answerData = ObjectMapper.Map<Answer>(newAnswer);
            answerData.FormQuestionId = id;
            answerData.IsActive = true;
            answerData.PersonalId = QuestionsBankHelper.CreateNewAnswerPersonalId(index + 1);

            await _answerRepository.InsertAsync(answerData);
        }
    }
}
