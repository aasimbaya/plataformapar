﻿using MediatR;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System.Collections.Generic;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries
{
    /// <summary>
    /// Get All Levels of structure Question By Question Id Query
    /// </summary>
    public class GetAllLevelsByQuestionIdQuery : IRequest<List<FormQuestionLevelListItemDto>>
    {
        public long FormQuestionId { get; set; }

        /// <summary>
        /// Main constructor
        /// </summary>
        public GetAllLevelsByQuestionIdQuery(long id)
        {
            FormQuestionId = id;
        }
    }
}
