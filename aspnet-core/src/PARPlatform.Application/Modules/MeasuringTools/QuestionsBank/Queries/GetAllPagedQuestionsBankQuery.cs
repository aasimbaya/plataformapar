﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries
{
    /// <summary>
    /// Get All Questions Bank Paged Query
    /// </summary>
    public class GetAllPagedQuestionsBankQuery : PagedAndSortedInputDto, IShouldNormalize, IRequest<PagedResultDto<QuestionsBankListItemDto>>
    {
        public string Filter { get; set; }
        public long Use { get; set; }
        public bool? IsForStructureQuestion { get; set; } = false;

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Question";
            }
        }
    }
}
