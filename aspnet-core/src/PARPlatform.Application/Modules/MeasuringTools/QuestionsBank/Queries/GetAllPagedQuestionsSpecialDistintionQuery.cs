﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries
{
    public class GetAllPagedQuestionsSpecialDistintionQuery : PagedAndSortedInputDto, IShouldNormalize, IRequest<PagedResultDto<QuestionsSpecialDistintionListItemDto>>
    {
        public string Filter { get; set; }

        public long FormId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Question DESC";
            }
            if (string.IsNullOrEmpty(Filter))
            {
                Filter = "";
            }
        }
    }
}
