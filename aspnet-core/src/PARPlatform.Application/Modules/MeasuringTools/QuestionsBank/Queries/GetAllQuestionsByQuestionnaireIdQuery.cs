﻿using MediatR;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries
{
    /// <summary>
    /// Get All Questions Bank Detailed information By Questionnaire Id
    /// </summary>
    public class GetAllQuestionsByQuestionnaireIdQuery : IRequest<List<QuestionnaireQuestionDetailDto>>
    {
        public long QuestionnaireId { get; set; }

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="questionnaireId"></param>
        public GetAllQuestionsByQuestionnaireIdQuery(long questionnaireId)
        {
            QuestionnaireId = questionnaireId;
        }
    }
}
