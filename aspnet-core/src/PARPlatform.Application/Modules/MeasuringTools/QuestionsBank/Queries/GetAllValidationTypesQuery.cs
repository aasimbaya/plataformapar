﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries
{
    /// <summary>
    /// Used to get Validation Types
    /// </summary>
    public class GetAllValidationTypesQuery : IRequest<List<DropdownItemDto>>
    {
    }
}
