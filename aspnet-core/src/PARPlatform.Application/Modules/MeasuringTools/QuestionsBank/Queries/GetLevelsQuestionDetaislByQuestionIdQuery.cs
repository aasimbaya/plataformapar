﻿using MediatR;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System.Collections.Generic;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries
{
    /// <summary>
    /// Get All Levels with questions detail of structure Question By Question Id Query
    /// </summary>
    public class GetLevelsQuestionDetaislByQuestionIdQuery : IRequest<List<FormQuestionLevelWithQuestionDataDto>>
    {
        public long FormQuestionId { get; set; }

        /// <summary>
        /// Main constructor
        /// </summary>
        public GetLevelsQuestionDetaislByQuestionIdQuery(long id)
        {
            FormQuestionId = id;
        }
    }
}
