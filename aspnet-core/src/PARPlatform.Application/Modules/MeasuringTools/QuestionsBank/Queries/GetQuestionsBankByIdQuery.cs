﻿using MediatR;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries
{
    /// <summary>
    /// Get Questions Bank Detail By Id Query
    /// </summary>
    public class GetQuestionsBankByIdQuery : IRequest<UpsertQuestionsBankDto>
    {
        public long Id { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="id"></param>
        public GetQuestionsBankByIdQuery(long id)
        {
            Id = id;
        }
    }
}
