﻿using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Commands
{
    /// <summary>
    /// Create question category command
    /// </summary>
    public class CreateQuestionCategoryCommand : IRequest<UpsertQuestionCategoryDto>, ICustomValidate
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public long QuestionCategoryTypeId { get; set; }
        public string Icon { get; set; }
        /// <summary>
        /// Validation command
        /// </summary>
        /// <param name="context"></param>
        public void AddValidationErrors(CustomValidationContext context)
        {
            if (Icon == null)
            {
                Icon = "";
            }
        }
    }
}
