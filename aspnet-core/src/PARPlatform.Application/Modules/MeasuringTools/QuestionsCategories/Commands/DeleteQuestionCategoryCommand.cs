﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Commands
{
    /// <summary>
    /// Delete question category command
    /// </summary>
    public class DeleteQuestionCategoryCommand: IRequest
    {
        public long Id { get; set; }
    }
}
