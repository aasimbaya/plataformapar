﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Commands;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Handlers
{
    /// <summary>
    /// Create question category command handler
    /// </summary>
    public class CreateQuestionCategoryCommandHandler : UseCaseServiceBase, IRequestHandler<CreateQuestionCategoryCommand, UpsertQuestionCategoryDto>
    {
        private readonly IQuestionCategoryRepository _questionCategoryRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="questionCategoryRepository"></param>
        public CreateQuestionCategoryCommandHandler(
            IQuestionCategoryRepository questionCategoryRepository)
        {
            _questionCategoryRepository = questionCategoryRepository;
        }
        /// <summary>
        /// Create question category use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertQuestionCategoryDto> Handle(CreateQuestionCategoryCommand input, CancellationToken cancellationToken)
        {
            QuestionCategory data = ObjectMapper.Map<QuestionCategory>(input);
            data.LastModificationTime = DateTime.Now;
            await _questionCategoryRepository.InsertAsync(data);
            return ObjectMapper.Map<UpsertQuestionCategoryDto>(data);
        }
    }
}
