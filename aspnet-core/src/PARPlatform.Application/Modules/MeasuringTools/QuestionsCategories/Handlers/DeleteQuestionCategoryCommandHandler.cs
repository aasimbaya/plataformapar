﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Commands;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Handlers
{
    /// <summary>
    /// Delete question category  command handler
    /// </summary>
    public class DeleteQuestionCategoryCommandHandler : UseCaseServiceBase, IRequestHandler<DeleteQuestionCategoryCommand>
    {
        private readonly IRepository<QuestionCategory> _questionCategoryRepository;


        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="questionCategoryRepository"></param>
        public DeleteQuestionCategoryCommandHandler(
            IRepository<QuestionCategory> questionCategoryRepository)
        {
            _questionCategoryRepository = questionCategoryRepository;
        }
        /// <summary>
        /// Delete question category
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteQuestionCategoryCommand command, CancellationToken cancellationToken)
        {
            QuestionCategory questionCategory = await _questionCategoryRepository.FirstOrDefaultAsync(x => x.Id == command.Id);
            questionCategory.IsDeleted = true;
            await _questionCategoryRepository.UpdateAsync(questionCategory);
            return Unit.Value;
        }
    }
}
