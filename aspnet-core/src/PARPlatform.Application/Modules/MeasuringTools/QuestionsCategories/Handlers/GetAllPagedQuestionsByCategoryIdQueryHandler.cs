﻿using Abp.Application.Services.Dto;
using MediatR;
using PARPlatform.Base;
using PARPlatform.PAREntities;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Handlers
{
    /// <summary>
    /// Get paged question categories
    /// </summary> 
    public class GetAllPagedQuestionsByCategoryIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedQuestionsByCategoryIdQuery, PagedResultDto<QuestionsByCategoryListItemDto>>
    {
        private readonly IRepository<FormQuestion> _formQuestionsRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="questionCategoryRepository"></param>
        public GetAllPagedQuestionsByCategoryIdQueryHandler(
            IRepository<FormQuestion> formQuestionsRepository)
        {
            _formQuestionsRepository = formQuestionsRepository;
        }
        /// <summary>
        /// Get paged list of questions by category
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<QuestionsByCategoryListItemDto>> Handle(GetAllPagedQuestionsByCategoryIdQuery query, CancellationToken cancellationToken)
        {

            IQueryable<FormQuestion> dbQuery = _formQuestionsRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(query.Filter), t => t.ShortVersion.Contains(query.Filter) && t.IsDeleted == false);

            int questionsByCategoryCount = await dbQuery.CountAsync(cancellationToken);
            var questionsByCategory = await dbQuery.OrderBy(query.Sorting).PageBy(query).ToListAsync(cancellationToken);

            List<QuestionsByCategoryListItemDto> questionsByCategoryDto = ObjectMapper.Map<List<QuestionsByCategoryListItemDto>>(questionsByCategory);

            for (int i = 0; i < questionsByCategoryDto.Count; i++)
            {
                questionsByCategoryDto[i].Order = i + 1;
            }

            return new PagedResultDto<QuestionsByCategoryListItemDto>(
                questionsByCategoryCount,
                questionsByCategoryDto
            );
        }
    }
}
