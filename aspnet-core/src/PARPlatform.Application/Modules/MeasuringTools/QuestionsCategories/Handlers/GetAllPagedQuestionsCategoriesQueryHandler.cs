﻿using Abp.Application.Services.Dto;
using MediatR;
using PARPlatform.Base;
using PARPlatform.PAREntities;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Handlers
{
    /// <summary>
    /// Get paged question categories
    /// </summary> 
    public class GetAllPagedQuestionsCategoriesQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedQuestionCategoryQuery, PagedResultDto<QuestionCategoryListItemDto>>
    {
        private readonly IRepository<QuestionCategory> _questionCategoryRepository;
        private readonly IRepository<QuestionCategoryType> _questionCategoryTypeRepository;
        private readonly IRepository<FormQuestion> _formQuestionsRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="questionCategoryRepository"></param>
        public GetAllPagedQuestionsCategoriesQueryHandler(
            IRepository<QuestionCategory> countryRepository,
            IRepository<QuestionCategoryType> questionCategoryTypeRepository,
            IRepository<FormQuestion> formQuestionsRepository)
        {
            _questionCategoryRepository = countryRepository;
            _questionCategoryTypeRepository = questionCategoryTypeRepository;
            _formQuestionsRepository = formQuestionsRepository;
        }
        /// <summary>
        /// Get paged list of question category
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<QuestionCategoryListItemDto>> Handle(GetAllPagedQuestionCategoryQuery query, CancellationToken cancellationToken)
        {
            //QuestionsRelations
            IQueryable<QuestionCategory> dbQuery = _questionCategoryRepository.GetAll()
            .WhereIf(!string.IsNullOrWhiteSpace(query.Filter), t => t.Name.Contains(query.Filter));

            int categoriesCount = await dbQuery.CountAsync(cancellationToken);
            List<QuestionCategoryType> types = _questionCategoryTypeRepository.GetAll().ToList();

            List<QuestionCategoryListItemDto> categoriesDto;
            if (query.Sorting.Contains("questionsRelations"))
            {
                categoriesDto =
                   (from category in dbQuery
                    join questionsR in _formQuestionsRepository.GetAll() on category.Id equals questionsR.QuestionCategoryId into questionsCout
                    from questions in questionsCout.DefaultIfEmpty()
                    group category by new
                    {
                        category.Id,
                        category.Name,
                        category.Description,
                        category.IsActive,
                        category.QuestionCategoryTypeId,
                        category.LastModificationTime,
                        count = questions.Id != null ? true : false
                    }
                   into questionsCategory
                        //orderby questionsCategory.Key
                    select new QuestionCategoryListItemDto
                    {
                        Id = questionsCategory.Key.Id,
                        Name = questionsCategory.Key.Name,
                        Description = questionsCategory.Key.Description,
                        IsActive = questionsCategory.Key.IsActive,
                        QuestionsRelations = (questionsCategory.Key.count) ? questionsCategory.Count() : 0,
                        QuestionCategoryTypeId = questionsCategory.Key.QuestionCategoryTypeId ?? 1,
                        LastModificationTime = questionsCategory.Key.LastModificationTime ?? DateTime.Now,
                    })
                   .ToList();
                categoriesDto = BuilData(categoriesDto, types);
            }
            else
            {
                List<QuestionCategory> categories = await dbQuery.OrderBy(query.Sorting).PageBy(query).ToListAsync(cancellationToken);
                categoriesDto = BuilData(categories, types);
            }

            return new PagedResultDto<QuestionCategoryListItemDto>(
                categoriesCount,
                categoriesDto
            );
        }

        /// <summary>
        /// Buil Data when is sorting for question category fields
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private List<QuestionCategoryListItemDto> BuilData(List<QuestionCategory> categories, List<QuestionCategoryType> types)
        {
            List<QuestionCategoryListItemDto> categoriesDto = ObjectMapper.Map<List<QuestionCategoryListItemDto>>(categories);
            foreach (var category in categoriesDto)
            {
                category.QuestionCategoryTypeName = types.FirstOrDefault(x => x.Id == category.QuestionCategoryTypeId).Name;
                category.QuestionsRelations = _formQuestionsRepository.GetAll().Where(question => question.QuestionCategoryId == category.Id).Count();
            }
            return categoriesDto;
        }

        /// <summary>
        /// Get paged list of question category
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private List<QuestionCategoryListItemDto> BuilData(List<QuestionCategoryListItemDto> categoriesDto, List<QuestionCategoryType> types)
        {
            foreach (var category in categoriesDto)
            {
                category.QuestionCategoryTypeName = types.FirstOrDefault(x => x.Id == category.QuestionCategoryTypeId).Name;
            }
            return categoriesDto;

        }
    }
}
