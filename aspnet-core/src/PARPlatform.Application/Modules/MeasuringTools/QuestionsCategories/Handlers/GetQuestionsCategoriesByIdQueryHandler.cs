﻿using Abp.ObjectMapping;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Queries;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Handlers
{
    /// <summary>
    /// Get paged categories by id
    /// </summary>
    public class GetQuestionsCategoriesByIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetQuestionCategoryByIdQuery, UpsertQuestionCategoryDto>
    {
        private readonly IRepository<QuestionCategory> _questionCategoryRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="questionCategoryRepository"></param>
        public GetQuestionsCategoriesByIdQueryHandler(
            IRepository<QuestionCategory> questionCategoryRepository)
        {
            _questionCategoryRepository = questionCategoryRepository;
        }

        /// <summary>
        /// Handles request for getting question category details data
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertQuestionCategoryDto> Handle(GetQuestionCategoryByIdQuery input, CancellationToken cancellationToken)
        {
            List<QuestionCategory> filteredQuestionCategory = _questionCategoryRepository.GetAll().
                Where(e => e.Id == input.Id)
                .ToList();
            UpsertQuestionCategoryDto result = ObjectMapper.Map<UpsertQuestionCategoryDto>(filteredQuestionCategory.First());
            return result;
        }
    }
}
