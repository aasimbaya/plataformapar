﻿using Abp.ObjectMapping;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Queries;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Handlers
{
    /// <summary>
    /// Get question categories type by name
    /// </summary>
    public class GetTypeCategoriesQueryHandler : UseCaseServiceBase, IRequestHandler<GetTypeCategoriesByNameQuery, List<UpsertQuestionCategoryTypeDto>>
    {
        private readonly IRepository<QuestionCategoryType> _questionCategoryTypeRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="questionCategoryRepository"></param>
        public GetTypeCategoriesQueryHandler(
            IRepository<QuestionCategoryType> questionCategoryTypeRepository)
        {
            _questionCategoryTypeRepository = questionCategoryTypeRepository;
        }

        /// <summary>
        /// Handles request for getting question category type details data
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<UpsertQuestionCategoryTypeDto>> Handle(GetTypeCategoriesByNameQuery input, CancellationToken cancellationToken)
        {
            List<QuestionCategoryType> filteredQuestionCategorys = _questionCategoryTypeRepository.GetAll().ToList();
            List<UpsertQuestionCategoryTypeDto> result = ObjectMapper.Map<List<UpsertQuestionCategoryTypeDto>>(filteredQuestionCategorys);

            return result;
        }
    }
}
