﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Commands;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using PARPlatform.PAREntities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Handlers
{
    /// <summary>
    /// Update question category command handler
    /// </summary>
    public class UpdateQuestionCategoryCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateQuestionCategoryCommand, UpsertQuestionCategoryDto>
    {
        private readonly IRepository<QuestionCategory> _questionCategoryRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="questionCategoryRepository"></param>
        public UpdateQuestionCategoryCommandHandler(
            IRepository<QuestionCategory> questionCategoryRepository )
        {
            _questionCategoryRepository = questionCategoryRepository;
        }
        /// <summary>
        /// Update question category use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertQuestionCategoryDto> Handle(UpdateQuestionCategoryCommand input, CancellationToken cancellationToken)
        {
            QuestionCategory questionCategory = await _questionCategoryRepository.FirstOrDefaultAsync(x => x.Id == input.Id);
            ObjectMapper.Map(input, questionCategory);
            questionCategory.LastModificationTime = DateTime.Now;
            await _questionCategoryRepository.UpdateAsync(questionCategory);
            return ObjectMapper.Map<UpsertQuestionCategoryDto>(questionCategory);
        }
    }
}
