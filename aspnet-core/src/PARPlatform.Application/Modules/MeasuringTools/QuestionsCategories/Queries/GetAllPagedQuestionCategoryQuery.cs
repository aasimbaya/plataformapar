﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Queries
{
    /// <summary>
    /// Get question category by filter
    /// </summary>
    public class GetAllPagedQuestionCategoryQuery : PagedAndSortedInputDto, IShouldNormalize, IRequest<PagedResultDto<QuestionCategoryListItemDto>>
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
            if (Sorting.Contains("questionCategoryTypeName"))
            {
                Sorting = Sorting.Replace("questionCategoryTypeName", "QuestionCategoryTypeId");
            }
        }
    }
}