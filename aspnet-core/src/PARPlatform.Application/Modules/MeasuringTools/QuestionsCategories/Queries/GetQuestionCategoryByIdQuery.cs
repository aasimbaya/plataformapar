﻿using MediatR;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Queries
{
    /// <summary>
    /// Get question category by Id query
    /// </summary>
    public class GetQuestionCategoryByIdQuery : IRequest<UpsertQuestionCategoryDto>
    {
        public long Id { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="id"></param>
        public GetQuestionCategoryByIdQuery(long id)
        {
            Id = id;
        }
    }
}
