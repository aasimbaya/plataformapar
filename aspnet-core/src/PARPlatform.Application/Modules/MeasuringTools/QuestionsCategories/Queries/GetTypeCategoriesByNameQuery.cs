﻿using MediatR;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.MeasuringTools.QuestionsCategories.Queries
{
    /// <summary>
    /// Get types question category by name query
    /// </summary>
    public class GetTypeCategoriesByNameQuery : IRequest<List<UpsertQuestionCategoryTypeDto>>
    {
        public string Name { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="name"></param>
        public GetTypeCategoriesByNameQuery(string name)
        {
            Name = name;
        }
    }
}
