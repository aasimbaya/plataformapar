﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.PersonalTypes.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.PersonalTypes.Handlers
{
    /// <summary>
    /// Handler to get all personal types
    /// </summary>
    public class GetAllPersonalTypeQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPersonalTypeQuery, List<DropdownItemDto>>
    {
        private readonly IRepository<PersonalType> _personalTypeRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="personalTypeRepository"></param>
        public GetAllPersonalTypeQueryHandler(IRepository<PersonalType> personalTypeRepository)
        {
            _personalTypeRepository = personalTypeRepository;
        }

        public async Task<List<DropdownItemDto>> Handle(GetAllPersonalTypeQuery request, CancellationToken cancellationToken)
        {
            return _personalTypeRepository.GetAll()
                .Select(t => new DropdownItemDto { Id = t.Id, Name = t.Name, Code = t.Abreviation }).ToList();
        }
    }
}


