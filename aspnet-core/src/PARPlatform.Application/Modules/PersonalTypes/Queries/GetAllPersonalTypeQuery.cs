﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.PersonalTypes.Queries
{
    /// <summary>
    /// Query to get all personal types
    /// </summary>
    public class GetAllPersonalTypeQuery : IRequest<List<DropdownItemDto>>
    {
    }
}

