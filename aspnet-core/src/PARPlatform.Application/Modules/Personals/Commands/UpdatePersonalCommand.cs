﻿using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;

namespace PARPlatform.Modules.Personals.Commands
{
    /// <summary>
    /// Command for update Personal
    /// </summary>
    public class UpdatePersonalCommand : IRequest<UpsertPersonalDto>, ICustomValidate
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string CellPhone { get; set; }
        public long? PositionId { get; set; }
        public long? PersonalTypeId { get; set; }
        public long? EnterpriseId { get; set; }
        public bool IsActive { get; set; }
        public void AddValidationErrors(CustomValidationContext context)
        {
        }
    }
}

