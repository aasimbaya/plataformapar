﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.EntityFrameworkCore;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.Personals.Commands;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Personals.Handlers
{
    public class CreatePersonalCommandHandler : UseCaseServiceBase, IRequestHandler<CreatePersonalCommand, UpsertPersonalDto>
    {
        private readonly IRepository<Personal> _personalRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="personalRepository"></param>
        public CreatePersonalCommandHandler(IRepository<Personal> personalRepository)
        {
            _personalRepository = personalRepository;
        }
        /// <summary>
        /// Create Personal use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertPersonalDto> Handle(CreatePersonalCommand input, CancellationToken cancellationToken)
        {
            Personal data = ObjectMapper.Map<Personal>(input);
            Personal newPersonal = await _personalRepository.InsertAsync(data);
            return ObjectMapper.Map<UpsertPersonalDto>(newPersonal);
        }
    }
}

