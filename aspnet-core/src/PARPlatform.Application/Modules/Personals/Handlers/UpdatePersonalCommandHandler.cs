﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.Personals.Commands;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Personals.Handlers
{
    /// <summary>
    /// Update Personal command handler
    /// </summary>
    public class UpdatePersonalCommandHandler : UseCaseServiceBase, IRequestHandler<UpdatePersonalCommand, UpsertPersonalDto>
    {
        private readonly IRepository<Personal> _personalRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="personalRepository"></param>
        public UpdatePersonalCommandHandler(IRepository<Personal> personalRepository)
        {
            _personalRepository = personalRepository;
        }

        /// <summary>
        /// Update personal use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertPersonalDto> Handle(UpdatePersonalCommand command, CancellationToken cancellationToken)
        {
            var personal = await _personalRepository.FirstOrDefaultAsync(p => p.Id == command.Id);
            ObjectMapper.Map(command, personal);

            await _personalRepository.UpdateAsync(personal);

            return ObjectMapper.Map<UpsertPersonalDto>(personal);
        }
    }
}

