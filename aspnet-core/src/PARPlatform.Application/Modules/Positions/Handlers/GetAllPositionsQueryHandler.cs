﻿
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Positions.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Positions.Handlers
{
    /// <summary>
    /// Handler to get all positions
    /// </summary>
    public class GetAllPositionsQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPositionQuery, List<DropdownItemDto>>
    {
        private readonly IRepository<Position> _positionRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="countryRepository"></param>
        public GetAllPositionsQueryHandler(IRepository<Position> positionRepository)
        {
            _positionRepository = positionRepository;
        }
        /// <summary>
        /// Get all sectors
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<DropdownItemDto>> Handle(GetAllPositionQuery request, CancellationToken cancellationToken)
        {
            var sectors = _positionRepository.GetAll()
                .Select(t => new DropdownItemDto { Id = t.Id, Name = t.Name });
            return ObjectMapper.Map(sectors, new List<DropdownItemDto>());
        }
    }
}
