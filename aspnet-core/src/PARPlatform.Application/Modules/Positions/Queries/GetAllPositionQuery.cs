﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Positions.Queries
{
    /// <summary>
    /// Query to get all postions
    /// </summary>
    public class GetAllPositionQuery : IRequest<List<DropdownItemDto>>
    {
    }
}
