﻿using Abp.Application.Services.Dto;
using MediatR;
using PARPlatform.Base;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Regions.Queries;

namespace PARPlatform.Modules.Regions.Handlers
{
    /// <summary>
    /// Get all regions by country Id
    /// </summary> 
    public class GetAllRegionByCountryIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllRegionByCountryIdQuery, List<DropdownItemDto>>
    {
        private readonly IRepository<Region> _regionRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="regionRepository"></param>
        public GetAllRegionByCountryIdQueryHandler(IRepository<Region> regionRepository)
        {
            _regionRepository = regionRepository;
        }
        public async Task<List<DropdownItemDto>> Handle(GetAllRegionByCountryIdQuery query, CancellationToken cancellationToken)
        {

            IQueryable<DropdownItemDto> countries = _regionRepository.GetAll()
                .Where(p => p.CountryId == query.CountryId)
                .Select(t => new DropdownItemDto { Id = t.Id, Name = t.Name })
                .OrderBy(p => p.Name);

            return countries.ToList();
        }
    }
}

