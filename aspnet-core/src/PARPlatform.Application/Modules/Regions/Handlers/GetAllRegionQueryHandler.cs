﻿using Abp.Collections.Extensions;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Regions.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Regions.Handlers
{
    /// <summary>
    /// Handler to get all regions
    /// </summary>
    public class GetAllRegionQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllRegionQuery, List<DropdownItemDto>>
    {
        private readonly IRepository<Region> _regionRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="regionRepository"></param>
        public GetAllRegionQueryHandler(IRepository<Region> regionRepository)
        {
            _regionRepository = regionRepository;
        }

        public async Task<List<DropdownItemDto>> Handle(GetAllRegionQuery query, CancellationToken cancellationToken)
        {
            return _regionRepository.GetAll()
            .WhereIf(true, t =>
            (t.Name.Contains(query.Filter ?? "") || t.Id == query.Id) &&
            t.CountryId == query.CountryId
            )
            .Select(t => new DropdownItemDto { Id = t.Id, Name = t.Name }).ToList();
        }
    }
}


