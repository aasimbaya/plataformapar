﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Regions.Queries
{
    /// <summary>
    /// Get regions by filter
    /// </summary>
    public class GetAllRegionByCountryIdQuery : IRequest<List<DropdownItemDto>>
    {
        public long CountryId { get; set; }
        public string Filter { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="id"></param>
        public GetAllRegionByCountryIdQuery(long countryId)
        {
            CountryId = countryId;
        }
    }
}

