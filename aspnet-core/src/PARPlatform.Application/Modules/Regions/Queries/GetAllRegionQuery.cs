﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Regions.Queries
{
    /// <summary>
    /// Query to get all regions
    /// </summary>
    public class GetAllRegionQuery : IRequest<List<DropdownItemDto>>
    {
        public string Filter { get; set; }
        public int Id { get; set; }
        public long CountryId { get; set; }
    }
}
