﻿using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Modules.Configuration.Sector.Dtos;

namespace PARPlatform.Modules.Sectors.Commands
{
    /// <summary>
    /// Command for create Sector
    /// </summary>
    public class CreateSectorCommand : IRequest<UpsertSectorDto>, ICustomValidate
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string InnerType { get; set; }
        public bool IsActive { get; set; }
        public string Ciiu { get; set; }
        public void AddValidationErrors(CustomValidationContext context)
        {
        }
    }
}

