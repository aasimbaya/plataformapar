﻿using MediatR;

namespace PARPlatform.Modules.Sectors.Commands
{
    /// <summary>
    /// Delete sector command
    /// </summary>
    public class DeleteSectorCommand : IRequest
    {
        public long Id { get; set; }
    }
}

