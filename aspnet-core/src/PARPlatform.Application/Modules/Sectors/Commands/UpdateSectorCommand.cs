﻿using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Modules.Configuration.Sector.Dtos;

namespace PARPlatform.Modules.Sectors.Commands
{
    /// <summary>
    /// Update sector command
    /// </summary>
    public class UpdateSectorCommand : IRequest<UpsertSectorDto>, ICustomValidate
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string InnerType { get; set; }
        public bool IsActive { get; set; }
        public string Ciiu { get; set; }

        /// <summary>
        /// Validation command
        /// </summary>
        /// <param name="context"></param>
        public void AddValidationErrors(CustomValidationContext context)
        {
            if (Id == 0)
            {
                context.Results.Add(new System.ComponentModel.DataAnnotations.ValidationResult("IdIsRequired"));
            }
        }
    }
}
