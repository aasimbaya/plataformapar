﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Configuration.Sector.Dtos;
using PARPlatform.Modules.Sectors.Commands;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Sectors.Handlers
{
    /// <summary>
    /// Create sector command handler
    /// </summary>
    public class CreateSectorCommandHandler : UseCaseServiceBase, IRequestHandler<CreateSectorCommand, UpsertSectorDto>
    {
        private readonly IRepository<Sector> _sectorRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="sectorRepository"></param>
        public CreateSectorCommandHandler(IRepository<Sector> sectorRepository)
        {
            _sectorRepository = sectorRepository;
        }
        /// <summary>
        /// Create sector use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertSectorDto> Handle(CreateSectorCommand input, CancellationToken cancellationToken)
        {
            input.InnerType = input.Type;
            var data = ObjectMapper.Map<Sector>(input);            
            await _sectorRepository.InsertAsync(data);
            return ObjectMapper.Map<UpsertSectorDto>(data);
        }
    }
}

