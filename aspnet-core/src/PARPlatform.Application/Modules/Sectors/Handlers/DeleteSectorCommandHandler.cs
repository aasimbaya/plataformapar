﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Sectors.Commands;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Sectors.Handlers
{
    /// <summary>
    /// Delete sector command handler
    /// </summary>
    public class DeleteSectorCommandHandler : UseCaseServiceBase, IRequestHandler<DeleteSectorCommand>
    {
        private readonly IRepository<Sector> _sectorRepository;


        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="sectorRepository"></param>
        public DeleteSectorCommandHandler(IRepository<Sector> sectorRepository)
        {
            _sectorRepository = sectorRepository;
        }

        /// <summary>
        /// Delete sector
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteSectorCommand command, CancellationToken cancellationToken)
        {
            Sector sector = await _sectorRepository.FirstOrDefaultAsync(x => x.Id == command.Id);
            sector.IsDeleted = true;
            await _sectorRepository.UpdateAsync(sector);
            return Unit.Value;
        }
    }
}
