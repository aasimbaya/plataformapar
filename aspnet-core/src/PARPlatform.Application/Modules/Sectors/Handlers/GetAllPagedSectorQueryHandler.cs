﻿using Abp.Application.Services.Dto;
using MediatR;
using PARPlatform.Base;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Modules.Sectors.Queries;
using PARPlatform.Modules.Configuration.Sector.Dtos;

namespace PARPlatform.Modules.Sectors.Handlers
{
    /// <summary>
    /// Get paged sectors
    /// </summary> 
    public class GetAllPagedSectorQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllPagedSectorQuery, PagedResultDto<UpsertSectorDto>>
    {
        private readonly IRepository<Sector> _sectorRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="sectorRepository"></param>
        public GetAllPagedSectorQueryHandler(IRepository<Sector> sectorRepository)
        {
            _sectorRepository = sectorRepository;
        }

        /// <summary>
        /// Get paged list of sectors
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<UpsertSectorDto>> Handle(GetAllPagedSectorQuery input, CancellationToken cancellationToken)
        {
            IQueryable<Sector> query = _sectorRepository.GetAll()
               .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                   u =>
                       u.Name.Contains(input.Filter) ||
                       u.Type.Contains(input.Filter) ||
                       u.InnerType.Contains(input.Filter)
               );

            int sectorCount = await query.CountAsync(cancellationToken);

            List<Sector> sectors = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            List<UpsertSectorDto> result = ObjectMapper.Map<List<UpsertSectorDto>>(sectors);

            return new PagedResultDto<UpsertSectorDto>(sectorCount, result);

        }

    }
}

