﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Sectors.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Sectors.Handlers
{
    /// <summary>
    /// Handler to get all sectors
    /// </summary>
    public class GetAllSectorQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllSectorQuery, List<DropdownItemDto>>
    {
        private readonly IRepository<Sector> _sectorRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="sectorRepository"></param>
        public GetAllSectorQueryHandler(IRepository<Sector> sectorRepository)
        {
            _sectorRepository = sectorRepository;
        }

        public async Task<List<DropdownItemDto>> Handle(GetAllSectorQuery request, CancellationToken cancellationToken)
        {
            return _sectorRepository.GetAll().Where(p => p.IsActive && !p.IsDeleted)
                .Select(t => new DropdownItemDto { Id = t.Id, Name = t.Name, Code = t.Ciiu }).ToList();
        }
    }
}


