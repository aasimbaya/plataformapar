﻿using Abp.ObjectMapping;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Configuration.Sector.Dtos;
using PARPlatform.Modules.Sectors.Queries;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Sectors.Handlers
{
    /// <summary>
    /// Get paged sector by id
    /// </summary>
    public class GetSectorByIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetSectorByIdQuery, UpsertSectorDto>
    {
        private readonly IRepository<Sector> _sectorRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="sectorRepository"></param>
        public GetSectorByIdQueryHandler(IRepository<Sector> sectorRepository)
        {
            _sectorRepository = sectorRepository;
        }

        /// <summary>
        /// Handles request for getting sector details data
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertSectorDto> Handle(GetSectorByIdQuery input, CancellationToken cancellationToken)
        {
            Task<Sector> query = _sectorRepository.FirstOrDefaultAsync(p => p.Id == input.Id);
            UpsertSectorDto result = ObjectMapper.Map<UpsertSectorDto>(query.Result);
            return result;
        }
    }
}

