﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Configuration.Sector.Dtos;
using PARPlatform.Modules.Sectors.Commands;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Sectors.Handlers
{
    /// <summary>
    /// Update sector command handler
    /// </summary>
    public class UpdateSectorCommandHandler : UseCaseServiceBase, IRequestHandler<UpdateSectorCommand, UpsertSectorDto>
    {
        private readonly IRepository<Sector> _sectorRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="sectorRepository"></param>
        public UpdateSectorCommandHandler(IRepository<Sector> sectorRepository)
        {
            _sectorRepository = sectorRepository;
        }
        /// <summary>
        /// Update sector use case
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertSectorDto> Handle(UpdateSectorCommand input, CancellationToken cancellationToken)
        {
            Sector sector = await _sectorRepository.FirstOrDefaultAsync(x => x.Id == input.Id);
            input.Ciiu = sector.Ciiu;
            input.InnerType = sector.InnerType;
            ObjectMapper.Map(input, sector);
            await _sectorRepository.UpdateAsync(sector);
            return ObjectMapper.Map<UpsertSectorDto>(sector);
        }
    }
}
