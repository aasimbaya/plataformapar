﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using MediatR;
using PARPlatform.Dto;
using PARPlatform.Modules.Configuration.Sector.Dtos;

namespace PARPlatform.Modules.Sectors.Queries
{
    /// <summary>
    /// Get sector by filter
    /// </summary>
    public class GetAllPagedSectorQuery : PagedAndSortedInputDto, IShouldNormalize, IRequest<PagedResultDto<UpsertSectorDto>>
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
            if (Sorting.Contains("innerType"))
            {
                Sorting = Sorting.Replace("innerType", "InnerType");
            }
            if (Sorting.Contains("type"))
            {
                Sorting = Sorting.Replace("type", "Type");
            }
        }
    }
}
