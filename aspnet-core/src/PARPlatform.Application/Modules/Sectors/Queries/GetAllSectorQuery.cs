﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Sectors.Queries
{
    /// <summary>
    /// Query to get all sectors
    /// </summary>
    public class GetAllSectorQuery : IRequest<List<DropdownItemDto>>
    {
    }
}
