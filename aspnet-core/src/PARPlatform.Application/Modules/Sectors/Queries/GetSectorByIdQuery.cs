﻿using MediatR;
using PARPlatform.Modules.Configuration.Sector.Dtos;

namespace PARPlatform.Modules.Sectors.Queries
{
    /// <summary>
    /// Get sector by Id query
    /// </summary>
    public class GetSectorByIdQuery : IRequest<UpsertSectorDto>
    {
        public long Id { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="id"></param>
        public GetSectorByIdQuery(long id)
        {
            Id = id;
        }
    }
}
