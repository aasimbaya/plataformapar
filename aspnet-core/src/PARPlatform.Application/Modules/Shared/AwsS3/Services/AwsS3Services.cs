﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.AspNetCore.Http;
using PARPlatform.Base;
using System;
using System.IO;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Shared.AwsS3.Services
{
    public class AwsS3Services : PARPlatformAppServiceBase, IAwsS3Services
    {
        private string _bucketName;
        private readonly IAmazonS3 _awsS3Client;

        public AwsS3Services(IAwsS3Configuration _appConfiguration)
        {
            _bucketName = _appConfiguration.BucketName;
            _awsS3Client = new AmazonS3Client(_appConfiguration.AwsAccessKey, _appConfiguration.AwsSecretAccessKey, RegionEndpoint.GetBySystemName(_appConfiguration.Region));
        }

        public async Task<bool> UploadFileAsync(IFormFile file)
        {
            try
            {
                using (var newMemoryStream = new MemoryStream())
                {
                    file.CopyTo(newMemoryStream);

                    var uploadRequest = new TransferUtilityUploadRequest
                    {
                        InputStream = newMemoryStream,
                        Key = file.FileName,
                        BucketName = _bucketName,
                        ContentType = file.ContentType
                    };

                    var fileTransferUtility = new TransferUtility(_awsS3Client);

                    await fileTransferUtility.UploadAsync(uploadRequest);

                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> DeleteFileAsync(string fileName, string versionId)
        {

            try
            {
                DeleteObjectRequest request = new DeleteObjectRequest
                {
                    BucketName = _bucketName,
                    Key = fileName
                };
                if (!string.IsNullOrEmpty(versionId))
                    request.VersionId = versionId;
                await _awsS3Client.DeleteObjectAsync(request);

                return true;

            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
