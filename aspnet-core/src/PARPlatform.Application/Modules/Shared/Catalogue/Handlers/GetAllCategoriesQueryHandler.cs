﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.MeasuringTools;
using PARPlatform.Modules.Shared.Catalogue.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Shared.Catalogue.Handlers
{
    /// <summary>
    /// Used to handle query Categories
    /// </summary>
    public class GetAllCategoriesQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllCategoriesQuery, List<DropdownItemDto>>
    {
        private readonly IQuestionCategoryRepository _questionCategoryRepository;
        private readonly IFormRepository _formRepository;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="questionCategoryRepository"></param>
        public GetAllCategoriesQueryHandler(IQuestionCategoryRepository questionCategoryRepository, IFormRepository formRepository)
        {
            _questionCategoryRepository = questionCategoryRepository;
            _formRepository = formRepository;
        }

        /// <summary>
        /// Get all list of categories or Special Distinction
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<DropdownItemDto>> Handle(GetAllCategoriesQuery request, CancellationToken cancellationToken)
        {
            if (request.Use == Constants.USE_SPECIAL_DISTINCTION)
            {
                var specialDistinction = await _formRepository.GetAllListAsync(e => !e.IsDeleted && e.FormNameId == Constants.SPECIAL_DISTINTION);

                return ObjectMapper.Map(specialDistinction, new List<DropdownItemDto>());
            }
            else
            {
                var categories = await _questionCategoryRepository.GetAllListAsync(e => !e.IsDeleted);

                return ObjectMapper.Map(categories, new List<DropdownItemDto>());
            }
        }
    }
}
