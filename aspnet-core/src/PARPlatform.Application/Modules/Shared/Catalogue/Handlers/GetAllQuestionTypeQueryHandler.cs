﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Shared.Catalogue.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Shared.Catalogue.Handlers
{
    /// <summary>
    /// Used to handle query Question Type
    /// </summary>
    public class GetAllQuestionTypeQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllQuestionTypeQuery, List<DropdownItemDto>>
    {
        private readonly IQuestionTypeRepository _questionTypeRepository;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="formTypeRepository"></param>
        public GetAllQuestionTypeQueryHandler(IQuestionTypeRepository questionTypeRepository)
        {
            _questionTypeRepository = questionTypeRepository;
        }

        /// <summary>
        /// Get all list of question types
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<DropdownItemDto>> Handle(GetAllQuestionTypeQuery request, CancellationToken cancellationToken)
        {
            var questionTypes = await _questionTypeRepository.GetAllListAsync(e => !e.IsDeleted);

            return ObjectMapper.Map(questionTypes, new List<DropdownItemDto>());
        }
    }
}
