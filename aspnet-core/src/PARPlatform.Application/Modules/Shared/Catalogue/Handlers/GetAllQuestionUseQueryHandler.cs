﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Shared.Catalogue.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Shared.Catalogue.Handlers
{
    /// <summary>
    /// Used to handle query Questions Use
    /// </summary>
    public class GetAllQuestionUseQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllQuestionUseQuery, List<DropdownItemDto>>
    {
        private readonly IUsageRepository _usageRepository;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="formTypeRepository"></param>
        public GetAllQuestionUseQueryHandler(IUsageRepository usageRepository)
        {
            _usageRepository = usageRepository;
        }

        /// <summary>
        /// Get all list of questions use
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<DropdownItemDto>> Handle(GetAllQuestionUseQuery request, CancellationToken cancellationToken)
        {
            List<Usage> questionsUse = await _usageRepository.GetAllListAsync(e => !e.IsDeleted);

            return ObjectMapper.Map(questionsUse, new List<DropdownItemDto>());
        }
    }
}
