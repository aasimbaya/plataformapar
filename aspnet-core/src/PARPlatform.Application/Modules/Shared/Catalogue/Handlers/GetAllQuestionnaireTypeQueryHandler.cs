﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Shared.Catalogue.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Shared.Catalogue.Handlers
{
    /// <summary>
    /// Used to handle query Questionnaire Type
    /// </summary>
    public class GetAllQuestionnaireTypeQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllQuestionnaireTypeQuery, List<DropdownItemDto>>
    {
        private readonly IFormTypeRepository _formTypeRepository;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="questionTypeRepository"></param>
        public GetAllQuestionnaireTypeQueryHandler(IFormTypeRepository formTypeRepository)
        {
            _formTypeRepository = formTypeRepository;
        }

        /// <summary>
        /// Get all list of Questionnaire types
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<DropdownItemDto>> Handle(GetAllQuestionnaireTypeQuery request, CancellationToken cancellationToken)
        {
            var questionnaireTypes = await _formTypeRepository.GetAllListAsync(e => !e.IsDeleted);

            return ObjectMapper.Map(questionnaireTypes, new List<DropdownItemDto>());
        }
    }
}
