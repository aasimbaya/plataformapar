﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Shared.Catalogue.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Shared.Catalogue.Handlers
{
    /// <summary>
    /// Used to handle query Score Type
    /// </summary>
    public class GetAllScoreTypeQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllScoreTypeQuery, List<DropdownItemDto>>
    {
        /// <summary>
        /// Main Constructor
        /// </summary>
        public GetAllScoreTypeQueryHandler()
        {

        }

        /// <summary>
        /// Get all list of score types
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<List<DropdownItemDto>> Handle(GetAllScoreTypeQuery request, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
    }
}
