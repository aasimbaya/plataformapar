﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Shared.Catalogue.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Shared.Catalogue.Handlers
{
    /// <summary>
    /// Used to handle query Thematics
    /// </summary>
    public class GetAllThematicsQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllThematicsQuery, List<DropdownItemDto>>
    {
        private readonly IFormThematicRepository _formThematicRepository;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="formThematicRepository"></param>
        public GetAllThematicsQueryHandler(IFormThematicRepository formThematicRepository)
        {
            _formThematicRepository = formThematicRepository;
        }

        /// <summary>
        /// Get all list of thematics
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<DropdownItemDto>> Handle(GetAllThematicsQuery request, CancellationToken cancellationToken)
        {
            var thematics = await _formThematicRepository.GetAllListAsync(e => !e.IsDeleted);

            return ObjectMapper.Map(thematics, new List<DropdownItemDto>());
        }
    }
}
