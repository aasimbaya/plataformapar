﻿using Abp.Collections.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Shared.Catalogue.Queries;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Shared.Catalogue.Handlers
{
    public class GetEnumerablesBySubtypeQueryHandler : UseCaseServiceBase, IRequestHandler<GetEnumerablesBySubtypeQuery, List<EnumDictionaryDataDto>>
    {
        private readonly IEnumDictionaryRepository _enumDictionaryRepository;

        /// <summary>
        /// Main Cosntructor
        /// </summary>
        /// <param name="enumDictionaryRepository"></param>
        public GetEnumerablesBySubtypeQueryHandler(IEnumDictionaryRepository enumDictionaryRepository)
        {
            _enumDictionaryRepository = enumDictionaryRepository;
        }

        /// <summary>
        /// Get all records of enumDictionary by subtype
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<EnumDictionaryDataDto>> Handle(GetEnumerablesBySubtypeQuery query, CancellationToken cancellationToken)
        {
            List<EnumDictionary> dbQuery = await _enumDictionaryRepository.GetAllListAsync(e => e.IsActive && !e.IsDeleted);
            List<EnumDictionary> enums = dbQuery
                .WhereIf(!string.IsNullOrWhiteSpace(query.Subtype), e => e.Subtype.Equals(query.Subtype))
                .ToList();

            return ObjectMapper.Map(enums, new List<EnumDictionaryDataDto>());
        }
    }
}
