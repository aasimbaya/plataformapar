﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System.Collections.Generic;

namespace PARPlatform.Modules.Shared.Catalogue.Queries
{
    /// <summary>
    /// Used to get Categories
    /// </summary>
    public class GetAllCategoriesQuery : IRequest<List<DropdownItemDto>>
    {
        public long Use { get; set; }

        public GetAllCategoriesQuery(long use)
        {
            Use = use;
        }
    }
}
