﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Shared.Catalogue.Queries
{
    /// <summary>
    /// Used to get Question Types
    /// </summary>
    public class GetAllQuestionTypeQuery : IRequest<List<DropdownItemDto>>
    {
    }
}
