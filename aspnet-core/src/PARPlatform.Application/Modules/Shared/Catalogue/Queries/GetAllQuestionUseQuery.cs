﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using System.Collections.Generic;

namespace PARPlatform.Modules.Shared.Catalogue.Queries
{
    /// <summary>
    /// Used to get Question Use
    /// </summary>
    public class GetAllQuestionUseQuery : IRequest<List<DropdownItemDto>>
    {
    }
}
