﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Shared.Catalogue.Queries
{
    /// <summary>
    /// Used to get Questionnaire Types
    /// </summary>
    public class GetAllQuestionnaireTypeQuery : IRequest<List<DropdownItemDto>>
    {
    }
}
