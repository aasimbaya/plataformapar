﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Shared.Catalogue.Queries
{
    /// <summary>
    /// Used to get Score types
    /// </summary>
    public class GetAllScoreTypeQuery : IRequest<List<DropdownItemDto>>
    {
    }
}
