﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Shared.Catalogue.Queries
{
    /// <summary>
    /// Used to get Thematics
    /// </summary>
    public class GetAllThematicsQuery : IRequest<List<DropdownItemDto>>
    {
    }
}
