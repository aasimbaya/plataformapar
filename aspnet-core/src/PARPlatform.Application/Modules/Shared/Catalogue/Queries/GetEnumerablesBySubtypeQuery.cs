﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Shared.Catalogue.Queries
{
    public class GetEnumerablesBySubtypeQuery : IRequest<List<EnumDictionaryDataDto>>
    {
        public string Subtype { get; set; }

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="name"></param>
        public GetEnumerablesBySubtypeQuery(string subtype)
        {
            Subtype = subtype;
        }
    }
}
