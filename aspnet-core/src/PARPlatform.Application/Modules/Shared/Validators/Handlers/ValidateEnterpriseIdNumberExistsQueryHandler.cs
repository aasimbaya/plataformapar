﻿using Abp.Domain.Uow;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Shared.Validators.Queries;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Shared.Validators.Handlers
{
    /// <summary>
    /// Validate if Enterprise Id number exist Query Handler
    /// </summary>
    public class ValidateEnterpriseIdNumberExistsQueryHandler : UseCaseServiceBase, IRequestHandler<ValidateEnterpriseIdNumberExistsQuery, bool>
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        /// <summary>
        /// Main Constructor
        /// </summary>
        public ValidateEnterpriseIdNumberExistsQueryHandler(IEnterpriseRepository enterpriseRepository)
        {
            _enterpriseRepository = enterpriseRepository;
        }

        /// <summary>
        /// Validate if entreprise id number exists case of use
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(ValidateEnterpriseIdNumberExistsQuery query, CancellationToken cancellationToken)
        {
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            PAREntities.Enterprise existingIdNumber = await _enterpriseRepository.FirstOrDefaultAsync(p => p.IdNumber.Equals(query.IdNumber));
            return existingIdNumber != null;
        }
    }
}
