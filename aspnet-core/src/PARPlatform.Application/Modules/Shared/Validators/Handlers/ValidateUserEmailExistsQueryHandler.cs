﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using MediatR;
using PARPlatform.Authorization.Users;
using PARPlatform.Modules.Shared.Validators.Queries;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Shared.Validators.Handlers
{
    /// <summary>
    /// Validate if emailAddress exist Query Handler
    /// </summary>
    public class ValidateUserEmailExistsQueryHandler : UseCaseServiceBase, IRequestHandler<ValidateUserEmailExistsQuery, bool>
    {
        private readonly IRepository<User, long> _userRepository;

        /// <summary>
        /// Main Constructor
        /// </summary>
        public ValidateUserEmailExistsQueryHandler(IRepository<User, long> userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>
        /// Validate if user email exists case of use
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> Handle(ValidateUserEmailExistsQuery query, CancellationToken cancellationToken)
        {
            CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant);
            User existingUser = await _userRepository.FirstOrDefaultAsync(p => p.EmailAddress.Equals(query.EmailAddress));
            return existingUser != null;
        }
    }
}
