﻿using MediatR;

namespace PARPlatform.Modules.Shared.Validators.Queries
{
    /// <summary>
    /// Validate if Enterprise Id number exist Query
    /// </summary>
    public class ValidateEnterpriseIdNumberExistsQuery : IRequest<bool>
    {
        public string IdNumber { get; set; }

        /// <summary>
        /// Main Cosntructor
        /// </summary>
        public ValidateEnterpriseIdNumberExistsQuery(string idNumber)
        {
            IdNumber = idNumber;
        }
    }
}
