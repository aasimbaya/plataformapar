﻿using MediatR;

namespace PARPlatform.Modules.Shared.Validators.Queries
{
    /// <summary>
    /// Validate if emailAddress exist Query
    /// </summary>
    public class ValidateUserEmailExistsQuery : IRequest<bool>
    {
        public string EmailAddress { get; set; }

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="name"></param>
        public ValidateUserEmailExistsQuery(string emailAddress)
        {
            EmailAddress = emailAddress;
        }
    }
}
