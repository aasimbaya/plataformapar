﻿using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Tenants.Queries;
using PARPlatform.MultiTenancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.Tenants.Handlers
{
    /// <summary>
    /// Handler to get all tenants
    /// </summary>
    public class GetAllTenantQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllTenantQuery, List<DropdownItemDto>>
    {
        private readonly IRepository<Tenant> _tenantRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="tenantRepository"></param>
        public GetAllTenantQueryHandler(IRepository<Tenant> tenantRepository)
        {
            _tenantRepository = tenantRepository;
        }

        public async Task<List<DropdownItemDto>> Handle(GetAllTenantQuery query, CancellationToken cancellationToken)
        {
            return _tenantRepository.GetAll()
                .Select(t => new DropdownItemDto { Id = t.Id, Name = t.Name }).ToList();
        }
    }
}
