﻿using MediatR;
using PARPlatform.Base;
using System.Threading;
using System.Threading.Tasks;
using PARPlatform.MultiTenancy;
using PARPlatform.Modules.Tenants.Dtos;
using System.Linq;
using PARPlatform.Modules.Tenants.Queries;

namespace PARPlatform.Modules.Tenants.Handlers
{
    /// <summary>
    /// Used to handle Query of Get Tenant By Name
    /// </summary>
    public class GetTenantByNameQueryHandler : UseCaseServiceBase, IRequestHandler<GetTenantByNameQuery, UpsertTenantDto>
    {
        private readonly IRepository<Tenant> _tenantRepository;

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="tenantRepository"></param>
        public GetTenantByNameQueryHandler(IRepository<Tenant> tenantRepository)
        {
            _tenantRepository = tenantRepository;
        }

        /// <summary>
        /// Handles request of tenant
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpsertTenantDto> Handle(GetTenantByNameQuery query, CancellationToken cancellationToken)
        {
            UpsertTenantDto tenantDto = _tenantRepository.GetAll()
                .Select(t => new UpsertTenantDto { Id = t.Id, Name = t.Name })
                .SingleOrDefault(p => p.Name == query.Name);
            return tenantDto;
        }
    }
}
