﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.Tenants.Queries
{
    /// <summary>
    /// Query to get all tenants
    /// </summary>
    public class GetAllTenantQuery : IRequest<List<DropdownItemDto>>
    {
    }
}
