﻿using MediatR;
using PARPlatform.Modules.Tenants.Dtos;

namespace PARPlatform.Modules.Tenants.Queries
{
    /// <summary>
    /// Get Tenant By Name Query
    /// </summary>
    public class GetTenantByNameQuery : IRequest<UpsertTenantDto>
    {
        public string Name { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="name"></param>
        public GetTenantByNameQuery(string name)
        {
            Name = name;
        }
    }
}
