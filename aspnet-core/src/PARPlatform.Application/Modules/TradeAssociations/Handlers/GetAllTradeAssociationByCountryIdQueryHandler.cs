﻿using Abp.Application.Services.Dto;
using MediatR;
using PARPlatform.Base;
using PARPlatform.PAREntities;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.TradeAssociations.Queries;

namespace PARPlatform.Modules.TradeAssociations.Handlers
{
    /// <summary>
    /// Get all trade associations by country Id
    /// </summary> 
    public class GetAllTradeAssociationByCountryIdQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllTradeAssociationByCountryIdQuery, List<DropdownItemDto>>
    {
        private readonly IRepository<TradeAssociation> _tradeAssociationRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="tradeAssociationRepository"></param>
        public GetAllTradeAssociationByCountryIdQueryHandler(IRepository<TradeAssociation> tradeAssociationRepository)
        {
            _tradeAssociationRepository = tradeAssociationRepository;
        }
        public async Task<List<DropdownItemDto>> Handle(GetAllTradeAssociationByCountryIdQuery query, CancellationToken cancellationToken)
        {

            IQueryable<DropdownItemDto> tradeAssociations = _tradeAssociationRepository.GetAll()
                .Where(p => p.CountryId == query.CountryId)
                .Select(t => new DropdownItemDto { Id = t.Id, Name = t.Name })
                .OrderBy(p => p.Name);

            return tradeAssociations.ToList();
        }
    }
}

