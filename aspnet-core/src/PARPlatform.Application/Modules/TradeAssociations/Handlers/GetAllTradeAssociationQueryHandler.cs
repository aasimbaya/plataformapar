﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Collections.Extensions;
using MediatR;
using PARPlatform.Base;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.TradeAssociations.Queries;
using PARPlatform.PAREntities;
using System.Threading;
using System.Threading.Tasks;

namespace PARPlatform.Modules.TradeAssociations.Handlers
{
    /// <summary>
    /// Handler to get all trade associations
    /// </summary>
    public class GetAllTradeAssociationQueryHandler : UseCaseServiceBase, IRequestHandler<GetAllTradeAssociationQuery, List<DropdownItemDto>>
    {
        private readonly IRepository<TradeAssociation> _tradeAssociationRepository;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="tradeAssociationRepository"></param>
        public GetAllTradeAssociationQueryHandler(IRepository<TradeAssociation> tradeAssociationRepository)
        {
            _tradeAssociationRepository = tradeAssociationRepository;
        }

        public async Task<List<DropdownItemDto>> Handle(GetAllTradeAssociationQuery query, CancellationToken cancellationToken)
        {
            return _tradeAssociationRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(query.Filter), t =>
                (t.Name.Contains(query.Filter) || t.Id == query.Id) &&
                t.CountryId == query.CountryId
                )
                .Select(t => new DropdownItemDto { Id = t.Id, Name = t.Name }).ToList();
        }
    }
}


