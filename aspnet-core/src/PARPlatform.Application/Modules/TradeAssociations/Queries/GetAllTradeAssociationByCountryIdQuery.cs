﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.TradeAssociations.Queries
{
    /// <summary>
    /// Get trade associations by filter
    /// </summary>
    public class GetAllTradeAssociationByCountryIdQuery : IRequest<List<DropdownItemDto>>
    {
        public long CountryId { get; set; }
        public string Filter { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="id"></param>
        public GetAllTradeAssociationByCountryIdQuery(long countryId)
        {
            CountryId = countryId;
        }
    }
}

