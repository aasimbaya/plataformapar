﻿using MediatR;
using PARPlatform.Modules.Common.Dto;
using System.Collections.Generic;

namespace PARPlatform.Modules.TradeAssociations.Queries
{
    /// <summary>
    /// Query to get all trade associations
    /// </summary>
    public class GetAllTradeAssociationQuery : IRequest<List<DropdownItemDto>>
    {
        public string Filter { get; set; }
        public int Id { get; set; }
        public long CountryId { get; set; }
    }
}
