﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using PARPlatform.MultiTenancy.Accounting.Dto;

namespace PARPlatform.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
