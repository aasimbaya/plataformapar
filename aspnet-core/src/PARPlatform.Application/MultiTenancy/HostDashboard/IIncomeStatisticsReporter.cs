﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PARPlatform.MultiTenancy.HostDashboard.Dto;

namespace PARPlatform.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}