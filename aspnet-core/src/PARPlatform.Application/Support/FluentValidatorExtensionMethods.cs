﻿using System.Collections.Generic;
using System.Linq;
using Abp.Runtime.Validation;
using Abp.UI;
using FluentValidation.Results;

namespace PARPlatform.Support
{
    /// <summary>
    /// Methods extensions related with Fluent Validation
    /// </summary>
    public static class FluentValidatorExtensionMethods
    {
        /// <summary>
        /// Convert FluentValidation validations to the <see cref="AbpValidationException"/> needed by the frontend.
        /// </summary>
        /// <param name="result"></param>
        /// <exception cref="AbpValidationException"></exception>
        public static void ThrowIfAreErrors(this ValidationResult result)
        {
            var standardValidationResult = new List<System.ComponentModel.DataAnnotations.ValidationResult>();

            foreach (ValidationFailure error in result.Errors)
            {
                if (error.CustomState is UserFriendlyException state)
                {
                    standardValidationResult.Add(new System.ComponentModel.DataAnnotations.ValidationResult(state.Message));
                }
                else
                {
                    standardValidationResult.Add(new System.ComponentModel.DataAnnotations.ValidationResult(error.ErrorMessage));
                }
            }

            if (standardValidationResult.Any())
            {
                throw new AbpValidationException(string.Empty, standardValidationResult);
            }
        }
    }
}
