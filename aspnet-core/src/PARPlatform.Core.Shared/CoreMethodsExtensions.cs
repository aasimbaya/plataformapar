﻿using Abp.Dependency;
using Abp.Localization;
using Abp.Runtime.Validation;

namespace PARPlatform
{
    /// <summary>
    /// Global method extensions
    /// </summary>
    public static class CoreMethodExtensions
    {
        /// <summary>
        /// Get localization message from validation context
        /// </summary>
        /// <param name="context"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetLocalizationMessage(this CustomValidationContext context, string key)
        {
            return context.IocResolver.Resolve<ILocalizationManager>().GetString(PARPlatformConsts.LocalizationSourceName, key);
        }

        /// <summary>
        /// Get the localization message using only the key value
        /// </summary>
        /// <param name="IocResolver">Implementation of IOC resolver</param>
        /// <param name="localizationKey">Key to search in the localization file</param>
        /// <returns>Localized Text</returns>
        public static string GetLocalizationMessage(this IIocResolver IocResolver, string localizationKey)
        {
            return IocResolver.Resolve<ILocalizationManager>().GetString(PARPlatformConsts.LocalizationSourceName, localizationKey);
        }
    }
}
