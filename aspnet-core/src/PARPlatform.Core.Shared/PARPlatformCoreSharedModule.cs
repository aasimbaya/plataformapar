﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace PARPlatform
{
    public class PARPlatformCoreSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PARPlatformCoreSharedModule).GetAssembly());
        }
    }
}