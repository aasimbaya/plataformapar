﻿using Abp.Authorization;
using PARPlatform.Authorization.Roles;
using PARPlatform.Authorization.Users;

namespace PARPlatform.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
