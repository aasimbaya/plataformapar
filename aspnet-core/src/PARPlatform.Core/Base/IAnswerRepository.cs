﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific Answer Repository interface
    /// </summary>
    public interface IAnswerRepository : Abp.Domain.Repositories.IRepository<Answer, long>
    {

    }
}
