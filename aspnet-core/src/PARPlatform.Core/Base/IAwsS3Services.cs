﻿using Abp.Application.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Base
{
    public interface IAwsS3Services : IApplicationService
    {
        Task<bool> UploadFileAsync(IFormFile file);

        //Task DeleteFileAsync(string fileName, string versionId = "");
    }
}
