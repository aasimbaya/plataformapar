﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific Document Estructure Repository interface
    /// </summary>
    public interface IDocumentEstructureRepository : Abp.Domain.Repositories.IRepository<DocumentEstructure, long>
    {
    }
}
