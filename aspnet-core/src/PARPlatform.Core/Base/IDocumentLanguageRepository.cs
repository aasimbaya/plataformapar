﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific Document Language Repository interface
    /// </summary>
    public interface IDocumentLanguageRepository : Abp.Domain.Repositories.IRepository<DocumentLanguage, long>
    {
    }
}
