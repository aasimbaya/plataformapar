﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific Document Location Repository interface
    /// </summary>
    public interface IDocumentLocationRepository : Abp.Domain.Repositories.IRepository<DocumentLocation, long>
    {
    }
}
