﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific Document Repository interface
    /// </summary>
    public interface IDocumentRepository : Abp.Domain.Repositories.IRepository<Document, long>
    {
    }
}
