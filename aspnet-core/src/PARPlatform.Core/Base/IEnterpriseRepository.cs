﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific QuestionsBank Repository interface
    /// </summary>
    public interface IEnterpriseRepository : Abp.Domain.Repositories.IRepository<Enterprise, long>
    {

    }
}
