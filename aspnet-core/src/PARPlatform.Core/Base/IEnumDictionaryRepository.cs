﻿using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific EnumDictionary Repository interface
    /// </summary>
    public interface IEnumDictionaryRepository : Abp.Domain.Repositories.IRepository<EnumDictionary, long>
    {
    }
}
