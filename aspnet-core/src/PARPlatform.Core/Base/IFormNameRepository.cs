﻿using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Base
{
    public interface IFormNameRepository: Abp.Domain.Repositories.IRepository<FormName, long>
    {
    }
}
