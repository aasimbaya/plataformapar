﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific FormQuestionLevel Repository interface
    /// </summary>
    public interface IFormQuestionLevelRepository : Abp.Domain.Repositories.IRepository<FormQuestionLevel, long>
    {

    }
}