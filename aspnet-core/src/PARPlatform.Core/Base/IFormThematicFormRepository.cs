﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    public interface IFormThematicFormRepository : Abp.Domain.Repositories.IRepository<FormThematicForm, long>
    {
    }
}
