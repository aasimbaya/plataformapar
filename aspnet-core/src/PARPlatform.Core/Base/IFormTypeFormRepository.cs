﻿using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Base
{
    public interface IFormTypeFormRepository : Abp.Domain.Repositories.IRepository<FormTypeForm, long>
    {
    }
}
