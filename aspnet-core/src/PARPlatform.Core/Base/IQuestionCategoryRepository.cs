﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific QuestionCategory Repository interface
    /// </summary>
    public interface IQuestionCategoryRepository : Abp.Domain.Repositories.IRepository<QuestionCategory, long>
    {
    }
}
