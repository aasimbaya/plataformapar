﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific QuestionType Repository interface
    /// </summary>
    public interface IQuestionTypeRepository : Abp.Domain.Repositories.IRepository<QuestionType, long>
    {
    }
}
