﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific QuestionsBank Repository interface
    /// </summary>
    public interface IQuestionsBankRepository : Abp.Domain.Repositories.IRepository<FormQuestion, long>
    {

    }
}
