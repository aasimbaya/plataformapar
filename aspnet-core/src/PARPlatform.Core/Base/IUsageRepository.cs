﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific Usage Repository interface
    /// </summary>
    public interface IUsageRepository : Abp.Domain.Repositories.IRepository<Usage, long>
    {
    }
}
