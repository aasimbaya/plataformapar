﻿using PARPlatform.PAREntities;

namespace PARPlatform.Base
{
    /// <summary>
    /// Specific Validation Type Repository
    /// </summary>
    public interface IValidationTypeRepository : Abp.Domain.Repositories.IRepository<ValidationType, long>
    {
    }
}
