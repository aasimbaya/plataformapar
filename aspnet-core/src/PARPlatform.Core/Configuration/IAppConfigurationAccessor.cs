﻿using Microsoft.Extensions.Configuration;

namespace PARPlatform.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
