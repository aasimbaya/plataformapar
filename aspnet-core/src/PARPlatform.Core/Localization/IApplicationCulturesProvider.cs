﻿using System.Globalization;

namespace PARPlatform.Localization
{
    public interface IApplicationCulturesProvider
    {
        CultureInfo[] GetAllCultures();
    }
}