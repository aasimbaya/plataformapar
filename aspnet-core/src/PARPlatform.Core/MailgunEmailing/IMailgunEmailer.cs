﻿using Abp.Application.Services;
using RestSharp;
using System.Threading.Tasks;

namespace PARPlatform.MailgunEmailing
{
    public interface IMailgunEmailer : IApplicationService
    {
        /// <summary>
        /// Allow send Emails using Mailgun API Rest
        /// </summary>
        /// <param name="to">Recipient(s) comma or semicolon separated string</param>
        /// <param name="subject">Subject text</param>
        /// <param name="message">Message body text</param>
        /// <returns></returns>
        Task<IRestResponse> SendAsync(string to, string subject, string message);
    }
}
