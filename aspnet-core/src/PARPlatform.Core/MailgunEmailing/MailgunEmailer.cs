﻿using Microsoft.Extensions.Configuration;
using PARPlatform.Configuration;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PARPlatform.MailgunEmailing
{
    public class MailgunEmailer : IMailgunEmailer
    {
        private readonly RestClient _client;
        private readonly IConfigurationRoot _appConfiguration;

        private readonly string Url;
        private readonly string ApiKey;
        private readonly string Domain;
        private readonly string SenderAccount;

        public MailgunEmailer(IAppConfigurationAccessor configurationAccessor)
        {
            _appConfiguration = configurationAccessor.Configuration;

            Url = _appConfiguration["Mailgun:Url"];
            ApiKey = _appConfiguration["Mailgun:ApiKey"];
            Domain = _appConfiguration["Mailgun:Domain"];
            SenderAccount = _appConfiguration["Mailgun:SenderAccount"];

            _client = new RestClient();
            _client.BaseUrl = new Uri(Url);
            _client.Authenticator = new HttpBasicAuthenticator("api", ApiKey);
        }

        public Task<IRestResponse> SendAsync(string to, string subject, string message)
        {            
            RestRequest request = new RestRequest();
            request.AddParameter("domain", Domain, ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", SenderAccount);            
            to = to.Replace(',', ';');
            var receiverList = to.Split(';').ToList();
            foreach (var receiver in receiverList)
            {
                request.AddParameter("to", receiver);
            }
            request.AddParameter("subject", Regex.Replace(subject, @"\t|\n|\r", ""));
            request.AddParameter("html", message);
            request.Method = Method.POST;
            return _client.ExecuteAsync(request);
        }
    }
}
