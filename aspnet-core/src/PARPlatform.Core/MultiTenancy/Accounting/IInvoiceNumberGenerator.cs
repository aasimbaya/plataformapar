﻿using System.Threading.Tasks;
using Abp.Dependency;

namespace PARPlatform.MultiTenancy.Accounting
{
    public interface IInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}