﻿using Abp.Events.Bus;

namespace PARPlatform.MultiTenancy
{
    public class RecurringPaymentsEnabledEventData : EventData
    {
        public int TenantId { get; set; }
    }
}