using System.Threading.Tasks;

namespace PARPlatform.Net.Sms
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}