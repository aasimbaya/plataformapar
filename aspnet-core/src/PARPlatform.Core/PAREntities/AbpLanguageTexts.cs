﻿using Abp.Domain.Entities.Auditing;
using Abp.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.PAREntities
{   
    /// <summary>
    /// CommentTypes Entity for PAR Schema
    /// </summary>
    [Table("AbpLanguageTexts", Schema = "dbo")]
    public class AbpLanguageTexts :FullAuditedEntity<long>
    {
        [Required]
        [StringLength(ApplicationLanguageText.MaxSourceNameLength)]
        public string SourceName { get; set; }

        [Required]
        [StringLength(ApplicationLanguage.MaxNameLength)]
        public string LanguageName { get; set; }

        [Required]
        [StringLength(ApplicationLanguageText.MaxKeyLength)]
        public string Key { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(ApplicationLanguageText.MaxValueLength)]
        public string targetValue { get; set; }
    }
}
