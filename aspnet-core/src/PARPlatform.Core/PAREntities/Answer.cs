﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// Answer Entity for PAR Schema
    /// </summary>
    [Table("Answers", Schema = "dbo")]
    [Index(nameof(FormQuestionId), Name = "Answers_FK_FormQuestionId_index")]
    [Index(nameof(ValidationTypeId), Name = "Answers_FK_ValidationTypeId_index")]
    [Index(nameof(PersonalId), Name = "Answers_PersonalId_index")]
    public class Answer : FullAuditedEntity<long>
    {
        [StringLength(150)]
        public string AnswerContent { get; set; }
        public virtual long? FormQuestionId { get; set; }
        [ForeignKey("FormQuestionId")]
        public FormQuestion FormQuestionFK { get; set; }
        public virtual long? ValidationTypeId { get; set; }
        [ForeignKey("ValidationTypeId")]
        public ValidationType ValidationTypeFk { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal Score { get; set; }
        public bool IsActive { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal? Percentage { get; set; }
        public bool AutoNull { get; set; }
        public bool SpecializedResource { get; set; }
        /// <summary>
        /// Unique Answer Identificator for use in formula
        /// </summary>
        [Required]
        [StringLength(50)]
        public string PersonalId { get; set; }
    }
}
