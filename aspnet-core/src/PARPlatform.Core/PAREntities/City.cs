﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// City Entity for PAR Schema
    /// </summary>
    [Table("City", Schema = "dbo")]
    [Index(nameof(RegionId), Name = "City_FK_RegionId_index")]

    public class City : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(10)]
        public string Abreviation { get; set; }
        public bool IsActive { get; set; }
        public virtual long RegionId { get; set; }
        [ForeignKey("RegionId")]
        public Region RegionFk { get; set; }
    }
}