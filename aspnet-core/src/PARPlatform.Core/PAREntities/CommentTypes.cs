﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// CommentTypes Entity for PAR Schema
    /// </summary>
    [Table("CommentTypes", Schema = "dbo")]
    public class CommentTypes : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [Required]
        [StringLength(150)]
        public string Description { get; set; }
        [StringLength(50)]
        public string Type { get; set; }
        public bool IsActive { get; set; }
    }
}
