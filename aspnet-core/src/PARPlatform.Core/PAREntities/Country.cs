﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// Country Entity for PAR Schema
    /// </summary>
    [Table("Country", Schema = "dbo")]
    public class Country : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(10)]
        public string Abreviation { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(10)]
        public string Code { get; set; }
        [Required]
        [StringLength(3)]
        public string Iso3Code { get; set; }
        public int NicRucLength { get; set; }
        public int PhoneLength { get; set; }
    }
}
