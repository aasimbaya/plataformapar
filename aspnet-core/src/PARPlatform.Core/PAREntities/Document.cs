﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// Document Entity for PAR Schema
    /// </summary>
    [Table("Document", Schema = "dbo")]

    public class Document : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        
        [StringLength(10)]
        public string Size { get; set; }

        [StringLength(10)]
        public string Extension { get; set; }

        [Required]
        public virtual long? EstructureId { get; set; }

        [ForeignKey("EstructureId")]
        public DocumentEstructure EstructureFk { get; set; }

        [Required]
        public virtual long? LanguageId { get; set; }

        [ForeignKey("LanguageId")]
        public DocumentLanguage LanguageFk { get; set; }

        [Required]
        public virtual long? LocationId { get; set; }

        [ForeignKey("LocationId")]
        public DocumentLocation LocationFk { get; set; }

        [Required]
        [StringLength(150)]
        public string URL { get; set; }

        /*[Required]
        [StringLength(50)]
        public string Location { get; set; }
        */

        public bool IsActive { get; set; }

    }
}