﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// DocumentEstructure Entity for PAR Schema
    /// </summary>
    [Table("DocumentEstructure", Schema = "dbo")]

    public class DocumentEstructure : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(150)]
        public string Description { get; set; }

        [StringLength(10)]
        public string Abreviation { get; set; }

        [StringLength(50)]
        public string Estructure { get; set; }

        public bool IsActive { get; set; }
       
    }
}