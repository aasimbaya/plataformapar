﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// DocumentLocation Entity for PAR Schema
    /// </summary>
    [Table("DocumentLocation", Schema = "dbo")]

    public class DocumentLocation : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(150)]
        public string Description { get; set; }

        [StringLength(10)]
        public string Abreviation { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        public bool IsActive { get; set; }
       
    }
}