﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using PARPlatform.Authorization.Users;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// Enterprise Entity for PAR Schema
    /// </summary>
    [Table("Enterprise", Schema = "dbo")]
    [Index(nameof(TenantId), Name = "Enterprise_TenantId_index")]
    [Index(nameof(UserId), Name = "Enterprise_FK_UserId_index")]
    [Index(nameof(CountryId), Name = "Enterprise_FK_CountryId_index")]
    [Index(nameof(RegionId), Name = "Enterprise_FK_RegionId_index")]
    [Index(nameof(CityId), Name = "Enterprise_FK_CityId_index")]
    [Index(nameof(SectorId), Name = "Enterprise_FK_SectorId_index")]
    [Index(nameof(LineBusinessId), Name = "Enterprise_FK_LineBusinessId_index")]
    [Index(nameof(TradeAssociationId), Name = "Enterprise_FK_TradeAssociationId_index")]

    public class Enterprise : FullAuditedEntity<long>, IMustHaveTenant
    {
        public int TenantId { get; set; }
        [Required]
        [StringLength(150)]
        public string Name { get; set; }
        [Required]
        [StringLength(150)]
        public string IdNumber { get; set; }
        public bool IsActive { get; set; }
        public virtual long UserId { get; set; }
        [ForeignKey("UserId")]
        public User UserFk { get; set; }
        public virtual long? CountryId { get; set; }
        [ForeignKey("CountryId")]
        public Country CountryFk { get; set; }
        public virtual long? RegionId { get; set; }
        [ForeignKey("RegionId")]
        public Region RegionFk { get; set; }
        public virtual long? CityId { get; set; }
        [ForeignKey("CityId")]
        public City CityFk { get; set; }
        public int EmployeesNumber { get; set; }
        public bool HasInclusion { get; set; }
        public bool IsInternational { get; set; }
        public virtual long? SectorId { get; set; }
        [ForeignKey("SectorId")]
        public Sector SectorFk { get; set; }
        public virtual long? LineBusinessId { get; set; }
        [ForeignKey("LineBusinessId")]
        public LineBusiness LineBusinessFk { get; set; }
        public bool ReceivePromoAccepted { get; set; }
        public bool IsPartOfEconomicGroup { get; set; }
        [StringLength(150)]
        public string EconomicGroupName { get; set; }
        public bool IsPartOfTradeAssociation { get; set; }
        public virtual long? TradeAssociationId { get; set; }
        [ForeignKey("TradeAssociationId")]
        public TradeAssociation TradeAssociationFk { get; set; }
        [StringLength(150)]
        public string InclusionRegionalManagerName { get; set; }
        [StringLength(150)]
        public string InclusionRegionalManagerEmail { get; set; }
        public string EnterpriseTypeId { get; set; }
    }
}