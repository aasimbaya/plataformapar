﻿using Abp.Authorization.Users;
using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// EnterpriseGroupEnterprise Entity for PAR Schema
    /// </summary>
    [Table("EnterpriseGroupEnterprise", Schema = "dbo")]
    [Index(nameof(EnterpriseGroupId), Name = "EnterpriseGroupEnterprise_FK_EnterpriseGroupId_index")]
    [Index(nameof(EnterpriseId), Name = "EnterpriseGroupEnterprise_FK_EnterpriseId_index")]

    public class EnterpriseGroupEnterprise : FullAuditedEntity<long>
    {
        public bool IsActive { get; set; }
        public virtual long? EnterpriseGroupId { get; set; }
        [ForeignKey("EnterpriseGroupId")]
        public EnterpriseGroup EnterpriseGroupFk { get; set; }
        public virtual long? EnterpriseId { get; set; }
        [ForeignKey("EnterpriseId")]
        public Enterprise EnterpriseFk { get; set; }
    }
}
