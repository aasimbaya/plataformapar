﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// EnumDictionary Entity for PAR Schema
    /// </summary>
    [Table("EnumDictionary", Schema = "dbo")]
    [Index(nameof(Subtype), Name = "EnumDictionary_Subtype_index")]
    public class EnumDictionary : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Type { get; set; }
        [Required]
        [StringLength(50)]
        public string Subtype { get; set; }
        [Required]
        [StringLength(10)]
        public string Abbreviation { get; set; }
        public bool IsActive { get; set; }
    }
}
