﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// Form Entity form PAR Schema
    /// </summary>
    [Table("Form", Schema = "dbo")]
    [Index(nameof(FormNameId), Name = "Form_FK_FormNameId_index")]
    public class Form : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public virtual long? FormNameId { get; set; }
        [ForeignKey("FormNameId")]
        public FormName FormNameFk { get; set; }
        [Required]
        [StringLength(150)]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [StringLength(150)]
        public string SpecialDistinctionIdArray { get; set; }
        [StringLength(150)]
        public string QuestionCategoryIdArray { get; set; }

    }
}