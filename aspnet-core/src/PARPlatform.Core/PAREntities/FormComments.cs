﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// FormComments Entity for PAR Schema
    /// </summary>
    [Table("FormComments", Schema = "dbo")]
    [Index(nameof(CommentTypeId), Name = "FormComments_FK_CommentTypeId_index")]
    public class FormComments : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public virtual long? CommentTypeId { get; set; }
        [ForeignKey("CommentTypeId")]
        public CommentTypes CommentTypesFk { get; set; }
        [Required]
        [StringLength(150)]
        public string Text { get; set; }
        public bool IsActive { get; set; }
    }
}