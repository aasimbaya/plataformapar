﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// FormQuestions Entity for PAR Schema
    /// </summary>
    [Table("FormQuestions", Schema = "dbo")]
    [Index(nameof(FormTypeId), Name = "FormQuestions_FK_FormTypeId_index")]
    [Index(nameof(FormThematicId), Name = "FormQuestions_FK_FormThematicId_index")]
    [Index(nameof(FormId), Name = "FormQuestions_FK_FormId_index")]
    [Index(nameof(QuestionTypeId), Name = "FormQuestions_FK_QuestionTypeId_index")]
    [Index(nameof(QuestionCategoryId), Name = "FormQuestions_FK_QuestionCategoryId_index")]
    [Index(nameof(UsageId), Name = "FormQuestions_FK_UsageId_index")]
    [Index(nameof(ParentId), Name = "FormQuestions_FK_ParentId_index")]
    public class FormQuestion : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(150)]
        public string ShortVersion { get; set; }
        public virtual long? FormTypeId { get; set; }
        [ForeignKey("FormTypeId")]
        public FormType FormTypeFk { get; set; }
        public virtual long? FormThematicId { get; set; }
        [ForeignKey("FormThematicId")]
        public FormThematic FormThematicFk { get; set; }
        public virtual long? QuestionTypeId { get; set; }
        [ForeignKey("QuestionTypeId")]
        public QuestionType QuestionTypeFk { get; set; }
        public virtual long? QuestionCategoryId { get; set; }
        [ForeignKey("QuestionCategoryId")]
        public QuestionCategory QuestionCategoryFk { get; set; }
        public virtual long? UsageId { get; set; }
        [ForeignKey("UsageId")]
        public Usage UsageFk { get; set; }
        public virtual long? FormId { get; set; }
        [ForeignKey("FormId")]
        public Form FormFk { get; set; }
        public virtual long? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public FormQuestion FormQuestionFK { get; set; }
        [Required]
        [StringLength(150)]
        public string Question { get; set; }
        [StringLength(80)]
        public string Tooltip { get; set; }
        [StringLength(80)]
        public string FixedText { get; set; }
        [Required]
        [StringLength(50)]
        public string Tag { get; set; }
        public bool IsActive { get; set; }
        public List<long> ConditionalParentIds { get; set; }
        [StringLength(150)]
        public string ImplementationTip { get; set; }
        public bool IsNullable { get; set; }
        public bool IsCertified { get; set; }
        public bool IsPriority { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal Score { get; set; }
        [StringLength(150)]
        public string Formula { get; set; }
        public long ParentValidation { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal TotalScore { get; set; }

    }
}