﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// FormQuestionLevel Entity form PAR Schema
    /// </summary>
    [Table("FormQuestionLevel", Schema = "dbo")]
    [Index(nameof(FormQuestionId), Name = "FormQuestionLevel_FK_FormQuestionId_index")]
    public class FormQuestionLevel : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [Required]
        [StringLength(150)]
        public string Description { get; set; }
        public virtual long? FormQuestionId { get; set; }
        [ForeignKey("FormQuestionId")]
        public FormQuestion FormQuestionFk { get; set; }
        public List<long> ChildrenQuestion { get; set; }
        public bool IsActive { get; set; }
    }
}
