﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// QuestionType Entity for PAR Schema
    /// </summary>
    [Table("FormResults", Schema = "dbo")]
    [Index(nameof(FormId), Name = "FormResults_FK_FormId_index")]
    [Index(nameof(FormCommentId), Name = "FormResults_FK_FormCommentId_index")]
    public class FormResults : FullAuditedEntity<long>
    {
        [Column(TypeName = "decimal(10, 2)")]
        public decimal TotalPoints { get; set; }

        public virtual long? FormCommentId { get; set; }
        [ForeignKey("FormCommentId")]
        public FormComments FormCommentsFk { get; set; }

        public virtual long? FormId { get; set; }
        [ForeignKey("FormId")]
        public Form FormFk { get; set; }

        [Column(TypeName = "decimal(10, 2)")]
        public decimal CompanyPoints { get; set; }

        public bool IsActive { get; set; }
    }
}