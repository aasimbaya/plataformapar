﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// FormStatusCompany Entity for PAR Schema
    /// </summary>
    [Table("FormStatusCompany", Schema = "dbo")]
    [Index(nameof(FormId), Name = "FormStatusCompany_FK_FormId_index")]
    [Index(nameof(FormStatusId), Name = "FormStatusCompany_FK_FormStatusId_index")]
    public class FormStatusCompany : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        public virtual long? FormStatusId { get; set; }
        [ForeignKey("FormStatusId")]
        public FormStatus FormStatusFk { get; set; }
        public virtual long? FormId { get; set; }
        [ForeignKey("FormId")]
        public Form FormFk { get; set; }
        [Required]
        [StringLength(150)]
        public string Description { get; set; }
        [Required]
        [StringLength(80)]
        public string Tooltip { get; set; }
        [Required]
        [StringLength(50)]
        public string Tag { get; set; }
        public bool IsActive { get; set; }
    }
}
