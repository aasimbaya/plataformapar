﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// FormThematic Entity for PAR Schema
    /// </summary>
    [Table("FormThematic", Schema = "dbo")]
    public class FormThematic : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [Required]
        [StringLength(150)]
        public string Description { get; set; }
        [StringLength(50)]
        public string Type { get; set; }
        public bool IsActive { get; set; }
    }
}
