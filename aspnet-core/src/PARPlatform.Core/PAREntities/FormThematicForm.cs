﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// FormThematicForm Entity for PAR Schema
    /// </summary>
    [Table("FormThematicForm", Schema = "dbo")]
    [Index(nameof(FormId), Name = "FormThematicForm_FK_FormId_index")]
    [Index(nameof(FormThematicId), Name = "FormThematicForm_FK_FormThematicId_index")]
    public class FormThematicForm : FullAuditedEntity<long>
    {   
        public virtual long? FormId { get; set; }
        [ForeignKey("FormId")]
        public Form FormFk { get; set; }

        public virtual long? FormThematicId { get; set; }
        [ForeignKey("FormThematicId")]
        public FormThematic FormThematicFk { get; set; }

        public bool IsActive { get; set; }
    }
}