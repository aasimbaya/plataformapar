﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// FormType Entity for PAR Schema (e.g.: Ranking PAR)
    /// </summary>
    [Table("FormType", Schema = "dbo")]
    public class FormType : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [Required]
        [StringLength(150)]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(50)]
        public string Type { get; set; }
    }
}
