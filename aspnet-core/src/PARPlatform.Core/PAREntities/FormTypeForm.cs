﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// FormTypeForm Entity for PAR Schema
    /// </summary>
    [Table("FormTypeForm", Schema = "dbo")]
    [Index(nameof(FormId), Name = "FormTypeForm_FK_FormId_index")]
    [Index(nameof(FormTypeId), Name = "FormTypeForm_FK_FormTypeId_index")]
    public class FormTypeForm : FullAuditedEntity<long>
    {
        public virtual long? FormId { get; set; }
        [ForeignKey("FormId")]
        public Form FormFk { get; set; }

        public virtual long? FormTypeId { get; set; }
        [ForeignKey("FormTypeId")]
        public FormType FormTypeFk { get; set; }

        public bool IsActive { get; set; }
    }
}