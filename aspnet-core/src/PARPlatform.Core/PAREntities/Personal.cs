﻿
using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// Personal Entity for PAR Schema
    /// </summary>
    [Table("Personal", Schema = "dbo")]
    [Index(nameof(PersonalTypeId), Name = "Personal_FK_PersonalTypeId_index")]
    [Index(nameof(EnterpriseId), Name = "Personal_FK_EnterpriseId_index")]
    [Index(nameof(PositionId), Name = "Personal_FK_PositionId_index")]
    public class Personal : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(150)]
        public string Name { get; set; }
        [Required]
        [StringLength(80)]
        public string Email { get; set; }
        [StringLength(50)]
        public string CellPhone { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public virtual long? PersonalTypeId { get; set; }
        [ForeignKey("PersonalTypeId")]
        public PersonalType PersonalTypeFk { get; set; }
        public virtual long? EnterpriseId { get; set; }
        [ForeignKey("EnterpriseId")]
        public Enterprise EnterpriseFk { get; set; }
        public virtual long? PositionId { get; set; }
        [ForeignKey("PositionId")]
        public Position PositionFk { get; set; }
    }
}
