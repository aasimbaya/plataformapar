﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// QuestionCategory Entity for PAR Schema
    /// </summary>
    [Table("QuestionCategory", Schema = "dbo")]
    [Index(nameof(QuestionCategoryTypeId), Name = "QuestionCategory_FK_QuestionCategoryTypeId_index")]

    public class QuestionCategory : FullAuditedEntity<long>,  ICreationAudited, IHasCreationTime, IModificationAudited, IHasModificationTime, IDeletionAudited, IHasDeletionTime, ISoftDelete
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public virtual long? QuestionCategoryTypeId { get; set; }
        [ForeignKey("QuestionCategoryTypeId")]
        public QuestionCategoryType QuestionCategoryTypeFk { get; set; }
        [StringLength(250)]
        public string Icon { get; set; }
        public long? FormId { get; set; }
    }
}