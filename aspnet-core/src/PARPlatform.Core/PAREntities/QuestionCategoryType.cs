﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// QuestionCategory Entity for PAR Schema
    /// </summary>
    [Table("QuestionCategoryType", Schema = "dbo")]
    public class QuestionCategoryType : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
    }
}
