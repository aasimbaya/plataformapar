﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// QuestionType Entity for PAR Schema  (e.g.: Boolean(Yes/No))
    /// </summary>
    [Table("QuestionType", Schema = "dbo")]
    public class QuestionType : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(150)] 
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Description { get; set; }
        [Required]
        [StringLength(50)]
        public string Type { get; set; }
        public bool IsActive { get; set; }
        
    }
}