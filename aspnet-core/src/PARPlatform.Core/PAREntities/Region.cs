﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// Region Entity for PAR Schema
    /// </summary>
    [Table("Region", Schema = "dbo")]
    [Index(nameof(CountryId), Name = "Region_FK_CountryId_index")]

    public class Region : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(10)]
        public string Abreviation { get; set; }
        public bool IsActive { get; set; }
        public virtual long CountryId { get; set; }
        [ForeignKey("CountryId")]
        public Country CountryFk { get; set; }        
    }
}