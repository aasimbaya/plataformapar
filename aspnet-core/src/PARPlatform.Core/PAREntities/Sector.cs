﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// Sector Entity for PAR Schema
    /// </summary>
    [Table("Sector", Schema = "dbo")]
    public class Sector : FullAuditedEntity<long>
    {        
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Type { get; set; }
        [Required]
        [StringLength(50)]
        public string InnerType { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(50)]
        public string Ciiu { get; set; }
    }
}
