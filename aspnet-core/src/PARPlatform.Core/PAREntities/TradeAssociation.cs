﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// TradeAssociation Entity for PAR Schema
    /// </summary>
    [Table("TradeAssociation", Schema = "dbo")]
    [Index(nameof(CountryId), Name = "TradeAssociation_FK_CountryId_index")]

    public class TradeAssociation : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(150)]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public virtual long? CountryId { get; set; }
        [ForeignKey("CountryId")]
        public Country CountryFk { get; set; }
    }
}