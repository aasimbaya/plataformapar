﻿using Abp.Domain.Entities.Auditing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PARPlatform.PAREntities
{
    /// <summary>
    /// ValidationType Entity for PAR Schema
    /// </summary>
    [Table("ValidationTypes", Schema = "dbo")]
    public class ValidationType : FullAuditedEntity<long>
    {
        [Required]
        [StringLength(150)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Type { get; set; }
        public bool IsActive { get; set; }
    }
}
