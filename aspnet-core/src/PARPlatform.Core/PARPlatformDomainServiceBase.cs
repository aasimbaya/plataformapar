﻿using Abp.Domain.Services;

namespace PARPlatform
{
    public abstract class PARPlatformDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected PARPlatformDomainServiceBase()
        {
            LocalizationSourceName = PARPlatformConsts.LocalizationSourceName;
        }
    }
}
