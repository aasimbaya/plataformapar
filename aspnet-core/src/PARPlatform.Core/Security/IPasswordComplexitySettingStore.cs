﻿using System.Threading.Tasks;

namespace PARPlatform.Security
{
    public interface IPasswordComplexitySettingStore
    {
        Task<PasswordComplexitySetting> GetSettingsAsync();
    }
}
