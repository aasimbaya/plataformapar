﻿using System.Threading.Tasks;
using PARPlatform.Authorization.Users;

namespace PARPlatform.WebHooks
{
    public interface IAppWebhookPublisher
    {
        Task PublishTestWebhook();
    }
}
