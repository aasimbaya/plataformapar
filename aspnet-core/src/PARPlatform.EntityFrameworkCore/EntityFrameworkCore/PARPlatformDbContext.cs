﻿using Abp.IdentityServer4vNext;
using Abp.Localization;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PARPlatform.Authorization.Delegation;
using PARPlatform.Authorization.Roles;
using PARPlatform.Authorization.Users;
using PARPlatform.Chat;
using PARPlatform.Editions;
using PARPlatform.Friendships;
using PARPlatform.MultiTenancy;
using PARPlatform.MultiTenancy.Accounting;
using PARPlatform.MultiTenancy.Payments;
using PARPlatform.PAREntities;
using PARPlatform.Storage;
using System.Collections.Generic;

namespace PARPlatform.EntityFrameworkCore
{
    public class PARPlatformDbContext : AbpZeroDbContext<Tenant, Role, User, PARPlatformDbContext>, IAbpPersistedGrantDbContext
    {
        /* Define an IDbSet for each entity of the application */

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Friendship> Friendships { get; set; }

        public virtual DbSet<ChatMessage> ChatMessages { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public virtual DbSet<SubscriptionPaymentExtensionData> SubscriptionPaymentExtensionDatas { get; set; }

        public virtual DbSet<UserDelegation> UserDelegations { get; set; }

        public virtual DbSet<RecentPassword> RecentPasswords { get; set; }

        //PAR Entities
        public virtual DbSet<QuestionType> QuestionTypes { get; set; }
        public virtual DbSet<QuestionCategory> QuestionCategories { get; set; }
        public virtual DbSet<QuestionCategoryType> QuestionCategoryType { get; set; }
        public virtual DbSet<FormQuestion> FormQuestions { get; set; }
        public virtual DbSet<FormType> FormTypes { get; set; }
        public virtual DbSet<FormThematic> FormThematics { get; set; }
        public virtual DbSet<FormThematicForm> FormThematicForms { get; set; }
        public virtual DbSet<Form> Forms { get; set; }
        public virtual DbSet<FormTypeForm> FormTypeForms { get; set; }
        public virtual DbSet<CommentTypes> CommentTypes { get; set; }
        public virtual DbSet<FormComments> FormComments { get; set; }
        public virtual DbSet<FormResults> FormResults { get; set; }
        public virtual DbSet<FormStatusCompany> FormStatusCompanies { get; set; }
        public virtual DbSet<FormStatus> FormStatus { get; set; }
        public virtual DbSet<FormName> FormName { get; set; }
        public virtual DbSet<Answer> Answers { get; set; }
        public virtual DbSet<ValidationType> ValidationTypes { get; set; }
        public virtual DbSet<Usage> Usages { get; set; }
        public virtual DbSet<FormQuestionLevel> FormQuestionLevels { get; set; }
        public virtual DbSet<EnumDictionary> EnumDictionaries { get; set; }
        public virtual DbSet<ApplicationLanguageText> ApplicationLanguageTexts { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Enterprise> Enterprises { get; set; }
        public virtual DbSet<Sector> Sector { get; set; }
        public virtual DbSet<LineBusiness> LineBusiness { get; set; }
        public virtual DbSet<PersonalType> PersonalType { get; set; }
        public virtual DbSet<Position> Position { get; set; }
        public virtual DbSet<Personal> Personal { get; set; }
        public virtual DbSet<EnterpriseGroup> EnterpriseGroups { get; set; }
        public virtual DbSet<EnterpriseGroupEnterprise> EnterpriseGroupEnterprises { get; set; }
        public virtual DbSet<Sector> Sectors { get; set; }
        public virtual DbSet<DocumentEstructure> DocumentEstructures { get; set; }

        public virtual DbSet<DocumentLanguage> DocumentLanguages { get; set; }

        public virtual DbSet<DocumentLocation> DocumentLocations { get; set; }

        public virtual DbSet<Document> Documents { get; set; }

        
        public PARPlatformDbContext(DbContextOptions<PARPlatformDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BinaryObject>(b =>
            {
                b.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
            });

            modelBuilder.Entity<SubscriptionPaymentExtensionData>(b =>
            {
                b.HasQueryFilter(m => !m.IsDeleted)
                    .HasIndex(e => new { e.SubscriptionPaymentId, e.Key, e.IsDeleted })
                    .IsUnique();
            });

            modelBuilder.Entity<UserDelegation>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.SourceUserId });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId });
            });

            //PAR Entities

            modelBuilder.Entity<FormQuestion>(b =>
            {
                b.Property(p => p.ConditionalParentIds).HasConversion(
                    v => JsonConvert.SerializeObject(v),
                    v => JsonConvert.DeserializeObject<List<long>>(v)
                );
            });

            modelBuilder.Entity<FormQuestionLevel>(b =>
            {
                b.Property(p => p.ChildrenQuestion).HasConversion(
                    v => JsonConvert.SerializeObject(v),
                    v => JsonConvert.DeserializeObject<List<long>>(v)
                );
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}
