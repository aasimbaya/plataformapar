using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace PARPlatform.EntityFrameworkCore
{
    public static class PARPlatformDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<PARPlatformDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<PARPlatformDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}