﻿using Abp.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.PAREntities;

namespace PARPlatform.EntityFrameworkCore.Repositories
{
    /// <summary>
    /// Specific Answer Repository implementation
    /// </summary>
    public class AnswerRepository : PARPlatformRepositoryBase<Answer, long>, IAnswerRepository
    {
        private readonly PARPlatformDbContext _dbContext;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="dbContextProvider"></param>
        public AnswerRepository(IDbContextProvider<PARPlatformDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContext = dbContextProvider.GetDbContext();
        }
    }
}
