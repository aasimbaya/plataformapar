﻿using Abp.EntityFrameworkCore;
using Abp.Localization;
using PARPlatform.Base;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.EntityFrameworkCore.Repositories
{
    public class ApplicationLanguageTextRepository : PARPlatformRepositoryBase<ApplicationLanguageText, long>, IApplicationLanguageTextRepository
    {
        private readonly PARPlatformDbContext _dbContext;
        public ApplicationLanguageTextRepository(IDbContextProvider<PARPlatformDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContext = dbContextProvider.GetDbContext();
        }
    }
}
