﻿using Abp.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.PAREntities;

namespace PARPlatform.EntityFrameworkCore.Repositories
{
    /// <summary>
    /// Specifi Document Language Repository Implementation
    /// </summary>
    public class DocumentLanguageRepository : PARPlatformRepositoryBase<DocumentLanguage, long>, IDocumentLanguageRepository
    {
        private readonly PARPlatformDbContext _dbContext;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="dbContextProvider"></param>
        public DocumentLanguageRepository(IDbContextProvider<PARPlatformDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContext = dbContextProvider.GetDbContext();
        }
    }
}
