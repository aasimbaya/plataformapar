﻿using Abp.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.PAREntities;

namespace PARPlatform.EntityFrameworkCore.Repositories
{
    /// <summary>
    /// Specifi Document Language Repository Implementation
    /// </summary>
    public class DocumentLocationRepository : PARPlatformRepositoryBase<DocumentLocation, long>, IDocumentLocationRepository
    {
        private readonly PARPlatformDbContext _dbContext;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="dbContextProvider"></param>
        public DocumentLocationRepository(IDbContextProvider<PARPlatformDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContext = dbContextProvider.GetDbContext();
        }
    }
}
