﻿using Abp.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.EntityFrameworkCore.Repositories
{
    public class FormNameRepository : PARPlatformRepositoryBase<FormName, long>, IFormNameRepository
    {
        private readonly PARPlatformDbContext _dbContext;
        public FormNameRepository(IDbContextProvider<PARPlatformDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContext = dbContextProvider.GetDbContext();
        }
    }
}
