﻿using Abp.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.PAREntities;

namespace PARPlatform.EntityFrameworkCore.Repositories
{
    /// <summary>
    /// Specific FormQuestionLevel Repository implementation
    /// </summary>
    public class FormQuestionLevelRepository : PARPlatformRepositoryBase<FormQuestionLevel, long>, IFormQuestionLevelRepository
    {
        private readonly PARPlatformDbContext _dbContext;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="dbContextProvider"></param>
        public FormQuestionLevelRepository(IDbContextProvider<PARPlatformDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContext = dbContextProvider.GetDbContext();
        }
    }
}