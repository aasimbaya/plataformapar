﻿using Abp.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.PAREntities;

namespace PARPlatform.EntityFrameworkCore.Repositories
{
    public class FormThematicFormRepository : PARPlatformRepositoryBase<FormThematicForm, long>, IFormThematicFormRepository
    {
        private readonly PARPlatformDbContext _dbContext;
        public FormThematicFormRepository(IDbContextProvider<PARPlatformDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContext = dbContextProvider.GetDbContext();
        }

    }
}
