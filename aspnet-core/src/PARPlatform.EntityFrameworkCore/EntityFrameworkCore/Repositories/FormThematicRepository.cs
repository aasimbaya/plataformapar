﻿using Abp.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.EntityFrameworkCore.Repositories
{
    public class FormThematicRepository : PARPlatformRepositoryBase<FormThematic, long>, IFormThematicRepository
    {
        private readonly PARPlatformDbContext _dbContext;
        public FormThematicRepository(IDbContextProvider<PARPlatformDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContext = dbContextProvider.GetDbContext();
        }
        
    }
}
