﻿using Abp.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.EntityFrameworkCore.Repositories
{
    public class FormTypeFormRepository : PARPlatformRepositoryBase<FormTypeForm, long>, IFormTypeFormRepository
    {
        private readonly PARPlatformDbContext _dbContext;
        public FormTypeFormRepository(IDbContextProvider<PARPlatformDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContext = dbContextProvider.GetDbContext();
        }
        
    }
}
