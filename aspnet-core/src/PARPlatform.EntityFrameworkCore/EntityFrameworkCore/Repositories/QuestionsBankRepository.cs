﻿using Abp.EntityFrameworkCore;
using PARPlatform.Base;
using PARPlatform.PAREntities;

namespace PARPlatform.EntityFrameworkCore.Repositories
{
    /// <summary>
    /// Specific QuestionsBank Repository implementation
    /// </summary>
    public class QuestionsBankRepository : PARPlatformRepositoryBase<FormQuestion, long>, IQuestionsBankRepository
    {
        private readonly PARPlatformDbContext _dbContext;

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="dbContextProvider"></param>
        public QuestionsBankRepository(IDbContextProvider<PARPlatformDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContext = dbContextProvider.GetDbContext();
            _dbContext.ChangeTracker.LazyLoadingEnabled = false;
            _dbContext.ChangeTracker.AutoDetectChangesEnabled = false;
        }
    }
}
