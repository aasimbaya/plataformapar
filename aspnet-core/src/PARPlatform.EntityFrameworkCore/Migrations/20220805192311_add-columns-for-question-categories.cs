﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class addcolumnsforquestioncategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Icon",
                schema: "dbo",
                table: "QuestionCategory",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "QuestionCategoryTypeId",
                schema: "dbo",
                table: "QuestionCategory",
                type: "bigint",
                maxLength: 250,
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Icon",
                schema: "dbo",
                table: "QuestionCategory");

            migrationBuilder.DropColumn(
                name: "QuestionCategoryTypeId",
                schema: "dbo",
                table: "QuestionCategory");
        }
    }
}
