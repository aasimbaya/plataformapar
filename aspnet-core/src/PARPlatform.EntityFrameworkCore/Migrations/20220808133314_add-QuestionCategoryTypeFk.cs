﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class addQuestionCategoryTypeFk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "QuestionCategor_FK_QuestionCategoryTypeId_index",
                schema: "dbo",
                table: "QuestionCategory",
                column: "QuestionCategoryTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_QuestionCategory_QuestionCategoryType_QuestionCategoryTypeId",
                schema: "dbo",
                table: "QuestionCategory",
                column: "QuestionCategoryTypeId",
                principalSchema: "dbo",
                principalTable: "QuestionCategoryType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuestionCategory_QuestionCategoryType_QuestionCategoryTypeId",
                schema: "dbo",
                table: "QuestionCategory");

            migrationBuilder.DropIndex(
                name: "QuestionCategor_FK_QuestionCategoryTypeId_index",
                schema: "dbo",
                table: "QuestionCategory");
        }
    }
}
