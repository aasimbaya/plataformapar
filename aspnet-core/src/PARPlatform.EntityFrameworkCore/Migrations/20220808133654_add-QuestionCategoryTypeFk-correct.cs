﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class addQuestionCategoryTypeFkcorrect : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameIndex(
                name: "QuestionCategor_FK_QuestionCategoryTypeId_index",
                schema: "dbo",
                table: "QuestionCategory",
                newName: "QuestionCategory_FK_QuestionCategoryTypeId_index");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameIndex(
                name: "QuestionCategory_FK_QuestionCategoryTypeId_index",
                schema: "dbo",
                table: "QuestionCategory",
                newName: "QuestionCategor_FK_QuestionCategoryTypeId_index");
        }
    }
}
