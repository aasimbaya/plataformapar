﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Extends_FormQuestions_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImplementationTip",
                schema: "dbo",
                table: "FormQuestions",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "QuestionUse",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImplementationTip",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "QuestionUse",
                schema: "dbo",
                table: "FormQuestions");
        }
    }
}
