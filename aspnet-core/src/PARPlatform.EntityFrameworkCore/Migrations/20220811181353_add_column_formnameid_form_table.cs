﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class add_column_formnameid_form_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FormNameId",
                schema: "dbo",
                table: "Form",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FormNameId",
                schema: "dbo",
                table: "Form");
        }
    }
}
