﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Add_Boolean_Fields_FormQuestions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Title",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.RenameColumn(
                name: "Description",
                schema: "dbo",
                table: "FormQuestions",
                newName: "ShortVersion");

            migrationBuilder.AddColumn<bool>(
                name: "CanBeCertified",
                schema: "dbo",
                table: "FormQuestions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "CanBeOverridden",
                schema: "dbo",
                table: "FormQuestions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "HasDependentQuestions",
                schema: "dbo",
                table: "FormQuestions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsPriority",
                schema: "dbo",
                table: "FormQuestions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Question",
                schema: "dbo",
                table: "FormQuestions",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CanBeCertified",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "CanBeOverridden",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "HasDependentQuestions",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "IsPriority",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "Question",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.RenameColumn(
                name: "ShortVersion",
                schema: "dbo",
                table: "FormQuestions",
                newName: "Description");

            migrationBuilder.AddColumn<string>(
                name: "Title",
                schema: "dbo",
                table: "FormQuestions",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");
        }
    }
}
