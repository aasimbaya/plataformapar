﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Cascade_Delete_Removed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FormComments_CommentTypes_CommentTypeId",
                schema: "dbo",
                table: "FormComments");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestionForm_Form_FormId",
                schema: "dbo",
                table: "FormQuestionForm");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestionForm_FormQuestions_FormQuestionId",
                schema: "dbo",
                table: "FormQuestionForm");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestions_FormThematic_FormThematicId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestions_FormType_FormTypeId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestions_QuestionCategory_QuestionCategoryId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestions_QuestionType_QuestionTypeId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_FormResults_Form_FormId",
                schema: "dbo",
                table: "FormResults");

            migrationBuilder.DropForeignKey(
                name: "FK_FormResults_FormComments_FormCommentId",
                schema: "dbo",
                table: "FormResults");

            migrationBuilder.DropForeignKey(
                name: "FK_FormStatusCompany_Form_FormId",
                schema: "dbo",
                table: "FormStatusCompany");

            migrationBuilder.DropForeignKey(
                name: "FK_FormStatusCompany_FormStatus_FormStatusId",
                schema: "dbo",
                table: "FormStatusCompany");

            migrationBuilder.DropForeignKey(
                name: "FK_FormThematicForm_Form_FormId",
                schema: "dbo",
                table: "FormThematicForm");

            migrationBuilder.DropForeignKey(
                name: "FK_FormThematicForm_FormThematic_FormThematicId",
                schema: "dbo",
                table: "FormThematicForm");

            migrationBuilder.DropForeignKey(
                name: "FK_FormTypeForm_Form_FormId",
                schema: "dbo",
                table: "FormTypeForm");

            migrationBuilder.DropForeignKey(
                name: "FK_FormTypeForm_FormType_FormTypeId",
                schema: "dbo",
                table: "FormTypeForm");

            migrationBuilder.DropForeignKey(
                name: "FK_QuestionCategory_QuestionCategoryType_QuestionCategoryTypeId",
                schema: "dbo",
                table: "QuestionCategory");

            migrationBuilder.AlterColumn<long>(
                name: "QuestionCategoryTypeId",
                schema: "dbo",
                table: "QuestionCategory",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormTypeId",
                schema: "dbo",
                table: "FormTypeForm",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormId",
                schema: "dbo",
                table: "FormTypeForm",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormThematicId",
                schema: "dbo",
                table: "FormThematicForm",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormId",
                schema: "dbo",
                table: "FormThematicForm",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormStatusId",
                schema: "dbo",
                table: "FormStatusCompany",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormId",
                schema: "dbo",
                table: "FormStatusCompany",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormId",
                schema: "dbo",
                table: "FormResults",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormCommentId",
                schema: "dbo",
                table: "FormResults",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "QuestionTypeId",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "QuestionCategoryId",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormTypeId",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormThematicId",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormQuestionId",
                schema: "dbo",
                table: "FormQuestionForm",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "FormId",
                schema: "dbo",
                table: "FormQuestionForm",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "CommentTypeId",
                schema: "dbo",
                table: "FormComments",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddForeignKey(
                name: "FK_FormComments_CommentTypes_CommentTypeId",
                schema: "dbo",
                table: "FormComments",
                column: "CommentTypeId",
                principalSchema: "dbo",
                principalTable: "CommentTypes",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestionForm_Form_FormId",
                schema: "dbo",
                table: "FormQuestionForm",
                column: "FormId",
                principalSchema: "dbo",
                principalTable: "Form",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestionForm_FormQuestions_FormQuestionId",
                schema: "dbo",
                table: "FormQuestionForm",
                column: "FormQuestionId",
                principalSchema: "dbo",
                principalTable: "FormQuestions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestions_FormThematic_FormThematicId",
                schema: "dbo",
                table: "FormQuestions",
                column: "FormThematicId",
                principalSchema: "dbo",
                principalTable: "FormThematic",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestions_FormType_FormTypeId",
                schema: "dbo",
                table: "FormQuestions",
                column: "FormTypeId",
                principalSchema: "dbo",
                principalTable: "FormType",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestions_QuestionCategory_QuestionCategoryId",
                schema: "dbo",
                table: "FormQuestions",
                column: "QuestionCategoryId",
                principalSchema: "dbo",
                principalTable: "QuestionCategory",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestions_QuestionType_QuestionTypeId",
                schema: "dbo",
                table: "FormQuestions",
                column: "QuestionTypeId",
                principalSchema: "dbo",
                principalTable: "QuestionType",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormResults_Form_FormId",
                schema: "dbo",
                table: "FormResults",
                column: "FormId",
                principalSchema: "dbo",
                principalTable: "Form",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormResults_FormComments_FormCommentId",
                schema: "dbo",
                table: "FormResults",
                column: "FormCommentId",
                principalSchema: "dbo",
                principalTable: "FormComments",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormStatusCompany_Form_FormId",
                schema: "dbo",
                table: "FormStatusCompany",
                column: "FormId",
                principalSchema: "dbo",
                principalTable: "Form",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormStatusCompany_FormStatus_FormStatusId",
                schema: "dbo",
                table: "FormStatusCompany",
                column: "FormStatusId",
                principalSchema: "dbo",
                principalTable: "FormStatus",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormThematicForm_Form_FormId",
                schema: "dbo",
                table: "FormThematicForm",
                column: "FormId",
                principalSchema: "dbo",
                principalTable: "Form",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormThematicForm_FormThematic_FormThematicId",
                schema: "dbo",
                table: "FormThematicForm",
                column: "FormThematicId",
                principalSchema: "dbo",
                principalTable: "FormThematic",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormTypeForm_Form_FormId",
                schema: "dbo",
                table: "FormTypeForm",
                column: "FormId",
                principalSchema: "dbo",
                principalTable: "Form",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FormTypeForm_FormType_FormTypeId",
                schema: "dbo",
                table: "FormTypeForm",
                column: "FormTypeId",
                principalSchema: "dbo",
                principalTable: "FormType",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_QuestionCategory_QuestionCategoryType_QuestionCategoryTypeId",
                schema: "dbo",
                table: "QuestionCategory",
                column: "QuestionCategoryTypeId",
                principalSchema: "dbo",
                principalTable: "QuestionCategoryType",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FormComments_CommentTypes_CommentTypeId",
                schema: "dbo",
                table: "FormComments");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestionForm_Form_FormId",
                schema: "dbo",
                table: "FormQuestionForm");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestionForm_FormQuestions_FormQuestionId",
                schema: "dbo",
                table: "FormQuestionForm");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestions_FormThematic_FormThematicId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestions_FormType_FormTypeId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestions_QuestionCategory_QuestionCategoryId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestions_QuestionType_QuestionTypeId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_FormResults_Form_FormId",
                schema: "dbo",
                table: "FormResults");

            migrationBuilder.DropForeignKey(
                name: "FK_FormResults_FormComments_FormCommentId",
                schema: "dbo",
                table: "FormResults");

            migrationBuilder.DropForeignKey(
                name: "FK_FormStatusCompany_Form_FormId",
                schema: "dbo",
                table: "FormStatusCompany");

            migrationBuilder.DropForeignKey(
                name: "FK_FormStatusCompany_FormStatus_FormStatusId",
                schema: "dbo",
                table: "FormStatusCompany");

            migrationBuilder.DropForeignKey(
                name: "FK_FormThematicForm_Form_FormId",
                schema: "dbo",
                table: "FormThematicForm");

            migrationBuilder.DropForeignKey(
                name: "FK_FormThematicForm_FormThematic_FormThematicId",
                schema: "dbo",
                table: "FormThematicForm");

            migrationBuilder.DropForeignKey(
                name: "FK_FormTypeForm_Form_FormId",
                schema: "dbo",
                table: "FormTypeForm");

            migrationBuilder.DropForeignKey(
                name: "FK_FormTypeForm_FormType_FormTypeId",
                schema: "dbo",
                table: "FormTypeForm");

            migrationBuilder.DropForeignKey(
                name: "FK_QuestionCategory_QuestionCategoryType_QuestionCategoryTypeId",
                schema: "dbo",
                table: "QuestionCategory");

            migrationBuilder.AlterColumn<long>(
                name: "QuestionCategoryTypeId",
                schema: "dbo",
                table: "QuestionCategory",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormTypeId",
                schema: "dbo",
                table: "FormTypeForm",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormId",
                schema: "dbo",
                table: "FormTypeForm",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormThematicId",
                schema: "dbo",
                table: "FormThematicForm",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormId",
                schema: "dbo",
                table: "FormThematicForm",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormStatusId",
                schema: "dbo",
                table: "FormStatusCompany",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormId",
                schema: "dbo",
                table: "FormStatusCompany",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormId",
                schema: "dbo",
                table: "FormResults",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormCommentId",
                schema: "dbo",
                table: "FormResults",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "QuestionTypeId",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "QuestionCategoryId",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormTypeId",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormThematicId",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormQuestionId",
                schema: "dbo",
                table: "FormQuestionForm",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FormId",
                schema: "dbo",
                table: "FormQuestionForm",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "CommentTypeId",
                schema: "dbo",
                table: "FormComments",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_FormComments_CommentTypes_CommentTypeId",
                schema: "dbo",
                table: "FormComments",
                column: "CommentTypeId",
                principalSchema: "dbo",
                principalTable: "CommentTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestionForm_Form_FormId",
                schema: "dbo",
                table: "FormQuestionForm",
                column: "FormId",
                principalSchema: "dbo",
                principalTable: "Form",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestionForm_FormQuestions_FormQuestionId",
                schema: "dbo",
                table: "FormQuestionForm",
                column: "FormQuestionId",
                principalSchema: "dbo",
                principalTable: "FormQuestions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestions_FormThematic_FormThematicId",
                schema: "dbo",
                table: "FormQuestions",
                column: "FormThematicId",
                principalSchema: "dbo",
                principalTable: "FormThematic",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestions_FormType_FormTypeId",
                schema: "dbo",
                table: "FormQuestions",
                column: "FormTypeId",
                principalSchema: "dbo",
                principalTable: "FormType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestions_QuestionCategory_QuestionCategoryId",
                schema: "dbo",
                table: "FormQuestions",
                column: "QuestionCategoryId",
                principalSchema: "dbo",
                principalTable: "QuestionCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestions_QuestionType_QuestionTypeId",
                schema: "dbo",
                table: "FormQuestions",
                column: "QuestionTypeId",
                principalSchema: "dbo",
                principalTable: "QuestionType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormResults_Form_FormId",
                schema: "dbo",
                table: "FormResults",
                column: "FormId",
                principalSchema: "dbo",
                principalTable: "Form",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormResults_FormComments_FormCommentId",
                schema: "dbo",
                table: "FormResults",
                column: "FormCommentId",
                principalSchema: "dbo",
                principalTable: "FormComments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormStatusCompany_Form_FormId",
                schema: "dbo",
                table: "FormStatusCompany",
                column: "FormId",
                principalSchema: "dbo",
                principalTable: "Form",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormStatusCompany_FormStatus_FormStatusId",
                schema: "dbo",
                table: "FormStatusCompany",
                column: "FormStatusId",
                principalSchema: "dbo",
                principalTable: "FormStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormThematicForm_Form_FormId",
                schema: "dbo",
                table: "FormThematicForm",
                column: "FormId",
                principalSchema: "dbo",
                principalTable: "Form",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormThematicForm_FormThematic_FormThematicId",
                schema: "dbo",
                table: "FormThematicForm",
                column: "FormThematicId",
                principalSchema: "dbo",
                principalTable: "FormThematic",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormTypeForm_Form_FormId",
                schema: "dbo",
                table: "FormTypeForm",
                column: "FormId",
                principalSchema: "dbo",
                principalTable: "Form",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FormTypeForm_FormType_FormTypeId",
                schema: "dbo",
                table: "FormTypeForm",
                column: "FormTypeId",
                principalSchema: "dbo",
                principalTable: "FormType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_QuestionCategory_QuestionCategoryType_QuestionCategoryTypeId",
                schema: "dbo",
                table: "QuestionCategory",
                column: "QuestionCategoryTypeId",
                principalSchema: "dbo",
                principalTable: "QuestionCategoryType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
