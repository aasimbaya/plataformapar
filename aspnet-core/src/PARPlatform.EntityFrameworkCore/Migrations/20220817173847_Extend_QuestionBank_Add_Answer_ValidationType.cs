﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Extend_QuestionBank_Add_Answer_ValidationType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ParentIds",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.AddColumn<string>(
                name: "Formula",
                schema: "dbo",
                table: "FormQuestions",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ParentId",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ParentValidation",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<decimal>(
                name: "Score",
                schema: "dbo",
                table: "FormQuestions",
                type: "decimal(10,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalScore",
                schema: "dbo",
                table: "FormQuestions",
                type: "decimal(10,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "ValidationTypes",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Type = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ValidationTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Answers",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AnswerContent = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    FormQuestionId = table.Column<long>(type: "bigint", nullable: true),
                    ValidationTypeId = table.Column<long>(type: "bigint", nullable: true),
                    Score = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Percentage = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    AutoNull = table.Column<bool>(type: "bit", nullable: false),
                    SpecializedResource = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Answers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Answers_FormQuestions_FormQuestionId",
                        column: x => x.FormQuestionId,
                        principalSchema: "dbo",
                        principalTable: "FormQuestions",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Answers_ValidationTypes_ValidationTypeId",
                        column: x => x.ValidationTypeId,
                        principalSchema: "dbo",
                        principalTable: "ValidationTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "FormQuestions_FK_ParentId_index",
                schema: "dbo",
                table: "FormQuestions",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "Answers_FK_FormQuestionId_index",
                schema: "dbo",
                table: "Answers",
                column: "FormQuestionId");

            migrationBuilder.CreateIndex(
                name: "Answers_FK_ValidationTypeId_index",
                schema: "dbo",
                table: "Answers",
                column: "ValidationTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestions_FormQuestions_ParentId",
                schema: "dbo",
                table: "FormQuestions",
                column: "ParentId",
                principalSchema: "dbo",
                principalTable: "FormQuestions",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestions_FormQuestions_ParentId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropTable(
                name: "Answers",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "ValidationTypes",
                schema: "dbo");

            migrationBuilder.DropIndex(
                name: "FormQuestions_FK_ParentId_index",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "Formula",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "ParentId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "ParentValidation",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "Score",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "TotalScore",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.AddColumn<string>(
                name: "ParentIds",
                schema: "dbo",
                table: "FormQuestions",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
