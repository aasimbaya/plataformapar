﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class addfieldsIsInternationalHasInclusionEmployeesNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EmployeesNumber",
                schema: "dbo",
                table: "Enterprise",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "HasInclusion",
                schema: "dbo",
                table: "Enterprise",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsInternational",
                schema: "dbo",
                table: "Enterprise",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmployeesNumber",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropColumn(
                name: "HasInclusion",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropColumn(
                name: "IsInternational",
                schema: "dbo",
                table: "Enterprise");
        }
    }
}
