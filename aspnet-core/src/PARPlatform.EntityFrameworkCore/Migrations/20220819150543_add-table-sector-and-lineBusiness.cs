﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class addtablesectorandlineBusiness : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "LineBusinessId",
                schema: "dbo",
                table: "Enterprise",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "SectorId",
                schema: "dbo",
                table: "Enterprise",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LineBusiness",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LineBusiness", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sector",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sector", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "Enterprise_FK_LineBusinessId_index",
                schema: "dbo",
                table: "Enterprise",
                column: "LineBusinessId");

            migrationBuilder.CreateIndex(
                name: "Enterprise_FK_SectorId_index",
                schema: "dbo",
                table: "Enterprise",
                column: "SectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Enterprise_LineBusiness_LineBusinessId",
                schema: "dbo",
                table: "Enterprise",
                column: "LineBusinessId",
                principalSchema: "dbo",
                principalTable: "LineBusiness",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Enterprise_Sector_SectorId",
                schema: "dbo",
                table: "Enterprise",
                column: "SectorId",
                principalSchema: "dbo",
                principalTable: "Sector",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enterprise_LineBusiness_LineBusinessId",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropForeignKey(
                name: "FK_Enterprise_Sector_SectorId",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropTable(
                name: "LineBusiness",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Sector",
                schema: "dbo");

            migrationBuilder.DropIndex(
                name: "Enterprise_FK_LineBusinessId_index",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropIndex(
                name: "Enterprise_FK_SectorId_index",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropColumn(
                name: "LineBusinessId",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropColumn(
                name: "SectorId",
                schema: "dbo",
                table: "Enterprise");
        }
    }
}
