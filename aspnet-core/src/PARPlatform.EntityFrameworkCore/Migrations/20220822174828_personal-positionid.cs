﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class personalpositionid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "PositionId",
                schema: "dbo",
                table: "Personal",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "Personal_FK_PositionId_index",
                schema: "dbo",
                table: "Personal",
                column: "PositionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Personal_Position_PositionId",
                schema: "dbo",
                table: "Personal",
                column: "PositionId",
                principalSchema: "dbo",
                principalTable: "Position",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Personal_Position_PositionId",
                schema: "dbo",
                table: "Personal");

            migrationBuilder.DropIndex(
                name: "Personal_FK_PositionId_index",
                schema: "dbo",
                table: "Personal");

            migrationBuilder.DropColumn(
                name: "PositionId",
                schema: "dbo",
                table: "Personal");
        }
    }
}
