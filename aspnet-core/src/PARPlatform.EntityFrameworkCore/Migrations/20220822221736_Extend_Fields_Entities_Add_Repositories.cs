﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Extend_Fields_Entities_Add_Repositories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CanBeCertified",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "QuestionUse",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.RenameColumn(
                name: "HasDependentQuestions",
                schema: "dbo",
                table: "FormQuestions",
                newName: "IsNullable");

            migrationBuilder.RenameColumn(
                name: "CanBeOverridden",
                schema: "dbo",
                table: "FormQuestions",
                newName: "IsCertified");

            migrationBuilder.AddColumn<string>(
                name: "Type",
                schema: "dbo",
                table: "QuestionType",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Type",
                schema: "dbo",
                table: "FormType",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Type",
                schema: "dbo",
                table: "FormThematic",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UsageId",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                schema: "dbo",
                table: "CommentTypes",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Usages",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    UsageContent = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usages", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "FormQuestions_FK_UsageId_index",
                schema: "dbo",
                table: "FormQuestions",
                column: "UsageId");

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestions_Usages_UsageId",
                schema: "dbo",
                table: "FormQuestions",
                column: "UsageId",
                principalSchema: "dbo",
                principalTable: "Usages",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestions_Usages_UsageId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropTable(
                name: "Usages",
                schema: "dbo");

            migrationBuilder.DropIndex(
                name: "FormQuestions_FK_UsageId_index",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "Type",
                schema: "dbo",
                table: "QuestionType");

            migrationBuilder.DropColumn(
                name: "Type",
                schema: "dbo",
                table: "FormType");

            migrationBuilder.DropColumn(
                name: "Type",
                schema: "dbo",
                table: "FormThematic");

            migrationBuilder.DropColumn(
                name: "UsageId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "Type",
                schema: "dbo",
                table: "CommentTypes");

            migrationBuilder.RenameColumn(
                name: "IsNullable",
                schema: "dbo",
                table: "FormQuestions",
                newName: "HasDependentQuestions");

            migrationBuilder.RenameColumn(
                name: "IsCertified",
                schema: "dbo",
                table: "FormQuestions",
                newName: "CanBeOverridden");

            migrationBuilder.AddColumn<bool>(
                name: "CanBeCertified",
                schema: "dbo",
                table: "FormQuestions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "QuestionUse",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
