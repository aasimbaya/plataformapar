﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class addAbpUser_FK_EnterpriseId_index : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "EnterpriseId",
                table: "AbpUsers",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "AbpUser_FK_EnterpriseId_index",
                table: "AbpUsers",
                column: "EnterpriseId");

            migrationBuilder.AddForeignKey(
                name: "FK_AbpUsers_Enterprise_EnterpriseId",
                table: "AbpUsers",
                column: "EnterpriseId",
                principalSchema: "dbo",
                principalTable: "Enterprise",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AbpUsers_Enterprise_EnterpriseId",
                table: "AbpUsers");

            migrationBuilder.DropIndex(
                name: "AbpUser_FK_EnterpriseId_index",
                table: "AbpUsers");

            migrationBuilder.DropColumn(
                name: "EnterpriseId",
                table: "AbpUsers");
        }
    }
}
