﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class NewFields_Enterprise : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasInclusion",
                schema: "dbo",
                table: "EnterpriseGroup",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsInternational",
                schema: "dbo",
                table: "EnterpriseGroup",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "ReceivePromoAccepted",
                schema: "dbo",
                table: "EnterpriseGroup",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "SectorId",
                schema: "dbo",
                table: "EnterpriseGroup",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ReceivePromoAccepted",
                schema: "dbo",
                table: "Enterprise",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "EnterpriseGroup_FK_SectorId_index",
                schema: "dbo",
                table: "EnterpriseGroup",
                column: "SectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_EnterpriseGroup_Sector_SectorId",
                schema: "dbo",
                table: "EnterpriseGroup",
                column: "SectorId",
                principalSchema: "dbo",
                principalTable: "Sector",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EnterpriseGroup_Sector_SectorId",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropIndex(
                name: "EnterpriseGroup_FK_SectorId_index",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropColumn(
                name: "HasInclusion",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropColumn(
                name: "IsInternational",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropColumn(
                name: "ReceivePromoAccepted",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropColumn(
                name: "SectorId",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropColumn(
                name: "ReceivePromoAccepted",
                schema: "dbo",
                table: "Enterprise");
        }
    }
}
