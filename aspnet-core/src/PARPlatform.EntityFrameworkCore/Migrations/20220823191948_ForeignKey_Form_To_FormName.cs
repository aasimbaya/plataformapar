﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class ForeignKey_Form_To_FormName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "FormNameId",
                schema: "dbo",
                table: "Form",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "Form_FK_FormNameId_index",
                schema: "dbo",
                table: "Form",
                column: "FormNameId");

            migrationBuilder.AddForeignKey(
                name: "FK_Form_FormName_FormNameId",
                schema: "dbo",
                table: "Form",
                column: "FormNameId",
                principalSchema: "dbo",
                principalTable: "FormName",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Form_FormName_FormNameId",
                schema: "dbo",
                table: "Form");

            migrationBuilder.DropIndex(
                name: "Form_FK_FormNameId_index",
                schema: "dbo",
                table: "Form");

            migrationBuilder.AlterColumn<int>(
                name: "FormNameId",
                schema: "dbo",
                table: "Form",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);
        }
    }
}
