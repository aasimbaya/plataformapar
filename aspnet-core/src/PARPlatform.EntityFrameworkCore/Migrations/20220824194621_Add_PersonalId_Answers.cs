﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Add_PersonalId_Answers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PersonalId",
                schema: "dbo",
                table: "Answers",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "Answers_PersonalId_index",
                schema: "dbo",
                table: "Answers",
                column: "PersonalId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "Answers_PersonalId_index",
                schema: "dbo",
                table: "Answers");

            migrationBuilder.DropColumn(
                name: "PersonalId",
                schema: "dbo",
                table: "Answers");
        }
    }
}
