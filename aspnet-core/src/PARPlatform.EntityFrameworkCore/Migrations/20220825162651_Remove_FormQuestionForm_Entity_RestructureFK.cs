﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Remove_FormQuestionForm_Entity_RestructureFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FormQuestionForm",
                schema: "dbo");

            migrationBuilder.AddColumn<long>(
                name: "FormId",
                schema: "dbo",
                table: "FormQuestions",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "FormQuestions_FK_FormId_index",
                schema: "dbo",
                table: "FormQuestions",
                column: "FormId");

            migrationBuilder.AddForeignKey(
                name: "FK_FormQuestions_Form_FormId",
                schema: "dbo",
                table: "FormQuestions",
                column: "FormId",
                principalSchema: "dbo",
                principalTable: "Form",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FormQuestions_Form_FormId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropIndex(
                name: "FormQuestions_FK_FormId_index",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.DropColumn(
                name: "FormId",
                schema: "dbo",
                table: "FormQuestions");

            migrationBuilder.CreateTable(
                name: "FormQuestionForm",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FormId = table.Column<long>(type: "bigint", nullable: true),
                    FormQuestionId = table.Column<long>(type: "bigint", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FormQuestionForm", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FormQuestionForm_Form_FormId",
                        column: x => x.FormId,
                        principalSchema: "dbo",
                        principalTable: "Form",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FormQuestionForm_FormQuestions_FormQuestionId",
                        column: x => x.FormQuestionId,
                        principalSchema: "dbo",
                        principalTable: "FormQuestions",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "FormQuestionForm_FK_FormId_index",
                schema: "dbo",
                table: "FormQuestionForm",
                column: "FormId");

            migrationBuilder.CreateIndex(
                name: "FormQuestionForm_FK_FormQuestionId_index",
                schema: "dbo",
                table: "FormQuestionForm",
                column: "FormQuestionId");
        }
    }
}
