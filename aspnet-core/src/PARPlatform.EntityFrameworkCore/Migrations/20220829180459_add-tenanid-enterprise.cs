﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class addtenanidenterprise : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                schema: "dbo",
                table: "Enterprise",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "Enterprise_TenantId_index",
                schema: "dbo",
                table: "Enterprise",
                column: "TenantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "Enterprise_TenantId_index",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropColumn(
                name: "TenantId",
                schema: "dbo",
                table: "Enterprise");
        }
    }
}
