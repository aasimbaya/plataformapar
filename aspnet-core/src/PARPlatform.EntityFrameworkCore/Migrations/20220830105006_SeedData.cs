﻿using Microsoft.EntityFrameworkCore.Migrations;
using PARPlatform.PAREntities;
using System;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class SeedData : Migration
    {

        protected override void Up(MigrationBuilder migrationBuilder)
        {
        
            migrationBuilder.Sql("delete from dbo.Form");
            migrationBuilder.Sql("delete from dbo.FormThematic");
            migrationBuilder.Sql("delete from dbo.FormQuestions");
            migrationBuilder.Sql("delete from dbo.FormType");
            migrationBuilder.Sql("delete from dbo.QuestionType");
            migrationBuilder.Sql("delete from dbo.Usages");
            migrationBuilder.Sql("delete from dbo.FormName");
            migrationBuilder.Sql("delete from dbo.QuestionCategory");
            migrationBuilder.Sql("delete from dbo.QuestionCategoryType");
            migrationBuilder.Sql("delete from dbo.ValidationTypes");
            migrationBuilder.Sql("delete from dbo.Personal");
            migrationBuilder.Sql("UPDATE dbo.AbpUsers set EnterpriseId = null");
            migrationBuilder.Sql("delete from dbo.Enterprise");
            migrationBuilder.Sql("delete from dbo.Position");
            migrationBuilder.Sql("delete from dbo.PersonalType");
            migrationBuilder.Sql("delete from dbo.Sector");
            migrationBuilder.Sql("delete from dbo.City");
            migrationBuilder.Sql("delete from dbo.Region");
            migrationBuilder.Sql("delete from dbo.Country");

            //Table QuestionCategoryType
            migrationBuilder.InsertData(table: "QuestionCategoryType",
                columns: new[] { "Id", "Name", "CreationTime", "IsDeleted" }, columnTypes: new[] { "long", "string", "datetime", "long" },
                values: new object[] { "1", "Opcional", DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "QuestionCategoryType",
                columns: new[] { "Id", "Name", "CreationTime", "IsDeleted" }, columnTypes: new[] { "long", "string", "datetime", "long" },
                values: new object[] { "2", "Obligatoria", DateTime.Now, 0 });

            //Form Name
            migrationBuilder.InsertData(table: "FormName",
               columns: new[] { "Id", "Name", "CreationTime", "IsDeleted", "IsActive" }, columnTypes: new[] { "long", "string", "datetime", "long", "long" },
               values: new object[] { "1", "SPECIAL_DISTINCTION", DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "FormName",
                columns: new[] { "Id", "Name", "CreationTime", "IsDeleted", "IsActive" }, columnTypes: new[] { "long", "string", "datetime", "long", "long" },
                values: new object[] { "2", "QUESTIONNAIRES", DateTime.Now, 0, 1 });


            //Form FormType
            migrationBuilder.InsertData(table: "FormType",
               columns: new[] { "Id", "Title", "Description", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long" },
               values: new object[] { "1", "Ranking PAR", "Ranking PAR", "rankingpar", DateTime.Now, 0, 1 });

            migrationBuilder.InsertData(table: "FormType",
               columns: new[] { "Id", "Title", "Description", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long" },
               values: new object[] { "2", "Fuera de Convocatoria", "Fuera de Convocatoria", "outofconvocatory", DateTime.Now, 0, 1 });

            migrationBuilder.InsertData(table: "FormType",
               columns: new[] { "Id", "Title", "Description", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long" },
               values: new object[] { "3", "Encuesta de Percepción", "Encuesta de Percepción", "perceptionsurvey", DateTime.Now, 0, 1 });

            migrationBuilder.InsertData(table: "FormType",
               columns: new[] { "Id", "Title", "Description", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long" },
               values: new object[] { "4", "Banco de Buenas Prácticas", "Banco de Buenas Prácticas", "goodpracticesbank", DateTime.Now, 0, 1 });


            //QuestionType
            migrationBuilder.InsertData(table: "QuestionType",
               columns: new[] { "Id", "Name", "Description", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long" },
               values: new object[] { "1", "Booleana (Sí/No)", "Booleana (Sí/No)", "boolean", DateTime.Now, 0, 1 });

            migrationBuilder.InsertData(table: "QuestionType",
               columns: new[] { "Id", "Name", "Description", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long" },
               values: new object[] { "2", "Selección Múltiple", "Selección Múltiple", "multipleselection", DateTime.Now, 0, 1 });

            migrationBuilder.InsertData(table: "QuestionType",
               columns: new[] { "Id", "Name", "Description", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long" },
               values: new object[] { "3", "Selección Única", "Selección Única", "singleselection", DateTime.Now, 0, 1 });

            migrationBuilder.InsertData(table: "QuestionType",
               columns: new[] { "Id", "Name", "Description", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long" },
               values: new object[] { "4", "Abierta", "Abierta", "openquestion", DateTime.Now, 0, 1 });


            //Usages
            migrationBuilder.InsertData(table: "Usages",
               columns: new[] { "Id", "Name", "Description", "UsageContent", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long" },
               values: new object[] { "1", "Categoría", "Categoría", "category", DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "Usages",
               columns: new[] { "Id", "Name", "Description", "UsageContent", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long" },
               values: new object[] { "2", "Distinción Especial", "Distinción Especial", "specialdistinction", DateTime.Now, 0, 1 });


            //ValidationTypes
            migrationBuilder.InsertData(table: "ValidationTypes",
               columns: new[] { "Id", "Name", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "datetime", "long", "long" },
               values: new object[] { "1", "Numérica", "numeric", DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "ValidationTypes",
               columns: new[] { "Id", "Name", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "datetime", "long", "long" },
               values: new object[] { "2", "Alfanumérica", "alphanumeric", DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "ValidationTypes",
               columns: new[] { "Id", "Name", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "datetime", "long", "long" },
               values: new object[] { "3", "Fecha", "date", DateTime.Now, 0, 1 });


            //Sector
            migrationBuilder.InsertData(table: "Sector",
               columns: new[] { "Id", "Name","CreationTime", "IsDeleted", "IsActive","Type" },
               columnTypes: new[] { "long", "string","datetime", "long", "long","string" },
               values: new object[] { "1", "Agricultura", DateTime.Now ,0, 1, "Extractivo" });
            migrationBuilder.InsertData(table: "Sector",
               columns: new[] { "Id", "Name", "CreationTime", "IsDeleted", "IsActive", "Type" },
               columnTypes: new[] { "long", "string", "datetime", "long", "long", "string" },
               values: new object[] { "2", "Ganadería", DateTime.Now, 0, 1, "Extractivo" });

            migrationBuilder.InsertData(table: "Sector",
               columns: new[] { "Id", "Name", "CreationTime", "IsDeleted", "IsActive", "Type" },
               columnTypes: new[] { "long", "string", "datetime", "long", "long", "string" },
               values: new object[] { "3", "Textil", DateTime.Now, 0, 1, "Industrial" });

            migrationBuilder.InsertData(table: "Sector",
               columns: new[] { "Id", "Name", "CreationTime", "IsDeleted", "IsActive", "Type" },
               columnTypes: new[] { "long", "string", "datetime", "long", "long", "string" },
               values: new object[] { "4", "Ensamblaje", DateTime.Now, 0, 1, "Industrial" });

            migrationBuilder.InsertData(table: "Sector",
               columns: new[] { "Id", "Name", "CreationTime", "IsDeleted", "IsActive", "Type" },
               columnTypes: new[] { "long", "string", "datetime", "long", "long", "string" },
               values: new object[] { "5", "Banca", DateTime.Now, 0, 1, "Financiero" });

            migrationBuilder.InsertData(table: "Sector",
               columns: new[] { "Id", "Name", "CreationTime", "IsDeleted", "IsActive", "Type" },
               columnTypes: new[] { "long", "string", "datetime", "long", "long", "string" },
               values: new object[] { "6", "Seguridad Privada", DateTime.Now, 0, 1, "Seguridad" });

            //PersonalType
            migrationBuilder.InsertData(table: "PersonalType",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "CreatedAt", "IsActive", "IsDeleted", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string","string","datetime", "long", "long", "datetime" , "datetime" },
               values: new object[] { "1", "Gerencia", "Gerencia","CEO",DateTime.Now, 1, 0, DateTime.Now, DateTime.Now });

            migrationBuilder.InsertData(table: "PersonalType",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "CreatedAt", "IsActive", "IsDeleted", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string","string","datetime", "long", "long" , "datetime", "datetime" },
               values: new object[] { "2", "Recursos Humanos", "Recursos Humanos", "HR",DateTime.Now,1, 0, DateTime.Now , DateTime.Now });

            migrationBuilder.InsertData(table: "PersonalType",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "CreatedAt", "IsActive", "IsDeleted", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string","string","datetime", "long", "long" , "datetime", "datetime" },
               values: new object[] { "3", "Comunicación", "Comunicación", "COM", DateTime.Now, 1, 0, DateTime.Now , DateTime.Now });

            //Position
            migrationBuilder.InsertData(table: "Position",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "IsActive", "IsDeleted", "CreatedAt", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string", "string","long", "long","datetime", "datetime" ,"datetime"},
               values: new object[] { "1", "Gerencia", "Gerencia", "Gerencia",1, 0, DateTime.Now, DateTime.Now , DateTime.Now });


            migrationBuilder.InsertData(table: "Position",
               columns: new[] { "Id", "Name", "Description", "Abreviation","IsActive", "IsDeleted" , "CreatedAt", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string", "string", "long", "long", "datetime", "datetime","datetime" },
               values: new object[] { "2", "Conserje", "Conserje", "Conserje", 1, 0, DateTime.Now, DateTime.Now , DateTime.Now });

            migrationBuilder.InsertData(table: "Position",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "IsActive", "IsDeleted", "CreatedAt", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string", "string", "long", "long", "datetime", "datetime","datetime" },
               values: new object[] { "3", "Contador", "Contador", "Contador", 1, 0, DateTime.Now, DateTime.Now, DateTime.Now });


            //Country
            migrationBuilder.InsertData(table: "Country",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string","long", "datetime", "long" },
               values: new object[] { "1", "Ecuador", "+593", 1, DateTime.Now, 0 });


            migrationBuilder.InsertData(table: "Country",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { "2", "Colombia", "+57", 1, DateTime.Now, 0 });

            migrationBuilder.InsertData(table: "Country",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { "3", "Venezuela", "+58", 1, DateTime.Now, 0 });

            migrationBuilder.InsertData(table: "Country",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { "4", "Peru", "+51", 1, DateTime.Now, 0 });


            //Region
            migrationBuilder.InsertData(table: "Region",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "CountryId", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "long", "datetime", "long" },
               values: new object[] { "1", "Pichincha", "PI", 1, 1, DateTime.Now, 0 });

            migrationBuilder.InsertData(table: "Region",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "CountryId", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "long", "datetime", "long" },
               values: new object[] { "2", "Azuay", "AZ", 1, 1, DateTime.Now, 0 });


            migrationBuilder.InsertData(table: "Region",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "CountryId", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "long", "datetime", "long" },
               values: new object[] { "3", "Cundinamarca", "CUN", 1, 2, DateTime.Now, 0 });


            migrationBuilder.InsertData(table: "Region",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "CountryId", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "long", "datetime", "long" },
               values: new object[] { "4", "Bolivar", "BOL", 1, 2, DateTime.Now, 0 });


            //City
            migrationBuilder.InsertData(table: "City",
               columns: new[] { "Id", "Name","Abreviation","IsActive", "RegionId", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "long", "datetime", "long" },
               values: new object[] { "1", "Quito", "UIO", 1, 1, DateTime.Now, 0 });

            migrationBuilder.InsertData(table: "City",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "RegionId", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "long", "datetime", "long" },
               values: new object[] { "2", "Cumbaya", "CBY", 1, 1, DateTime.Now, 0 });

            migrationBuilder.InsertData(table: "City",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "RegionId", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "long", "datetime", "long" },
               values: new object[] { "3", "Cuenca", "CUE", 1, 1, DateTime.Now, 0 });

            migrationBuilder.InsertData(table: "City",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "RegionId", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "long", "datetime", "long" },
               values: new object[] { "4", "Gualaceo", "GUA", 1, 1, DateTime.Now, 0 });

            migrationBuilder.InsertData(table: "City",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "RegionId", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "long", "datetime", "long" },
               values: new object[] { "5", "Bogota", "BOG", 1, 1, DateTime.Now, 0 });

            migrationBuilder.InsertData(table: "City",
               columns: new[] { "Id", "Name", "Abreviation", "IsActive", "RegionId", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "long", "datetime", "long" },
               values: new object[] { "6", "Cartagena", "CTG", 1, 1, DateTime.Now, 0 });

            
            //QuestionCategory
            /*int x = 1;
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 1", "This is the description of my category 1", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 2", "This is the description of my category 2", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 3", "This is the description of my category 3", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 4", "This is the description of my category 4", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 5", "This is the description of my category 5", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 6", "This is the description of my category 6", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 7", "This is the description of my category 7", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 8", "This is the description of my category 8", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 9", "This is the description of my category 9", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 10", "This is the description of my category 10", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 11", "This is the description of my category 11", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 12", "This is the description of my category 12", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 13", "This is the description of my category 13", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 14", "This is the description of my category 14", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 15", "This is the description of my category 15", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 16", "This is the description of my category 16", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 17", "This is the description of my category 17", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 18", "This is the description of my category 18", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 19", "This is the description of my category 19", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 20", "This is the description of my category 20", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 21", "This is the description of my category 21", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 22", "This is the description of my category 22", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 23", "This is the description of my category 23", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 24", "This is the description of my category 24", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 25", "This is the description of my category 25", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 26", "This is the description of my category 26", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 27", "This is the description of my category 27", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 28", "This is the description of my category 28", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 29", "This is the description of my category 29", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 30", "This is the description of my category 30", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 31", "This is the description of my category 31", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 32", "This is the description of my category 32", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 33", "This is the description of my category 33", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 34", "This is the description of my category 34", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 35", "This is the description of my category 35", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 36", "This is the description of my category 36", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 37", "This is the description of my category 37", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 38", "This is the description of my category 38", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 39", "This is the description of my category 39", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 40", "This is the description of my category 40", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 41", "This is the description of my category 41", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 42", "This is the description of my category 42", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 43", "This is the description of my category 43", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 44", "This is the description of my category 44", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 45", "This is the description of my category 45", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 46", "This is the description of my category 46", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 47", "This is the description of my category 47", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 48", "This is the description of my category 48", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 49", "This is the description of my category 49", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 50", "This is the description of my category 50", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 51", "This is the description of my category 51", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 52", "This is the description of my category 52", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 53", "This is the description of my category 53", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 54", "This is the description of my category 54", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 55", "This is the description of my category 55", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 56", "This is the description of my category 56", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 57", "This is the description of my category 57", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 58", "This is the description of my category 58", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 59", "This is the description of my category 59", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 60", "This is the description of my category 60", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 61", "This is the description of my category 61", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 62", "This is the description of my category 62", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 63", "This is the description of my category 63", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 64", "This is the description of my category 64", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 65", "This is the description of my category 65", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 66", "This is the description of my category 66", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 67", "This is the description of my category 67", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 68", "This is the description of my category 68", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 69", "This is the description of my category 69", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 70", "This is the description of my category 70", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 71", "This is the description of my category 71", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 72", "This is the description of my category 72", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 73", "This is the description of my category 73", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 74", "This is the description of my category 74", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 75", "This is the description of my category 75", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 76", "This is the description of my category 76", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 77", "This is the description of my category 77", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 78", "This is the description of my category 78", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 79", "This is the description of my category 79", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 80", "This is the description of my category 80", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 81", "This is the description of my category 81", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 82", "This is the description of my category 82", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 83", "This is the description of my category 83", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 84", "This is the description of my category 84", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 85", "This is the description of my category 85", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 86", "This is the description of my category 86", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 87", "This is the description of my category 87", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 88", "This is the description of my category 88", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 89", "This is the description of my category 89", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 90", "This is the description of my category 90", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 91", "This is the description of my category 91", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 92", "This is the description of my category 92", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 93", "This is the description of my category 93", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 94", "This is the description of my category 94", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 95", "This is the description of my category 95", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 96", "This is the description of my category 96", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 97", "This is the description of my category 97", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 98", "This is the description of my category 98", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 99", "This is the description of my category 99", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 100", "This is the description of my category 100", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 101", "This is the description of my category 101", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 102", "This is the description of my category 102", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 103", "This is the description of my category 103", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 104", "This is the description of my category 104", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 105", "This is the description of my category 105", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 106", "This is the description of my category 106", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 107", "This is the description of my category 107", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 108", "This is the description of my category 108", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 109", "This is the description of my category 109", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 110", "This is the description of my category 110", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 111", "This is the description of my category 111", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 112", "This is the description of my category 112", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 113", "This is the description of my category 113", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 114", "This is the description of my category 114", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 115", "This is the description of my category 115", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 116", "This is the description of my category 116", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 117", "This is the description of my category 117", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 118", "This is the description of my category 118", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 119", "This is the description of my category 119", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 120", "This is the description of my category 120", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 121", "This is the description of my category 121", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 122", "This is the description of my category 122", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 123", "This is the description of my category 123", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 124", "This is the description of my category 124", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 125", "This is the description of my category 125", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 126", "This is the description of my category 126", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 127", "This is the description of my category 127", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 128", "This is the description of my category 128", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 129", "This is the description of my category 129", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 130", "This is the description of my category 130", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 131", "This is the description of my category 131", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 132", "This is the description of my category 132", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 133", "This is the description of my category 133", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 134", "This is the description of my category 134", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 135", "This is the description of my category 135", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 136", "This is the description of my category 136", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 137", "This is the description of my category 137", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 138", "This is the description of my category 138", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 139", "This is the description of my category 139", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 140", "This is the description of my category 140", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 141", "This is the description of my category 141", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 142", "This is the description of my category 142", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 143", "This is the description of my category 143", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 144", "This is the description of my category 144", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 145", "This is the description of my category 145", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 146", "This is the description of my category 146", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 147", "This is the description of my category 147", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 148", "This is the description of my category 148", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 149", "This is the description of my category 149", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 150", "This is the description of my category 150", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 151", "This is the description of my category 151", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 152", "This is the description of my category 152", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 153", "This is the description of my category 153", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 154", "This is the description of my category 154", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 155", "This is the description of my category 155", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 156", "This is the description of my category 156", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 157", "This is the description of my category 157", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 158", "This is the description of my category 158", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 159", "This is the description of my category 159", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 160", "This is the description of my category 160", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 161", "This is the description of my category 161", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 162", "This is the description of my category 162", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 163", "This is the description of my category 163", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 164", "This is the description of my category 164", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 165", "This is the description of my category 165", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 166", "This is the description of my category 166", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 167", "This is the description of my category 167", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 168", "This is the description of my category 168", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 169", "This is the description of my category 169", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 170", "This is the description of my category 170", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 171", "This is the description of my category 171", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 172", "This is the description of my category 172", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 173", "This is the description of my category 173", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 174", "This is the description of my category 174", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 175", "This is the description of my category 175", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 176", "This is the description of my category 176", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 177", "This is the description of my category 177", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 178", "This is the description of my category 178", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 179", "This is the description of my category 179", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 180", "This is the description of my category 180", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 181", "This is the description of my category 181", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 182", "This is the description of my category 182", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 183", "This is the description of my category 183", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 184", "This is the description of my category 184", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 185", "This is the description of my category 185", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 186", "This is the description of my category 186", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 187", "This is the description of my category 187", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 188", "This is the description of my category 188", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 189", "This is the description of my category 189", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 190", "This is the description of my category 190", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 191", "This is the description of my category 191", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 192", "This is the description of my category 192", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 193", "This is the description of my category 193", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 194", "This is the description of my category 194", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 195", "This is the description of my category 195", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 196", "This is the description of my category 196", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 197", "This is the description of my category 197", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 198", "This is the description of my category 198", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 199", "This is the description of my category 199", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 200", "This is the description of my category 200", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 201", "This is the description of my category 201", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 202", "This is the description of my category 202", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 203", "This is the description of my category 203", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 204", "This is the description of my category 204", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 205", "This is the description of my category 205", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 206", "This is the description of my category 206", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 207", "This is the description of my category 207", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 208", "This is the description of my category 208", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 209", "This is the description of my category 209", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 210", "This is the description of my category 210", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 211", "This is the description of my category 211", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 212", "This is the description of my category 212", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 213", "This is the description of my category 213", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 214", "This is the description of my category 214", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 215", "This is the description of my category 215", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 216", "This is the description of my category 216", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 217", "This is the description of my category 217", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 218", "This is the description of my category 218", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 219", "This is the description of my category 219", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 220", "This is the description of my category 220", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 221", "This is the description of my category 221", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 222", "This is the description of my category 222", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 223", "This is the description of my category 223", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 224", "This is the description of my category 224", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 225", "This is the description of my category 225", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 226", "This is the description of my category 226", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 227", "This is the description of my category 227", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 228", "This is the description of my category 228", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 229", "This is the description of my category 229", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 230", "This is the description of my category 230", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 231", "This is the description of my category 231", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 232", "This is the description of my category 232", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 233", "This is the description of my category 233", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 234", "This is the description of my category 234", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 235", "This is the description of my category 235", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 236", "This is the description of my category 236", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 237", "This is the description of my category 237", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 238", "This is the description of my category 238", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 239", "This is the description of my category 239", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 240", "This is the description of my category 240", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 241", "This is the description of my category 241", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 242", "This is the description of my category 242", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 243", "This is the description of my category 243", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 244", "This is the description of my category 244", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 245", "This is the description of my category 245", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 246", "This is the description of my category 246", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 247", "This is the description of my category 247", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 248", "This is the description of my category 248", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 249", "This is the description of my category 249", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "QuestionCategory",
               columns: new[] { "Id", "Name", "Description", "IsActive", "CreationTime", "IsDeleted", "QuestionCategoryTypeId" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
               values: new object[] { x++, "Category 250", "This is the description of my category 250", 1, DateTime.Now, 0, 1 });

            //Form Thematic

            x = 1;
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 1", "This is the description of my thematic 1", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 2", "This is the description of my thematic 2", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 3", "This is the description of my thematic 3", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 4", "This is the description of my thematic 4", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 5", "This is the description of my thematic 5", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 6", "This is the description of my thematic 6", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 7", "This is the description of my thematic 7", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 8", "This is the description of my thematic 8", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 9", "This is the description of my thematic 9", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 10", "This is the description of my thematic 10", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 11", "This is the description of my thematic 11", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 12", "This is the description of my thematic 12", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 13", "This is the description of my thematic 13", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 14", "This is the description of my thematic 14", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 15", "This is the description of my thematic 15", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 16", "This is the description of my thematic 16", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 17", "This is the description of my thematic 17", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 18", "This is the description of my thematic 18", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 19", "This is the description of my thematic 19", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 20", "This is the description of my thematic 20", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 21", "This is the description of my thematic 21", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 22", "This is the description of my thematic 22", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 23", "This is the description of my thematic 23", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 24", "This is the description of my thematic 24", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 25", "This is the description of my thematic 25", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 26", "This is the description of my thematic 26", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 27", "This is the description of my thematic 27", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 28", "This is the description of my thematic 28", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 29", "This is the description of my thematic 29", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 30", "This is the description of my thematic 30", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 31", "This is the description of my thematic 31", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 32", "This is the description of my thematic 32", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 33", "This is the description of my thematic 33", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 34", "This is the description of my thematic 34", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 35", "This is the description of my thematic 35", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 36", "This is the description of my thematic 36", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 37", "This is the description of my thematic 37", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 38", "This is the description of my thematic 38", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 39", "This is the description of my thematic 39", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 40", "This is the description of my thematic 40", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 41", "This is the description of my thematic 41", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 42", "This is the description of my thematic 42", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 43", "This is the description of my thematic 43", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 44", "This is the description of my thematic 44", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 45", "This is the description of my thematic 45", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 46", "This is the description of my thematic 46", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 47", "This is the description of my thematic 47", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 48", "This is the description of my thematic 48", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 49", "This is the description of my thematic 49", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 50", "This is the description of my thematic 50", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 51", "This is the description of my thematic 51", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 52", "This is the description of my thematic 52", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 53", "This is the description of my thematic 53", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 54", "This is the description of my thematic 54", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 55", "This is the description of my thematic 55", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 56", "This is the description of my thematic 56", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 57", "This is the description of my thematic 57", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 58", "This is the description of my thematic 58", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 59", "This is the description of my thematic 59", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 60", "This is the description of my thematic 60", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 61", "This is the description of my thematic 61", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 62", "This is the description of my thematic 62", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 63", "This is the description of my thematic 63", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 64", "This is the description of my thematic 64", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 65", "This is the description of my thematic 65", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 66", "This is the description of my thematic 66", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 67", "This is the description of my thematic 67", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 68", "This is the description of my thematic 68", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 69", "This is the description of my thematic 69", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 70", "This is the description of my thematic 70", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 71", "This is the description of my thematic 71", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 72", "This is the description of my thematic 72", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 73", "This is the description of my thematic 73", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 74", "This is the description of my thematic 74", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 75", "This is the description of my thematic 75", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 76", "This is the description of my thematic 76", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 77", "This is the description of my thematic 77", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 78", "This is the description of my thematic 78", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 79", "This is the description of my thematic 79", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 80", "This is the description of my thematic 80", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 81", "This is the description of my thematic 81", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 82", "This is the description of my thematic 82", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 83", "This is the description of my thematic 83", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 84", "This is the description of my thematic 84", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 85", "This is the description of my thematic 85", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 86", "This is the description of my thematic 86", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 87", "This is the description of my thematic 87", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 88", "This is the description of my thematic 88", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 89", "This is the description of my thematic 89", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 90", "This is the description of my thematic 90", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 91", "This is the description of my thematic 91", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 92", "This is the description of my thematic 92", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 93", "This is the description of my thematic 93", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 94", "This is the description of my thematic 94", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 95", "This is the description of my thematic 95", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 96", "This is the description of my thematic 96", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 97", "This is the description of my thematic 97", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 98", "This is the description of my thematic 98", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 99", "This is the description of my thematic 99", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 100", "This is the description of my thematic 100", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 101", "This is the description of my thematic 101", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 102", "This is the description of my thematic 102", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 103", "This is the description of my thematic 103", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 104", "This is the description of my thematic 104", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 105", "This is the description of my thematic 105", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 106", "This is the description of my thematic 106", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 107", "This is the description of my thematic 107", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 108", "This is the description of my thematic 108", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 109", "This is the description of my thematic 109", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 110", "This is the description of my thematic 110", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 111", "This is the description of my thematic 111", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 112", "This is the description of my thematic 112", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 113", "This is the description of my thematic 113", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 114", "This is the description of my thematic 114", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 115", "This is the description of my thematic 115", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 116", "This is the description of my thematic 116", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 117", "This is the description of my thematic 117", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 118", "This is the description of my thematic 118", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 119", "This is the description of my thematic 119", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 120", "This is the description of my thematic 120", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 121", "This is the description of my thematic 121", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 122", "This is the description of my thematic 122", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 123", "This is the description of my thematic 123", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 124", "This is the description of my thematic 124", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 125", "This is the description of my thematic 125", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 126", "This is the description of my thematic 126", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 127", "This is the description of my thematic 127", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 128", "This is the description of my thematic 128", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 129", "This is the description of my thematic 129", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 130", "This is the description of my thematic 130", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 131", "This is the description of my thematic 131", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 132", "This is the description of my thematic 132", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 133", "This is the description of my thematic 133", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 134", "This is the description of my thematic 134", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 135", "This is the description of my thematic 135", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 136", "This is the description of my thematic 136", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 137", "This is the description of my thematic 137", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 138", "This is the description of my thematic 138", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 139", "This is the description of my thematic 139", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 140", "This is the description of my thematic 140", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 141", "This is the description of my thematic 141", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 142", "This is the description of my thematic 142", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 143", "This is the description of my thematic 143", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 144", "This is the description of my thematic 144", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 145", "This is the description of my thematic 145", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 146", "This is the description of my thematic 146", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 147", "This is the description of my thematic 147", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 148", "This is the description of my thematic 148", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 149", "This is the description of my thematic 149", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 150", "This is the description of my thematic 150", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 151", "This is the description of my thematic 151", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 152", "This is the description of my thematic 152", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 153", "This is the description of my thematic 153", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 154", "This is the description of my thematic 154", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 155", "This is the description of my thematic 155", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 156", "This is the description of my thematic 156", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 157", "This is the description of my thematic 157", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 158", "This is the description of my thematic 158", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 159", "This is the description of my thematic 159", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 160", "This is the description of my thematic 160", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 161", "This is the description of my thematic 161", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 162", "This is the description of my thematic 162", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 163", "This is the description of my thematic 163", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 164", "This is the description of my thematic 164", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 165", "This is the description of my thematic 165", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 166", "This is the description of my thematic 166", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 167", "This is the description of my thematic 167", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 168", "This is the description of my thematic 168", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 169", "This is the description of my thematic 169", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 170", "This is the description of my thematic 170", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 171", "This is the description of my thematic 171", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 172", "This is the description of my thematic 172", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 173", "This is the description of my thematic 173", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 174", "This is the description of my thematic 174", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 175", "This is the description of my thematic 175", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 176", "This is the description of my thematic 176", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 177", "This is the description of my thematic 177", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 178", "This is the description of my thematic 178", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 179", "This is the description of my thematic 179", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 180", "This is the description of my thematic 180", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 181", "This is the description of my thematic 181", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 182", "This is the description of my thematic 182", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 183", "This is the description of my thematic 183", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 184", "This is the description of my thematic 184", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 185", "This is the description of my thematic 185", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 186", "This is the description of my thematic 186", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 187", "This is the description of my thematic 187", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 188", "This is the description of my thematic 188", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 189", "This is the description of my thematic 189", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 190", "This is the description of my thematic 190", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 191", "This is the description of my thematic 191", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 192", "This is the description of my thematic 192", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 193", "This is the description of my thematic 193", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 194", "This is the description of my thematic 194", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
                columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 195", "This is the description of my thematic 195", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 196", "This is the description of my thematic 196", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 197", "This is the description of my thematic 197", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 198", "This is the description of my thematic 198", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 199", "This is the description of my thematic 199", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "FormThematic",
               columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "long", "datetime", "long" },
               values: new object[] { x++, "My Thematic 200", "This is the description of my thematic 200", 1, DateTime.Now, 0 });

            //Form
            x = 1;
            migrationBuilder.InsertData(table: "Form",
              columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted", "FormNameId" },
              columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
              values: new object[] { x++, "Mi Distinción especial 1", "Descripción de mi distinción especial 1", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "Form",
              columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted", "FormNameId" },
              columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
              values: new object[] { x++, "Mi Distinción especial 2", "Descripción de mi distinción especial 2", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "Form",
              columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted", "FormNameId" },
              columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
              values: new object[] { x++, "Mi Distinción especial 3", "Descripción de mi distinción especial 3", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "Form",
              columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted", "FormNameId" },
              columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
              values: new object[] { x++, "Mi Distinción especial 4", "Descripción de mi distinción especial 4", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "Form",
              columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted", "FormNameId" },
              columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
              values: new object[] { x++, "Mi Distinción especial 5", "Descripción de mi distinción especial 5", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "Form",
              columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted", "FormNameId" },
              columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
              values: new object[] { x++, "Mi Distinción especial 6", "Descripción de mi distinción especial 6", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "Form",
              columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted", "FormNameId" },
              columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
              values: new object[] { x++, "Mi Distinción especial 7", "Descripción de mi distinción especial 7", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "Form",
              columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted", "FormNameId" },
              columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
              values: new object[] { x++, "Mi Distinción especial 8", "Descripción de mi distinción especial 8", 1, DateTime.Now, 0, 1 });
            migrationBuilder.InsertData(table: "Form",
              columns: new[] { "Id", "Title", "Description", "IsActive", "CreationTime", "IsDeleted", "FormNameId" },
              columnTypes: new[] { "long", "string", "string", "long", "datetime", "long", "long" },
              values: new object[] { x++, "Mi Distinción especial 9", "Descripción de mi distinción especial 9", 1, DateTime.Now, 0, 1 });*/

            //FormQuestions}
            //x = 1;
            /*migrationBuilder.InsertData(table: "FormQuestions",
                columns: new[] { "Id", "FormTypeId", "FormThematicId", "QuestionTypeId", "QuestionCategoryId", "ShortVersion","Tooltip","Tag", "IsActive", 
                    "CreationTime", "IsDeleted", "ImplementationTip" ,"UsageId","IsCertified","IsNullable","IsPriority","Question"},
                columnTypes: new[] { "long", "long", "long", "long", "long", "string", "string" , "string" ,"long",
                    "datetime","long","string","long","long","long","long","string"},
                values: new object[] { x++, "Mi Distinción especial 9", "Descripción de mi distinción especial 9", 1, DateTime.Now, 0, 1 });
            */
        }


        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

