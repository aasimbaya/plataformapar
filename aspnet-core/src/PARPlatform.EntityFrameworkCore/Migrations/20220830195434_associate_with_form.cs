﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class associate_with_form : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "QuestionCategoryIdArray",
                schema: "dbo",
                table: "Form",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SpecialDistinctionIdArray",
                schema: "dbo",
                table: "Form",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "QuestionCategoryIdArray",
                schema: "dbo",
                table: "Form");

            migrationBuilder.DropColumn(
                name: "SpecialDistinctionIdArray",
                schema: "dbo",
                table: "Form");
        }
    }
}
