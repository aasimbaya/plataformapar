﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class EnableIdioms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("update AbpLanguages set IsDisabled=1 where not Name in ('en-GB','es')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
