﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class setTenants : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("delete from dbo.AbpTenants");
            migrationBuilder.Sql("delete from dbo.AbpEditions");
            migrationBuilder.Sql("delete from dbo.Personal");
            migrationBuilder.Sql("UPDATE dbo.AbpUsers set EnterpriseId = null");
            migrationBuilder.Sql("delete from dbo.LineBusiness");
            migrationBuilder.Sql("delete from dbo.Enterprise");
            migrationBuilder.Sql("delete from dbo.Position");
            migrationBuilder.Sql("delete from dbo.PersonalType");

            //Table LineBusiness
            migrationBuilder.InsertData(table: "LineBusiness",
                columns: new[] { "Id", "Name", "IsActive", "CreationTime", "IsDeleted" }, columnTypes: new[] { "long", "string", "bool", "datetime", "long" },
                values: new object[] { "1", "Bursatil", "1", DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "LineBusiness",
                columns: new[] { "Id", "Name", "IsActive", "CreationTime", "IsDeleted" }, columnTypes: new[] { "long", "string", "bool", "datetime", "long" },
                values: new object[] { "2", "Bienes raises", "1", DateTime.Now, 0 });

            //Tenants
            migrationBuilder.InsertData(table: "AbpEditions",
                columns: new[] { "Id", "CreationTime", "DisplayName", "IsDeleted", "Name", "Discriminator" },
                columnTypes: new[] { "long", "datetime", "string", "long", "string", "string" },
                values: new object[] { "1", DateTime.Now, "Standard", 0, "Standard", "SubscribableEdition" });

            migrationBuilder.InsertData(table: "AbpTenants",
               columns: new[] { "Id", "CreationTime", "EditionId", "IsActive", "IsDeleted", "Name", "TenancyName", "IsInTrialPeriod", "SubscriptionPaymentType" },
               columnTypes: new[] { "long", "datetime", "long", "bool", "bool", "string", "string", "bool", "long" },
               values: new object[] { "1", DateTime.Now, "1", "1", 0, "Aequales", "Aequales", 0, 0 });
            migrationBuilder.InsertData(table: "AbpTenants",
               columns: new[] { "Id", "CreationTime", "EditionId", "IsActive", "IsDeleted", "Name", "TenancyName", "IsInTrialPeriod", "SubscriptionPaymentType" },
               columnTypes: new[] { "long", "datetime", "long", "bool", "bool", "string", "string", "bool", "long" },
               values: new object[] { "2", DateTime.Now, "1", "1", 0, "Enterprise", "Enterprise", 0, 0 });
            migrationBuilder.InsertData(table: "AbpTenants",
               columns: new[] { "Id", "CreationTime", "EditionId", "IsActive", "IsDeleted", "Name", "TenancyName", "IsInTrialPeriod", "SubscriptionPaymentType" },
               columnTypes: new[] { "long", "datetime", "long", "bool", "bool", "string", "string", "bool", "long" },
               values: new object[] { "3", DateTime.Now, "1", "1", 0, "Extern", "Extern", 0, 0 });


            //PersonalType
            migrationBuilder.InsertData(table: "PersonalType",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "CreatedAt", "IsActive", "IsDeleted", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long", "datetime", "datetime" },
               values: new object[] { "1", "Gerencia", "Gerencia", "CEO", DateTime.Now, 1, 0, DateTime.Now, DateTime.Now });

            migrationBuilder.InsertData(table: "PersonalType",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "CreatedAt", "IsActive", "IsDeleted", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long", "datetime", "datetime" },
               values: new object[] { "2", "Recursos Humanos", "Recursos Humanos", "HR", DateTime.Now, 1, 0, DateTime.Now, DateTime.Now });

            migrationBuilder.InsertData(table: "PersonalType",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "CreatedAt", "IsActive", "IsDeleted", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long", "datetime", "datetime" },
               values: new object[] { "3", "Comunicación", "Comunicación", "COM", DateTime.Now, 1, 0, DateTime.Now, DateTime.Now });

            //Position
            migrationBuilder.InsertData(table: "Position",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "IsActive", "IsDeleted", "CreatedAt", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string", "string", "long", "long", "datetime", "datetime", "datetime" },
               values: new object[] { "1", "Gerencia", "Gerencia", "Gerencia", 1, 0, DateTime.Now, DateTime.Now, DateTime.Now });


            migrationBuilder.InsertData(table: "Position",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "IsActive", "IsDeleted", "CreatedAt", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string", "string", "long", "long", "datetime", "datetime", "datetime" },
               values: new object[] { "2", "Conserje", "Conserje", "Conserje", 1, 0, DateTime.Now, DateTime.Now, DateTime.Now });

            migrationBuilder.InsertData(table: "Position",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "IsActive", "IsDeleted", "CreatedAt", "UpdatedAt", "CreationTime" },
               columnTypes: new[] { "long", "string", "string", "string", "long", "long", "datetime", "datetime", "datetime" },
               values: new object[] { "3", "Contador", "Contador", "Contador", 1, 0, DateTime.Now, DateTime.Now, DateTime.Now });


            //Table Enterprise
            migrationBuilder.InsertData(table: "Enterprise",
                columns: new[] { "Id", "Name", "IdNumber", "IsActive", "UserId", "CountryId", "RegionId", "CityId", "CreationTime", "CreatorUserId", "LastModificationTime", "LastModifierUserId", "IsDeleted", "EmployeesNumber", "HasInclusion", "IsInternational", "LineBusinessId", "SectorId", "ReceivePromoAccepted", "TenantId" },
                columnTypes: new[] { "long", "string", "string", "bool", "long", "long", "long", "long", "datetime", "long", "datetime", "long", "bool", "int", "bool", "bool", "long", "long", "bool", "int" },
                values: new object[] { "1", "primera compañia", "1", "1", "1", "1", "1", "1", DateTime.Now, "1", DateTime.Now, 1, 0, 1, 1, 0, 1, 1, 0, 0 });

            migrationBuilder.InsertData(table: "Enterprise",
                columns: new[] { "Id", "Name", "IdNumber", "IsActive", "UserId", "CountryId", "RegionId", "CityId", "CreationTime", "CreatorUserId", "LastModificationTime", "LastModifierUserId", "IsDeleted", "EmployeesNumber", "HasInclusion", "IsInternational", "LineBusinessId", "SectorId", "ReceivePromoAccepted", "TenantId" },
                columnTypes: new[] { "long", "string", "string", "bool", "long", "long", "long", "long", "datetime", "long", "datetime", "long", "bool", "int", "bool", "bool", "long", "long", "bool", "int" },
                values: new object[] { "2", "con tenan", "1", "1", "1", "1", "1", "1", DateTime.Now, "1", DateTime.Now, 1, 0, 1, 1, 0, 1, 1, 0, 1 });

            //Table Personal
            migrationBuilder.InsertData(table: "Personal",
                columns: new[] { "Id", "Name", "Email", "CellPhone", "IsActive", "CreatedAt", "UpdatedAt", "PersonalTypeId", "EnterpriseId", "CreationTime", "CreatorUserId", "LastModificationTime", "LastModifierUserId", "IsDeleted", "PositionId" },
                columnTypes: new[] { "long", "string", "string", "string", "bool", "datetime", "datetime", "long", "long", "datetime", "long", "datetime", "long", "bool", "long" },
                values: new object[] { "1", "gerentehhrr", "gerentehhrr@gmail.com", "+582123334443", 1, DateTime.Now, DateTime.Now, 1, 1, DateTime.Now, 1, DateTime.Now, 1, 0, 1 });

            migrationBuilder.InsertData(table: "Personal",
                columns: new[] { "Id", "Name", "Email", "CellPhone", "IsActive", "CreatedAt", "UpdatedAt", "PersonalTypeId", "EnterpriseId", "CreationTime", "CreatorUserId", "LastModificationTime", "LastModifierUserId", "IsDeleted", "PositionId" },
                columnTypes: new[] { "long", "string", "string", "string", "bool", "datetime", "datetime", "long", "long", "datetime", "long", "datetime", "long", "bool", "long" },
                values: new object[] { "2", "gerenteceo", "gerenteceo@fe.com", "+582123334442", 1, DateTime.Now, DateTime.Now, 1, 1, DateTime.Now, 1, DateTime.Now, 1, 0, 1 });

            migrationBuilder.Sql("UPDATE dbo.AbpUsers set TenantId = 1");
            migrationBuilder.Sql("UPDATE dbo.AbpRoles set TenantId = 1");
            migrationBuilder.Sql("UPDATE dbo.AbpUserRoles set TenantId = 1");
            migrationBuilder.Sql("UPDATE dbo.AbpPermissions set TenantId = 1");
            migrationBuilder.Sql("UPDATE dbo.AbpUserAccounts set TenantId = 1");
            migrationBuilder.Sql("UPDATE dbo.AbpUserNotifications set TenantId = 1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
