﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class setPrivileges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(table: "AbpPermissions",
               columns: new[] { "CreationTime", "CreatorUserId", "Discriminator", "IsGranted", "Name", "TenantId", "RoleId" },
               columnTypes: new[] { "datetime", "long", "string", "bool", "string", "long", "long" },
               values: new object[] { DateTime.Now, "1", "RolePermissionSetting", "0", "Pages.DashboardName", "1", "1" });
            
            migrationBuilder.InsertData(table: "AbpPermissions",
               columns: new[] { "CreationTime", "CreatorUserId", "Discriminator", "IsGranted", "Name", "TenantId", "RoleId" },
               columnTypes: new[] { "datetime", "long", "string", "bool", "string", "long", "long" },
               values: new object[] { DateTime.Now, "1", "RolePermissionSetting", "0", "Pages.Measuringtools.ScoreTypes", "1", "1" });

            migrationBuilder.InsertData(table: "AbpPermissions",
               columns: new[] { "CreationTime", "CreatorUserId", "Discriminator", "IsGranted", "Name", "TenantId", "RoleId" },
               columnTypes: new[] { "datetime", "long", "string", "bool", "string", "long", "long" },
               values: new object[] { DateTime.Now, "1", "RolePermissionSetting", "0", "Pages.ResultsConfiguration.EquityScale", "1", "1" });

            migrationBuilder.InsertData(table: "AbpPermissions",
               columns: new[] { "CreationTime", "CreatorUserId", "Discriminator", "IsGranted", "Name", "TenantId", "RoleId" },
               columnTypes: new[] { "datetime", "long", "string", "bool", "string", "long", "long" },
               values: new object[] { DateTime.Now, "1", "RolePermissionSetting", "0", "Pages.ResultsConfiguration.Recommendations", "1", "1" }); 
            
            migrationBuilder.InsertData(table: "AbpPermissions",
               columns: new[] { "CreationTime", "CreatorUserId", "Discriminator", "IsGranted", "Name", "TenantId", "RoleId" },
               columnTypes: new[] { "datetime", "long", "string", "bool", "string", "long", "long" },
               values: new object[] { DateTime.Now, "1", "RolePermissionSetting", "0", "Pages.ResultsConfiguration.GroupFeedBack", "1", "1" });

            migrationBuilder.InsertData(table: "AbpPermissions",
               columns: new[] { "CreationTime", "CreatorUserId", "Discriminator", "IsGranted", "Name", "TenantId", "RoleId" },
               columnTypes: new[] { "datetime", "long", "string", "bool", "string", "long", "long" },
               values: new object[] { DateTime.Now, "1", "RolePermissionSetting", "0", "Pages.ResultsConfiguration.SubscriptionPlans", "1", "1" });
            
            migrationBuilder.InsertData(table: "AbpPermissions",
               columns: new[] { "CreationTime", "CreatorUserId", "Discriminator", "IsGranted", "Name", "TenantId", "RoleId" },
               columnTypes: new[] { "datetime", "long", "string", "bool", "string", "long", "long" },
               values: new object[] { DateTime.Now, "1", "RolePermissionSetting", "0", "Pages.Content.ModulesForExternalUsers", "1", "1" });

            migrationBuilder.InsertData(table: "AbpPermissions",
               columns: new[] { "CreationTime", "CreatorUserId", "Discriminator", "IsGranted", "Name", "TenantId", "RoleId" },
               columnTypes: new[] { "datetime", "long", "string", "bool", "string", "long", "long" },
               values: new object[] { DateTime.Now, "1", "RolePermissionSetting", "0", "Pages.OtherProducts", "1", "1" });
            
            migrationBuilder.InsertData(table: "AbpPermissions",
               columns: new[] { "CreationTime", "CreatorUserId", "Discriminator", "IsGranted", "Name", "TenantId", "RoleId" },
               columnTypes: new[] { "datetime", "long", "string", "bool", "string", "long", "long" },
               values: new object[] { DateTime.Now, "1", "RolePermissionSetting", "0", "Pages.Evaluations", "1", "1" });

            migrationBuilder.InsertData(table: "AbpPermissions",
               columns: new[] { "CreationTime", "CreatorUserId", "Discriminator", "IsGranted", "Name", "TenantId", "RoleId" },
               columnTypes: new[] { "datetime", "long", "string", "bool", "string", "long", "long" },
               values: new object[] { DateTime.Now, "1", "RolePermissionSetting", "0", "Pages.SettingsMain.SearchOptions", "1", "1" });

            //migrationBuilder.InsertData(table: "AbpPermissions",
            //   columns: new[] { "CreationTime", "CreatorUserId", "Discriminator", "IsGranted", "Name", "TenantId", "RoleId" },
            //   columnTypes: new[] { "datetime", "long", "string", "bool", "string", "long", "long" },
            //   values: new object[] { DateTime.Now, "1", "RolePermissionSetting", "1", "Pages.Measuringtools.Categories", "1", "1" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
