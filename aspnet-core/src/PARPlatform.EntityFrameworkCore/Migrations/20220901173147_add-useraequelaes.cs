﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class adduseraequelaes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("UPDATE dbo.AbpUsers set " +
                "EmailAddress = 'aequales@aspnetzero.com', " +
                "Name = 'aequeles', " +
                "NormalizedEmailAddress = 'AEQUALES@ASPNETZERO.COM', " +
                "NormalizedUserName = 'AEQUALES', " +
                "Surname = 'aequeles', " +
                "TenantId = 1, " +
                "UserName = 'aequeles' " +
                "where id = 1");

            migrationBuilder.Sql("UPDATE dbo.AbpEditions set " +
                "DisplayName = 'Standard', " +
                "Name = 'Standard', " +
                "Discriminator = 'SubscribableEdition' " +
                "where id = 1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
