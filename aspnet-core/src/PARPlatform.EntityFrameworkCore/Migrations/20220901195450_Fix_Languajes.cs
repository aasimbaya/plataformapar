﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Fix_Languajes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("update AbpLanguages set IsDisabled=1,IsDeleted=1");
            migrationBuilder.Sql("update AbpLanguages set IsDisabled=0,IsDeleted=0 where Name in ('en','es')");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
