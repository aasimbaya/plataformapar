﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class DataDocuments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.Sql("delete from dbo.Document");
            migrationBuilder.Sql("delete from dbo.DocumentEstructure");
            migrationBuilder.Sql("delete from dbo.DocumentLocation");
            migrationBuilder.Sql("delete from dbo.DocumentLanguage");
            
            migrationBuilder.InsertData(table: "DocumentEstructure",
                columns: new[] { "Id", "Name", "Description", "Abreviation", "Estructure", "IsActive", "CreationTime", "IsDeleted" },
                columnTypes: new[] { "long", "string", "string", "string", "string", "long", "datetime", "long" },
                values: new object[] { "1", "Empresa", "Empresa", "EM", "Empresa", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "DocumentEstructure",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "Estructure", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "string", "string", "long", "datetime", "long" },
               values: new object[] { "2", "Administrador Externo", "Administrador Externo", "AE", "Administrador Externo", 1, DateTime.Now, 0 });

            migrationBuilder.InsertData(table: "DocumentLocation",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "Location", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "string", "string", "long", "datetime", "long" },
               values: new object[] { "1", "Pantalla de Registro", "Pantalla de Registro", "PR", "Pantalla de Registro", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "DocumentLocation",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "Location", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "string", "string", "long", "datetime", "long" },
               values: new object[] { "2", "Pantalla de Inicio de Sesión", "Pantalla de Inicio de Sesión", "PI", "Pantalla de Inicio de Sesión", 1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "DocumentLocation",
               columns: new[] { "Id", "Name", "Description", "Abreviation", "Location", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "string", "string", "long", "datetime", "long" },
               values: new object[] { "3", "Documentos", "Documentos", "PI", "Documentos", 1, DateTime.Now, 0 });

            migrationBuilder.InsertData(table: "DocumentLanguage",
              columns: new[] { "Id", "Name", "Description", "Language", "Location", "IsActive", "CreationTime", "IsDeleted" },
              columnTypes: new[] { "long", "string", "string", "string", "string", "long", "datetime", "long" },
              values: new object[] { "1", "Español", "Español", "ES","Pantalla",1, DateTime.Now, 0 });
            migrationBuilder.InsertData(table: "DocumentLanguage",
               columns: new[] { "Id", "Name", "Description", "Language", "Location", "IsActive", "CreationTime", "IsDeleted" },
               columnTypes: new[] { "long", "string", "string", "string", "string", "long", "datetime", "long" },
               values: new object[] { "2", "Ingles", "Ingles", "EN", "Ingles", 1, DateTime.Now, 0 });
             
            migrationBuilder.InsertData(table: "Document",
              columns: new[] { "Id", "Name", "EstructureId", "EstructureFkId", "LanguageId",
                  "LanguageFkId", "LocationId", "LocationFkId","URL","Location","IsActive", "CreationTime", "IsDeleted"  },
              columnTypes: new[] { "long", "string", "long", "long", "long", "long", "long", "long", "string", "string", "long", "datetime", "long" },
              values: new object[] { "1", "Acta de preguntas", 1, 1, 1, 1, 1, 1, "servidor", "NoAplica", 1, DateTime.Now, 0 });
            

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
