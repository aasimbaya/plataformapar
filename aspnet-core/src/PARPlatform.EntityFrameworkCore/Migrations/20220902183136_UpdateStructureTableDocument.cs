﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class UpdateStructureTableDocument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Document_DocumentEstructure_EstructureFkId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropForeignKey(
                name: "FK_Document_DocumentLanguage_LanguageFkId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropForeignKey(
                name: "FK_Document_DocumentLocation_LocationFkId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropIndex(
                name: "IX_Document_EstructureFkId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropIndex(
                name: "IX_Document_LanguageFkId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropIndex(
                name: "IX_Document_LocationFkId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropColumn(
                name: "EstructureFkId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropColumn(
                name: "LanguageFkId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropColumn(
                name: "LocationFkId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.CreateIndex(
                name: "IX_Document_EstructureId",
                schema: "dbo",
                table: "Document",
                column: "EstructureId");

            migrationBuilder.CreateIndex(
                name: "IX_Document_LanguageId",
                schema: "dbo",
                table: "Document",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Document_LocationId",
                schema: "dbo",
                table: "Document",
                column: "LocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Document_DocumentEstructure_EstructureId",
                schema: "dbo",
                table: "Document",
                column: "EstructureId",
                principalSchema: "dbo",
                principalTable: "DocumentEstructure",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Document_DocumentLanguage_LanguageId",
                schema: "dbo",
                table: "Document",
                column: "LanguageId",
                principalSchema: "dbo",
                principalTable: "DocumentLanguage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Document_DocumentLocation_LocationId",
                schema: "dbo",
                table: "Document",
                column: "LocationId",
                principalSchema: "dbo",
                principalTable: "DocumentLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Document_DocumentEstructure_EstructureId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropForeignKey(
                name: "FK_Document_DocumentLanguage_LanguageId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropForeignKey(
                name: "FK_Document_DocumentLocation_LocationId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropIndex(
                name: "IX_Document_EstructureId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropIndex(
                name: "IX_Document_LanguageId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.DropIndex(
                name: "IX_Document_LocationId",
                schema: "dbo",
                table: "Document");

            migrationBuilder.AddColumn<long>(
                name: "EstructureFkId",
                schema: "dbo",
                table: "Document",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LanguageFkId",
                schema: "dbo",
                table: "Document",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LocationFkId",
                schema: "dbo",
                table: "Document",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Document_EstructureFkId",
                schema: "dbo",
                table: "Document",
                column: "EstructureFkId");

            migrationBuilder.CreateIndex(
                name: "IX_Document_LanguageFkId",
                schema: "dbo",
                table: "Document",
                column: "LanguageFkId");

            migrationBuilder.CreateIndex(
                name: "IX_Document_LocationFkId",
                schema: "dbo",
                table: "Document",
                column: "LocationFkId");

            migrationBuilder.AddForeignKey(
                name: "FK_Document_DocumentEstructure_EstructureFkId",
                schema: "dbo",
                table: "Document",
                column: "EstructureFkId",
                principalSchema: "dbo",
                principalTable: "DocumentEstructure",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Document_DocumentLanguage_LanguageFkId",
                schema: "dbo",
                table: "Document",
                column: "LanguageFkId",
                principalSchema: "dbo",
                principalTable: "DocumentLanguage",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Document_DocumentLocation_LocationFkId",
                schema: "dbo",
                table: "Document",
                column: "LocationFkId",
                principalSchema: "dbo",
                principalTable: "DocumentLocation",
                principalColumn: "Id");
        }
    }
}
