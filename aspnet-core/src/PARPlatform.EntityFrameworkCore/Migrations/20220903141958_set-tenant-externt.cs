﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class settenantexternt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //            migrationBuilder.Sql("INSERT INTO[dbo].[AbpUsers] " +
            //                "([AccessFailedCount] " +
            //                ",[ConcurrencyStamp] " +
            //                ",[CreationTime] " +
            //                ",[EmailAddress] " +
            //                ",[IsActive] " +
            //                ",[IsDeleted] " +
            //                ",[IsEmailConfirmed] " +
            //                ",[IsLockoutEnabled] " +
            //                ",[IsPhoneNumberConfirmed] " +
            //                ",[IsTwoFactorEnabled] " +
            //                ",[Name] " +
            //                ",[NormalizedEmailAddress] " +
            //                ",[NormalizedUserName] " +
            //                ",[Password] " +
            //                ",[SecurityStamp] " +
            //                ",[ShouldChangePasswordOnNextLogin] " +
            //                ",[Surname] " +
            //                ",[TenantId] " +
            //                ",[UserName] " +
            //                ") " +
            //                "VALUES " +
            //                "(0 " +
            //                ", '78fcb0a5-1236-4c53-a074-43955b02ce38' " +
            //                ", GETDATE() " +
            //                ", 'GrupoEmpresa@webcreek.com' " +
            //                ", 1 " +
            //                ", 0 " +
            //                ", 1 " +
            //                ", 1 " +
            //                ", 0 " +
            //                ", 1 " +
            //                ", 'GrupoEmpresa' " +
            //                ", 'GRUPOEmpresa@WEBCREEK.COM' " +
            //                ", 'GRUPOEMPRESA' " +
            //                ", 'AQAAAAEAACcQAAAAEO65RTAVrEoO0NS5yn/coLdvzQAGU9Jx/ChAIxyZzXXPP+iopymE8uDFw0nJjZBhxg==' " +
            //                ", 'GSSIA2YZRKRANCQUOEH36AABENSO3LU4' " +
            //                ", 0 " +
            //                ", 'GroupEnterprise' " +
            //                ", 3 " +
            //                ", 'grupoempresa' " +
            //                ") "
            //);

            migrationBuilder.Sql("INSERT INTO[dbo].[AbpRoles]  " +
                "([ConcurrencyStamp]  " +
                ",[CreationTime] " +
                ",[DisplayName]  " +
                ",[IsDefault]  " +
                ",[IsDeleted]  " +
                ",[IsStatic]  " +
                ",[Name]  " +
                ",[NormalizedName]  " +
                ",[TenantId]) " +
                "VALUES  " +
                "('65b3bea4-0a3a-4ecc-a7fd-6c3bcfbc6575'  " +
                ", GETDATE() " +
                ", 'ExternalUser'  " +
                ", 0  " +
                ", 0  " +
                ", 1  " +
                ", 'ExternalUser'  " +
                ", 'EXTERNALUSER'  " +
                ", 3)  "
                );
            migrationBuilder.Sql("INSERT INTO[dbo].[AbpRoles]  " +
               "([ConcurrencyStamp]  " +
               ",[CreationTime] " +
               ",[DisplayName]  " +
               ",[IsDefault]  " +
               ",[IsDeleted]  " +
               ",[IsStatic]  " +
               ",[Name]  " +
               ",[NormalizedName]  " +
               ",[TenantId]) " +
               "VALUES  " +
               "('65b3bea4-0a3a-4ecc-a7fd-6c3bcfbc6576'  " +
               ", GETDATE() " +
               ", 'Enterprise'  " +
               ", 0  " +
               ", 0  " +
               ", 1  " +
               ", 'Enterprise'  " +
               ", 'ENTERPRISE'  " +
               ", 2)  "
               );
            //            migrationBuilder.Sql("INSERT INTO[dbo].[AbpUserRoles] " +
            //                "([CreationTime] " +
            //                ",[RoleId] " +
            //                ",[TenantId] " +
            //                ",[UserId]) " +
            //                "VALUES " +
            //                "( " +
            //                "GETDATE() " +
            //                ", (select id from AbpRoles where ConcurrencyStamp = '65b3bea4-0a3a-4ecc-a7fd-6c3bcfbc6575') " +
            //                ",3 " +
            //                ",(select id from AbpUsers where name = 'GrupoEmpresa') " +
            //                ") "
            //               );

            //            migrationBuilder.Sql("INSERT INTO[dbo].[AbpUserAccounts] " +
            //                "([CreationTime] " +
            //                ",[EmailAddress] " +
            //                ",[IsDeleted] " +
            //                ",[TenantId] " +
            //                ",[UserId] " +
            //                ",[UserName]) " +
            //                "VALUES " +
            //                "(GETDATE() " +
            //                ", 'GrupoEmpresa@webcreek.com' " +
            //                ", 0 " +
            //                ", 3 " +
            //                ", (select id from AbpUsers where name = 'GrupoEmpresa') " +
            //                ",'grupoempresa') "
            //               );

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
