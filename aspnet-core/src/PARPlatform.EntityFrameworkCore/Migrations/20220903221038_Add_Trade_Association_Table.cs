﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Add_Trade_Association_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EconomicGroupName",
                schema: "dbo",
                table: "EnterpriseGroup",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsPartOfEconomicGroup",
                schema: "dbo",
                table: "EnterpriseGroup",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsPartOfTradeAssociation",
                schema: "dbo",
                table: "EnterpriseGroup",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "TradeAssociationId",
                schema: "dbo",
                table: "EnterpriseGroup",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EconomicGroupName",
                schema: "dbo",
                table: "Enterprise",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsPartOfEconomicGroup",
                schema: "dbo",
                table: "Enterprise",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsPartOfTradeAssociation",
                schema: "dbo",
                table: "Enterprise",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "TradeAssociationId",
                schema: "dbo",
                table: "Enterprise",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TradeAssociation",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CountryId = table.Column<long>(type: "bigint", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TradeAssociation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TradeAssociation_Country_CountryId",
                        column: x => x.CountryId,
                        principalSchema: "dbo",
                        principalTable: "Country",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "EnterpriseGroup_FK_TradeAssociationId_index",
                schema: "dbo",
                table: "EnterpriseGroup",
                column: "TradeAssociationId");

            migrationBuilder.CreateIndex(
                name: "Enterprise_FK_TradeAssociationId_index",
                schema: "dbo",
                table: "Enterprise",
                column: "TradeAssociationId");

            migrationBuilder.CreateIndex(
                name: "TradeAssociation_FK_CountryId_index",
                schema: "dbo",
                table: "TradeAssociation",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Enterprise_TradeAssociation_TradeAssociationId",
                schema: "dbo",
                table: "Enterprise",
                column: "TradeAssociationId",
                principalSchema: "dbo",
                principalTable: "TradeAssociation",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EnterpriseGroup_TradeAssociation_TradeAssociationId",
                schema: "dbo",
                table: "EnterpriseGroup",
                column: "TradeAssociationId",
                principalSchema: "dbo",
                principalTable: "TradeAssociation",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enterprise_TradeAssociation_TradeAssociationId",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropForeignKey(
                name: "FK_EnterpriseGroup_TradeAssociation_TradeAssociationId",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropTable(
                name: "TradeAssociation",
                schema: "dbo");

            migrationBuilder.DropIndex(
                name: "EnterpriseGroup_FK_TradeAssociationId_index",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropIndex(
                name: "Enterprise_FK_TradeAssociationId_index",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropColumn(
                name: "EconomicGroupName",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropColumn(
                name: "IsPartOfEconomicGroup",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropColumn(
                name: "IsPartOfTradeAssociation",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropColumn(
                name: "TradeAssociationId",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropColumn(
                name: "EconomicGroupName",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropColumn(
                name: "IsPartOfEconomicGroup",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropColumn(
                name: "IsPartOfTradeAssociation",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropColumn(
                name: "TradeAssociationId",
                schema: "dbo",
                table: "Enterprise");
        }
    }
}
