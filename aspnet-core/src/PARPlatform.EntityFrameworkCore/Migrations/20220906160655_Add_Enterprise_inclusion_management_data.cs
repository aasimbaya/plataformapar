﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Add_Enterprise_inclusion_management_data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "InclusionRegionalManagerEmail",
                schema: "dbo",
                table: "EnterpriseGroup",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InclusionRegionalManagerName",
                schema: "dbo",
                table: "EnterpriseGroup",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InclusionRegionalManagerEmail",
                schema: "dbo",
                table: "Enterprise",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InclusionRegionalManagerName",
                schema: "dbo",
                table: "Enterprise",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InclusionRegionalManagerEmail",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropColumn(
                name: "InclusionRegionalManagerName",
                schema: "dbo",
                table: "EnterpriseGroup");

            migrationBuilder.DropColumn(
                name: "InclusionRegionalManagerEmail",
                schema: "dbo",
                table: "Enterprise");

            migrationBuilder.DropColumn(
                name: "InclusionRegionalManagerName",
                schema: "dbo",
                table: "Enterprise");
        }
    }
}
