﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Add_Country_Fields_Length : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NicRucLength",
                schema: "dbo",
                table: "Country",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PhoneLength",
                schema: "dbo",
                table: "Country",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NicRucLength",
                schema: "dbo",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "PhoneLength",
                schema: "dbo",
                table: "Country");
        }
    }
}
