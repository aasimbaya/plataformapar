﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class Include_Structure_Question_Type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(table: "QuestionType",
               columns: new[] { "Id", "Name", "Description", "Type", "CreationTime", "IsDeleted", "IsActive" },
               columnTypes: new[] { "long", "string", "string", "string", "datetime", "long", "long" },
               values: new object[] { "5", "Estructura", "Estructura", "structure", DateTime.Now, 0, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
