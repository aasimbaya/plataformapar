﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PARPlatform.Migrations
{
    public partial class addEnterpriseTypeId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EmterpriseTypeId",
                schema: "dbo",
                table: "Enterprise",
                newName: "EnterpriseTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EnterpriseTypeId",
                schema: "dbo",
                table: "Enterprise",
                newName: "EmterpriseTypeId");
        }
    }
}
