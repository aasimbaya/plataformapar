﻿using PARPlatform.EntityFrameworkCore;

namespace PARPlatform.Migrations.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly PARPlatformDbContext _context;

        public InitialHostDbBuilder(PARPlatformDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();

            _context.SaveChanges();
        }
    }
}
