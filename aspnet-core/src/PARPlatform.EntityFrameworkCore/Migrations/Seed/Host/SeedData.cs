﻿using Abp;
using Abp.Dependency;
using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PARPlatform.EntityFrameworkCore;
using PARPlatform.PAREntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PARPlatform.Migrations.Seed.Host
{
    public class SeedData
    {
        private readonly PARPlatformDbContext _context;
        
        public SeedData(PARPlatformDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateDataAsync();
        }



        private void CreateDataAsync()
        {

            ActivateIdentityInsertAll();

            _context.Add(new QuestionCategoryType { Id = 1, Name = "Opcional" });
            _context.Add(new QuestionCategoryType { Id = 2, Name = "Obligatoria" });
            _context.SaveChanges();
            
            DeactivateIdentityInsertAll();

        }

        private void ActivateIdentityInsertAll()
        {
            ActivateIdentityInsert("QuestionCategoryType");
        }

        private void DeactivateIdentityInsertAll()
        {
            DeactivateIdentityInsert("QuestionCategoryType");

        }

        private void ActivateIdentityInsert(String entity) {
            _context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT "+ entity + " ON");
        }

        private void DeactivateIdentityInsert(String entity)
        {
            _context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT " + entity + " OFF");
        }
    }
}
