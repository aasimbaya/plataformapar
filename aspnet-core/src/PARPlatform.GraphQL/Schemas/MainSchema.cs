﻿using Abp.Dependency;
using GraphQL.Types;
using GraphQL.Utilities;
using PARPlatform.Queries.Container;
using System;

namespace PARPlatform.Schemas
{
    public class MainSchema : Schema, ITransientDependency
    {
        public MainSchema(IServiceProvider provider) :
            base(provider)
        {
            Query = provider.GetRequiredService<QueryContainer>();
        }
    }
}