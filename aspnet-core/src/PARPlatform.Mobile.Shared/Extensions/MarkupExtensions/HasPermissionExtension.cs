﻿using System;
using PARPlatform.Core;
using PARPlatform.Core.Dependency;
using PARPlatform.Services.Permission;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PARPlatform.Extensions.MarkupExtensions
{
    [ContentProperty("Text")]
    public class HasPermissionExtension : IMarkupExtension
    {
        public string Text { get; set; }
        
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (ApplicationBootstrapper.AbpBootstrapper == null || Text == null)
            {
                return false;
            }

            var permissionService = DependencyResolver.Resolve<IPermissionService>();
            return permissionService.HasPermission(Text);
        }
    }
}