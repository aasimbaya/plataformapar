﻿using Abp.AutoMapper;
using PARPlatform.Organizations.Dto;

namespace PARPlatform.Models.Users
{
    [AutoMapFrom(typeof(OrganizationUnitDto))]
    public class OrganizationUnitModel : OrganizationUnitDto
    {
        public bool IsAssigned { get; set; }
    }
}