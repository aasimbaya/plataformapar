﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace PARPlatform
{
    [DependsOn(typeof(PARPlatformClientModule), typeof(AbpAutoMapperModule))]
    public class PARPlatformXamarinSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.IsEnabled = false;
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PARPlatformXamarinSharedModule).GetAssembly());
        }
    }
}