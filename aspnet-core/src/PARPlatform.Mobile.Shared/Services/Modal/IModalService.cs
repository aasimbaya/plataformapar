﻿using System.Threading.Tasks;
using PARPlatform.Views;
using Xamarin.Forms;

namespace PARPlatform.Services.Modal
{
    public interface IModalService
    {
        Task ShowModalAsync(Page page);

        Task ShowModalAsync<TView>(object navigationParameter) where TView : IXamarinView;

        Task<Page> CloseModalAsync();
    }
}
