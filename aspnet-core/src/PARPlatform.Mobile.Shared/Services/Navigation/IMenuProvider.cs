using System.Collections.Generic;
using MvvmHelpers;
using PARPlatform.Models.NavigationMenu;

namespace PARPlatform.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}