﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace PARPlatform
{
    [DependsOn(typeof(PARPlatformXamarinSharedModule))]
    public class PARPlatformXamarinIosModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PARPlatformXamarinIosModule).GetAssembly());
        }
    }
}