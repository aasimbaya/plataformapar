﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace PARPlatform.Web.Authentication.JwtBearer
{
    public class AsyncJwtBearerOptions : JwtBearerOptions
    {
        public readonly List<IAsyncSecurityTokenValidator> AsyncSecurityTokenValidators;
        
        private readonly PARPlatformAsyncJwtSecurityTokenHandler _defaultAsyncHandler = new PARPlatformAsyncJwtSecurityTokenHandler();

        public AsyncJwtBearerOptions()
        {
            AsyncSecurityTokenValidators = new List<IAsyncSecurityTokenValidator>() {_defaultAsyncHandler};
        }
    }

}
