﻿namespace PARPlatform.Web.Authentication.JwtBearer
{
    public enum TokenType
    {
        AccessToken,
        RefreshToken
    }
}
