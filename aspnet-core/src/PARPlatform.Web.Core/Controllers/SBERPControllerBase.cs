using System;
using Abp.AspNetCore.Mvc.Controllers;
using Abp.Configuration.Startup;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace PARPlatform.Web.Controllers
{
    /// <summary>
    /// ERP Controller base
    /// </summary>
    [Route("/api/v1.0/services/[controller]/[action]")]
    public abstract class SBERPControllerBase : PARPlatformControllerBase
    {
       
    }
}