﻿using Microsoft.Extensions.DependencyInjection;
using PARPlatform.HealthChecks;

namespace PARPlatform.Web.HealthCheck
{
    public static class AbpZeroHealthCheck
    {
        public static IHealthChecksBuilder AddAbpZeroHealthCheck(this IServiceCollection services)
        {
            var builder = services.AddHealthChecks();
            builder.AddCheck<PARPlatformDbContextHealthCheck>("Database Connection");
            builder.AddCheck<PARPlatformDbContextUsersHealthCheck>("Database Connection with user check");
            builder.AddCheck<CacheHealthCheck>("Cache");

            // add your custom health checks here
            // builder.AddCheck<MyCustomHealthCheck>("my health check");

            return builder;
        }
    }
}
