﻿using System.Threading.Tasks;
using PARPlatform.Sessions.Dto;

namespace PARPlatform.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
