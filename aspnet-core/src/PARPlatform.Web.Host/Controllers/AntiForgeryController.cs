﻿using Microsoft.AspNetCore.Antiforgery;

namespace PARPlatform.Web.Controllers
{
    public class AntiForgeryController : PARPlatformControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
