﻿using Abp.Application.Services.Dto;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Modules.Communication.Mailing.Dto;
using PARPlatform.Modules.Communication.Mailing.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PARPlatform.Web.Controllers.Modules.Communication.Mailing
{
    /// <summary>
    /// Mailing controller
    /// </summary>
    //[AbpMvcAuthorize]
    public class MailingController : SBERPControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="mediator"></param>
        public MailingController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Get all pages destinary
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllDestinaryPagedQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<ListItemDestinaryDto>> GetAllDestinary([FromQuery] GetAllDestinaryPagedQuery query)
        {
            return await _mediator.Send(query);
        }
    }
}
