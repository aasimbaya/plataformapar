﻿using Abp.Application.Services.Dto;
using Abp.UI;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Modules.Configuration.Sector.Dtos;
using PARPlatform.Modules.Sectors.Commands;
using PARPlatform.Modules.Sectors.Queries;
using System.Linq;
using System.Threading.Tasks;

namespace PARPlatform.Web.Controllers.Modules.Configurations
{
    /// <summary>
    /// Sector controller
    /// </summary>
    public class SectorController : SBERPControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="mediator"></param>
        public SectorController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Get all pages from sectors
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllPagedSectorQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<UpsertSectorDto>> GetAllPaged([FromQuery] GetAllPagedSectorQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Create Sector
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="CreateSectorCommandHandler"/>
        /// <returns></returns>
        [HttpPost]
        public async Task<UpsertSectorDto> Create([FromBody] UpsertSectorDto input)
        {
            var sectores = await _mediator.Send(new GetAllSectorQuery());
            if(sectores.Any(p => p.Code.Equals(input.Ciiu)))
            {
                throw new UserFriendlyException(L("SectorCiiuAlreadyExist"));
            }

            var createSectorCommand = ObjectMapper.Map<CreateSectorCommand>(input);
            return await _mediator.Send(createSectorCommand);
        }

        /// <summary>
        /// Gets the sector given id
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetSectorByIdQueryHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public async Task<UpsertSectorDto> Get([FromRoute] long id)
        {
            return await _mediator.Send(new GetSectorByIdQuery(id));
        }

        /// <summary>
        /// Update sector
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="UpdateSectorCommandHandler"/>
        /// <returns></returns>    
        [Route("{id}")]
        [HttpPut]
        public async Task Update([FromRoute] long id, [FromBody] UpsertSectorDto input)
        {
            UpdateSectorCommand data = ObjectMapper.Map<UpdateSectorCommand>(input);
            data.Id = id;
            await _mediator.Send(data);
        }

        /// <summary>
        /// Delete sector
        /// </summary>
        /// <param name="id"></param>
        /// <see cref="DeleteSectorCommandHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpDelete]
        public async Task Delete([FromRoute] long id)

        {
            var input = new DeleteSectorCommand()
            {
                Id = id,
            };
            await _mediator.Send(input);
        }
    }
}

