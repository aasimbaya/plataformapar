﻿
using Abp.Application.Services.Dto;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Base;
using PARPlatform.Modules.Documents.Commands;
using PARPlatform.Modules.Documents.Dtos;
using PARPlatform.Modules.Documents.Queries;
using System;
using System.Threading.Tasks;

namespace PARPlatform.Web.Controllers.Modules.Documents
{
    /// <summary>
    /// Form controller
    /// </summary>
    //[AbpMvcAuthorize]
    public class DocumentsController : SBERPControllerBase
    {
        private readonly IAwsS3Services _awsS3Service;
        private readonly IMediator _mediator;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="mediator"></param>
        public DocumentsController(IMediator mediator, IAwsS3Services awsS3Services)
        {
            _awsS3Service = awsS3Services;
            _mediator = mediator;
        }

        /// <summary>
        /// Get all pages from forms
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllPagedDocumentsQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<DocumentDto>> GetAllPaged([FromQuery] GetAllPagedDocumentsQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Get Document by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public async Task<UpsertDocumentDto> GetDocumentById([FromRoute] long id)
        {
            return await _mediator.Send(new GetDocumentByIdQuery(id));
        }

        /// <summary>
        /// Delete document by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}")]
        [HttpDelete]
        public async Task<Unit> DeleteDocumentById([FromRoute] long id)
        {
            return await _mediator.Send(new DeleteDocumentCommand(id));

        }

        /// <summary>
        /// Command for document
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<bool> UploadDocumentToS3(S3DocumentDto input)
        {
            return await _awsS3Service.UploadFileAsync(input.File);
        }

        /// <summary>
        /// Command for document
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<UpsertDocumentDto> Create([FromBody] UpsertDocumentDto input)
        {
            var createDocument = ObjectMapper.Map<CreateDocumentCommand>(input);
            return await _mediator.Send(createDocument);
        }

        /// <summary>
        /// Command for update document
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [Route("{id}")]
        [HttpPut]
        public async Task<UpsertDocumentDto> Update([FromRoute] long id, [FromBody] UpsertDocumentDto input)
        {
            var updateDoc = ObjectMapper.Map<UpdateDocumentCommand>(input);
            updateDoc.Id = id;
            return await _mediator.Send(updateDoc);
        }


    }
}

