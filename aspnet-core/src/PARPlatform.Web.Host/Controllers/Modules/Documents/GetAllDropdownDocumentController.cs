﻿
using Abp.Application.Services.Dto;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Documents.Dtos;
using PARPlatform.Modules.Documents.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PARPlatform.Web.Controllers.Modules.Documents
{
    /// <summary>
    /// Form controller
    /// </summary>
    //[AbpMvcAuthorize]
    public class GetAllDropdownDocumentController : SBERPControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="mediator"></param>
        public GetAllDropdownDocumentController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Gets all document estructures
        /// </summary>
        /// <see cref="GetAllDocumentEstructure"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownDocumentEstructureDto>> GetAllDocumentEstructure()
        {
            return await _mediator.Send(new GetAllDocumentEstructureQuery());
        }

        /// <summary>
        /// Gets all document locations
        /// </summary>
        /// <see cref="GetAllDocumentLocation"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownDocumentLocationDto>> GetAllDocumentLocation()
        {
            return await _mediator.Send(new GetAllDocumentLocationQuery());
        }

        /// <summary>
        /// Gets all document languages
        /// </summary>
        /// <see cref="GetAllDocumentLanguage"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownDocumentLanguageDto>> GetAllDocumentLanguage()
        {
            return await _mediator.Send(new GetAllDocumentLanguageQuery());
        }

    }
}

