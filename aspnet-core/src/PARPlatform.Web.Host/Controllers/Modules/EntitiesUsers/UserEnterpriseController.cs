﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Queries;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Handlers;
using System.Threading.Tasks;
using PARPlatform.Modules.EntitiesUsers.Enterprise.Dtos;
using PARPlatform.Modules.EntitiesUsers.UserEnterprise.Commands;
using System.Collections.Generic;
using Abp.Authorization;
using PARPlatform.Authorization;

namespace PARPlatform.Web.Controllers.Modules.EntitiesUsers
{
    /// <summary>
    /// Enterprise controller
    /// </summary>
    [AbpMvcAuthorize]
    [AbpAuthorize(AppPermissions.Pages_Entities)]
    public class UserEnterpriseController : SBERPControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="mediator"></param>
        public UserEnterpriseController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Retrieve paged user enterprise
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllPagedUsersEnterpriseQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<EnterpriseListItemDto>> GetAllPaged([FromQuery] GetAllPagedUserEnterpriseQuery query)
        {
            return await _mediator.Send(query);
        }


        /// <summary>
        /// Retrieve list user enterprise
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllUsersEnterpriseQueryHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public async Task<List<EnterpriseListItemDto>> GetAll([FromRoute] long id)
        {

            return await _mediator.Send(new GetAllUserEnterpriseQuery(id));
        }

        /// <summary>
        /// Get user enterprise given id
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetUsersEnterpriseByIdQueryHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public async Task<UpsertUserEnterpriseWrapperDto> Get([FromRoute] long id)
        {
            return await _mediator.Send(new GetUserEnterpriseByIdQuery(id));
        }

        /// <summary>
        /// Get users by enterprise given id
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetUsersByEnterpriseIdQueryHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public async Task<List<UpsertUsersByEnterpriseDto>> GetUsersByEnterpriseId([FromRoute] long id)
        {
            return await _mediator.Send(new GetUsersByEnterpriseIdQuery(id));
        }

        /// <summary>
        /// Get users 
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetUsersQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<UpsertUsersByEnterpriseDto>> GetUsers([FromQuery] GetUsersFilteredInput query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Create user enterprise 
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="CreateUserEnterpriseCommandHandler"/>
        /// <returns></returns>    
        [HttpPost]
        public async Task<UpsertEnterpriseDto> Create([FromBody] UpsertUserEnterpriseWrapperDto input)
        {
            CreateUserEnterpriseCommand createUserAequalesCommand = ObjectMapper.Map<CreateUserEnterpriseCommand>(input);

            return await _mediator.Send(createUserAequalesCommand);
        }
        /// <summary>
        /// Update user enterprise 
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="UpdateUserEnterpriseCommandHandler"/>
        /// <returns></returns>    
        [Route("{id}")]
        [HttpPut]
        public async Task<UpsertEnterpriseDto> Update([FromRoute] long id, [FromBody] UpsertUserEnterpriseWrapperDto input)
        {
            UpdateUserEnterpriseCommand data = ObjectMapper.Map<UpdateUserEnterpriseCommand>(input);
            data.Id = id;
            return await _mediator.Send(data);
        }

        /// <summary>
        /// Delete user enterprise
        /// </summary>
        /// <param name="id"></param>
        /// <see cref="DeleteUserEnterpriseCommandHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpDelete]
        public async Task Delete([FromRoute] long id)

        {
            var input = new DeleteUserEnterpriseCommand()
            {
                Id = id,
            };
            await _mediator.Send(input);
        }
    }
}
