﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Dtos;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Handlers;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Queries;
using System.Threading.Tasks;
using PARPlatform.Modules.EntitiesUsers.UsersAequales.Commands;
using PARPlatform.Authorization.Users.Dto;
using System.Collections.Generic;

namespace PARPlatform.Web.Controllers.Modules.EntitiesUsers
{
    /// <summary>
    /// Users aequales controller
    /// </summary>
    [AbpMvcAuthorize]
    public class UsersAequalesController : SBERPControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="mediator"></param>
        public UsersAequalesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Retrieve paged users aequales
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllPagedUsersAequalesQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<UserAequalesListItemDto>> GetAllPaged([FromQuery] GetAllPagedUserAequalesQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Gets user aequales given id
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetUsersAequalesByIdQueryHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public async Task<UpsertUserAequalesDto> Get([FromRoute] long id)
        {
            return await _mediator.Send(new GetUserAequalesByIdQuery(id));
        }
        /// <summary>
        /// Gets all roles
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetRolesUsersAequalesByNameQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<UserRoleDto>> GetRoles([FromQuery] GetRolesUserAequalesByNameQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Create user aequales 
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="CreateUserAequalesCommandHandler"/>
        /// <returns></returns>    
        [HttpPost]
        public async Task<UpsertUserAequalesDto> Create([FromBody] UpsertUserAequalesDto input)
        {
            CreateUserAequalesCommand createUserAequalesCommand = ObjectMapper.Map<CreateUserAequalesCommand>(input);

            return await _mediator.Send(createUserAequalesCommand);
        }

        /// <summary>
        /// Update user aequales 
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="UpdateUserAequalesCommandHandler"/>
        /// <returns></returns>    
        [Route("{id}")]
        [HttpPut]
        public async Task<UpsertUserAequalesDto> Update([FromRoute] long id, [FromBody] UpsertUserAequalesDto input)
        {
            UpdateUserAequalesCommand data = ObjectMapper.Map<UpdateUserAequalesCommand>(input);
            data.Id = id;
            return await _mediator.Send(data);
        }
        /// <summary>
        /// Delete user aequales
        /// </summary>
        /// <param name="id"></param>
        /// <see cref="DeleteUserAequalesCommandHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpDelete]
        public async Task Delete([FromRoute] long id)

        {
            var input = new DeleteUserAequalesCommand()
            {
                Id = id,
            };
            await _mediator.Send(input);
        }
    }
}
