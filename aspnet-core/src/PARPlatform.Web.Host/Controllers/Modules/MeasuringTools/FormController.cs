﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Modules.MeasuringTools.Form.Commands;
using PARPlatform.Modules.MeasuringTools.Forms.Commands;
using PARPlatform.Modules.MeasuringTools.Forms.Dtos;
using PARPlatform.Modules.MeasuringTools.Forms.Queries;
using System.Threading.Tasks;

namespace PARPlatform.Web.Controllers.Modules.MeasuringTools
{
    /// <summary>
    /// Form controller
    /// </summary>
    [AbpMvcAuthorize]
    public class FormController : SBERPControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="mediator"></param>
        public FormController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Get all pages from forms
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllPagedFormsQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<FormDto>> GetAllPaged([FromQuery] GetAllPagedFormsQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Create Form
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="CreateFormCommandHandler"/>
        /// <returns></returns>
        [HttpPost]
        public async Task<UpsertFormDto> Create([FromBody] UpsertFormDto input)
        {
            var createFormCommand = ObjectMapper.Map<CreateFormCommand>(input);
            return await _mediator.Send(createFormCommand);
        }

        /// <summary>
        /// Update Form
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="UpdateFormCommandHandler"/>
        /// <returns></returns>
        [HttpPut]
        public async Task<UpsertFormDto> Update([FromBody] UpsertFormDto input)
        {
            var updateFormCommand = ObjectMapper.Map<UpdateFormCommand>(input);
            updateFormCommand.Id = input.Id;
            return await _mediator.Send(updateFormCommand);

        }

        /// <summary>
        /// Get form by Id
        /// </summary>
        /// <param name="id"></param>
        /// <see cref="GetFormByIdQueryHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public async Task<UpsertFormDto> GetFormById([FromRoute] long id)
        {
            return await _mediator.Send(new GetFormByIdQuery(id));
        }

        /// <summary>
        /// Delete form by Id
        /// </summary>
        /// <param name="id"></param>
        /// <see cref="DeleteFormCommandHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpDelete]
        public async Task<Unit> DeleteFormById([FromRoute] long id)
        {
            return await _mediator.Send(new DeleteFormCommand(id));

        }

        /// <summary>
        /// GetFormName ByName controller
        /// </summary>
        /// <param name="name"></param>
        /// <see cref="GetFormNameByNameQueryHandler"/>
        /// <returns></returns>
        [Route("{name}")]
        [HttpGet]
        public async Task<FormNameDto> GetFormNameByName([FromRoute] string name)
        {
            return await _mediator.Send(new GetFormNameByNameQuery(name));
        }

        /// <summary>
        /// Get all pages from forms
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllPagedAssociatedQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<ListItemAssociatedToFormDto>> GetAllPagedAssociated([FromQuery] GetAllPagedAssociatedQuery query)
        {
            return await _mediator.Send(query);
        }

        ///// <summary>
        ///// Get all pages category associated to forms
        ///// </summary>
        ///// <param name="query"></param>
        ///// <see cref="GetAllPagedAssociatedCategoryQueryHandler"/>
        ///// <returns></returns>
        //[HttpGet]
        //public async Task<PagedResultDto<ListItemAssociatedToFormDto>> GetAllPagedAssociatedCategory([FromQuery] GetAllPagedAssociatedQuery query)
        //{
        //    return await _mediator.Send(query);
        //}

    }
}

