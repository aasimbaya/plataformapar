﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Queries;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Handlers;
using System.Threading.Tasks;
using PARPlatform.Modules.MeasuringTools.QuestionsCategories.Commands;
using System.Collections.Generic;

namespace PARPlatform.Web.Controllers.Modules.MeasuringTools
{
    /// <summary>
    /// Question category controller
    /// </summary>
    //[AbpMvcAuthorize]
    public class QuestionCategoryController : SBERPControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="mediator"></param>
        public QuestionCategoryController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Retrieve paged question category
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllPagedQuestionsCategoriesQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<QuestionCategoryListItemDto>> GetAllPaged([FromQuery] GetAllPagedQuestionCategoryQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Retrieve paged questions by category
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllPagedQuestionsByCategoryIdQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<QuestionsByCategoryListItemDto>> GetAllQuestionsByCategoryIdPaged([FromQuery] GetAllPagedQuestionsByCategoryIdQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Gets all the question category given id
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetQuestionsCategoriesByIdQueryHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public async Task<UpsertQuestionCategoryDto> Get([FromRoute] long id)
        {
            return await _mediator.Send(new GetQuestionCategoryByIdQuery(id));
        }

        /// <summary>
        /// Gets all type question category given name
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetTypeCategoriesQueryHandler"/>
        /// <returns></returns>
        [Route("{name?}")]
        [HttpGet]
        public async Task<List<UpsertQuestionCategoryTypeDto>> GetTypeCategories([FromRoute] string name)
        {
            return await _mediator.Send(new GetTypeCategoriesByNameQuery(name));
        }

        /// <summary>
        /// Create question category
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="CreateQuestionCategoryCommandHandler"/>
        /// <returns></returns>    
        [HttpPost]
        public async Task Create([FromBody] UpsertQuestionCategoryDto input)
        {
            CreateQuestionCategoryCommand data = ObjectMapper.Map<CreateQuestionCategoryCommand>(input);
            await _mediator.Send(data);
        }
        /// <summary>
        /// Update question category
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="UpdateQuestionCategoryCommandHandler"/>
        /// <returns></returns>    
        [Route("{id}")]
        [HttpPut]
        public async Task Update([FromRoute] long id, [FromBody] UpsertQuestionCategoryDto input)
        {
            UpdateQuestionCategoryCommand data = ObjectMapper.Map<UpdateQuestionCategoryCommand>(input);
            data.Id = id;
            await _mediator.Send(data);
        }
        /// <summary>
        /// Delete question category
        /// </summary>
        /// <param name="id"></param>
        /// <see cref="DeleteQuestionCategoryCommandHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpDelete]
        public async Task Delete([FromRoute] long id)

        {
            var input = new DeleteQuestionCategoryCommand()
            {
                Id = id,
            };
            await _mediator.Send(input);
        }
    }
}
