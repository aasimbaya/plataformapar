﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Commands;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries;
using PARPlatform.Modules.Shared.Catalogue.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PARPlatform.Web.Controllers.Modules.MeasuringTools
{
    /// <summary>
    /// Questions Bank controller
    /// </summary>
    [AbpMvcAuthorize]
    public class QuestionsBankController : SBERPControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="mediator"></param>
        public QuestionsBankController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Retrieve paged questions bank
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllPagedQuestionsBankQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<QuestionsBankListItemDto>> GetAllPagedQuestionsBank([FromQuery] GetAllPagedQuestionsBankQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Get Question details from Questions Bank by id.
        /// </summary>
        /// <param name="id"></param>
        /// <see cref="GetQuestionsBankByIdQueryHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public async Task<UpsertQuestionsBankDto> Get([FromRoute] long id)
        {
            var query = new GetQuestionsBankByIdQuery(id);

            return await _mediator.Send(query);
        }

        /// <summary>
        /// Create Question of Questions Bank
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="CreateQuestionsBankCommandHandler"/>
        /// <returns></returns>
        [HttpPost]
        public async Task<UpsertQuestionsBankDto> Create([FromBody] UpsertQuestionsBankDto input)
        {
            var createQuestion = ObjectMapper.Map<CreateQuestionsBankCommand>(input);

            return await _mediator.Send(createQuestion);
        }

        /// <summary>
        /// Update Question from Questions Bank
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <see cref="UpdateQuestionsBankCommandHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpPut]
        public async Task<UpsertQuestionsBankDto> Update([FromRoute] long id, [FromBody] UpsertQuestionsBankDto input)
        {
            var updateQuestion = ObjectMapper.Map<UpdateQuestionsBankCommand>(input);
            updateQuestion.Id = id;

            return await _mediator.Send(updateQuestion);
        }

        /// <summary>
        /// Delete Question from Questions Bank
        /// </summary>
        /// <param name="id"></param>
        /// <see cref="DeleteQuestionsBankCommandHandler"/>
        /// <returns></returns>
        [Route("{id}")]
        [HttpDelete]
        public async Task Delete([FromRoute] long id)
        {
            var input = new DeleteQuestionsBankCommand()
            {
                Id = id,
            };
            await _mediator.Send(input);
        }

        /// <summary>
        /// Retrieve paged questions associated to a special distintion
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllPagedQuestionsSpecialDistintionQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<QuestionsSpecialDistintionListItemDto>> GetAllPagedQuestionsSpecialDistition([FromQuery] GetAllPagedQuestionsSpecialDistintionQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Get all questions related with questionnaire identificator
        /// </summary>
        /// <param name="id">Questionnaire identificator</param>
        /// <returns></returns>
        [Route("{id}")]
        [HttpGet]
        public async Task<List<QuestionnaireQuestionDetailDto>> GetAllQuestionsByQuestionnaireId([FromRoute] long id)
        {
            var query = new GetAllQuestionsByQuestionnaireIdQuery(id);

            return await _mediator.Send(query);
        }
    }
}
