﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Modules.Tenants.Dtos;
using PARPlatform.Modules.Tenants.Queries;
using System.Threading.Tasks;

namespace PARPlatform.Web.Controllers.Modules.Tenants
{
    /// <summary>
    /// Tenant controller
    /// </summary>
    public class TenantController : SBERPControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="mediator"></param>
        public TenantController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Get tenant given name
        /// </summary>
        /// <param name="name"></param>
        /// <see cref="GetTenantByNameQueryHandler"/>
        /// <returns></returns>
        [Route("{name?}")]
        [HttpGet]
        public async Task<UpsertTenantDto> GetTenantByName([FromRoute] string name)
        {
            return await _mediator.Send(new GetTenantByNameQuery(name));
        }
    }
}
