﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authorization;
using PARPlatform.Authorization.Users.Profile;
using PARPlatform.Storage;

namespace PARPlatform.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ProfileController : ProfileControllerBase
    {
        public ProfileController(
            ITempFileCacheManager tempFileCacheManager,
            IProfileAppService profileAppService) :
            base(tempFileCacheManager, profileAppService)
        {
        }
    }
}