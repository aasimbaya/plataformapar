﻿using Abp.AspNetCore.Mvc.Authorization;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Modules.Cities.Queries;
using PARPlatform.Modules.Cities.Handlers;
using PARPlatform.Modules.Common.Dto;
using PARPlatform.Modules.Countries.Queries;
using PARPlatform.Modules.Countries.Handlers;
using PARPlatform.Modules.LineBusiness.Handlers;
using PARPlatform.Modules.LineBusiness.Queries;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Dtos;
using PARPlatform.Modules.MeasuringTools.QuestionsBank.Queries;
using PARPlatform.Modules.PersonalTypes.Queries;
using PARPlatform.Modules.Positions.Handlers;
using PARPlatform.Modules.Positions.Queries;
using PARPlatform.Modules.Regions.Queries;
using PARPlatform.Modules.Regions.Handlers;
using PARPlatform.Modules.Sectors.Handlers;
using PARPlatform.Modules.Sectors.Queries;
using PARPlatform.Modules.Shared.Catalogue.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;
using PARPlatform.Modules.TradeAssociations.Queries;
using PARPlatform.Modules.Tenants.Queries;
using PARPlatform.Modules.Tenants.Handlers;
using PARPlatform.Modules.Countries.Dtos;

namespace PARPlatform.Web.Controllers.Shared.Catalogue
{
    /// <summary>
    /// GetAll for dropdown lists controller
    /// </summary>
    public class GetAllDropdownController : SBERPControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="mediator"></param>
        public GetAllDropdownController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Gets all questions use
        /// </summary>
        /// <see cref="GetAllQuestionUseQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllQuestionUse()
        {
            return await _mediator.Send(new GetAllQuestionUseQuery());
        }

        /// <summary>
        /// Gets all question categories or special distinction based on question use
        /// </summary>
        /// <param name="query"></param>
        /// <see cref="GetAllCategoriesQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllCategories([FromQuery] long use)
        {
            return await _mediator.Send(new GetAllCategoriesQuery(use));
        }

        /// <summary>
        /// Gets all questionnaire types
        /// </summary>
        /// <see cref="GetAllQuestionnaireTypeQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllQuestionnaireType()
        {
            return await _mediator.Send(new GetAllQuestionnaireTypeQuery());
        }

        /// <summary>
        /// Gets all question types
        /// </summary>
        /// <see cref="GetAllQuestionTypeQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllQuestionType()
        {
            return await _mediator.Send(new GetAllQuestionTypeQuery());
        }

        /// <summary>
        /// Gets all score types
        /// </summary>
        /// <see cref="GetAllScoreTypeQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllScoreType()
        {
            return await _mediator.Send(new GetAllScoreTypeQuery());
        }

        /// <summary>
        /// Gets all thematics
        /// </summary>
        /// <see cref="GetAllThematicsQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllThematics()
        {
            return await _mediator.Send(new GetAllThematicsQuery());
        }

        /// <summary>
        /// Gets all validation types
        /// </summary>
        /// <see cref="GetAllValidationTypesQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllValidationType()
        {
            return await _mediator.Send(new GetAllValidationTypesQuery());
        }

        /// <summary>
        /// Gets all countries
        /// </summary>
        /// <see cref="GetAllCountryQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<UpsertCountryDto>> GetAllCountries([FromQuery] GetAllCountryQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Gets all tenants
        /// </summary>
        /// <see cref="GetAllTenantQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllTenants()
        {
            return await _mediator.Send(new GetAllTenantQuery());
        }

        /// <summary>
        /// Gets all sectors
        /// </summary>
        /// <see cref="GetAllSectorsQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllSectors()
        {
            return await _mediator.Send(new GetAllSectorQuery());
        }

        /// <summary>
        /// Gets all line business
        /// </summary>
        /// <see cref="GetAllLineBusinessQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllLineBusiness()
        {
            return await _mediator.Send(new GetAllLineBusinessQuery());
        }
        /// <summary>
        /// Gets all line positions
        /// </summary>
        /// <see cref="GetAllPositionsQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllPositions()
        {
            return await _mediator.Send(new GetAllPositionQuery());
        }

        /// <summary>
        /// Gets all regions
        /// </summary>
        /// <see cref="GetAllRegionQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllRegions(GetAllRegionQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Gets all regions by Country Id
        /// </summary>
        /// <param name="CountryId"></param>
        /// <see cref="GetAllRegionByCountryIdQueryHandler"/>
        /// <returns></returns>
        [Route("{CountryId}")]
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllRegionsByCountryId([FromRoute] long CountryId)
        {
            return await _mediator.Send(new GetAllRegionByCountryIdQuery(CountryId));
        }

        /// <summary>
        /// Gets all cities
        /// </summary>
        /// <see cref="GetAllCityQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllCities(GetAllCityQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Gets all cities by Region Id
        /// </summary>
        /// <param name="RegionId"></param>
        /// <see cref="GetAllCityByRegionIdQueryHandler"/>
        /// <returns></returns>
        [Route("{RegionId}")]
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllCitiesByRegionId([FromRoute] long RegionId)
        {
            return await _mediator.Send(new GetAllCityByRegionIdQuery(RegionId));
        }

        /// <summary>
        /// Gets all personal types
        /// </summary>
        /// <see cref="GetAllPersonalTypeQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllPersonalTypes()
        {
            return await _mediator.Send(new GetAllPersonalTypeQuery());
        }

        /// <summary>
        /// Gets all trade associations
        /// </summary>
        /// <see cref="GetAllTradeAssociationQueryHandler"/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllTradeAssociations(GetAllTradeAssociationQuery query)
        {
            return await _mediator.Send(query);
        }

        /// <summary>
        /// Gets all trade associations by Country Id
        /// </summary>
        /// <param name="CountryId"></param>
        /// <see cref="GetAllTradeAssociationByCountryIdQueryHandler"/>
        /// <returns></returns>
        [Route("{CountryId}")]
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllTradeAssociationsByCountryId([FromRoute] long CountryId)
        {
            return await _mediator.Send(new GetAllTradeAssociationByCountryIdQuery(CountryId));
        }

        /// <summary>
        /// Gets all Collaborators (TO-DO: link to a dictionary table)
        /// </summary>
        /// <see cref=""/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetAllCollaborators()
        {
            List<DropdownItemDto> collaborators = new List<DropdownItemDto>()
            {
                new DropdownItemDto{ Id = 1, Name = "De 1 a 200 colaboradores/as" },
                new DropdownItemDto{ Id = 2, Name = "De 201 a 1000 colaboradores/as" },
                new DropdownItemDto{ Id = 3, Name = "De 1001 a 5000 colaboradores/as" },
                new DropdownItemDto{ Id = 4, Name = "Más de 5000 colaboradores/as" }
            };

            return collaborators;
        }

        /// <summary>
        /// Gets all email domains black list (TO-DO: link to a dictionary table)
        /// </summary>
        /// <see cref=""/>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DropdownItemDto>> GetEmailDomainsBlackList()
        {
            List<DropdownItemDto> collaborators = new List<DropdownItemDto>()
            {
                new DropdownItemDto{ Id = 1, Name = "gmail.com" },
                new DropdownItemDto{ Id = 2, Name = "hotmail.com" },
                new DropdownItemDto{ Id = 3, Name = "facebook.com" },
                new DropdownItemDto{ Id = 4, Name = "yahoo.com" },
                new DropdownItemDto{ Id = 5, Name = "msm.com" },
                new DropdownItemDto{ Id = 6, Name = "yahoo.es" }
            };

            return collaborators;
        }

        /// <summary>
        /// Get all records that belongs to the specific subtype
        /// </summary>
        /// <param name="subtype">Subtype of enumerable dictionary</param>
        /// <see cref="GetEnumerablesBySubtypeQueryHandler"/>
        /// <returns></returns>
        [Route("{subtype}")]
        [HttpGet]
        public async Task<List<EnumDictionaryDataDto>> GetEnumerablesBySubtype([FromRoute] string subtype)
        {
            return await _mediator.Send(new GetEnumerablesBySubtypeQuery(subtype));
        }

    }
}
