﻿using Abp.AspNetCore.Mvc.Authorization;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PARPlatform.Modules.Shared.Validators.Queries;
using System.Threading.Tasks;

namespace PARPlatform.Web.Controllers.Shared.Validator
{
    /// <summary>
    /// Validators controller
    /// </summary>
    public class ValidatorController : SBERPControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="mediator"></param>
        public ValidatorController(IMediator mediator)
        {
           _mediator = mediator;
        }

        /// <summary>
        /// Validate if email address exist for any user
        /// </summary>
        /// <param name="email">Email address for validate</param>
        /// <see cref="ValidateUserEmailExistsQueryHandler"/>
        /// <returns></returns>
        [Route("{emailaddress}")]
        [HttpGet]
        public async Task<bool> ValidateUserEmailExists([FromRoute] string emailaddress)
        {
            return await _mediator.Send(new ValidateUserEmailExistsQuery(emailaddress));
        }

        /// <summary>
        /// Validate if Enterprise id number exists
        /// </summary>
        /// <param name="idnumber">Enterprise id number for validate</param>
        /// <see cref="ValidateEnterpriseIdNumberExistsQueryHandler"/>
        /// <returns></returns>
        [Route("{idnumber}")]
        [HttpGet]
        public async Task<bool> ValidateEnterpriseIdNumberExists([FromRoute] string idnumber)
        {
            return await _mediator.Send(new ValidateEnterpriseIdNumberExistsQuery(idnumber));
        }
    }
}
