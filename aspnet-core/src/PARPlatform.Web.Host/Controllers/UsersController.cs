﻿using Abp.AspNetCore.Mvc.Authorization;
using PARPlatform.Authorization;
using PARPlatform.Storage;
using Abp.BackgroundJobs;

namespace PARPlatform.Web.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Users)]
    public class UsersController : UsersControllerBase
    {
        public UsersController(IBinaryObjectManager binaryObjectManager, IBackgroundJobManager backgroundJobManager)
            : base(binaryObjectManager, backgroundJobManager)
        {
        }
    }
}