#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["src/PARPlatform.Web.Host/PARPlatform.Web.Host.csproj", "src/PARPlatform.Web.Host/"]
COPY ["src/PARPlatform.Web.Core/PARPlatform.Web.Core.csproj", "src/PARPlatform.Web.Core/"]
COPY ["src/PARPlatform.Application/PARPlatform.Application.csproj", "src/PARPlatform.Application/"]
COPY ["src/PARPlatform.Application.Shared/PARPlatform.Application.Shared.csproj", "src/PARPlatform.Application.Shared/"]
COPY ["src/PARPlatform.Core.Shared/PARPlatform.Core.Shared.csproj", "src/PARPlatform.Core.Shared/"]
COPY ["src/PARPlatform.Core/PARPlatform.Core.csproj", "src/PARPlatform.Core/"]
COPY ["src/PARPlatform.EntityFrameworkCore/PARPlatform.EntityFrameworkCore.csproj", "src/PARPlatform.EntityFrameworkCore/"]
COPY ["src/PARPlatform.GraphQL/PARPlatform.GraphQL.csproj", "src/PARPlatform.GraphQL/"]
RUN dotnet restore "src/PARPlatform.Web.Host/PARPlatform.Web.Host.csproj"
COPY . .
WORKDIR "/src/src/PARPlatform.Web.Host"
RUN dotnet build "PARPlatform.Web.Host.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "PARPlatform.Web.Host.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "PARPlatform.Web.Host.dll"]
