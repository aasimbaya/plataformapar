﻿using Abp.AspNetCore.Mvc.Views;

namespace PARPlatform.Web.Views
{
    public abstract class PARPlatformRazorPage<TModel> : AbpRazorPage<TModel>
    {
        protected PARPlatformRazorPage()
        {
            LocalizationSourceName = PARPlatformConsts.LocalizationSourceName;
        }
    }
}
