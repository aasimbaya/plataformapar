﻿using Microsoft.AspNetCore.Mvc;
using PARPlatform.Web.Controllers;

namespace PARPlatform.Web.Public.Controllers
{
    public class AboutController : PARPlatformControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}