using Microsoft.AspNetCore.Mvc;
using PARPlatform.Web.Controllers;

namespace PARPlatform.Web.Public.Controllers
{
    public class HomeController : PARPlatformControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}