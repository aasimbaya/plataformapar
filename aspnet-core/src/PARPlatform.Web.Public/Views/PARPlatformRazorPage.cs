﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace PARPlatform.Web.Public.Views
{
    public abstract class PARPlatformRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected PARPlatformRazorPage()
        {
            LocalizationSourceName = PARPlatformConsts.LocalizationSourceName;
        }
    }
}
