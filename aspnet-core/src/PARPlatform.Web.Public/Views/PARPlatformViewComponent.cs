﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace PARPlatform.Web.Public.Views
{
    public abstract class PARPlatformViewComponent : AbpViewComponent
    {
        protected PARPlatformViewComponent()
        {
            LocalizationSourceName = PARPlatformConsts.LocalizationSourceName;
        }
    }
}