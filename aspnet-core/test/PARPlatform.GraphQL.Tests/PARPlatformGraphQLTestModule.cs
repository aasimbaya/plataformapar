﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using PARPlatform.Configure;
using PARPlatform.Startup;
using PARPlatform.Test.Base;

namespace PARPlatform.GraphQL.Tests
{
    [DependsOn(
        typeof(PARPlatformGraphQLModule),
        typeof(PARPlatformTestBaseModule))]
    public class PARPlatformGraphQLTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            IServiceCollection services = new ServiceCollection();
            
            services.AddAndConfigureGraphQL();

            WindsorRegistrationHelper.CreateServiceProvider(IocManager.IocContainer, services);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PARPlatformGraphQLTestModule).GetAssembly());
        }
    }
}