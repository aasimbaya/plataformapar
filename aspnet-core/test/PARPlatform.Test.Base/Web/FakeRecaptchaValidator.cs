﻿using System.Threading.Tasks;
using PARPlatform.Security.Recaptcha;

namespace PARPlatform.Test.Base.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}
