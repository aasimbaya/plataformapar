﻿using Abp.Modules;
using PARPlatform.Test.Base;

namespace PARPlatform.Tests
{
    [DependsOn(typeof(PARPlatformTestBaseModule))]
    public class PARPlatformTestModule : AbpModule
    {
       
    }
}
